CREATE  TABLE IF NOT EXISTS `naha_regi`.`rel_product_to_product` (
  `products_id` INT(10) UNSIGNED NOT NULL COMMENT 'リレーション元' ,
  `rel_product_id` INT(10) UNSIGNED NOT NULL COMMENT '関連商品ID' ,
  PRIMARY KEY (`products_id`, `rel_product_id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
