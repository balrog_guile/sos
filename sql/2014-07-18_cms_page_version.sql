CREATE  TABLE IF NOT EXISTS `naha_regi`.`cms_page_version` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `page_id` INT(10) UNSIGNED NOT NULL COMMENT 'ページID' ,
  `version` INT(10) UNSIGNED NOT NULL COMMENT 'バージョン番号' ,
  `version_status` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT 'バージョンステータス\n0:公開\n1:非公開' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
