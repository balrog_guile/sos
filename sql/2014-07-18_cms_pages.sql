CREATE  TABLE IF NOT EXISTS `naha_regi`.`cms_pages` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `page_root` VARCHAR(255) NOT NULL COMMENT 'ページルート（実質的カテゴリ）' ,
  `page_path` VARCHAR(255) NOT NULL COMMENT 'ページパス' ,
  `subject` TEXT NULL DEFAULT NULL COMMENT 'タイトル' ,
  `create_date` DATETIME NOT NULL COMMENT '作成日' ,
  `update_date` DATETIME NOT NULL ,
  `display_date` DATETIME NOT NULL COMMENT '表示作成日' ,
  `open_date` DATETIME NULL DEFAULT NULL COMMENT '公開日' ,
  `close_date` DATETIME NULL DEFAULT NULL COMMENT '公開終了日' ,
  `page_status` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '公開ステータス\n-2:完全消去\n-1:ごみばこ\n0:下書き\n1:公開' ,
  PRIMARY KEY (`id`) ,
  INDEX `index_page_path` (`page_path` ASC) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
