CREATE  TABLE IF NOT EXISTS `chatan`.`payment_method` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(512) NOT NULL COMMENT '支払い方法名' ,
  `create_date` DATETIME NOT NULL COMMENT '作成日' ,
  `update_date` DATETIME NOT NULL COMMENT '更新日' ,
  `rank` INT(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '並び順' ,
  `usage` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '利用中か' ,
  `delete_flag` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '削除フラグ' ,
  `shipment_type` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '配送料金体型\n1 一律\n2 都道府県別' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci



ALTER TABLE `chatan`.`payment_method` ADD COLUMN `method` VARCHAR(512) NOT NULL COMMENT '計算実行メソッド'  AFTER `shipment_type` , ADD COLUMN `json_data` LONGTEXT NOT NULL COMMENT 'JSONデータ'  AFTER `method` 
