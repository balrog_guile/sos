
CREATE  TABLE IF NOT EXISTS `chatan`.`delivery_method` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `name` VARCHAR(512) NOT NULL COMMENT '配送方法名' ,
  `create_date` DATETIME NOT NULL COMMENT '作成日' ,
  `update_date` DATETIME NOT NULL COMMENT '更新日' ,
  `rank` INT(10) UNSIGNED NOT NULL DEFAULT 1 COMMENT '並び順' ,
  `usage` TINYINT(3) UNSIGNED NOT NULL DEFAULT 1 COMMENT '利用中か' ,
  `delete_flag` TINYINT(3) UNSIGNED NOT NULL DEFAULT 0 COMMENT '削除フラグ' ,
  `method` TINYINT(4) NOT NULL COMMENT '計算実行メソッド' ,
  `json_data` LONGTEXT NOT NULL COMMENT 'JSONデータ' ,
  `limit_theme` LONGTEXT NOT NULL COMMENT '配送方法限定' ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
