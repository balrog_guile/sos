CREATE  TABLE IF NOT EXISTS `sos`.`shop_contents` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT ,
  `shop_id` INT(10) UNSIGNED NOT NULL COMMENT 'ショップID' ,
  `subject` VARCHAR(512) NOT NULL COMMENT 'タイトル' ,
  `body` LONGTEXT NOT NULL COMMENT '本文' ,
  `create_date` DATETIME NOT NULL COMMENT '作成日' ,
  `update_date` DATETIME NOT NULL COMMENT '更新日' ,
  `deleted_flag` TINYINT(4) NOT NULL DEFAULT 0 COMMENT '削除フラグ' ,
  `deleted_date` DATETIME NULL DEFAULT NULL ,
  PRIMARY KEY (`id`) )
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8
COLLATE = utf8_general_ci
