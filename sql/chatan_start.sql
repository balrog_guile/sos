-- phpMyAdmin SQL Dump
-- version 4.4.9
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: 2015 年 7 月 06 日 16:10
-- サーバのバージョン： 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `naha_regi`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `categories`
--

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '親カテゴリID',
  `categori_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort_key` varchar(225) DEFAULT NULL,
  `parent_sort_key` varchar(255) DEFAULT NULL,
  `main_image` varchar(255) DEFAULT NULL COMMENT 'カテゴリメインイメージ',
  `thumb_image` varchar(255) DEFAULT NULL COMMENT 'サムネイル',
  `description` longtext,
  `link_url` varchar(30) DEFAULT NULL COMMENT 'パーマリンク用ID',
  `page_desc` varchar(512) DEFAULT NULL COMMENT 'ページ概要',
  `page_key` varchar(256) DEFAULT NULL COMMENT 'ページキーワード',
  `item_page_disp` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品ページ内で基本カテゴリとして認識'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='カテゴリ';

--
-- テーブルのデータのダンプ `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `categori_name`, `create_date`, `update_date`, `rank`, `depth`, `sort_key`, `parent_sort_key`, `main_image`, `thumb_image`, `description`, `link_url`, `page_desc`, `page_key`, `item_page_disp`) VALUES
(1, 0, 'テスト', '2015-07-06 16:02:58', '2015-07-06 16:02:58', 0, 0, '00000', NULL, '', '', '', 'test', '', '', 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(10) unsigned NOT NULL,
  `page_root` varchar(255) NOT NULL COMMENT 'ページルート（実質的カテゴリ）',
  `page_path` varchar(255) NOT NULL COMMENT 'ページパス',
  `subject` text COMMENT 'タイトル',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL,
  `display_date` datetime NOT NULL COMMENT '表示作成日',
  `open_date` datetime DEFAULT NULL COMMENT '公開日',
  `close_date` datetime DEFAULT NULL COMMENT '公開終了日',
  `page_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開ステータス\n-2:完全消去\n-1:ごみばこ\n0:下書き\n1:公開'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `cms_page_block_html`
--

CREATE TABLE `cms_page_block_html` (
  `id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL COMMENT 'ページID',
  `version_id` int(10) unsigned NOT NULL COMMENT 'バージョンID',
  `content` longtext COMMENT 'コンテンツ',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '変更日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `cms_page_version`
--

CREATE TABLE `cms_page_version` (
  `id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL COMMENT 'ページID',
  `version` int(10) unsigned NOT NULL COMMENT 'バージョン番号',
  `version_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'バージョンステータス\n0:公開\n1:非公開'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `comming`
--

CREATE TABLE `comming` (
  `id` int(10) unsigned NOT NULL,
  `cusomer_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '件名',
  `comming_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '来店日時',
  `memo1` longtext COMMENT 'メモ1',
  `memo2` longtext COMMENT 'メモ2',
  `memo3` longtext COMMENT 'メモ3',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '対応スタッフid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='来店管理';

-- --------------------------------------------------------

--
-- テーブルの構造 `comming_assets`
--

CREATE TABLE `comming_assets` (
  `id` int(10) unsigned NOT NULL,
  `comming_id` int(10) unsigned NOT NULL COMMENT '来店ID',
  `hashcode` varchar(255) DEFAULT NULL COMMENT 'パスハッシュ',
  `data_type` tinyint(3) unsigned NOT NULL COMMENT 'データのタイプ\n0:画像\n1:動画\n2:その他',
  `path` varchar(255) NOT NULL COMMENT '保存パス',
  `memo1` longtext COMMENT 'メモ',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='来店履歴用画像/動画管理';

-- --------------------------------------------------------

--
-- テーブルの構造 `coupon`
--

CREATE TABLE `coupon` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL COMMENT 'クーポンコード',
  `password` varchar(255) DEFAULT NULL COMMENT 'パスフレーズ（必要時）',
  `amount` int(10) unsigned NOT NULL COMMENT '額面',
  `usage_count` int(11) NOT NULL DEFAULT '-1' COMMENT '有効利用回数',
  `expiration` date NOT NULL COMMENT '有効期限（無期限は遠い未来を設定）',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '発行者ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='クーポン管理';

-- --------------------------------------------------------

--
-- テーブルの構造 `coupon_usage`
--

CREATE TABLE `coupon_usage` (
  `id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'クーポンID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'お客様ID',
  `comming_id` int(10) unsigned DEFAULT NULL COMMENT '来店ID',
  `usage_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '利用日時',
  `return_date` datetime DEFAULT NULL COMMENT '返品返金日時',
  `use_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '利用ステータス\n1:利用済\n2:返品返金'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='クーポン利用履歴';

-- --------------------------------------------------------

--
-- テーブルの構造 `customers`
--

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL,
  `name1` varchar(125) NOT NULL COMMENT '姓',
  `name2` varchar(125) NOT NULL COMMENT '名',
  `kana1` varchar(125) NOT NULL COMMENT '姓（かな）',
  `kana2` varchar(125) NOT NULL COMMENT '名（かな）',
  `mailaddress` varchar(255) NOT NULL COMMENT 'メールアドレス1',
  `customer_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様ステータス\n-1:退会\n0:非アクティブ\n1:アクティブ',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '入会日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '変更日',
  `password` varchar(255) DEFAULT NULL COMMENT 'パスワード',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'パスワードハッシュ',
  `time_limit` datetime DEFAULT NULL COMMENT '認証コード期限',
  `authentication_code` varchar(255) DEFAULT NULL COMMENT '認証コード',
  `zip` varchar(8) DEFAULT NULL COMMENT '郵便番号',
  `pref` varchar(4) DEFAULT NULL COMMENT '都道府県',
  `address_1` text COMMENT '住所１',
  `address_2` text COMMENT '住所2\n',
  `address_3` text COMMENT '住所3',
  `tell` varchar(45) DEFAULT NULL COMMENT '電話番号',
  `fax` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='お客様情報';

-- --------------------------------------------------------

--
-- テーブルの構造 `customer_profile_field`
--

CREATE TABLE `customer_profile_field` (
  `id` int(10) unsigned NOT NULL,
  `field_name` varchar(45) NOT NULL,
  `field_label` varchar(255) NOT NULL,
  `field_type` varchar(125) NOT NULL DEFAULT 'INT' COMMENT 'データ型\nINT\nFLOAT\nTEXT\nLONGTEXT\nDATE\nTIME\nDATETIME\n7:FILE(未実装)\n',
  `input_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '入力欄タイプ\n0:テキスト\n1:テキストエリア\n2:チェックボックス\n3:ラジオボタン\n4:セレクトボックス',
  `selecte_column` text COMMENT '入力項目',
  `html_class` varchar(125) DEFAULT NULL COMMENT '出力時振るHTMLCLASS',
  `html_id` varchar(125) DEFAULT NULL COMMENT '出力時振るHTMLID',
  `search_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '検索対象にするか？',
  `view_display` tinyint(4) NOT NULL DEFAULT '0' COMMENT '一覧表示の対象？\n0:表示しない\n1:表示する',
  `summary_display` tinyint(4) NOT NULL DEFAULT '0' COMMENT '概要に表示',
  `customer_profile_fieldcol` varchar(45) DEFAULT NULL,
  `required` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '必須条項か',
  `min_length` int(10) unsigned DEFAULT NULL COMMENT '最短文字数',
  `max_length` int(10) unsigned DEFAULT NULL COMMENT '最大長',
  `regrex` varchar(125) DEFAULT NULL COMMENT '正規表現データ',
  `default_value` text COMMENT '初期値',
  `other_validation` text COMMENT 'そのほかのバリーションを実行するか？(yiiでの指定の必要性）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `faq`
--

CREATE TABLE `faq` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `question` longtext NOT NULL COMMENT '質問',
  `answer` longtext NOT NULL COMMENT '回答',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `information`
--

CREATE TABLE `information` (
  `id` int(10) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL COMMENT 'タイトル',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日',
  `display_date` datetime NOT NULL COMMENT '表示する日付',
  `begin_date` datetime DEFAULT NULL COMMENT '表示開始日',
  `end_date` datetime DEFAULT NULL COMMENT '表示終了日',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開ステータス\n-1:ゴミ箱\n0:非公開\n1:公開',
  `description` varchar(255) DEFAULT NULL COMMENT '概要',
  `contents` longtext COMMENT '本文'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `metainfo`
--

CREATE TABLE `metainfo` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品に対するメタ情報の場合商品のID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'カテゴリに対するメタであればここに買えゴリID',
  `meta_title` varchar(255) NOT NULL COMMENT 'メタタイトル',
  `meta_value` text COMMENT 'メタ情報',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL COMMENT 'オーダーID',
  `sku_id` int(10) unsigned DEFAULT NULL COMMENT '商品DBにデーたがある場合はSKUのID',
  `item_id` varchar(255) NOT NULL COMMENT '品番',
  `name` varchar(255) NOT NULL COMMENT '製品名',
  `price` int(11) NOT NULL COMMENT '単価',
  `qty` int(11) NOT NULL COMMENT '数量',
  `sub_total` int(11) NOT NULL COMMENT '合計',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `parente_detail_id` int(10) unsigned DEFAULT NULL COMMENT '親IDを持っている場合'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `order_history`
--

CREATE TABLE `order_history` (
  `id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT '顧客ID',
  `order_date` datetime NOT NULL COMMENT 'オーダー日',
  `update_date` datetime NOT NULL COMMENT 'アップデート日',
  `order_status` tinyint(4) NOT NULL COMMENT 'オーダステータス -2:問題あり顧客 -1:キャンセル 1:仮受注 2:受注 3:確認済み 4:在庫割り当て 5:発送完了',
  `payment_status` tinyint(4) NOT NULL COMMENT '支払ステータス 0:未支払い 1:支払い',
  `item_total` int(11) NOT NULL DEFAULT '0' COMMENT '商品代金合計',
  `payment_method` varchar(125) NOT NULL DEFAULT '0',
  `payment_fee` int(11) NOT NULL DEFAULT '0' COMMENT '支払い手数料',
  `delivery_fee` int(11) NOT NULL DEFAULT '0' COMMENT '送料',
  `grand_total` int(11) NOT NULL DEFAULT '0' COMMENT '支払い総合計',
  `payment_date` datetime DEFAULT NULL COMMENT '支払い実行日',
  `canceld_date` datetime DEFAULT NULL COMMENT 'キャンセル実行日',
  `name1` varchar(125) DEFAULT NULL COMMENT '姓',
  `name2` varchar(125) DEFAULT NULL COMMENT '名',
  `kana1` varchar(125) DEFAULT NULL COMMENT 'かな（姓）',
  `kana2` varchar(125) DEFAULT NULL COMMENT 'かな（名）',
  `zip` varchar(45) DEFAULT NULL COMMENT '郵便番号',
  `pref` varchar(125) DEFAULT NULL COMMENT '都道府県',
  `address1` varchar(125) DEFAULT NULL COMMENT '住所1',
  `address2` varchar(125) DEFAULT NULL COMMENT '住所2',
  `address3` varchar(125) DEFAULT NULL COMMENT '住所3',
  `mailaddress` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `tel` varchar(45) DEFAULT NULL COMMENT '電話',
  `fax` varchar(45) DEFAULT NULL COMMENT 'fax',
  `d_name1` varchar(125) DEFAULT NULL COMMENT '姓',
  `d_name2` varchar(125) DEFAULT NULL COMMENT '名',
  `d_kana1` varchar(125) DEFAULT NULL COMMENT 'かな（姓）',
  `d_kana2` varchar(125) DEFAULT NULL COMMENT 'かな（名）',
  `d_zip` varchar(45) DEFAULT NULL COMMENT '郵便番号',
  `d_pref` varchar(125) DEFAULT NULL COMMENT '都道府県',
  `d_address1` varchar(125) DEFAULT NULL COMMENT '住所1',
  `d_address2` varchar(125) DEFAULT NULL COMMENT '住所2',
  `d_address3` varchar(125) DEFAULT NULL COMMENT '住所3',
  `d_tel` varchar(45) DEFAULT NULL COMMENT '電話',
  `order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '販売場所 0:店舗 1:通販'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `page_blocks`
--

CREATE TABLE `page_blocks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'ページブロックの名前（日本語OK!）',
  `type` varchar(255) NOT NULL,
  `page_path` varchar(255) DEFAULT NULL COMMENT 'ページパス（グローバルならなし）',
  `global` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '全ページ共通なら1',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順',
  `open_date` datetime DEFAULT NULL COMMENT '表示開始日時',
  `close_date` datetime DEFAULT NULL COMMENT '表示終了日',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '編集日時',
  `image_path` varchar(255) DEFAULT NULL COMMENT '画像パス',
  `content` longtext,
  `alt` text COMMENT 'alt属性',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'リンク（youtube)',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開ステータス'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `partner`
--

CREATE TABLE `partner` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '取引先名',
  `code` varchar(255) NOT NULL COMMENT '取引先コード',
  `created_date` datetime DEFAULT NULL COMMENT '作成日',
  `partner_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '取引ステータス\n1:取引中\n0:商談中\n-1:取引停止'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `products`
--

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL,
  `item_id` varchar(125) NOT NULL COMMENT '品番',
  `item_name` varchar(225) NOT NULL COMMENT '品名',
  `price` int(10) unsigned DEFAULT NULL COMMENT '定価',
  `sale_price` int(10) unsigned DEFAULT NULL COMMENT '売価',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '公開ステータス\n1:公開\n2:非公開',
  `item_status` text COMMENT '商品ステータスフラグ（新商品など）',
  `postage` int(10) unsigned DEFAULT NULL COMMENT '商品別送料',
  `point_rate` int(10) unsigned DEFAULT '5' COMMENT 'ポイント付与率',
  `shipment` varchar(125) DEFAULT NULL COMMENT '発送日目安',
  `qty_limit` int(10) unsigned DEFAULT NULL COMMENT '販売数制限',
  `serch_word` text COMMENT '検索キーワード（EC-CUBE)用',
  `remark` text COMMENT '備考',
  `main_view_comment` longtext COMMENT 'メインコメント(一覧用)',
  `main_detail_comment` longtext NOT NULL COMMENT 'メインコメント(詳細ページ)',
  `thumbnail` text COMMENT 'サムネイル',
  `main_image` text COMMENT 'メイン画像',
  `main_big_image` text COMMENT 'メインの拡大画像',
  `sub1_title` text COMMENT '拡張エリア1 タイトル',
  `sub1_text` longtext COMMENT '拡張エリア1 コメント',
  `sub1_img` text COMMENT '拡張エリア1 画像',
  `sub2_title` text COMMENT '拡張エリア2 タイトル',
  `sub2_text` longtext COMMENT '拡張エリア2 コメント',
  `sub2_img` text COMMENT '拡張エリア2 画像',
  `sub3_title` text COMMENT '拡張エリア3 タイトル',
  `sub3_img` text COMMENT '拡張エリア3 画像',
  `sub3_text` longtext COMMENT '拡張エリア3 コメント',
  `sub4_title` text COMMENT '拡張エリア4 タイトル',
  `sub4_text` longtext COMMENT '拡張エリア4 コメント',
  `sub4_img` text COMMENT '拡張エリア4 画像',
  `sub5_title` text COMMENT '拡張エリア5 タイトル',
  `sub5_text` longtext COMMENT '拡張エリア5 コメント',
  `sub5_img` text COMMENT '拡張エリア5 画像',
  `sub6_title` text COMMENT '拡張エリア6タイトル',
  `sub6_text` longtext COMMENT '拡張エリア6 コメント',
  `sub6_img` text COMMENT '拡張エリア6 画像',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  `link_url` varchar(30) DEFAULT NULL COMMENT 'パーマリンク用ID',
  `page_key` varchar(256) DEFAULT NULL COMMENT 'ページキーワード',
  `page_desc` varchar(512) DEFAULT NULL COMMENT 'ページ概要',
  `sale_status` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '販売ステータス\n1:通常販売\n2:問い合わせのみ\n3:商品紹介のみ',
  `youtube` varchar(255) NOT NULL COMMENT 'youtubeURL',
  `template_file` varchar(255) NOT NULL DEFAULT '' COMMENT '利用テンプレートファイル',
  `option_template_file` varchar(255) NOT NULL DEFAULT '' COMMENT 'オプション用テンプレートファイル'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品リスト';

-- --------------------------------------------------------

--
-- テーブルの構造 `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `image` text COMMENT '保存場所',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longtext,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `product_options`
--

CREATE TABLE `product_options` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned DEFAULT NULL COMMENT '商品とひもづける場合',
  `sku_id` int(10) unsigned DEFAULT NULL COMMENT 'SKUとひもづける場合',
  `name` varchar(125) DEFAULT NULL COMMENT '選択肢名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '選択肢のタイプ\n1:セレクトボックス\n2:ラジオボタン\n3:チェックボックス\n4:自由入力（ライン）\n5:自由入力（ボックス）',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順',
  `init` text COMMENT 'テキスト入力欄の場合の初期値',
  `validate` longtext COMMENT 'バリデート条件のJSONを記録',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `product_options_values`
--

CREATE TABLE `product_options_values` (
  `id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL COMMENT 'オプションのID',
  `label` text COMMENT '表示ラベル',
  `value` text COMMENT '値',
  `rank` int(10) unsigned DEFAULT '1' COMMENT '表示順',
  `specific_price` int(11) DEFAULT NULL COMMENT '金額特定をする場合の金額',
  `add_price` int(11) NOT NULL DEFAULT '0' COMMENT '金額修正がある場合',
  `init` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '初期値の場合1',
  `validation` longtext,
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `product_sku`
--

CREATE TABLE `product_sku` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL COMMENT '商品テーブルのID',
  `brunch_item_id` varchar(125) NOT NULL COMMENT '枝番',
  `brunch_item_name` varchar(225) NOT NULL COMMENT '枝番品名',
  `price` int(10) unsigned DEFAULT '0' COMMENT '売価',
  `sale_price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '売価',
  `stock` int(10) unsigned DEFAULT NULL COMMENT '在庫数',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `code_str` varchar(255) DEFAULT NULL COMMENT 'バーコード用文字列',
  `content` longtext COMMENT '説明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品SKU';

-- --------------------------------------------------------

--
-- テーブルの構造 `product_sku_images`
--

CREATE TABLE `product_sku_images` (
  `id` int(10) unsigned NOT NULL,
  `sku_id` int(10) unsigned NOT NULL,
  `image` text COMMENT '保存場所',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longtext,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `profiles`
--

CREATE TABLE `profiles` (
  `user_id` int(11) unsigned NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `profiles`
--

INSERT INTO `profiles` (`user_id`, `first_name`, `last_name`) VALUES
(1, 'Administrator', 'Admin');

-- --------------------------------------------------------

--
-- テーブルの構造 `profiles_fields`
--

CREATE TABLE `profiles_fields` (
  `id` int(11) unsigned NOT NULL,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'first_name', 'First Name', 'VARCHAR', 255, 3, 2, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'last_name', 'Last Name', 'VARCHAR', 255, 3, 2, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 2, 3);

-- --------------------------------------------------------

--
-- テーブルの構造 `receipt`
--

CREATE TABLE `receipt` (
  `id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'お客様ID',
  `comming_id` int(10) unsigned DEFAULT NULL COMMENT '来店ID',
  `id_hash` varchar(125) DEFAULT NULL COMMENT 'コードIDのハッシュ',
  `receipt_name` varchar(256) DEFAULT NULL COMMENT '案件名',
  `item_total` int(11) NOT NULL DEFAULT '0' COMMENT '商品合計',
  `item_total_tax` int(11) NOT NULL DEFAULT '0' COMMENT '商品総合計税込',
  `tax` int(11) NOT NULL DEFAULT '0' COMMENT '税額',
  `grand_total` int(11) NOT NULL DEFAULT '0' COMMENT 'お支払合計',
  `grand_total_tax` int(11) NOT NULL DEFAULT '0' COMMENT 'お支払い総合計税込み',
  `payment` int(11) DEFAULT NULL COMMENT 'お支払い金額',
  `change_val` int(11) DEFAULT NULL COMMENT '釣り銭',
  `order_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注文ステータス\n0:入力中\n1:オーダー確定\n2:お支払い済み\n3:掛売\n4:キャンセル\n5:返品',
  `reserve_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '予約オーダーステータス\n0:予約ではない\n1:予約',
  `deliv_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '発送ステータス\n0:発送不要\n1:発送準備\n2:発送完了\n3:受取確認済',
  `received` tinyint(3) unsigned DEFAULT NULL COMMENT '受取フラグ\n0:未確認\n1:受取確認',
  `deliv_code` varchar(225) DEFAULT NULL COMMENT '配送伝票コード',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '変更日',
  `payment_date` datetime DEFAULT NULL,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '担当スタッフID',
  `coupon_id` int(10) unsigned DEFAULT NULL COMMENT 'クーポン利用時IDを格納',
  `coupon_amount` int(10) unsigned DEFAULT NULL COMMENT 'クーポン利用時クーポン金額',
  `order_type` int(10) unsigned DEFAULT '0' COMMENT 'オーダータイプ\n0:POS\n1:通販（オンボード通販システム）',
  `order_history_id` int(10) unsigned DEFAULT NULL COMMENT '通販オーダーヒストリーの詳細ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='レシート';

-- --------------------------------------------------------

--
-- テーブルの構造 `receipt_detail`
--

CREATE TABLE `receipt_detail` (
  `id` int(10) unsigned NOT NULL,
  `receipt_id` int(10) unsigned NOT NULL COMMENT 'レシートID',
  `produt_id` int(10) unsigned DEFAULT NULL COMMENT '商品テーブルのID',
  `item_id` varchar(125) DEFAULT NULL COMMENT '商品番号',
  `item_name` varchar(225) DEFAULT NULL COMMENT '商品名',
  `remark` varchar(225) DEFAULT NULL COMMENT '備考',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '単価',
  `price_tax` int(11) NOT NULL DEFAULT '0' COMMENT '税込単価',
  `qty` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  `sub_total` int(11) NOT NULL DEFAULT '0' COMMENT '小計',
  `sub_total_tax` int(11) NOT NULL DEFAULT '0' COMMENT '小計税込',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '変更日',
  `sku_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='レシート明細';

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_option_sku`
--

CREATE TABLE `rel_option_sku` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL COMMENT '商品のID',
  `sku_id` int(11) NOT NULL COMMENT 'SKUのID',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='他の商品のSKUをオプションとして指定できる機能';

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_product_categories`
--

CREATE TABLE `rel_product_categories` (
  `categories_id` int(10) unsigned NOT NULL COMMENT 'カテゴリID',
  `product_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品カテゴリヒモ付';

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_product_tags`
--

CREATE TABLE `rel_product_tags` (
  `product_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_product_to_product`
--

CREATE TABLE `rel_product_to_product` (
  `products_id` int(10) unsigned NOT NULL COMMENT 'リレーション元',
  `rel_product_id` int(10) unsigned NOT NULL COMMENT '関連商品ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `sessions`
--

CREATE TABLE `sessions` (
  `id` char(32) CHARACTER SET latin1 NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `sessions`
--

INSERT INTO `sessions` (`id`, `expire`, `data`) VALUES
('59198c5db4851b8f15e3ba62714d34a2', 1436345788, ''),
('1440019a4374d8d9b824d096b7793aae', 1436598144, 0x36323933303235633463363432616265336565303535303861336530383161305f5f72657475726e55726c7c733a32363a222f696e6465782e7068702f70726f647563742f70726f64756374223b36323933303235633463363432616265336565303535303861336530383161305f5f69647c733a313a2231223b36323933303235633463363432616265336565303535303861336530383161305f5f6e616d657c733a353a2261646d696e223b36323933303235633463363432616265336565303535303861336530383161305f5f7374617465737c613a303a7b7d),
('2a837a44bf8c135f0088b9a53e17e956', 1436598179, 0x36323933303235633463363432616265336565303535303861336530383161305f5f72657475726e55726c7c733a33323a222f696e6465782e7068702f70726f647563742f70726f647563742f61646d696e223b36323933303235633463363432616265336565303535303861336530383161305f5f69647c733a313a2231223b36323933303235633463363432616265336565303535303861336530383161305f5f6e616d657c733a353a2261646d696e223b36323933303235633463363432616265336565303535303861336530383161305f5f7374617465737c613a303a7b7d3632393330323563346336343261626533656530353530386133653038316130656d61696c7c733a32313a227765626d6173746572406578616d706c652e636f6d223b3632393330323563346336343261626533656530353530386133653038316130757365726e616d657c733a353a2261646d696e223b36323933303235633463363432616265336565303535303861336530383161306372656174655f61747c733a31393a22323031352d30312d30372031303a34303a3137223b36323933303235633463363432616265336565303535303861336530383161306c61737476697369745f61747c733a31393a22323031352d30372d30362030363a33373a3534223b3632393330323563346336343261626533656530353530386133653038316130757365725f69647c733a313a2231223b363239333032356334633634326162653365653035353038613365303831613066697273745f6e616d657c733a31333a2241646d696e6973747261746f72223b36323933303235633463363432616265336565303535303861336530383161306c6173745f6e616d657c733a353a2241646d696e223b),
('798e23c55b77c306fe269d694c8c4074', 1436598498, 0x36323933303235633463363432616265336565303535303861336530383161305f5f72657475726e55726c7c733a32363a222f696e6465782e7068702f70726f647563742f70726f64756374223b36323933303235633463363432616265336565303535303861336530383161305f5f69647c733a313a2231223b36323933303235633463363432616265336565303535303861336530383161305f5f6e616d657c733a353a2261646d696e223b36323933303235633463363432616265336565303535303861336530383161305f5f7374617465737c613a303a7b7d3632393330323563346336343261626533656530353530386133653038316130656d61696c7c733a32313a227765626d6173746572406578616d706c652e636f6d223b3632393330323563346336343261626533656530353530386133653038316130757365726e616d657c733a353a2261646d696e223b36323933303235633463363432616265336565303535303861336530383161306372656174655f61747c733a31393a22323031352d30312d30372031303a34303a3137223b36323933303235633463363432616265336565303535303861336530383161306c61737476697369745f61747c733a31393a22323031352d30372d30362030363a33373a3534223b3632393330323563346336343261626533656530353530386133653038316130757365725f69647c733a313a2231223b363239333032356334633634326162653365653035353038613365303831613066697273745f6e616d657c733a31333a2241646d696e6973747261746f72223b36323933303235633463363432616265336565303535303861336530383161306c6173745f6e616d657c733a353a2241646d696e223b);

-- --------------------------------------------------------

--
-- テーブルの構造 `shop`
--

CREATE TABLE `shop` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(125) NOT NULL COMMENT 'ショップ名',
  `zip` varchar(8) DEFAULT NULL,
  `pref` varchar(45) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL COMMENT '住所1',
  `address2` varchar(255) DEFAULT NULL COMMENT '住所2',
  `address3` varchar(45) DEFAULT NULL COMMENT '住所3',
  `tel` varchar(45) DEFAULT NULL COMMENT '電話',
  `fax` varchar(45) DEFAULT NULL COMMENT 'fax',
  `mailaddress` varchar(255) DEFAULT NULL COMMENT '代表メールアドレス',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `shop`
--

INSERT INTO `shop` (`id`, `name`, `zip`, `pref`, `address1`, `address2`, `address3`, `tel`, `fax`, `mailaddress`, `rank`, `create_date`) VALUES
(1, 'name', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `shop_info`
--

CREATE TABLE `shop_info` (
  `id` int(10) unsigned NOT NULL,
  `shop_name` varchar(255) NOT NULL COMMENT 'ショップ名',
  `zip` varchar(45) NOT NULL COMMENT '郵便番号',
  `pref` varchar(45) NOT NULL COMMENT '都道府県',
  `address1` varchar(255) NOT NULL COMMENT '住所1',
  `address2` varchar(255) NOT NULL COMMENT '住所2',
  `address3` varchar(255) NOT NULL COMMENT '住所3',
  `tel` varchar(255) DEFAULT NULL COMMENT '電話番号',
  `fax` varchar(255) DEFAULT NULL COMMENT 'FAX',
  `email` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `url` varchar(255) DEFAULT NULL COMMENT 'ショップURL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `stocking`
--

CREATE TABLE `stocking` (
  `id` bigint(19) unsigned NOT NULL,
  `sku_id` int(10) unsigned NOT NULL COMMENT 'D',
  `insert_date` datetime NOT NULL COMMENT '仕入れ日',
  `purchase_price` int(10) unsigned NOT NULL COMMENT '仕入れ単価',
  `purchase_qty` int(10) unsigned DEFAULT NULL COMMENT '仕入れ個数',
  `sub_total` int(10) unsigned DEFAULT NULL COMMENT '仕入れ総額',
  `leaving_date` datetime DEFAULT NULL COMMENT '出庫日',
  `leaving_qty` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出庫個数',
  `memo` text,
  `partner_id` int(10) unsigned DEFAULT NULL COMMENT '仕入先ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `stocking_leave`
--

CREATE TABLE `stocking_leave` (
  `id` int(10) unsigned NOT NULL,
  `stocking_id` int(10) unsigned NOT NULL COMMENT '入庫ID',
  `leaving_date` datetime NOT NULL COMMENT '出庫日',
  `leaving_count` int(10) unsigned NOT NULL COMMENT '出庫個数',
  `leaving_status` tinyint(4) DEFAULT '1' COMMENT '出庫ステータス\n1:出庫\n-1:キャンセル',
  `canseled_date` datetime DEFAULT NULL COMMENT '出庫キャンセル日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `tags`
--

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'タグ名',
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `tbl_migration`
--

CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shop_id` int(10) unsigned DEFAULT '0' COMMENT 'ショップID'
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`, `shop_id`) VALUES
(1, 'admin', 'ebf4ffd4358379b57524deaecf713547', 'webmaster@example.com', 'd816b7f83e35affebdb0c8d78a6a51b8', 1, 1, '2015-01-07 01:40:17', '2015-07-05 21:37:54', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_parent_id` (`parent_id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_page_path` (`page_path`);

--
-- Indexes for table `cms_page_block_html`
--
ALTER TABLE `cms_page_block_html`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_page_version`
--
ALTER TABLE `cms_page_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comming`
--
ALTER TABLE `comming`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comming_assets`
--
ALTER TABLE `comming_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_usage`
--
ALTER TABLE `coupon_usage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_profile_field`
--
ALTER TABLE `customer_profile_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metainfo`
--
ALTER TABLE `metainfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_history`
--
ALTER TABLE `order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_blocks`
--
ALTER TABLE `page_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_item_id` (`item_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options_values`
--
ALTER TABLE `product_options_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_product_id` (`product_id`);

--
-- Indexes for table `product_sku_images`
--
ALTER TABLE `product_sku_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_hash` (`id_hash`);

--
-- Indexes for table `receipt_detail`
--
ALTER TABLE `receipt_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rel_option_sku`
--
ALTER TABLE `rel_option_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rel_product_categories`
--
ALTER TABLE `rel_product_categories`
  ADD PRIMARY KEY (`categories_id`,`product_id`);

--
-- Indexes for table `rel_product_tags`
--
ALTER TABLE `rel_product_tags`
  ADD PRIMARY KEY (`product_id`,`tag_id`);

--
-- Indexes for table `rel_product_to_product`
--
ALTER TABLE `rel_product_to_product`
  ADD PRIMARY KEY (`products_id`,`rel_product_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_info`
--
ALTER TABLE `shop_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocking`
--
ALTER TABLE `stocking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocking_leave`
--
ALTER TABLE `stocking_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_migration`
--
ALTER TABLE `tbl_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username` (`username`),
  ADD UNIQUE KEY `user_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_page_block_html`
--
ALTER TABLE `cms_page_block_html`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_page_version`
--
ALTER TABLE `cms_page_version`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comming`
--
ALTER TABLE `comming`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comming_assets`
--
ALTER TABLE `comming_assets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon_usage`
--
ALTER TABLE `coupon_usage`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customer_profile_field`
--
ALTER TABLE `customer_profile_field`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `metainfo`
--
ALTER TABLE `metainfo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_history`
--
ALTER TABLE `order_history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `page_blocks`
--
ALTER TABLE `page_blocks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_options`
--
ALTER TABLE `product_options`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_options_values`
--
ALTER TABLE `product_options_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_sku_images`
--
ALTER TABLE `product_sku_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receipt_detail`
--
ALTER TABLE `receipt_detail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rel_option_sku`
--
ALTER TABLE `rel_option_sku`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_info`
--
ALTER TABLE `shop_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stocking`
--
ALTER TABLE `stocking`
  MODIFY `id` bigint(19) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stocking_leave`
--
ALTER TABLE `stocking_leave`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
