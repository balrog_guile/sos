-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: 2016 年 6 月 24 日 18:08
-- サーバのバージョン： 5.5.42
-- PHP Version: 5.6.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `chatan`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `block_table_html`
--

CREATE TABLE `block_table_html` (
  `id` int(11) NOT NULL,
  `blocks_id` int(11) NOT NULL,
  `blocks_version_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL,
  `html` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `categories`
--

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '親カテゴリID',
  `categori_name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `depth` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `sort_key` varchar(225) DEFAULT NULL,
  `parent_sort_key` varchar(255) DEFAULT NULL,
  `main_image` varchar(255) DEFAULT NULL COMMENT 'カテゴリメインイメージ',
  `thumb_image` varchar(255) DEFAULT NULL COMMENT 'サムネイル',
  `description` longtext,
  `link_url` varchar(30) DEFAULT NULL COMMENT 'パーマリンク用ID',
  `page_desc` varchar(512) DEFAULT NULL COMMENT 'ページ概要',
  `page_key` varchar(256) DEFAULT NULL COMMENT 'ページキーワード',
  `item_page_disp` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '商品ページ内で基本カテゴリとして認識'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='カテゴリ';

--
-- テーブルのデータのダンプ `categories`
--

INSERT INTO `categories` (`id`, `parent_id`, `categori_name`, `create_date`, `update_date`, `rank`, `depth`, `sort_key`, `parent_sort_key`, `main_image`, `thumb_image`, `description`, `link_url`, `page_desc`, `page_key`, `item_page_disp`) VALUES
(1, 0, 'テスト', '2015-07-06 16:02:58', '2015-07-06 16:02:58', 0, 0, '00000', NULL, '', '', '', 'test', '', '', 0),
(2, 1, 'category2', '2015-12-05 14:59:17', '2015-12-05 14:59:17', 0, 1, '00000-00000', NULL, '', '', '', 'category2', '', '', 0),
(3, 0, 'もういっこ', '2015-12-22 10:53:40', '2015-12-22 10:53:40', 1, 0, '00001', NULL, '', '', '', 'categoy2', '', '', 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `cms2_blocks`
--

CREATE TABLE `cms2_blocks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'ブロック名',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `delete_date` datetime DEFAULT NULL COMMENT '削除日',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `cms2_blocks`
--

INSERT INTO `cms2_blocks` (`id`, `name`, `create_date`, `update_date`, `delete_flag`, `delete_date`, `rank`) VALUES
(1, 'ページブロック', '2016-03-12 19:01:42', '2016-03-12 19:01:42', 0, NULL, 1),
(2, 'ページブロック2', '2016-03-12 19:01:53', '2016-03-12 19:01:53', 0, NULL, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `cms2_blocks_admin`
--

CREATE TABLE `cms2_blocks_admin` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT 'ブロック名',
  `desc` varchar(512) DEFAULT NULL COMMENT '詳細',
  `path` varchar(255) DEFAULT NULL,
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `delete_date` datetime DEFAULT NULL COMMENT '削除実行日時'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `cms2_blocks_admin`
--

INSERT INTO `cms2_blocks_admin` (`id`, `name`, `desc`, `path`, `create_date`, `update_date`, `delete_flag`, `delete_date`) VALUES
(1, 'HTMLブロック', 'HTMLブロック', 'blockPluginHtmlController', '2016-03-16 20:27:02', '2016-03-16 20:27:02', 0, NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `cms2_blocks_version`
--

CREATE TABLE `cms2_blocks_version` (
  `id` int(10) unsigned NOT NULL,
  `cms_blocks_id` int(10) unsigned NOT NULL,
  `version_name` varchar(255) NOT NULL COMMENT 'バージョン名',
  `version_num` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'バージョン番号',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開状態フラグ',
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `cms2_blocks_version`
--

INSERT INTO `cms2_blocks_version` (`id`, `cms_blocks_id`, `version_name`, `version_num`, `open_status`, `create_date`, `update_date`) VALUES
(1, 1, '初稿', 1, 1, '2016-03-17 13:43:33', '2016-03-17 13:43:33');

-- --------------------------------------------------------

--
-- テーブルの構造 `cms_pages`
--

CREATE TABLE `cms_pages` (
  `id` int(10) unsigned NOT NULL,
  `page_root` varchar(255) NOT NULL COMMENT 'ページルート（実質的カテゴリ）',
  `page_path` varchar(255) NOT NULL COMMENT 'ページパス',
  `subject` text COMMENT 'タイトル',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL,
  `display_date` datetime NOT NULL COMMENT '表示作成日',
  `open_date` datetime DEFAULT NULL COMMENT '公開日',
  `close_date` datetime DEFAULT NULL COMMENT '公開終了日',
  `page_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開ステータス\n-2:完全消去\n-1:ごみばこ\n0:下書き\n1:公開'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `cms_page_block_html`
--

CREATE TABLE `cms_page_block_html` (
  `id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL COMMENT 'ページID',
  `version_id` int(10) unsigned NOT NULL COMMENT 'バージョンID',
  `content` longtext COMMENT 'コンテンツ',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '変更日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `cms_page_version`
--

CREATE TABLE `cms_page_version` (
  `id` int(10) unsigned NOT NULL,
  `page_id` int(10) unsigned NOT NULL COMMENT 'ページID',
  `version` int(10) unsigned NOT NULL COMMENT 'バージョン番号',
  `version_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT 'バージョンステータス\n0:公開\n1:非公開'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `comming`
--

CREATE TABLE `comming` (
  `id` int(10) unsigned NOT NULL,
  `cusomer_id` int(10) unsigned DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL COMMENT '件名',
  `comming_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '来店日時',
  `memo1` longtext COMMENT 'メモ1',
  `memo2` longtext COMMENT 'メモ2',
  `memo3` longtext COMMENT 'メモ3',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '対応スタッフid'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='来店管理';

-- --------------------------------------------------------

--
-- テーブルの構造 `comming_assets`
--

CREATE TABLE `comming_assets` (
  `id` int(10) unsigned NOT NULL,
  `comming_id` int(10) unsigned NOT NULL COMMENT '来店ID',
  `hashcode` varchar(255) DEFAULT NULL COMMENT 'パスハッシュ',
  `data_type` tinyint(3) unsigned NOT NULL COMMENT 'データのタイプ\n0:画像\n1:動画\n2:その他',
  `path` varchar(255) NOT NULL COMMENT '保存パス',
  `memo1` longtext COMMENT 'メモ',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='来店履歴用画像/動画管理';

-- --------------------------------------------------------

--
-- テーブルの構造 `coupon`
--

CREATE TABLE `coupon` (
  `id` int(10) unsigned NOT NULL,
  `code` varchar(255) NOT NULL COMMENT 'クーポンコード',
  `password` varchar(255) DEFAULT NULL COMMENT 'パスフレーズ（必要時）',
  `amount` int(10) unsigned NOT NULL COMMENT '額面',
  `usage_count` int(11) NOT NULL DEFAULT '-1' COMMENT '有効利用回数',
  `expiration` date NOT NULL COMMENT '有効期限（無期限は遠い未来を設定）',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '発行者ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='クーポン管理';

-- --------------------------------------------------------

--
-- テーブルの構造 `coupon_usage`
--

CREATE TABLE `coupon_usage` (
  `id` int(10) unsigned NOT NULL,
  `coupon_id` int(10) unsigned NOT NULL COMMENT 'クーポンID',
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'お客様ID',
  `comming_id` int(10) unsigned DEFAULT NULL COMMENT '来店ID',
  `usage_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '利用日時',
  `return_date` datetime DEFAULT NULL COMMENT '返品返金日時',
  `use_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '利用ステータス\n1:利用済\n2:返品返金'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='クーポン利用履歴';

-- --------------------------------------------------------

--
-- テーブルの構造 `customers`
--

CREATE TABLE `customers` (
  `id` int(10) unsigned NOT NULL,
  `name1` varchar(125) NOT NULL COMMENT '姓',
  `name2` varchar(125) NOT NULL COMMENT '名',
  `kana1` varchar(125) NOT NULL COMMENT '姓（かな）',
  `kana2` varchar(125) NOT NULL COMMENT '名（かな）',
  `company_name` varchar(255) DEFAULT NULL COMMENT '会社名・屋号',
  `mailaddress` varchar(255) NOT NULL COMMENT 'メールアドレス1',
  `customer_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT 'お客様ステータス\n-1:退会\n0:非アクティブ\n1:アクティブ',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '入会日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '変更日',
  `password` varchar(255) DEFAULT NULL COMMENT 'パスワード',
  `password_hash` varchar(255) DEFAULT NULL COMMENT 'パスワードハッシュ',
  `time_limit` datetime DEFAULT NULL COMMENT '認証コード期限',
  `authentication_code` varchar(255) DEFAULT NULL COMMENT '認証コード',
  `zip` varchar(8) DEFAULT NULL COMMENT '郵便番号',
  `pref` varchar(4) DEFAULT NULL COMMENT '都道府県',
  `address_1` varchar(512) DEFAULT NULL COMMENT '市区町村',
  `address_2` varchar(512) DEFAULT NULL COMMENT '町域番地',
  `address_3` varchar(512) DEFAULT NULL COMMENT 'マンションなど',
  `tel` varchar(45) DEFAULT NULL COMMENT '電話番号',
  `tel2` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='お客様情報';

--
-- テーブルのデータのダンプ `customers`
--

INSERT INTO `customers` (`id`, `name1`, `name2`, `kana1`, `kana2`, `company_name`, `mailaddress`, `customer_status`, `create_date`, `update_date`, `password`, `password_hash`, `time_limit`, `authentication_code`, `zip`, `pref`, `address_1`, `address_2`, `address_3`, `tel`, `tel2`, `fax`) VALUES
(1, '伊藤', '清徳', 'いとう', 'きよのり', '会社名', 'kiyotchi46@gmail.com', 1, '2015-11-08 14:20:44', '2015-11-08 14:27:44', 'b5f3c4fe334105f9100782a7d6d3569503531d70', 'bbS1u3N3eV8LjXRp', '2015-11-09 14:20:44', 'f16f52b9b4f004b9b80f7443b5c75e2d1c3ca524', '462-0801', '愛知県', 'asfa', 'asdfsafdas', '', '09087333607', '09087333607', '');

-- --------------------------------------------------------

--
-- テーブルの構造 `customer_profile_field`
--

CREATE TABLE `customer_profile_field` (
  `id` int(10) unsigned NOT NULL,
  `field_name` varchar(45) NOT NULL,
  `field_label` varchar(255) NOT NULL,
  `field_type` varchar(125) NOT NULL DEFAULT 'INT' COMMENT 'データ型\nINT\nFLOAT\nTEXT\nLONGTEXT\nDATE\nTIME\nDATETIME\n7:FILE(未実装)\n',
  `input_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '入力欄タイプ\n0:テキスト\n1:テキストエリア\n2:チェックボックス\n3:ラジオボタン\n4:セレクトボックス',
  `selecte_column` text COMMENT '入力項目',
  `html_class` varchar(125) DEFAULT NULL COMMENT '出力時振るHTMLCLASS',
  `html_id` varchar(125) DEFAULT NULL COMMENT '出力時振るHTMLID',
  `search_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '検索対象にするか？',
  `view_display` tinyint(4) NOT NULL DEFAULT '0' COMMENT '一覧表示の対象？\n0:表示しない\n1:表示する',
  `summary_display` tinyint(4) NOT NULL DEFAULT '0' COMMENT '概要に表示',
  `customer_profile_fieldcol` varchar(45) DEFAULT NULL,
  `required` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '必須条項か',
  `min_length` int(10) unsigned DEFAULT NULL COMMENT '最短文字数',
  `max_length` int(10) unsigned DEFAULT NULL COMMENT '最大長',
  `regrex` varchar(125) DEFAULT NULL COMMENT '正規表現データ',
  `default_value` text COMMENT '初期値',
  `other_validation` text COMMENT 'そのほかのバリーションを実行するか？(yiiでの指定の必要性）'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `delivery_method`
--

CREATE TABLE `delivery_method` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(512) NOT NULL COMMENT '配送方法名',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '並び順',
  `usage` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '利用中か',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `method` varchar(512) NOT NULL COMMENT '計算実行メソッド',
  `json_data` longtext NOT NULL COMMENT 'JSONデータ',
  `limit_theme` longtext NOT NULL COMMENT '配送方法限定',
  `delivery_time` longtext NOT NULL COMMENT '配送時間設定',
  `specific_interval` int(11) NOT NULL DEFAULT '4'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `delivery_method`
--

INSERT INTO `delivery_method` (`id`, `name`, `create_date`, `update_date`, `rank`, `usage`, `delete_flag`, `method`, `json_data`, `limit_theme`, `delivery_time`, `specific_interval`) VALUES
(1, 'ヤマト運輸', '2016-01-19 14:32:49', '2016-05-23 12:06:50', 1, 1, 0, 'yamato', '{\r\n"0": 520,\r\n"10000000": 0\r\n}', '[]', '{\r\n"指定なし": "指定なし",\r\n"午前中": "午前中"\r\n}', 6);

-- --------------------------------------------------------

--
-- テーブルの構造 `faq`
--

CREATE TABLE `faq` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `question` longtext NOT NULL COMMENT '質問',
  `answer` longtext NOT NULL COMMENT '回答',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `information`
--

CREATE TABLE `information` (
  `id` int(10) unsigned NOT NULL,
  `subject` varchar(255) NOT NULL COMMENT 'タイトル',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日',
  `display_date` datetime NOT NULL COMMENT '表示する日付',
  `begin_date` datetime DEFAULT NULL COMMENT '表示開始日',
  `end_date` datetime DEFAULT NULL COMMENT '表示終了日',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開ステータス\n-1:ゴミ箱\n0:非公開\n1:公開',
  `description` varchar(255) DEFAULT NULL COMMENT '概要',
  `contents` longtext COMMENT '本文'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `member`
--

CREATE TABLE `member` (
  `id` int(10) unsigned NOT NULL COMMENT 'ID',
  `login_id` varchar(255) DEFAULT NULL COMMENT 'ログインID',
  `password` varchar(512) NOT NULL COMMENT 'パスワード',
  `password_hash` varchar(512) NOT NULL COMMENT 'パスワードハッシュ',
  `email` varchar(512) NOT NULL COMMENT 'メールアドレス',
  `member_status` int(11) NOT NULL COMMENT '会員ステータス\n-1:退会\n1:未アクティベート\n2:ログイン可能',
  `create_at` datetime NOT NULL COMMENT '登録日',
  `update_at` datetime NOT NULL COMMENT '更新日',
  `last_login_time` datetime DEFAULT NULL COMMENT '最終ログイン時間',
  `leave_at` datetime DEFAULT NULL COMMENT '退会時間',
  `activate_hash` varchar(512) DEFAULT NULL COMMENT 'アクティベートキー',
  `activate_expire` datetime DEFAULT NULL COMMENT 'アクティベート有効期限',
  `mailaddress` varchar(55) DEFAULT NULL COMMENT 'メールアドレス',
  `company` varchar(512) DEFAULT NULL COMMENT '会社名・屋号',
  `name1` varchar(256) NOT NULL COMMENT '姓',
  `name2` varchar(256) NOT NULL COMMENT '名',
  `kana1` varchar(256) NOT NULL COMMENT 'かな(姓)',
  `kana2` varchar(256) NOT NULL COMMENT 'かな(名)',
  `zip` varchar(45) NOT NULL COMMENT '郵便番号',
  `pref` varchar(45) NOT NULL COMMENT '都道府県',
  `address1` varchar(512) NOT NULL COMMENT '市区町村',
  `address2` varchar(512) NOT NULL COMMENT '町域番地',
  `address3` varchar(512) DEFAULT NULL COMMENT 'マンション名など',
  `tel1` varchar(45) NOT NULL COMMENT '電話番号',
  `tel2` varchar(45) DEFAULT NULL COMMENT '電話番号2',
  `fax` varchar(45) DEFAULT NULL COMMENT 'FAX',
  `memo` text COMMENT '運営利用メモ'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `member`
--

INSERT INTO `member` (`id`, `login_id`, `password`, `password_hash`, `email`, `member_status`, `create_at`, `update_at`, `last_login_time`, `leave_at`, `activate_hash`, `activate_expire`, `mailaddress`, `company`, `name1`, `name2`, `kana1`, `kana2`, `zip`, `pref`, `address1`, `address2`, `address3`, `tel1`, `tel2`, `fax`, `memo`) VALUES
(2, NULL, 'c1b3da3f81e26488b8763f12dd011c77e14a7be2', 'D564xtSVASxS', 'kiyotchi46@gmail.com', 2, '2015-11-14 22:06:23', '2015-11-14 22:14:07', NULL, NULL, '', '0000-00-00 00:00:00', NULL, '屋号', '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', 'あっち', 'そっち', 'こっち', '09087333607', '000-000-000', '111-111-1111', NULL),
(3, NULL, '46e73f0b9c4bbcba52206471073cb17fb6745664', 'LX1EEtQ_S9iU', 'kiyotchi46+1@gmail.com', 1, '2016-03-22 10:24:17', '2016-03-22 10:24:17', NULL, NULL, 'maLARS2snpj3Uq5Djj4lPlnWHoAHhL9V', '2016-03-23 10:24:17', NULL, '春芳堂', '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', '名古屋市', '北区', '', '09087333607', '09087333607', '', NULL),
(4, NULL, '1f44b3d0464943add71567161007dbbc724087b4', 'TLyEqBMrYEU1', 'kiyotchi46+2@gmail.com', 1, '2016-03-22 10:24:41', '2016-03-22 10:24:41', NULL, NULL, 'N7YZpnbqEhsHK_8KBE8oyAHc95MVK57u', '2016-03-23 10:24:41', NULL, '春芳堂', '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', '名古屋市', '北区', '', '09087333607', '09087333607', '', NULL),
(5, NULL, 'ac2903b86cbf14da0aa9f8d7ebcace8f9a3db1e9', 'nYWYQFsZTnRF', 'kiyotchi46+3@gmail.com', 2, '2016-03-22 10:26:55', '2016-03-22 10:27:52', NULL, NULL, '', '0000-00-00 00:00:00', NULL, '春芳堂', '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', '名古屋市', '北区', '', '09087333607', '09087333607', '', NULL),
(6, NULL, 'f0f1dee716ee501c07339d48c0f6e41ad3c2c275', 'WX0Vkus_QGVm', 'kiyotchi46+5@gmail.com', 1, '2016-03-22 12:05:16', '2016-03-22 12:05:16', NULL, NULL, 'r4BsIMA3Av26_AeBR6i99eC32ljVEPPW', '2016-03-23 12:05:16', NULL, '', '伊藤', '清徳', 'いとう', 'きよのり', '451-0014', '愛知県', '名古屋市西区又穂町', 'C', '', '000-111-0001', '', '', NULL),
(7, NULL, 'af1b7849f11683c7e280a26bb69f3747eede964e', '9KpexeTqqNHP', 'kiyotchi46+6@gmail.com', 1, '2016-03-22 12:05:57', '2016-03-22 12:05:57', NULL, NULL, 'LLXWRJpPQ5pYmt~X1dxpoKjoCFI7nWpK', '2016-03-23 12:05:57', NULL, '', '伊藤', '清徳', 'いとう', 'きよのり', '451-0014', '愛知県', '名古屋市西区又穂町', 'C', '', '000-111-0001', '', '', NULL),
(8, NULL, '518a916d2a4df6b6c59930bc75f982de3bb80940', 'M_JYT2jjNjml', 'kiyotchi46+7@gmail.com', 1, '2016-03-22 12:06:48', '2016-03-22 12:06:48', NULL, NULL, 't6h5SYarpwrOAUYIcvuzbScT4x03fgRU', '2016-03-23 12:06:48', NULL, '', '伊藤', '清徳', 'いとう', 'きよのり', '451-0014', '愛知県', '名古屋市西区又穂町', 'C', '', '000-111-0001', '', '', NULL),
(9, NULL, '4bb35e2be76f580c1878107816c3489891774b35', '4Z5JittDW0rz', 'kiyotchi46+8@gmail.com', 1, '2016-03-22 12:07:24', '2016-03-22 12:07:24', NULL, NULL, '_Mwbh_rn6XY1IcpFIWpnmZsuWThzZLye', '2016-03-23 12:07:24', NULL, '', '伊藤', '清徳', 'いとう', 'きよのり', '451-0014', '愛知県', '名古屋市西区又穂町', 'C', '', '000-111-0001', '', '', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `metainfo`
--

CREATE TABLE `metainfo` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '商品に対するメタ情報の場合商品のID',
  `category_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'カテゴリに対するメタであればここに買えゴリID',
  `meta_title` varchar(255) NOT NULL COMMENT 'メタタイトル',
  `meta_value` text COMMENT 'メタ情報',
  `rank` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `order_detail`
--

CREATE TABLE `order_detail` (
  `id` int(10) unsigned NOT NULL,
  `order_id` int(10) unsigned NOT NULL COMMENT 'オーダーID',
  `product_id` int(10) unsigned DEFAULT NULL,
  `sku_id` int(10) unsigned DEFAULT NULL COMMENT '商品DBにデーたがある場合はSKUのID',
  `item_id` varchar(255) NOT NULL COMMENT '品番',
  `name` varchar(255) NOT NULL COMMENT '製品名',
  `price` int(11) NOT NULL COMMENT '単価',
  `qty` int(11) NOT NULL COMMENT '数量',
  `sub_total` int(11) NOT NULL COMMENT '合計',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `parente_detail_id` int(10) unsigned DEFAULT NULL COMMENT '親IDを持っている場合'
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `order_detail`
--

INSERT INTO `order_detail` (`id`, `order_id`, `product_id`, `sku_id`, `item_id`, `name`, `price`, `qty`, `sub_total`, `create_date`, `parente_detail_id`) VALUES
(1, 3, 1, 1, '', '岡本玲 岡本1', 29000, 1, 29000, '2016-05-23 14:43:09', NULL),
(2, 3, 2, 3, '', '本田翼 asdf', 2600, 1, 2600, '2016-05-23 14:43:09', NULL),
(3, 4, 1, 1, '', '岡本玲 岡本1', 29000, 1, 29000, '2016-05-23 14:43:33', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `order_history`
--

CREATE TABLE `order_history` (
  `id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT '顧客ID',
  `order_date` datetime NOT NULL COMMENT 'オーダー日',
  `update_date` datetime NOT NULL COMMENT 'アップデート日',
  `order_status` tinyint(4) NOT NULL COMMENT 'オーダステータス -2:問題あり顧客 -1:キャンセル 1:仮受注 2:受注 3:確認済み 4:在庫割り当て 5:発送完了',
  `payment_status` tinyint(4) NOT NULL COMMENT '支払ステータス 0:未支払い 1:支払い',
  `item_total` int(11) NOT NULL DEFAULT '0' COMMENT '商品代金合計',
  `payment_method` varchar(125) NOT NULL DEFAULT '0',
  `payment_fee` int(11) NOT NULL DEFAULT '0' COMMENT '支払い手数料',
  `delivery_fee` int(11) NOT NULL DEFAULT '0' COMMENT '送料',
  `grand_total` int(11) NOT NULL DEFAULT '0' COMMENT '支払い総合計',
  `payment_date` datetime DEFAULT NULL COMMENT '支払い実行日',
  `canceld_date` datetime DEFAULT NULL COMMENT 'キャンセル実行日',
  `name1` varchar(125) DEFAULT NULL COMMENT '姓',
  `name2` varchar(125) DEFAULT NULL COMMENT '名',
  `kana1` varchar(125) DEFAULT NULL COMMENT 'かな（姓）',
  `kana2` varchar(125) DEFAULT NULL COMMENT 'かな（名）',
  `zip` varchar(45) DEFAULT NULL COMMENT '郵便番号',
  `pref` varchar(125) DEFAULT NULL COMMENT '都道府県',
  `address1` varchar(125) DEFAULT NULL COMMENT '住所1',
  `address2` varchar(125) DEFAULT NULL COMMENT '住所2',
  `address3` varchar(125) DEFAULT NULL COMMENT '住所3',
  `mailaddress` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `tel` varchar(45) DEFAULT NULL COMMENT '電話',
  `fax` varchar(45) DEFAULT NULL COMMENT 'fax',
  `d_name1` varchar(125) DEFAULT NULL COMMENT '姓',
  `d_name2` varchar(125) DEFAULT NULL COMMENT '名',
  `d_kana1` varchar(125) DEFAULT NULL COMMENT 'かな（姓）',
  `d_kana2` varchar(125) DEFAULT NULL COMMENT 'かな（名）',
  `d_zip` varchar(45) DEFAULT NULL COMMENT '郵便番号',
  `d_pref` varchar(125) DEFAULT NULL COMMENT '都道府県',
  `d_address1` varchar(125) DEFAULT NULL COMMENT '住所1',
  `d_address2` varchar(125) DEFAULT NULL COMMENT '住所2',
  `d_address3` varchar(125) DEFAULT NULL COMMENT '住所3',
  `d_tel` varchar(45) DEFAULT NULL COMMENT '電話',
  `order_type` tinyint(4) NOT NULL DEFAULT '0' COMMENT '販売場所 0:店舗 1:通販'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `order_history`
--

INSERT INTO `order_history` (`id`, `customer_id`, `order_date`, `update_date`, `order_status`, `payment_status`, `item_total`, `payment_method`, `payment_fee`, `delivery_fee`, `grand_total`, `payment_date`, `canceld_date`, `name1`, `name2`, `kana1`, `kana2`, `zip`, `pref`, `address1`, `address2`, `address3`, `mailaddress`, `tel`, `fax`, `d_name1`, `d_name2`, `d_kana1`, `d_kana2`, `d_zip`, `d_pref`, `d_address1`, `d_address2`, `d_address3`, `d_tel`, `order_type`) VALUES
(1, NULL, '2016-05-23 14:41:19', '2016-05-23 14:41:19', 1, 0, 29000, '', 2000, 520, 31520, NULL, NULL, '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', 'asfa', 'asdfa', '', NULL, '000-000-0000', NULL, '', '', NULL, NULL, '', '', '', '', '', '000-000-0000', 0),
(2, NULL, '2016-05-23 14:41:28', '2016-05-23 14:41:28', 1, 0, 31600, '', 2000, 520, 34120, NULL, NULL, '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', 'asfa', 'asdfa', '', NULL, '000-000-0000', NULL, '', '', NULL, NULL, '', '', '', '', '', '000-000-0000', 0),
(3, NULL, '2016-05-23 14:43:09', '2016-05-23 14:43:09', 1, 0, 31600, '', 2000, 520, 34120, NULL, NULL, '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', 'asfa', 'asdfa', '', NULL, '000-000-0000', NULL, '', '', NULL, NULL, '', '', '', '', '', '000-000-0000', 0),
(4, NULL, '2016-05-23 14:43:33', '2016-05-23 14:43:33', 1, 0, 29000, '', 2000, 520, 31520, NULL, NULL, '伊藤', '清徳', 'いとう', 'きよのり', '462-0801', '愛知県', 'asfa', 'asdfa', '', NULL, '000-000-0000', NULL, '', '', NULL, NULL, '', '', '', '', '', '000-000-0000', 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `page_blocks`
--

CREATE TABLE `page_blocks` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'ページブロックの名前（日本語OK!）',
  `type` varchar(255) NOT NULL,
  `page_path` varchar(255) DEFAULT NULL COMMENT 'ページパス（グローバルならなし）',
  `global` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '全ページ共通なら1',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順',
  `open_date` datetime DEFAULT NULL COMMENT '表示開始日時',
  `close_date` datetime DEFAULT NULL COMMENT '表示終了日',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '編集日時',
  `image_path` varchar(255) DEFAULT NULL COMMENT '画像パス',
  `content` longtext,
  `alt` text COMMENT 'alt属性',
  `link_url` varchar(255) DEFAULT NULL COMMENT 'リンク（youtube)',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '公開ステータス'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `page_blocks`
--

INSERT INTO `page_blocks` (`id`, `name`, `type`, `page_path`, `global`, `rank`, `open_date`, `close_date`, `create_date`, `update_date`, `image_path`, `content`, `alt`, `link_url`, `open_status`) VALUES
(1, 'お知らせ', 'html', '/', 1, 1, NULL, NULL, '2016-03-18 12:46:41', '2016-03-18 12:46:41', '', 'asdfasfdasdfasfd', NULL, NULL, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `partner`
--

CREATE TABLE `partner` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT '取引先名',
  `code` varchar(255) NOT NULL COMMENT '取引先コード',
  `created_date` datetime DEFAULT NULL COMMENT '作成日',
  `partner_status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '取引ステータス\n1:取引中\n0:商談中\n-1:取引停止'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(512) NOT NULL COMMENT '支払い方法名',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '並び順',
  `usage` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '利用中か',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `shipment_type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '配送料金体型\n1 一律\n2 都道府県別',
  `method` varchar(512) NOT NULL COMMENT '計算実行メソッド',
  `json_data` longtext NOT NULL COMMENT 'JSONデータ'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `payment_method`
--

INSERT INTO `payment_method` (`id`, `name`, `create_date`, `update_date`, `rank`, `usage`, `delete_flag`, `shipment_type`, `method`, `json_data`) VALUES
(1, 'クレジットカード', '2016-01-14 01:22:34', '2016-05-23 11:53:48', 1, 1, 0, 1, 'credit', '{\r\n"0":2000,\r\n"100000":0\r\n}'),
(2, '後払い', '2016-05-23 11:15:22', '2016-05-23 11:18:17', 2, 1, 0, 1, 'atobarai', '{"0":350,"5000":0}');

-- --------------------------------------------------------

--
-- テーブルの構造 `products`
--

CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL,
  `item_id` varchar(125) NOT NULL COMMENT '品番',
  `item_name` varchar(225) NOT NULL COMMENT '品名',
  `price` int(10) unsigned DEFAULT NULL COMMENT '定価',
  `sale_price` int(10) unsigned DEFAULT NULL COMMENT '売価',
  `open_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '公開ステータス\n1:公開\n2:非公開',
  `item_status` text COMMENT '商品ステータスフラグ（新商品など）',
  `postage` int(10) unsigned DEFAULT NULL COMMENT '商品別送料',
  `point_rate` int(10) unsigned DEFAULT '5' COMMENT 'ポイント付与率',
  `shipment` varchar(125) DEFAULT NULL COMMENT '発送日目安',
  `qty_limit` int(10) unsigned DEFAULT NULL COMMENT '販売数制限',
  `serch_word` text COMMENT '検索キーワード（EC-CUBE)用',
  `remark` text COMMENT '備考',
  `main_view_comment` longtext COMMENT 'メインコメント(一覧用)',
  `main_detail_comment` longtext NOT NULL COMMENT 'メインコメント(詳細ページ)',
  `thumbnail` text COMMENT 'サムネイル',
  `main_image` text COMMENT 'メイン画像',
  `main_big_image` text COMMENT 'メインの拡大画像',
  `sub1_title` text COMMENT '拡張エリア1 タイトル',
  `sub1_text` longtext COMMENT '拡張エリア1 コメント',
  `sub1_img` text COMMENT '拡張エリア1 画像',
  `sub2_title` text COMMENT '拡張エリア2 タイトル',
  `sub2_text` longtext COMMENT '拡張エリア2 コメント',
  `sub2_img` text COMMENT '拡張エリア2 画像',
  `sub3_title` text COMMENT '拡張エリア3 タイトル',
  `sub3_img` text COMMENT '拡張エリア3 画像',
  `sub3_text` longtext COMMENT '拡張エリア3 コメント',
  `sub4_title` text COMMENT '拡張エリア4 タイトル',
  `sub4_text` longtext COMMENT '拡張エリア4 コメント',
  `sub4_img` text COMMENT '拡張エリア4 画像',
  `sub5_title` text COMMENT '拡張エリア5 タイトル',
  `sub5_text` longtext COMMENT '拡張エリア5 コメント',
  `sub5_img` text COMMENT '拡張エリア5 画像',
  `sub6_title` text COMMENT '拡張エリア6タイトル',
  `sub6_text` longtext COMMENT '拡張エリア6 コメント',
  `sub6_img` text COMMENT '拡張エリア6 画像',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '更新日',
  `link_url` varchar(30) DEFAULT NULL COMMENT 'パーマリンク用ID',
  `page_key` varchar(256) DEFAULT NULL COMMENT 'ページキーワード',
  `page_desc` varchar(512) DEFAULT NULL COMMENT 'ページ概要',
  `sale_status` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '販売ステータス\n1:通常販売\n2:問い合わせのみ\n3:商品紹介のみ',
  `youtube` varchar(255) NOT NULL COMMENT 'youtubeURL',
  `template_file` varchar(255) NOT NULL DEFAULT '' COMMENT '利用テンプレートファイル',
  `option_template_file` varchar(255) NOT NULL DEFAULT '' COMMENT 'オプション用テンプレートファイル'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='商品リスト';

--
-- テーブルのデータのダンプ `products`
--

INSERT INTO `products` (`id`, `item_id`, `item_name`, `price`, `sale_price`, `open_status`, `item_status`, `postage`, `point_rate`, `shipment`, `qty_limit`, `serch_word`, `remark`, `main_view_comment`, `main_detail_comment`, `thumbnail`, `main_image`, `main_big_image`, `sub1_title`, `sub1_text`, `sub1_img`, `sub2_title`, `sub2_text`, `sub2_img`, `sub3_title`, `sub3_img`, `sub3_text`, `sub4_title`, `sub4_text`, `sub4_img`, `sub5_title`, `sub5_text`, `sub5_img`, `sub6_title`, `sub6_text`, `sub6_img`, `delete_flag`, `create_date`, `update_date`, `link_url`, `page_key`, `page_desc`, `sale_status`, `youtube`, `template_file`, `option_template_file`) VALUES
(1, 'okamoto-rei', '岡本玲', NULL, NULL, 1, '', 0, 5, '', 0, '', '', '節名', 'せつめい', '/files/product_images/res_fe880ba3233dd1aa0d2a0d2cf5c7887b08623242.jpg', '/files/product_images/res_e80e5df71e307892d0661a4d23b00653e98253d3.jpg', '/files/product_images/res_ad942b6e1c50143ce142490f7a63296b0535f39d.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '2015-11-07 20:03:20', '2015-12-22 10:53:56', 'okamoto-rei', '', '', 1, '', 'pattern2.php', ''),
(2, 'honda', '本田翼', NULL, NULL, 1, '1\n4', 0, 5, '', 0, '', '', 'asdf', 'asdf', '/files/product_images/res_4759a9aedf87269df41061a47bdb2c6c1fd077da.jpg', '/files/product_images/res_912e34c4645b7c83688ab2cff3b9dd598afa1b2f.jpg', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, '2015-12-22 10:56:16', '2016-05-19 16:03:27', 'honda', '', '', 1, '', 'index.php', '');

-- --------------------------------------------------------

--
-- テーブルの構造 `product_images`
--

CREATE TABLE `product_images` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL,
  `image` text COMMENT '保存場所',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longtext,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `product_images`
--

INSERT INTO `product_images` (`id`, `product_id`, `image`, `rank`, `content`, `create_date`, `update_date`) VALUES
(29, 1, '/files/product_images/res_abdb22bcc0aa977bf55b9b3e242c1e53895a574e.jpg', 0, '', '2015-12-22 10:53:56', '2015-12-22 10:53:56'),
(30, 1, '/files/product_images/res_73ab21d5bb49191583c06eb7f9dcd78b434c81ea.jpg', 1, '', '2015-12-22 10:53:56', '2015-12-22 10:53:56'),
(64, 2, '/files/product_images/res_f1036264640e7b3fdf76f51eb785c05fc4148459.jpg', 0, '', '2016-05-19 16:03:27', '2016-05-19 16:03:27'),
(65, 2, '/files/product_images/res_e768dcaa1c0967b8e727e61eb246303e3a2f6302.jpg', 1, '', '2016-05-19 16:03:27', '2016-05-19 16:03:27'),
(66, 2, '/files/product_images/res_d81d1aca103e80cbb2ab60a835462662844a92a5.jpg', 2, '', '2016-05-19 16:03:27', '2016-05-19 16:03:27');

-- --------------------------------------------------------

--
-- テーブルの構造 `product_options`
--

CREATE TABLE `product_options` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned DEFAULT NULL COMMENT '商品とひもづける場合',
  `sku_id` int(10) unsigned DEFAULT NULL COMMENT 'SKUとひもづける場合',
  `name` varchar(125) DEFAULT NULL COMMENT '選択肢名',
  `type` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '選択肢のタイプ\n1:セレクトボックス\n2:ラジオボタン\n3:チェックボックス\n4:自由入力（ライン）\n5:自由入力（ボックス）',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順',
  `init` text COMMENT 'テキスト入力欄の場合の初期値',
  `validate` longtext COMMENT 'バリデート条件のJSONを記録',
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `product_options_values`
--

CREATE TABLE `product_options_values` (
  `id` int(10) unsigned NOT NULL,
  `option_id` int(10) unsigned NOT NULL COMMENT 'オプションのID',
  `label` text COMMENT '表示ラベル',
  `value` text COMMENT '値',
  `rank` int(10) unsigned DEFAULT '1' COMMENT '表示順',
  `specific_price` int(11) DEFAULT NULL COMMENT '金額特定をする場合の金額',
  `add_price` int(11) NOT NULL DEFAULT '0' COMMENT '金額修正がある場合',
  `init` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '初期値の場合1',
  `validation` longtext,
  `create_date` datetime NOT NULL COMMENT '作成日',
  `update_date` datetime NOT NULL COMMENT '更新日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `product_sku`
--

CREATE TABLE `product_sku` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL COMMENT '商品テーブルのID',
  `brunch_item_id` varchar(125) NOT NULL COMMENT '枝番',
  `brunch_item_name` varchar(225) NOT NULL COMMENT '枝番品名',
  `price` int(10) unsigned DEFAULT '0' COMMENT '売価',
  `sale_price` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '売価',
  `stock` int(10) unsigned DEFAULT NULL COMMENT '在庫数',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `delete_flag` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '削除フラグ',
  `code_str` varchar(255) DEFAULT NULL COMMENT 'バーコード用文字列',
  `content` longtext COMMENT '説明'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COMMENT='商品SKU';

--
-- テーブルのデータのダンプ `product_sku`
--

INSERT INTO `product_sku` (`id`, `product_id`, `brunch_item_id`, `brunch_item_name`, `price`, `sale_price`, `stock`, `rank`, `delete_flag`, `code_str`, `content`) VALUES
(1, 1, 'rei1', '岡本1', 29800, 29000, NULL, 0, 0, '000001', ''),
(2, 1, 'rei2', 'れいちゃん2', 12900, 12000, NULL, 0, 0, '000002', ''),
(3, 2, 'fasdf', 'asdf', 12900, 2600, 10, 0, 0, '000003', 'asfda'),
(4, 2, 'sdda', 'gsafsd', 2900, 1299, NULL, 0, 0, '000004', 'fadsdf');

-- --------------------------------------------------------

--
-- テーブルの構造 `product_sku_images`
--

CREATE TABLE `product_sku_images` (
  `id` int(10) unsigned NOT NULL,
  `sku_id` int(10) unsigned NOT NULL,
  `image` text COMMENT '保存場所',
  `rank` int(10) unsigned NOT NULL DEFAULT '0',
  `content` longtext,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `product_sku_images`
--

INSERT INTO `product_sku_images` (`id`, `sku_id`, `image`, `rank`, `content`, `create_date`, `update_date`) VALUES
(9, 1, '/files/product_images/res_8b4ce5b42f093606099a37352265a187a379d250.jpg', 0, NULL, '2015-12-22 10:53:56', '2015-12-22 10:53:56'),
(10, 2, '/files/product_images/res_10703dd814ed00c31c59ba8085858edf0895acf7.jpg', 0, NULL, '2015-12-22 10:53:56', '2015-12-22 10:53:56'),
(17, 3, '/files/product_images/res_0ba6cc094ecea85e556f9aa299d0dd80896c97a1.jpg', 0, NULL, '2016-05-19 16:03:27', '2016-05-19 16:03:27'),
(18, 4, '/files/product_images/res_5ee17e4fd02c684e3c1ebcaeb5b7f484fead7eb6.jpg', 0, NULL, '2016-05-19 16:03:27', '2016-05-19 16:03:27');

-- --------------------------------------------------------

--
-- テーブルの構造 `profiles`
--

CREATE TABLE `profiles` (
  `user_id` int(11) unsigned NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `profiles`
--

INSERT INTO `profiles` (`user_id`, `first_name`, `last_name`) VALUES
(1, 'Administrator', 'Admin');

-- --------------------------------------------------------

--
-- テーブルの構造 `profiles_fields`
--

CREATE TABLE `profiles_fields` (
  `id` int(11) unsigned NOT NULL,
  `varname` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL DEFAULT '',
  `field_type` varchar(50) NOT NULL DEFAULT '',
  `field_size` int(3) NOT NULL DEFAULT '0',
  `field_size_min` int(3) NOT NULL DEFAULT '0',
  `required` int(1) NOT NULL DEFAULT '0',
  `match` varchar(255) NOT NULL DEFAULT '',
  `range` varchar(255) NOT NULL DEFAULT '',
  `error_message` varchar(255) NOT NULL DEFAULT '',
  `other_validator` text,
  `default` varchar(255) NOT NULL DEFAULT '',
  `widget` varchar(255) NOT NULL DEFAULT '',
  `widgetparams` text,
  `position` int(3) NOT NULL DEFAULT '0',
  `visible` int(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `profiles_fields`
--

INSERT INTO `profiles_fields` (`id`, `varname`, `title`, `field_type`, `field_size`, `field_size_min`, `required`, `match`, `range`, `error_message`, `other_validator`, `default`, `widget`, `widgetparams`, `position`, `visible`) VALUES
(1, 'first_name', 'First Name', 'VARCHAR', 255, 3, 2, '', '', 'Incorrect First Name (length between 3 and 50 characters).', '', '', '', '', 1, 3),
(2, 'last_name', 'Last Name', 'VARCHAR', 255, 3, 2, '', '', 'Incorrect Last Name (length between 3 and 50 characters).', '', '', '', '', 2, 3);

-- --------------------------------------------------------

--
-- テーブルの構造 `receipt`
--

CREATE TABLE `receipt` (
  `id` int(10) unsigned NOT NULL,
  `customer_id` int(10) unsigned DEFAULT NULL COMMENT 'お客様ID',
  `comming_id` int(10) unsigned DEFAULT NULL COMMENT '来店ID',
  `id_hash` varchar(125) DEFAULT NULL COMMENT 'コードIDのハッシュ',
  `receipt_name` varchar(256) DEFAULT NULL COMMENT '案件名',
  `item_total` int(11) NOT NULL DEFAULT '0' COMMENT '商品合計',
  `item_total_tax` int(11) NOT NULL DEFAULT '0' COMMENT '商品総合計税込',
  `tax` int(11) NOT NULL DEFAULT '0' COMMENT '税額',
  `grand_total` int(11) NOT NULL DEFAULT '0' COMMENT 'お支払合計',
  `grand_total_tax` int(11) NOT NULL DEFAULT '0' COMMENT 'お支払い総合計税込み',
  `payment` int(11) DEFAULT NULL COMMENT 'お支払い金額',
  `change_val` int(11) DEFAULT NULL COMMENT '釣り銭',
  `order_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '注文ステータス\n0:入力中\n1:オーダー確定\n2:お支払い済み\n3:掛売\n4:キャンセル\n5:返品',
  `reserve_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '予約オーダーステータス\n0:予約ではない\n1:予約',
  `deliv_status` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '発送ステータス\n0:発送不要\n1:発送準備\n2:発送完了\n3:受取確認済',
  `received` tinyint(3) unsigned DEFAULT NULL COMMENT '受取フラグ\n0:未確認\n1:受取確認',
  `deliv_code` varchar(225) DEFAULT NULL COMMENT '配送伝票コード',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '変更日',
  `payment_date` datetime DEFAULT NULL,
  `staff_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '担当スタッフID',
  `coupon_id` int(10) unsigned DEFAULT NULL COMMENT 'クーポン利用時IDを格納',
  `coupon_amount` int(10) unsigned DEFAULT NULL COMMENT 'クーポン利用時クーポン金額',
  `order_type` int(10) unsigned DEFAULT '0' COMMENT 'オーダータイプ\n0:POS\n1:通販（オンボード通販システム）',
  `order_history_id` int(10) unsigned DEFAULT NULL COMMENT '通販オーダーヒストリーの詳細ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='レシート';

-- --------------------------------------------------------

--
-- テーブルの構造 `receipt_detail`
--

CREATE TABLE `receipt_detail` (
  `id` int(10) unsigned NOT NULL,
  `receipt_id` int(10) unsigned NOT NULL COMMENT 'レシートID',
  `produt_id` int(10) unsigned DEFAULT NULL COMMENT '商品テーブルのID',
  `item_id` varchar(125) DEFAULT NULL COMMENT '商品番号',
  `item_name` varchar(225) DEFAULT NULL COMMENT '商品名',
  `remark` varchar(225) DEFAULT NULL COMMENT '備考',
  `price` int(11) NOT NULL DEFAULT '0' COMMENT '単価',
  `price_tax` int(11) NOT NULL DEFAULT '0' COMMENT '税込単価',
  `qty` int(11) NOT NULL DEFAULT '1' COMMENT '数量',
  `sub_total` int(11) NOT NULL DEFAULT '0' COMMENT '小計',
  `sub_total_tax` int(11) NOT NULL DEFAULT '0' COMMENT '小計税込',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日',
  `update_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '変更日',
  `sku_id` int(10) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='レシート明細';

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_option_sku`
--

CREATE TABLE `rel_option_sku` (
  `id` int(10) unsigned NOT NULL,
  `product_id` int(10) unsigned NOT NULL COMMENT '商品のID',
  `sku_id` int(11) NOT NULL COMMENT 'SKUのID',
  `rank` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '表示順'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='他の商品のSKUをオプションとして指定できる機能';

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_product_categories`
--

CREATE TABLE `rel_product_categories` (
  `categories_id` int(10) unsigned NOT NULL COMMENT 'カテゴリID',
  `product_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品カテゴリヒモ付';

--
-- テーブルのデータのダンプ `rel_product_categories`
--

INSERT INTO `rel_product_categories` (`categories_id`, `product_id`) VALUES
(1, 1),
(1, 2),
(2, 1),
(2, 2),
(3, 1),
(3, 2);

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_product_tags`
--

CREATE TABLE `rel_product_tags` (
  `product_id` int(10) unsigned NOT NULL,
  `tag_id` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `rel_product_tags`
--

INSERT INTO `rel_product_tags` (`product_id`, `tag_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- テーブルの構造 `rel_product_to_product`
--

CREATE TABLE `rel_product_to_product` (
  `products_id` int(10) unsigned NOT NULL COMMENT 'リレーション元',
  `rel_product_id` int(10) unsigned NOT NULL COMMENT '関連商品ID'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `sessions`
--

CREATE TABLE `sessions` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `sessions`
--

INSERT INTO `sessions` (`id`, `expire`, `data`) VALUES
('5f29438e8ddefa17cb3205d9780538d0', 1463984811, 0x636172747c613a353a7b733a353a226974656d73223b613a313a7b733a34303a2233303937666331663831313337393062383435336534386533633665636638383366396538653337223b613a31303a7b733a363a22736b755f6964223b733a313a2231223b733a31303a2270726f647563745f6964223b733a313a2231223b733a373a226974656d5f6964223b733a31313a226f6b616d6f746f2d726569223b733a393a226974656d5f6e616d65223b733a393a22e5b2a1e69cace78eb2223b733a31343a226272756e63685f6974656d5f6964223b733a343a2272656931223b733a31363a226272756e63685f6974656d5f6e616d65223b733a373a22e5b2a1e69cac31223b733a31303a2263616c635f7072696365223b733a353a223239303030223b733a31303a2273616c655f7072696365223b733a353a223239303030223b733a353a227072696365223b733a353a223239383030223b733a333a22717479223b733a313a2231223b7d7d733a31343a227061796d656e745f6d6574686f64223b613a343a7b733a31343a227061796d656e745f6d6574686f64223b733a313a2231223b733a31353a2264656c69766572795f6d6574686f64223b733a313a2231223b733a31323a2264656c697665725f74696d65223b733a31323a22e68c87e5ae9ae381aae38197223b733a31323a2264656c697665725f64617465223b733a303a22223b7d733a343a22696e666f223b613a32323a7b733a353a226e616d6531223b733a363a22e4bc8ae897a4223b733a353a226e616d6532223b733a363a22e6b885e5beb3223b733a353a226b616e6131223b733a393a22e38184e381a8e38186223b733a353a226b616e6132223b733a31323a22e3818de38288e381aee3828a223b733a333a2274656c223b733a31323a223030302d3030302d30303030223b733a31313a226d61696c61646472657373223b733a32303a226b69796f74636869343640676d61696c2e636f6d223b733a31323a226d61696c6164647265737332223b733a32303a226b69796f74636869343640676d61696c2e636f6d223b733a333a227a6970223b733a383a223436322d30383031223b733a343a2270726566223b733a393a22e6849be79fa5e79c8c223b733a383a226164647265737331223b733a343a2261736661223b733a383a226164647265737332223b733a353a226173646661223b733a383a226164647265737333223b733a303a22223b733a343a22796f626f223b4e3b733a373a22645f636865636b223b733a313a2230223b733a373a22645f6e616d6531223b733a303a22223b733a373a22645f6e616d6532223b733a303a22223b733a353a22645f74656c223b733a303a22223b733a353a22645f7a6970223b733a303a22223b733a363a22645f70726566223b733a303a22223b733a31303a22645f6164647265737331223b733a303a22223b733a31303a22645f6164647265737332223b733a303a22223b733a31303a22645f6164647265737333223b733a303a22223b7d733a31303a226f746865725f64617461223b613a303a7b7d733a31333a226572726f725f6d657373616765223b733a303a22223b7d6f7264657265645f64617461737c613a333a7b733a353a226974656d73223b613a313a7b733a34303a2233303937666331663831313337393062383435336534386533633665636638383366396538653337223b613a31313a7b733a363a22736b755f6964223b733a313a2231223b733a31303a2270726f647563745f6964223b733a313a2231223b733a373a226974656d5f6964223b733a31313a226f6b616d6f746f2d726569223b733a393a226974656d5f6e616d65223b733a393a22e5b2a1e69cace78eb2223b733a31343a226272756e63685f6974656d5f6964223b733a343a2272656931223b733a31363a226272756e63685f6974656d5f6e616d65223b733a373a22e5b2a1e69cac31223b733a31303a2263616c635f7072696365223b733a353a223239303030223b733a31303a2273616c655f7072696365223b733a353a223239303030223b733a353a227072696365223b733a353a223239383030223b733a333a22717479223b733a313a2231223b733a393a227375625f746f74616c223b693a32393030303b7d7d733a343a2263616c63223b613a31313a7b733a31303a226974656d5f746f74616c223b693a32393030303b733a31343a226974656d5f746f74616c5f746178223b693a32393030303b733a31303a226974656d5f636f756e74223b693a313b733a333a22746178223b693a303b733a31343a22636f6d6d697373696f6e5f666565223b693a323030303b733a31383a22636f6d6d697373696f6e5f6665655f746178223b693a323030303b733a31323a2264656c69766572795f666565223b693a3532303b733a31363a2264656c69766572795f6665655f746178223b693a3532303b733a31313a226772616e645f746f74616c223b693a33313532303b733a31353a226772616e645f746f74616c5f746178223b693a33313532303b733a31313a2273746f636b5f6572726f72223b623a303b7d733a363a22737472696e67223b613a313a7b733a31343a227061796d656e745f6d6574686f64223b733a303a22223b7d7d6f7264657265645f64617461735f6d6f64656c7c613a32363a7b733a353a226e616d6531223b733a363a22e4bc8ae897a4223b733a353a226e616d6532223b733a363a22e6b885e5beb3223b733a353a226b616e6131223b733a393a22e38184e381a8e38186223b733a353a226b616e6132223b733a31323a22e3818de38288e381aee3828a223b733a333a227a6970223b733a383a223436322d30383031223b733a333a2274656c223b733a31323a223030302d3030302d30303030223b733a31313a226d61696c61646472657373223b733a32303a226b69796f74636869343640676d61696c2e636f6d223b733a31323a226d61696c6164647265737332223b733a32303a226b69796f74636869343640676d61696c2e636f6d223b733a343a2270726566223b733a393a22e6849be79fa5e79c8c223b733a383a226164647265737331223b733a343a2261736661223b733a383a226164647265737332223b733a353a226173646661223b733a383a226164647265737333223b733a303a22223b733a343a22796f626f223b4e3b733a373a22645f636865636b223b733a313a2230223b733a373a22645f6e616d6531223b733a303a22223b733a373a22645f6e616d6532223b733a303a22223b733a353a22645f7a6970223b733a303a22223b733a363a22645f70726566223b733a303a22223b733a353a22645f74656c223b733a303a22223b733a31303a22645f6164647265737331223b733a303a22223b733a31303a22645f6164647265737332223b733a303a22223b733a31303a22645f6164647265737333223b733a303a22223b733a31343a227061796d656e745f6d6574686f64223b733a313a2231223b733a31353a2264656c69766572795f6d6574686f64223b733a313a2231223b733a31323a2264656c697665725f74696d65223b733a31323a22e68c87e5ae9ae381aae38197223b733a31323a2264656c697665725f64617465223b733a303a22223b7d6f726465725f69647c693a343b);

-- --------------------------------------------------------

--
-- テーブルの構造 `shop`
--

CREATE TABLE `shop` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(125) NOT NULL COMMENT 'ショップ名',
  `zip` varchar(8) DEFAULT NULL,
  `pref` varchar(45) DEFAULT NULL,
  `address1` varchar(255) DEFAULT NULL COMMENT '住所1',
  `address2` varchar(255) DEFAULT NULL COMMENT '住所2',
  `address3` varchar(45) DEFAULT NULL COMMENT '住所3',
  `tel` varchar(45) DEFAULT NULL COMMENT '電話',
  `fax` varchar(45) DEFAULT NULL COMMENT 'fax',
  `mailaddress` varchar(255) DEFAULT NULL COMMENT '代表メールアドレス',
  `rank` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '表示順',
  `create_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT '作成日'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `shop`
--

INSERT INTO `shop` (`id`, `name`, `zip`, `pref`, `address1`, `address2`, `address3`, `tel`, `fax`, `mailaddress`, `rank`, `create_date`) VALUES
(1, 'name', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- テーブルの構造 `shop_info`
--

CREATE TABLE `shop_info` (
  `id` int(10) unsigned NOT NULL,
  `shop_name` varchar(255) NOT NULL COMMENT 'ショップ名',
  `zip` varchar(45) NOT NULL COMMENT '郵便番号',
  `pref` varchar(45) NOT NULL COMMENT '都道府県',
  `address1` varchar(255) NOT NULL COMMENT '住所1',
  `address2` varchar(255) NOT NULL COMMENT '住所2',
  `address3` varchar(255) NOT NULL COMMENT '住所3',
  `tel` varchar(255) DEFAULT NULL COMMENT '電話番号',
  `fax` varchar(255) DEFAULT NULL COMMENT 'FAX',
  `email` varchar(255) DEFAULT NULL COMMENT 'メールアドレス',
  `url` varchar(255) DEFAULT NULL COMMENT 'ショップURL'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `site_info`
--

CREATE TABLE `site_info` (
  `id` int(10) unsigned NOT NULL,
  `site_name` varchar(512) NOT NULL COMMENT 'サイト名',
  `theme` varchar(512) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `stocking`
--

CREATE TABLE `stocking` (
  `id` bigint(19) unsigned NOT NULL,
  `sku_id` int(10) unsigned NOT NULL COMMENT 'D',
  `insert_date` datetime NOT NULL COMMENT '仕入れ日',
  `purchase_price` int(10) unsigned NOT NULL COMMENT '仕入れ単価',
  `purchase_qty` int(10) unsigned DEFAULT NULL COMMENT '仕入れ個数',
  `sub_total` int(10) unsigned DEFAULT NULL COMMENT '仕入れ総額',
  `leaving_date` datetime DEFAULT NULL COMMENT '出庫日',
  `leaving_qty` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '出庫個数',
  `memo` text,
  `partner_id` int(10) unsigned DEFAULT NULL COMMENT '仕入先ID'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `stocking`
--

INSERT INTO `stocking` (`id`, `sku_id`, `insert_date`, `purchase_price`, `purchase_qty`, `sub_total`, `leaving_date`, `leaving_qty`, `memo`, `partner_id`) VALUES
(1, 3, '2016-01-30 16:35:21', 120, 10, 1200, NULL, 0, '', 0);

-- --------------------------------------------------------

--
-- テーブルの構造 `stocking_leave`
--

CREATE TABLE `stocking_leave` (
  `id` int(10) unsigned NOT NULL,
  `stocking_id` int(10) unsigned NOT NULL COMMENT '入庫ID',
  `leaving_date` datetime NOT NULL COMMENT '出庫日',
  `leaving_count` int(10) unsigned NOT NULL COMMENT '出庫個数',
  `leaving_status` tinyint(4) DEFAULT '1' COMMENT '出庫ステータス\n1:出庫\n-1:キャンセル',
  `canseled_date` datetime DEFAULT NULL COMMENT '出庫キャンセル日'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `tags`
--

CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'タグ名',
  `create_date` datetime NOT NULL,
  `update_date` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `tags`
--

INSERT INTO `tags` (`id`, `name`, `create_date`, `update_date`) VALUES
(1, 'okamotorei', '2015-11-07 20:03:20', '2015-11-07 20:03:20');

-- --------------------------------------------------------

--
-- テーブルの構造 `tbl_migration`
--

CREATE TABLE `tbl_migration` (
  `version` varchar(255) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` int(11) unsigned NOT NULL,
  `username` varchar(20) NOT NULL DEFAULT '',
  `password` varchar(128) NOT NULL DEFAULT '',
  `email` varchar(128) NOT NULL DEFAULT '',
  `activkey` varchar(128) NOT NULL DEFAULT '',
  `superuser` int(1) NOT NULL DEFAULT '0',
  `status` int(1) NOT NULL DEFAULT '0',
  `create_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `lastvisit_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shop_id` int(10) unsigned DEFAULT '0' COMMENT 'ショップID'
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- テーブルのデータのダンプ `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `activkey`, `superuser`, `status`, `create_at`, `lastvisit_at`, `shop_id`) VALUES
(1, 'admin', 'ebf4ffd4358379b57524deaecf713547', 'webmaster@example.com', 'd816b7f83e35affebdb0c8d78a6a51b8', 1, 1, '2015-01-07 01:40:17', '2015-07-05 21:37:54', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `block_table_html`
--
ALTER TABLE `block_table_html`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_parent_id` (`parent_id`);

--
-- Indexes for table `cms2_blocks`
--
ALTER TABLE `cms2_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms2_blocks_admin`
--
ALTER TABLE `cms2_blocks_admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms2_blocks_version`
--
ALTER TABLE `cms2_blocks_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_pages`
--
ALTER TABLE `cms_pages`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_page_path` (`page_path`);

--
-- Indexes for table `cms_page_block_html`
--
ALTER TABLE `cms_page_block_html`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_page_version`
--
ALTER TABLE `cms_page_version`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comming`
--
ALTER TABLE `comming`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comming_assets`
--
ALTER TABLE `comming_assets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coupon_usage`
--
ALTER TABLE `coupon_usage`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customer_profile_field`
--
ALTER TABLE `customer_profile_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_method`
--
ALTER TABLE `delivery_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `faq`
--
ALTER TABLE `faq`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `information`
--
ALTER TABLE `information`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `member`
--
ALTER TABLE `member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `metainfo`
--
ALTER TABLE `metainfo`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_detail`
--
ALTER TABLE `order_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_history`
--
ALTER TABLE `order_history`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `page_blocks`
--
ALTER TABLE `page_blocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partner`
--
ALTER TABLE `partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_item_id` (`item_id`);

--
-- Indexes for table `product_images`
--
ALTER TABLE `product_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options`
--
ALTER TABLE `product_options`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_options_values`
--
ALTER TABLE `product_options_values`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_sku`
--
ALTER TABLE `product_sku`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_product_id` (`product_id`);

--
-- Indexes for table `product_sku_images`
--
ALTER TABLE `product_sku_images`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profiles`
--
ALTER TABLE `profiles`
  ADD PRIMARY KEY (`user_id`);

--
-- Indexes for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `receipt`
--
ALTER TABLE `receipt`
  ADD PRIMARY KEY (`id`),
  ADD KEY `index_hash` (`id_hash`);

--
-- Indexes for table `receipt_detail`
--
ALTER TABLE `receipt_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rel_option_sku`
--
ALTER TABLE `rel_option_sku`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rel_product_categories`
--
ALTER TABLE `rel_product_categories`
  ADD PRIMARY KEY (`categories_id`,`product_id`);

--
-- Indexes for table `rel_product_tags`
--
ALTER TABLE `rel_product_tags`
  ADD PRIMARY KEY (`product_id`,`tag_id`);

--
-- Indexes for table `rel_product_to_product`
--
ALTER TABLE `rel_product_to_product`
  ADD PRIMARY KEY (`products_id`,`rel_product_id`);

--
-- Indexes for table `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop`
--
ALTER TABLE `shop`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shop_info`
--
ALTER TABLE `shop_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `site_info`
--
ALTER TABLE `site_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocking`
--
ALTER TABLE `stocking`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stocking_leave`
--
ALTER TABLE `stocking_leave`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_migration`
--
ALTER TABLE `tbl_migration`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_username` (`username`),
  ADD UNIQUE KEY `user_email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `block_table_html`
--
ALTER TABLE `block_table_html`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `cms2_blocks`
--
ALTER TABLE `cms2_blocks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `cms2_blocks_admin`
--
ALTER TABLE `cms2_blocks_admin`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms2_blocks_version`
--
ALTER TABLE `cms2_blocks_version`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cms_pages`
--
ALTER TABLE `cms_pages`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_page_block_html`
--
ALTER TABLE `cms_page_block_html`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_page_version`
--
ALTER TABLE `cms_page_version`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comming`
--
ALTER TABLE `comming`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `comming_assets`
--
ALTER TABLE `comming_assets`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `coupon_usage`
--
ALTER TABLE `coupon_usage`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `customer_profile_field`
--
ALTER TABLE `customer_profile_field`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `delivery_method`
--
ALTER TABLE `delivery_method`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `faq`
--
ALTER TABLE `faq`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `information`
--
ALTER TABLE `information`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `member`
--
ALTER TABLE `member`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `metainfo`
--
ALTER TABLE `metainfo`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `order_detail`
--
ALTER TABLE `order_detail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `order_history`
--
ALTER TABLE `order_history`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `page_blocks`
--
ALTER TABLE `page_blocks`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `partner`
--
ALTER TABLE `partner`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `product_images`
--
ALTER TABLE `product_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `product_options`
--
ALTER TABLE `product_options`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_options_values`
--
ALTER TABLE `product_options_values`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `product_sku`
--
ALTER TABLE `product_sku`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `product_sku_images`
--
ALTER TABLE `product_sku_images`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `profiles`
--
ALTER TABLE `profiles`
  MODIFY `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `profiles_fields`
--
ALTER TABLE `profiles_fields`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `receipt`
--
ALTER TABLE `receipt`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `receipt_detail`
--
ALTER TABLE `receipt_detail`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rel_option_sku`
--
ALTER TABLE `rel_option_sku`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shop`
--
ALTER TABLE `shop`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `shop_info`
--
ALTER TABLE `shop_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `site_info`
--
ALTER TABLE `site_info`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stocking`
--
ALTER TABLE `stocking`
  MODIFY `id` bigint(19) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `stocking_leave`
--
ALTER TABLE `stocking_leave`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
