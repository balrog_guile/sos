<div class="helpcenter">
    <div class="helpcenter-inner">
        <p class="help_status js-status"></p>
        
        <div class="streams">
            <ul>
                <li><video id="others-video" class="stream" autoplay></video>
                <li><video id="my-video" class="stream" autoplay muted></video></li>
            </ul>
        </div>

        <p class="help_btn"><a href="javascript:void(0);" class="btn btn-default js-peerStart">
            お助けボタン
        </a></p>
    </div>
</div>