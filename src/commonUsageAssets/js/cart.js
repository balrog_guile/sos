/* =============================================================================
 * カート操作関連
 * ========================================================================== */
$(document).ready(function(){
    
    // ----------------------------------------------------
    /**
     * 全体で使う変数
     */
    
    
    
    
    
    // ----------------------------------------------------
    /**
     * ダイアログ用ボックスを作成
     */
    (function(){
        if( $('.sos-add-temp-form-dialog').length > 0 )
        {
            $('.sos-add-temp-form-dialog').dialog({
                autoOpen: false,
                width: $(window).width() * 0.8,
                height: $(window).height() * 0.8,
                modal: true
            });
        }
        if( $('.sos-input-seat-number').length > 0 )
        {
            $('.sos-input-seat-number').dialog({
                autoOpen: false,
                width: $(window).width() * 0.8,
                height: $(window).height() * 0.8,
                modal: true
            });
        }
    })();
    
    
    // ----------------------------------------------------
    /**
     * ビューモデル追加
     */
    var orderTempModelView = new orderTempModel();
    ko.applyBindings(orderTempModelView);
    
    
    // ----------------------------------------------------
    /**
     * 仮に追加
     */
    $('.add-temp-form').on(
        'submit',
        function(e){
            var url = APIURL['addTempOrder'];
            var skuId = $(this).find('[name=skuId]').val();
            var qty = $(this).find('[name=qty]').val();
            
            var params = {
                'skuId': skuId,
                'qty': qty
            };
            
            $.ajax({
                url: url, data: params, type: 'post', dataType: 'json'
            })
            .success(function(data){
                orderTempModelView.items.removeAll();
                $.each( data.items, function( key, val ){
                    val.key = key;
                    orderTempModelView.items.push( val );
                })
                $('.sos-add-temp-form-dialog').dialog('open');
            })
                    .error(function(data){
                alert('通信エラー');
            });
            
            
            return false;
        }
    );
    
    // ----------------------------------------------------
    /**
     * 仮オーダーダイアログを閉じる
     */
    function tempOrderDialogClose()
    {
        $('.sos-add-temp-form-dialog').dialog('close');
    }
    $('.sos-add-temp-form-dialog-continue').on('click',function(e){
        tempOrderDialogClose();
    });
    
    
    // ----------------------------------------------------
    /**
     * オーダー実行
     */
    function orderExec()
    {
        $('.sos-add-temp-form-dialog').dialog('close');
        $('.sos-input-seat-number').dialog('open');
    }
    $('.sos-add-temp-form-dialog-exec-order').on( 'click',
        function(e){ orderExec(); }
    );
    
    
    function orderExecForm( seatNumber ){
        
        var url = APIURL['orderExec'];
        var params = {
            seat_number: seatNumber
        };
        $.ajax({
            url: url,
            type: 'post',
            data: params,
            dataType: 'json'
        })
        .success(function(data){
            if( data == true )
            {
                orderTempModelView.items.removeAll();
                $('.sos-input-seat-number').dialog('close');
            }
            else
            {
                alert('セッティグエラー');
            }
        })
        .error(function(data){
            alert('通信エラー');
        });
        
        
    }
    $('.sos-input-seat-number-form').on(
        'submit',
        function(e){
            var seatNum = $(this).find('.sos-input-seat-number-form-qty').val();
            orderExecForm( seatNum );
            return false;
        }
    );
    
    // ----------------------------------------------------
});