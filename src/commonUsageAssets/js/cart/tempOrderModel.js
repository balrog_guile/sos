/* =============================================================================
 * 仮オーダーモデル
 * ========================================================================== */

// -----------------------------------------------------------------------------
/**
 * 全体オーダーモデル
 */
var orderTempModel = function(){
    
    var self = this;
    
    //アイテムの配列
    this.items = ko.observableArray([]);
    
    
    // ----------------------------------------------------
    /**
     * 仮オーダー削除
     */
    this.tempOrderDelete = function( obj, evObject ){
        
        var url = APIURL['deleteTempOrder'];
        var params = { 'key': obj.key };
        
        $.ajax({
            url: url, data: params, type: 'post', dataType: 'json'
        })
        .success(function(data){
            self.items.removeAll();
            $.each( data.items, function( key, val ){
                val.key = key;
                self.items.push( val );
            })
            $('.add-temp-form-dialog').dialog('open');
        }).error(function(data){
            alert('通信エラー');
        });
        
        
    }
    
    // ----------------------------------------------------
    
    
    
    
};




// -----------------------------------------------------------------------------