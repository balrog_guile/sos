/* =============================================================================
 * トップページ
 * ========================================================================== */
$(document).ready(function(){
    
    // ----------------------------------------------------
    /**
     * 変数
     */

    var localstream;
    //PeerId
    var peer = new Peer({key: peerValues['key']});
    var call;
    
    //ペアリスト
    // var peerList = [];

    // connect data
    var connectData = {};

    //環境差を考慮したシステム要件チェック
    navigator.getUserMedia =
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia;

    // ----------------------------------------------------
    /**
     * peer IDの取得
     */
    peer.on('open', function(id){
        connectData.peer_id = id;
        console.log(id);
    });
    
    
    // ----------------------------------------------------
    /**
     * コールを受ける
     */
    peer.on('call', function(call){

        navigator.getUserMedia({audio: true, video: true}, function(stream){
            
            // 自身の、映像を表示する
            $('#my-video').prop('src', URL.createObjectURL(stream)).addClass('is-active');
            $('.js-status').empty();

            // コールを受ける
            call.answer(stream);

            // 相手を表示
            call.on('stream', function(othersStream){
                $('#others-video').prop('src', URL.createObjectURL(othersStream)).addClass('is-active');
            });

            // 動かない
            call.on('close', function(e){
                alert('close!');
            });
            
            call.on('error', function(err){
                console.log(err);
            });


        },
        function() { alert("Error!"); });

    });

    // ----------------------------------------------------
    /**
     * お助けボタンポチ
     */
    $('.js-peerStart').on('click', function(){
        savePeer();
    });

    // ----------------------------------------------------
    /**
     * クローズ時
     */
    peer.on('close', function(){
        // うごかない
        alert('close!');
    });

    // ----------------------------------------------------
    /**
     * ブラウザ閉じる
     */
    $(window).on('beforeunload' ,function(e){
        changePeerStatus();
        peer.destroy();
    });

    // ----------------------------------------------------
    /**
     * peerリスト取得
     */
    function getPeerListId()
    {
        peer.listAllPeers(function( list ){
            var i = 0, len = list.length;
            peerList = [];
            while( i < len )
            {
                if( list[i].match(/^center\-/) )
                {
                    peerList.push( list[i] );
                }
                i ++ ;
            }
            console.log(peerList);
        });
    }
    
    // ----------------------------------------------------
    /**
     * peerのconnect情報をDBに保存
     */
    function savePeer()
    {
        var url = location.href;
        var q = getUrlVars();

        connectData.url = url;
        connectData.shop_id = q.SOS_THEME_SELECT;
        
        $.ajax({
            url: baseUrl + '/peerConnects/createAsync/',
            type: 'post',
            data: {connect: connectData},
            dataType: 'json'
        }).done(function(data){
            $('.js-status').text('接続中です...');
        }).fail(function(e){
            console.log(e);
        });
    }

    // ----------------------------------------------------
    /**
     * peerのconnect情報のステータスを変更
     */
    function changePeerStatus()
    {
        $.ajax({
            url: baseUrl + '/peerConnects/statusUpdateAsync/',
            type: 'post',
            data: {connect: connectData},
            dataType: 'json'
        }).done(function(data){
            console.log(data);
        }).fail(function(e){
            console.log(e);
        });
    }

    // ----------------------------------------------------

    /**
     *  GETパラメータを配列にして返す
     *  
     *  @return     パラメータのObject
     *
     */
    function getUrlVars()
    {
        var vars = {}; 
        var param = location.search.substring(1).split('&');
        for(var i = 0; i < param.length; i++) {
            var keySearch = param[i].search(/=/);
            var key = '';
            if(keySearch != -1) key = param[i].slice(0, keySearch);
            var val = param[i].slice(param[i].indexOf('=', 0) + 1);
            if(key != '') vars[key] = decodeURI(val);
        } 
        return vars; 
    }

    // ----------------------------------------------------
    /**
     * ブラウザを閉じた時
     */
    $(window).on('beforeunload' ,function(e){
        changePeerStatus();
        peer.destroy();
    });
    
});