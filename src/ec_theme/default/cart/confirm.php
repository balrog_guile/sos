<!doctype html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>テスト</title>
		<meta name="description" content="Unika - Responsive One Page HTML5 Template">
		<meta name="keywords" content="HTML5, Bootsrtrap, One Page, Responsive, Template, Portfolio" />
		<meta name="author" content="imransdesign.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
                
                <!-- Google Fonts  -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'> <!-- Body font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'> <!-- Navbar font -->

		<!-- Libs and Plugins CSS -->
                <link rel="stylesheet" href="<?php echo $templatePath; ?>inc/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/animations/css/animate.min.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/font-awesome/css/font-awesome.min.css"> <!-- Font Icons -->
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/owl-carousel/css/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/owl-carousel/css/owl.theme.css">

		<!-- Theme CSS -->
                <link rel="stylesheet" href="<?php echo $templatePath; ?>css/reset.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>css/style.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>css/mobile.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/cool-gray.css">
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/ice-blue.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/summer-orange.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/fresh-lime.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/night-purple.css"> -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

    <body data-spy="scroll" data-target="#main-navbar">
        <div class="page-loader"></div>  <!-- Display loading image while page loads -->
    	<div class="body">
        
            <!--========== BEGIN HEADER ==========-->
            <header id="header" class="header-main">

                <!-- Begin Navbar -->
                <nav id="main-navbar" class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

                  <div class="container">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand page-scroll" href="index.html">Unika</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="page-scroll" href="body">Home</a></li>
                            <li><a class="page-scroll" href="#about-section">About</a></li>
                            <li><a class="page-scroll" href="#services-section">Services</a></li>
                            <li><a class="page-scroll" href="#portfolio-section">Works</a></li>
                            <li><a class="page-scroll" href="#team-section">Team</a></li>                            
                            <li><a class="page-scroll" href="#prices-section">Prices</a></li>
                            <li><a class="page-scroll" href="#contact-section">Contact</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container -->
                </nav>
                <!-- End Navbar -->

            </header>
            <!-- ========= END HEADER =========-->
            
            <hr />
			<hr />
			<hr />
			<hr />
			<hr />
			<hr />
			<hr />
			<hr />
			<hr />
			<div class="container">
				
				<!-- カートの中 -->
				<table class="table table-bordered table-hover table-striped">
					<thead>
						<tr><th colspan="5">カートの中確認</th></tr>
					</thead>
					<tbody>
						<?php foreach( $items as $key => $item ): ?>
						<tr>
							<td>
								<?= $item['item_id']; ?> <?= $item['brunch_item_name']; ?><br />
								<?= $item['item_name']; ?> <?= $item['brunch_item_id']; ?>
							</td>
							<td>
								<?= number_format($item['calc_price']); ?>円
							</td>
							<td>
								<?= CHtml::dropDownList( 'qty[' . $key .']', $item['qty'], [1=>1, 2=>2, 3=>3 ] ); ?>
							</td>
							<td>
								<?= number_format($item['sub_total']); ?>円
							</td>
						</tr>
						<?php endforeach; ?>
					</tbody>
				</table>
				<!-- /カートの中 -->
				
				
				<hr />
				
				
				
				
				<!--  お客様情報 -->
				<div class="cart-section">
					<h2 class="heading"><span>お客様情報</span></h2>
					<div class="container margin-t margin-b">
						<div class="row">
							<div class="col-md-12">
								<div class="form-group">
									<label for="HaisaiModel_name1" class="col-sm-3 col-xs-12 control-label">お名前</label>
									<div class="col-sm-8 col-xs-12">
										<?= $model->name1; ?> <?= $model->name2; ?>
									</div>
								</div>
								<div class="form-group">
									<label for="HaisaiModel_kana1" class="col-sm-3 col-xs-12 control-label">お名前(かな)</label>
									<div class="col-sm-8 col-xs-12">
										<?= $model->kana1; ?> <?= $model->kana2; ?>
									</div>
								</div>
								<div class="form-group">
									<label for="HaisaiModel_tel" class="col-sm-3 control-label">電話番号</label>
									<div class="col-sm-4">
										<?= $model->tel; ?>
									</div>
								</div>
								<div class="form-group">
									<label for="HaisaiModel_zip" class="col-sm-3 col-xs-12 control-label">郵便番号</label>
									<div class="col-sm-4 col-xs-8">
										<?= $model->zip; ?>
									</div>
								</div>
								<div class="form-group">
									<label for="HaisaiModel_pref" class="col-sm-3 control-label">ご住所</label>
									<div class="col-sm-4">
										<?= $model->pref; ?>
									</div>
									<div class="col-sm-6 col-sm-offset-3">
										<?= $model->address1; ?><br />
										<?= $model->address2; ?><br />
										<?= $model->address3; ?>
									</div>
								</div>
								<div class="form-group">
									<label for="HaisaiModel_mailaddress" class="col-sm-3 control-label">メールアドレス</label>
									<div class="col-sm-4">
										<?= $model->mailaddress; ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /お客様情報 -->
		
				<!-- 配送先があれば -->
				<div class="cart-section">
					<h2 class="heading"><span>配送先</span></h2>
					<div class="container form_area margin-t margin-b">
					  <div class="row">
						<div class="col-md-12">
							<div style="text-align: center;">
						  <div class="form-group">
							  <?= $model->d_check; ?>
						  </div>
								</div>
						  <div id="d-form-area">
						  <div class="form-group">
							  <?= $model->d_name1; ?> <?= $model->d_name2; ?>
						  </div>
						  <div class="form-group">
							<label for="HaisaiModel_d_tel" class="col-sm-3 control-label">電話番号</label>
							<div class="col-sm-4">
								<?= $model->d_tel; ?>
							</div>
						  </div>
						  <div class="form-group">
							<label for="HaisaiModel_d_zip" class="col-sm-3 col-xs-12 control-label">郵便番号</label>
							<div class="col-sm-4 col-xs-8">
								<?= $model->d_zip; ?>
							</div>
							<div class="col-sm-4 col-xs-4">
							  <button type="button" class="get_address btn btn-default">住所検索</button>
							</div>
						  </div>
						  <div class="form-group">
							<label for="HaisaiModel_d_pref" class="col-sm-3 control-label">ご住所</label>
							<div class="col-sm-4">
								<?= $model->d_pref; ?>
							</div>
							<div class="col-sm-6 col-sm-offset-3">
								<?= $model->d_address1; ?><br />
								<?= $model->d_address2; ?><br />
								<?= $model->d_address3; ?>
							</div>
						  </div>
						</div>
						</div>
					  </div>
					</div>
				  </div>
				<!-- /配送先があれば -->
				
				
				<!-- 配送方法  -->
				<div class="cart-section">
					<h2 class="heading"><span>配送について</span></h2>
					<div class="container margin-t margin-b">
						<div class="row">
							<div class="col-sm-6 col-sm-offset-3">
								<div class="form-group">
									<?= $delivery_list_values[$model->delivery_method] ?>
								</div>
							</div>
							<div class="col-sm-6 col-sm-offset-3">
								<div class="form-group">
									<?= $delivery_date_list[1][$model->deliver_date] ?>
								</div>
							</div>
							<div class="col-sm-6 col-sm-offset-3">
								<div class="form-group">
									
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /配送方法  -->
				
				
				<hr />
				
				
				<!-- 配送方法  -->
				<div class="cart-section">
					<h2 class="heading"><span>お支払い方法</span></h2>
					<div class="container margin-t margin-b">
						<div class="row">
							<div class="col-sm-6 col-sm-offset-3">
								<div class="form-group">
									<?= $payment_method_values[$model->payment_method]; ?>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /配送方法  -->
				
				
				<hr />
				
				<?= CHtml::form(Yii::app()->createUrl('cart/submit')); ?>
					<?= CHtml::submitButton( '送信', [ 'class' => 'btn btn-info' ] ); ?>
				<?= CHtml::endForm(); ?>
				
				
			</div>
                
                
                
            
        </div><!-- body ends -->
        
        
        
        
        <!-- Plugins JS -->
		<script src="<?php echo $templatePath; ?>inc/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/owl-carousel/js/owl.carousel.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/stellar/js/jquery.stellar.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/animations/js/wow.min.js"></script>
                <script src="<?php echo $templatePath; ?>inc/waypoints.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/isotope.pkgd.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/classie.js"></script>
		<script src="<?php echo $templatePath; ?>inc/jquery.easing.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/jquery.counterup.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/smoothscroll.js"></script>

		<!-- Theme JS -->
		<script src="<?php echo $templatePath; ?>js/theme.js"></script>

    </body> 
        
            
</html>
