<!doctype html>
<html lang="en">
	<head>

		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>テスト</title>
		<meta name="description" content="Unika - Responsive One Page HTML5 Template">
		<meta name="keywords" content="HTML5, Bootsrtrap, One Page, Responsive, Template, Portfolio" />
		<meta name="author" content="imransdesign.com">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, initial-scale=1">
                
                <!-- Google Fonts  -->
		<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,500' rel='stylesheet' type='text/css'> <!-- Body font -->
		<link href='http://fonts.googleapis.com/css?family=Lato:300,400' rel='stylesheet' type='text/css'> <!-- Navbar font -->

		<!-- Libs and Plugins CSS -->
                <link rel="stylesheet" href="<?php echo $templatePath; ?>inc/bootstrap/css/bootstrap.min.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/animations/css/animate.min.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/font-awesome/css/font-awesome.min.css"> <!-- Font Icons -->
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/owl-carousel/css/owl.carousel.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>inc/owl-carousel/css/owl.theme.css">

		<!-- Theme CSS -->
                <link rel="stylesheet" href="<?php echo $templatePath; ?>css/reset.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>css/style.css">
		<link rel="stylesheet" href="<?php echo $templatePath; ?>css/mobile.css">

		<!-- Skin CSS -->
		<link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/cool-gray.css">
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/ice-blue.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/summer-orange.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/fresh-lime.css"> -->
        <!-- <link rel="stylesheet" href="<?php echo $templatePath; ?>css/skin/night-purple.css"> -->

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->

		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->

	</head>

    <body data-spy="scroll" data-target="#main-navbar">
        <div class="page-loader"></div>  <!-- Display loading image while page loads -->
    	<div class="body">
        
            <!--========== BEGIN HEADER ==========-->
            <header id="header" class="header-main">

                <!-- Begin Navbar -->
                <nav id="main-navbar" class="navbar navbar-default navbar-fixed-top" role="navigation"> <!-- Classes: navbar-default, navbar-inverse, navbar-fixed-top, navbar-fixed-bottom, navbar-transparent. Note: If you use non-transparent navbar, set "height: 98px;" to #header -->

                  <div class="container">

                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                      </button>
                      <a class="navbar-brand page-scroll" href="index.html">Unika</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li><a class="page-scroll" href="body">Home</a></li>
                            <li><a class="page-scroll" href="#about-section">About</a></li>
                            <li><a class="page-scroll" href="#services-section">Services</a></li>
                            <li><a class="page-scroll" href="#portfolio-section">Works</a></li>
                            <li><a class="page-scroll" href="#team-section">Team</a></li>                            
                            <li><a class="page-scroll" href="#prices-section">Prices</a></li>
                            <li><a class="page-scroll" href="#contact-section">Contact</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                  </div><!-- /.container -->
                </nav>
                <!-- End Navbar -->

            </header>
            <!-- ========= END HEADER =========-->
            
            
            
            
        	
			<section id="social-section">
            
                 <!-- Begin page header-->
                <div class="page-header-wrapper">
                    <div class="container">
                        <div class="page-header text-center wow fadeInDown" data-wow-delay="0.4s">
                            <h1>商品処理</h1>
                            <div class="devider"></div>
                            <h2 class="subtitle">承認番号を入れて下さい</h2>
                        </div>
                    </div>
                </div>
                <!-- End page header-->
                
                <div class="container">
					
					<!-- 承認フォーム -->
					<?php if( $authorized === FALSE ): ?>
						<?php $form=$this->beginWidget('CActiveForm', array(
							'id'=>'member-model-form',
							'enableAjaxValidation'=>false,
						)); ?>

						<?php if( $authorizeMiss ): ?>
							承認に失敗しました
						<?php endif; ?>


						<div class="form-group">
							<?php echo $form->labelEx($model,'activate_hash'); ?>
							<?php echo $form->textField($model,'activate_hash',array('class' => 'form-control')); ?>
							<?php echo $form->error($model,'activate_hash'); ?>
						</div>

						<?php
							echo CHtml::submitButton(
								'承認',
								array(
									'class' => 'btn'
								)
							);
						?>
						<?php $this->endWidget(); ?>
					<?php endif; ?>
					<!-- 承認フォーム -->
					
					
					<!-- 承認OK -->
					<?php if( $authorized ): ?>
						承認完了 ログインしてください
					<?php endif; ?>
					<!-- /承認OK -->
					
					
                </div>
                
            </section>
            <!-- End social section -->
			
			
                
                

                
            <!-- Begin footer -->
            <footer class="text-off-white">
            
                <div class="footer-top">
                	<div class="container">
                    	<div class="row wow bounceInLeft" data-wow-delay="0.4s">

                            <div class="col-sm-6 col-md-4">
                            	<h4>Useful Links</h4>
                                <ul class="imp-links">
                                	<li><a href="">About</a></li>
                                	<li><a href="">Services</a></li>
                                	<li><a href="">Press</a></li>
                                	<li><a href="">Copyright</a></li>
                                	<li><a href="">Advertise</a></li>
                                	<li><a href="">Legal</a></li>
                                </ul>
                            </div>
                        
                        	<div class="col-sm-6 col-md-4">
                                <h4>Subscribe</h4>
                            	<div id="footer_signup">
                                    <div id="email">
                                        <form id="subscribe" method="POST">
                                            <input type="text" placeholder="Enter email address" name="email" id="address" data-validate="validate(required, email)"/>
                                            <button type="submit">Submit</button>
                                            <span id="result" class="section-description"></span>
                                        </form> 
                                    </div>
                                </div> 
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p> 
                            </div>

                            <div class="col-sm-12 col-md-4">
                                <h4>Recent Tweets</h4>
                                <div class="single-tweet">
                                    <div class="tweet-content"><span>@Unika</span> Excepteur sint occaecat cupidatat non proident</div>
                                    <div class="tweet-date">1 Hour ago</div>
                                </div>
                                <div class="single-tweet">
                                    <div class="tweet-content"><span>@Unika</span> Excepteur sint occaecat cupidatat non proident uku shumaru</div>
                                    <div class="tweet-date">1 Hour ago</div>
                                </div>
                            </div>
                            
                        </div> <!-- /.row -->
                    </div> <!-- /.container -->
                </div>
                
                <div class="footer">
                    <div class="container text-center wow fadeIn" data-wow-delay="0.4s">
                        <p class="copyright">Copyright &copy; 2015 - Designed By <a href="https://www.behance.net/poljakova" class="theme-author">Veronika Poljakova</a> &amp; Developed by <a href="http://www.imransdesign.com/" class="theme-author">Imransdesign</a></p>
                    </div>
                </div>

            </footer>
            <!-- End footer -->

            <a href="#" class="scrolltotop"><i class="fa fa-arrow-up"></i></a> <!-- Scroll to top button -->
                                              
        </div><!-- body ends -->
        
        
        
        
        <!-- Plugins JS -->
		<script src="<?php echo $templatePath; ?>inc/jquery/jquery-1.11.1.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/bootstrap/js/bootstrap.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/owl-carousel/js/owl.carousel.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/stellar/js/jquery.stellar.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/animations/js/wow.min.js"></script>
                <script src="<?php echo $templatePath; ?>inc/waypoints.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/isotope.pkgd.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/classie.js"></script>
		<script src="<?php echo $templatePath; ?>inc/jquery.easing.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/jquery.counterup.min.js"></script>
		<script src="<?php echo $templatePath; ?>inc/smoothscroll.js"></script>

		<!-- Theme JS -->
		<script src="<?php echo $templatePath; ?>js/theme.js"></script>

    </body> 
        
            
</html>
