<?php
class mail_attribute extends NahaRegiMailModel
{
	// ----------------------------------------------------
	//項目の名前を定義
	protected $mailAttributeNames = array(
		'name' => 'お名前',
		'tel' => 'お電話番号',
		'mailaddress' => 'メールアドレス',
		'message' => 'お問い合わせ内容',
		'hobby' => '趣味',
		'radio' => 'ラジオボタン',
		'check_test' => 'チェックのテスト',
		//'image' => '画像',
	);
	
	// ----------------------------------------------------
	//各項目のルールを定義
	
	protected $mailRules = array(
		
		//名前を必須に
		array( 'name', 'required' ),
		
		//電話番号を電話番号形式に
		array( 'tel', 'tel' ),
		
		//メールアドレス
		array( 'mailaddress', 'required' ),
		array( 'mailaddress', 'email' ),
		array( 'mailaddress', 'autoreply' ),
		
		
		//セレクトボックス
		array( 'hobby', 'required' ),
		
		
		//ラジオボタン
		array( 'radio', 'safe' ),//←特にチェックのいらないものは「safe」を一個指定します。
		
		
		//チェックボックス
		array( 'check_test', 'required' ),
		
		
		//画像
		//array('image', 'file', 'types'=>'jpg,jpeg,gif,png'),
		//array('image', 'required'),

		
		//内容
		array( 'message', 'required' ),
		
		//必須でない項目はsafeを指定してください。
	);
	
	
	// ----------------------------------------------------
	
	/**
	 * CSRF対策
	 */
	protected $csrf = array(
		//CSRF対策のスイッチ
		'switch' => false,
		//セッション名
		'session' => 'naharegi_csrf',
	);
	
	// ----------------------------------------------------
	//メール送信設定
	protected $config = array(
		
		//運営者宛メール
		'sitemanager' => array(
			//送信先
			'To' => 'kiyotchi46@gmail.com',
			//送信先の名前
			'ToName' => 'La-Dea',
			//自動返信メールの設定がなかった場合のFrom
			'From' => 'ito@shunpodo.net',
			//タイトル
			'Subject' => 'お問い合わせがありました'
		),
		
		//自動返信
		'reply' => array(
			//送信元メアド
			'From' => 'kiyonori@keeprun.jp',
			//送信元名前
			'FromName' => 'La-Dea',
			//タイトル
			'Subject' => 'お問い合わせありがとうございます'
			
		)
		
	);
	
	
	// ----------------------------------------------------
	//以下編集をしないでください
	public function init()
	{
		parent::init();
		$this->dir = dirname(__FILE__).'/';
	}
	
}