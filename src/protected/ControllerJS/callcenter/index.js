/* =============================================================================
 * コールセンター業務
 * ========================================================================== */
$(document).ready(function(){
    
    //環境差を考慮したシステム要件チェック
    navigator.getUserMedia =
            navigator.getUserMedia ||
            navigator.webkitGetUserMedia ||
            navigator.mozGetUserMedia;

    var localStream;
    
    
    //peer接続
    var peer = new Peer( peerValue['id'], {key: peerValue['key']}); 
    var call;

    // Deferred用
    var df;

    // コール待ち一覧用
    var indexList;

    // タイマー
    var timer,
        duration = 1000;

    // デバッグモード
    var debug = false;
    
    // ----------------------------------------------------
    /**
     * データ送信
     */
    // peer.on('connection', function(conn) {
    //     conn.on('data', function(data){
    //         $('.panel-body').append(data);
    //         $('.panel-body').append($('<br>'));
    //     });
    // });
    
    // ----------------------------------------------------
    /*
     * peer接続時処理
     */
    peer.on('open', function(id){
        updateNotActiveId();
        updateIndex();
        console.log(peer.disconnected);
    });
    
    // ----------------------------------------------------
    /*
     * コールあった場合
     */
    // peer.on('call', function(call) {
    //     navigator.getUserMedia({video: true, audio: true}, function(stream) {
    //         call.answer(stream); // Answer the call with an A/V stream.
    //         call.on('stream', function(remoteStream) {
    //                //ここにかく
    //         });
    //     },
    //     function(err) {
    //             alert('接続エラー');
    //     });
    // });

    // ----------------------------------------------------
    /*
     * 定期実行
     */
    if(!debug) {
        timer = setInterval(function(){
            updateIndex();
        }, duration);
    }
    
    // ----------------------------------------------------

    /*
     * コールする（イベント）
     */
    $(document).on('click', '.js-call', function(e){
        e.preventDefault();
        var peer_id = $(this).data('peerid');

        // コールを受けたデータは処理済みということなので、statusを0にする
        $.ajax({
            url: baseUrl +  '/peerConnects/statusUpdateAsync/',
            type: 'post',
            data: {connect: {peer_id: peer_id}},
            dataType: 'json'
        }).done(function(data){
            // 成功時
            if(data.result == 'success') {
                callTo(peer_id);
            } else {
            // 失敗時
                alert('接続に失敗しました。');
            }
            
        });
    });
    
    // ----------------------------------------------------
    /*
     * コールする（実行）
     */
    function callTo(peerId) {
        conn = peer.connect(peerId);
        navigator.getUserMedia({audio: true, video: true}, function(stream){
            localStream  = stream;

            // 自身の映像を表示する
            $('#my-video').prop('src', URL.createObjectURL(stream)).addClass('is-active');
            $('.streams').addClass('is-active');

            call = peer.call(peerId, stream);

            call.on('stream', function(othersStream){
                // 相手のストリームを表示
                $('#others-video').prop('src', URL.createObjectURL(othersStream)).addClass('is-active');
                $('.js-callclose').addClass('is-active');
            });

            call.on('close', function(e){
                $('.streams').removeClass('is-active');
            });

            call.on('error', function(err){
                console.log(err);
            });            
        },
        function() { alert("Error!"); });
        
    }
    // ----------------------------------------------------
    /*
     * コールの接続を切る
     */
    $('.js-callclose').on('click', function(e){
        e.preventDefault();
        localStream.getVideoTracks()[0].stop();
        peer.destroy();
    });

    // ----------------------------------------------------
    /*
     * コール待ち一覧を更新
     */
    function updateIndex()
    {
        df = new $.Deferred();
        var tmp = '<tr>' +
                    '<td><a href="#" data-peerid="{#peer_id}" class="js-call">{#peer_id}</a></td>' +
                    '<td><a href="{#url}" target="_blank">{#shop_id}</td>' +
                    '<td>{#connect_date}</td>' +
                  '</tr>';

        // dbからアクティブなconnectリストを取得
        getPeerList();

        // リスト取得後処理
        df.done(function(){
            var html = '';
            for(var i=0; i < indexList.length; i++) {
                var connect = indexList[i];
                html += tmp.replace(/{#(\w+)}/g, function(m, key){
                    return connect[key];
                });
            }
            $('.js-connects').html(html);
        });
    }

    // ----------------------------------------------------
    /*
     * アクティブでないpeerのステータスを変更
     */
    function updateNotActiveId()
    {
        peer.listAllPeers(function( list ){
            var res = [];
            var i = 0, len = list.length;
            
            while( i < len )
            {
                if( !list[i].match(/^center\-/) )
                {
                    res.push( list[i] );
                }
                i ++ ;
            }

            if(!res.length) {
                res = null;
            }
            console.log(res);
            $.ajax({
                url: baseUrl + '/peerConnects/statusUpdatesAsync/',
                type: 'post',
                data: {connects: res},
                dataType: 'json'
            }).done(function(data){
                console.log(data);
                // if(data.update.length > 0) {
                //     location.reload();
                // }
            });

        });
        
    }
    

    // ----------------------------------------------------
    /**
     * connectのリスト非同期で取得
     */
    function getPeerList()
    {
        $.get(baseUrl + '/callcenter/indexAsync/').done(function(data){
            indexList = data;
            df.resolve();
            return df.promise();
        });
    }

    // ----------------------------------------------------
    /*
     * デバッグ用
     */
    peer.on('error', function(e){
      console.log(e.message);
    });
    
    
    
});