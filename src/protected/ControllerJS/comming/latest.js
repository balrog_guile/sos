/* =============================================================================
 * 最新来店
 * ========================================================================== */
$(document).ready(function(){
	
	//$(".asset_select_image").uniform({'fileButtonHtml': '画像選択/撮影'});
	//$(".asset_select_movie").uniform({'fileButtonHtml': '動画選択/撮影'});
	
	
	
	//INTER−Mediatorのロード
	INTERMediator.construct(true);
	
	// ----------------------------------------------------
	
	/**
	 * リピーター
	 */
	INTERMediatorOnPage.expandingRecordFinish = function(name,repeaters){
		
		
		//アセット
		if(name == 'comming_assets' ){
			record_finish_assets(repeaters);
		}
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * アセットのリピーター終了時
	 **/
	function record_finish_assets(repeaters){
		
		//ファイルフィールドのラベル変更
		$(repeaters).find(".asset_select_image").nextAll('span.action').empty().append('画像選択/撮影');
		$(repeaters).find(".asset_select_movie").nextAll('span.action').empty().append('動画選択/撮影');
		
		
		//アセットのID
		var assets_id = $(repeaters).find(".assets_id").eq(0).val();
		
		
		
		
		/////ハッシュコードの作成
		var hash = $(repeaters).find('.hashcode').eq(0).val();
		if( hash == '' ){
			$(repeaters).find('.hashcode').eq(0).val();
			hash = CybozuLabs.SHA1.calc(assets_id);
			$(repeaters).find('.hashcode').val(hash);
			IMHelper_update(
				'comming_assets',
				{ 'id':[ '=', assets_id ] },
				{ 'hashcode': hash }
			);
		}
		
		
		
		/////画像変更時
		$(repeaters).find('.asset_select_image').on(
			'change',
			function(e){
				/*
				var createObjectURL 
					= window.URL && window.URL.createObjectURL
						? function (file) { return window.URL.createObjectURL (file); }
						: window.webkitURL && window.webkitURL.createObjectURL
							? function (file) { return window.webkitURL.createObjectURL (file); }
							: undefined;
				
				alert( createObjectURL( $(this).get(0).files[0] ) )
				return;
				*/
				do_upload( 'image', this );
			}
		);
		
		/////動画変更時
		$(repeaters).find('.asset_select_movie').on(
			'change',
			function(e){
				do_upload( 'movie', this );
			}
		);
		
		
		//表示領域制御
		set_asets_area();
		
		
		
		
		// -----------------------------------------------
		
		/**
		 * アップロード関数
		 * @param mode image|movie
		 * @param elem
		 **/
		function do_upload( mode, elem ){
			
			//エレメントをコピー
			var cl = $(elem).clone();
			
			
			
			/*
			if( MobileEsp.DetectIos() ){
				cl = $(elem).get(0).files[0];
				$(cl).attr('name','FileUploadModel[image]');
			}
			*/
			
			//フォームを作成
			var form = $('<form></form>').
						//append(cl).
						append('<input type="hidden" name="FileUploadModel[mode]" value="customer_assets" />' ).
						append('<input type="hidden" name="FileUploadModel[filename]" value="' + hash + '" />' ).
						get(0);
			if(! MobileEsp.DetectIos() ){
				$(cl).attr('name','FileUploadModel[image]');
				$(form).append(cl);
			}
			
			var formData = new FormData(form);
			if( MobileEsp.DetectIos() ){
				formData.append( 'FileUploadModel[image]', $(elem).get(0).files[0] );
			}
			
			//実行
			$.ajax(
				upload_helper_url,
				{
				'type': 'post',
				'contentType': false,
				'processData': false,
				'data': formData,
				'dataType': 'json',
				error: function() {
					alert('アップロードに失敗しました');
				},
				success: function( data ) {
					if( data.result == true ){
						
						//OK表示
						alert('アップロードに成功しました');
						
						
						//データ保存/エレメント書き換え
						var update = {};
						
						if( mode == 'image'){
							update['data_type'] = 1;
							$(repeaters).find('.data_type').val(1);
						}
						else{
							update['data_type'] = 2;
							$(repeaters).find('.data_type').val(2);
						}
						
						update['path'] = data.absolute_url;
						$(repeaters).find('.path').val( data.absolute_url );
						IMHelper_update(
							'comming_assets',
							{ 'id':[ '=', assets_id ] },
							update
						);
						
						//表示領域の制御
						set_asets_area();
						
						
					}
					else{
						alert('アップロードエラーが発生' + "\r\n" + data.error_message );
					}
				}
			});
			
			
			
			
		}
		
		// -----------------------------------------------
		
		/**
		 * アセットの表示処理
		 **/
		function set_asets_area(){
			
			var data_type = $(repeaters).find('.data_type').eq(0).val();
			var path = $(repeaters).find('.path').eq(0).val();
			
			
			
			
			//画像領域を表示
			if( data_type == 1 ){
				$(repeaters).find('.image_place_area').css( 'display', 'block' );
				$(repeaters).find('.movie_place_area').css( 'display', 'none' );
				$(repeaters).find('.movie_place_area_no_sp').css( 'display', 'none' );
				$(repeaters).find('.image_place_holder').attr( 'src', path );
			}
			
			//動画領域を表示
			if( ( data_type == 2 )&&( MobileEsp.DetectSmartphone() == true )){
				$(repeaters).find('.image_place_area').css( 'display', 'none' );
				$(repeaters).find('.movie_place_area').css( 'display', 'block' );
				$(repeaters).find('.movie_place_area_no_sp').css( 'display', 'none' );
				$(repeaters).find('.movie_place_holder').attr( 'src', path );
				//$(repeaters).find('.movie_place_holder').append(
				//	$('<source>').attr('src',path).attr('type','video/quicktime')
				//);
			}
			else if( data_type == 2 ){
				
				$(repeaters).find('.image_place_area').css( 'display', 'none' );
				$(repeaters).find('.movie_place_area').css( 'display', 'none' );
				$(repeaters).find('.movie_place_area_no_sp').css( 'display', 'block' );
				$(repeaters).find('.movie_place_area_no_sp .movie_href').attr( 'href', path );
			}
			
			
			//何もなければ閉じる
			if( data_type == 0 ){
				$(repeaters).find('.image_place_area').css( 'display', 'none' );
				$(repeaters).find('.movie_place_area').css( 'display', 'none' );
				$(repeaters).find('.movie_place_area_no_sp').css( 'display', 'none' );
			}
			
			
			//データが入っていれば選択を閉じる
			if( data_type > 0 ){
				$(repeaters).find('.file_select_area').css( 'display', 'none' );
			}
			
		}
		
		// -----------------------------------------------
	}
	
	// ----------------------------------------------------
	
	/**
	 * レジスターへジャンプ
	 **/
	$('.go2register').on(
		'click',
		function(e){
			go2register = go2register.replace( '____targe____', $('#main_id').val() );
			location.href = go2register;
		}
	);
	
	// ----------------------------------------------------
	
	/**
	 * 顧客の来店履歴へ
	 */
	$('.history').on(
		'click',
		function(e){
			location.href = history_url;
		}
	);
	
	
	// ----------------------------------------------------
	
	
});