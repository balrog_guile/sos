/* =============================================================================
 * ビューモデルと関数群
 * ========================================================================= */

// ============================================================================
/**
 * オーダーデータクラス
 */
var orderDatas = function(){
    
    // ----------------------------------------------------
    /**
     * プロパティ
     */
    var self = this;
    
    //詳細
    this.detail = ko.observableArray();
    
    // ----------------------------------------------------
    /**
     * モデルオブジェクトのプロパティをアサイン
     */
    this.set = function( name, val )
    {
        self[name] = val;
    }
    
    // ----------------------------------------------------
    
    
};


// ============================================================================
/**
 * 詳細クラス
 */
var orderDetail = function(){
    
    // ----------------------------------------------------
    /**
     * プロパティ
     */
    var self = this;
    
    //親オブジェクト
    this.parent = null;
    
    //ルートオブジェクト
    this.root = null;
    
    
    // ----------------------------------------------------
    /**
     * モデルオブジェクトのプロパティをアサイン
     */
    this.set = function( name, val )
    {
        self[name] = val;
    }
    
    // ----------------------------------------------------
    /**
     * ステータス変更
     */
    this.changeStatus = function( elem, currentData, eve ){
        var status = $(elem).attr('data-status');
        var id = currentData.id;
        var url = urlList.changeStatus;
        
        $.ajax({
            'url': url,
            'data': { id: id, status: status },
            'dataType': 'json',
            'type': 'post',
        })
        .success(function(data){
            if( data == true ){
                self.root.getOrders();
            }
        })
        .error(function(data){
            alert('通信エラー');
        });
        
        
        
    };
    
    
    // ----------------------------------------------------
}



// ============================================================================
/**
 * ビューモデル本体
 * @returns {viewModel}
 */
var viewModel = function(urlList){
    
    // ----------------------------------------------------
    /**
     * プロパティ
     */
    
    //URLリスト
    this.urlList = urlList;
    
    //このオブジェクト
    var self = this;
    
    //オーダーデータ
    this.orderList = ko.observableArray();
    
    //繰り返し取得タイマーオブジェクト
    this.timerObject;
    
    
    // ----------------------------------------------------
    /**
     * 取得
     */
    this.getOrders = function(){
        var url = self.urlList['getOrderNow'];
        $.ajax({
            'url': url,
            'dataType': 'json',
            'data': {},
            'type': 'get'
        })
        .success(function(data){
            //一旦消して追加
            self.orderList.removeAll();
            $.each( data['orderList'], function(index){
                var order = data['orderList'][index];
                
                
                //オーダーリストオブジェクト
                var orderObj = new orderDatas();
                for( var key in order ){
                    if( key == 'detail' ){continue;}
                    orderObj.set( key, order[key] );
                }
                
                
                //詳細オブジェクトを作成
                $.each( order['detail'], function(index2){
                    var detail = order['detail'][index2];
                    var detailObject = new orderDetail();
                    detailObject.root = self;
                    detailObject.parent = orderObj;
                    for( var key in detail ){
                        detailObject.set( key, detail[key] );
                    }
                    orderObj.detail.push( detailObject );
                });
                
                
                self.orderList.push( orderObj );
            });
        })
        .error(function(data){
            alert('通信エラー');
        });
    };
    
    // ----------------------------------------------------
    /**
     * 初期処理
     * @type viewModel
     */
    (function(){
        
        //最初取得
        self.getOrders();
        
        //15秒に一回チェック
        self.timerObject = setInterval(
            function(){
                self.getOrders();
                //console.log(1);
            },
            10000
        );
        
    })();
    
    // ----------------------------------------------------
    
    
};
