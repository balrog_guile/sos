/* =============================================================================
 * admin用処理
 * ========================================================================== */
$(document).ready(function(){
	
	// ----------------------------------------------------
	/**
	 * 並べ替え
	 */
	$('.rankchange').on(
		'click',
		function(e){
			
			var type = $(this).attr('data-changetype');
			var index = $('.rankchange[data-changetype="' + type + '"]').index(this);
			var len = $('.datarow tr').length;
			
			//読み飛ばし
			if( ( type == 'down' ) && ( (index+1) == len ) )
			{
				return;
			}
			if( ( type == 'up' ) && ( index == 0 ) )
			{
				return;
			}
			
			var target = $('.datarow tr').eq(index);
			$(target).fadeOut( 'normal', function(){
				if( type == 'down' ){
					$('.datarow tr').eq(index+1).after(target);
				}
				else{
					$('.datarow tr').eq(index-1).before(target);
				}
				$(target).fadeIn('normal');
			});
		}
	);
	
	// ----------------------------------------------------
	
	
	
});