$(document).ready(function(){

  // ----------------------------------------------------

  /**
   * 画像手動選択
   */

  $('#image_path').on(
    'change',
    function(e){
      var url = homeUrl + '/fileupload/';
      var selecter = $('#thumbnail label');

      // form生成
      var _form = $('<form></form>');
      _form.append('<input type="hidden" name="FileUploadModel[mode]" value="product_images" />' );
      var formData = new FormData(_form.get(0));
      formData.append('FileUploadModel[image]', $(this).get(0).files[0]);

      $.ajax(
        url,
        {
          'type': 'post',
          'contentType': false,
          'processData': false,
          'data': formData,
          'async': false,
          'dataType': 'json',
          'success': function(data){
            if(data.result == false ){
              alert(data.error_message);
              return false;
            }
            var imgResPath = data.absolute_res_url;
            $('#thumbnail').find('img').attr('src',baseUrl + imgResPath);
            $('#thumbnail').find('input[name="PageBlocksModel[image_path]"]').attr('value',baseUrl + imgResPath);
          },
          'error': function(e){
            $('#loading_modal').modal('hide');
            alert('アップロードに失敗しました');
          }
        }
      );
    }
  );

});
