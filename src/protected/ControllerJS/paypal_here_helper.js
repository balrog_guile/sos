function　paypal_here_helper(){
	
	//商品データ保存用
	this._items = [];
	
	//設定
	this._params = {};
	
	//コンストラクタ
	this.initialize.apply(this, arguments);
	
}

paypal_here_helper.prototype = {
	
	
	//コンストラクタ
	initialize: function( params ){
		this._params['callback'] = params['callback'];
		this._params['merchantEmail'] = params['merchantEmail'];
	},
	
	//商品追加
	add_item: function( itemname, price, quantity ){
		
		var item = {};
		item['name'] = itemname;
		item['description'] = itemname;
		item['quantity'] = quantity;
		item['unitPrice'] = price;
		item['taxRate'] = '0.0';
		item['taxName'] = 'Tax';
		
		
		this._items.push( item );
		
	},
	
	
	//支払い実行
	goto_here: function(){
		
		//設定
		var invoice = {};
		invoice['itemList'] = {};
		invoice['itemList']['item'] = this._items;
		invoice['paymentTerms'] = 'DueOnReceipt';
		invoice['currencyCode'] = 'JPY';
		invoice['discountPercent'] = '0';
		invoice['merchantEmail'] = this._params['merchantEmail'];
		
		
		//URL生成
		var retUrl = encodeURIComponent(　this._params['callback'] + "?{result}?Type={Type}&InvoiceId={InvoiceId}&Tip={Tip}&Email={Email}&TxId={TxId}");
		var pphereUrl = "paypalhere://takePayment?returnUrl=" + retUrl;
		pphereUrl = pphereUrl + "&accepted=cash,card,paypal"
		pphereUrl = pphereUrl + "&step=choosePayment";
		pphereUrl = pphereUrl + '&invoice=' + encodeURIComponent(JSON.stringify(invoice));
		
		
		
		//ジャンプ
		location.href = pphereUrl;
	}
	
	
	
};