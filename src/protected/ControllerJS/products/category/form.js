$(document).ready(function(){
    
    // ----------------------------------------------------
    //親選択
    $('.parent_btn').on(
        'click',
        function(e){
            
            $.ajax({
                'url': get_parent_categories,
                'data': {
                    'current': $('#CategoryModel_parent_id').val()
                },
                'type': 'get',
                'success': function( data ){
                    $('#jquimodal #jquimodal_content').empty().append( data );
                    
                    $('#jquimodal #jquimodal_ok').unbind();
                    $('#jquimodal #jquimodal_ok').bind(
                        'click',
                        function(e){
                            var i = 0;
                            while( $('#jquimodal_content .parent').length > i ){
                                if( $('#jquimodal_content .parent').get(i).checked == true){
                                    $('#CategoryModel_parent_id').val( $('#jquimodal_content .parent').eq(i).val() );
                                    break;
                                }
                                i ++ ;
                            }
                            $('#jquimodal').dialog( 'close' );
                        }
                    );
                    
                    
                    $('#jquimodal').dialog({
                        width: '90%',
                        height: 'auto',
                        modal: true
                    });
                    
                    var i = 0;
                    while( $('#jquimodal_content .parent').length > i ){
                        if( $('#jquimodal_content .parent').eq(i).val() == $('#CategoryModel_parent_id').val() ){
                            $('#jquimodal_content .parent').get(i).checked = true;
                            break;
                        }
                        i ++ ;
                    }
                    
                }
            });
        }
    );
    
    
    // ----------------------------------------------------
    /**
     * アップロード
     */
    $('.image-upload').on('drop', function (e) {
        var files = e.originalEvent.dataTransfer.files;
        var url = uploadUrl;
        var target = $(this).attr('data-target');
        
        
        
        // form生成
        var _form = $('<form></form>');
        if( target == 'main_image' ){
            _form.append('<input type="hidden" name="FileUploadModel[mode]" value="category_main" />' );
        }
        else{
            _form.append('<input type="hidden" name="FileUploadModel[mode]" value="category_thumb" />' );
        }
        
        var formData = new FormData(_form.get(0));
        formData.append('FileUploadModel[image]', files[0]);
        
        var self = this;
        
        
        $.ajax(
            url,
            {
                'type': 'post',
                'contentType': false,
                'processData': false,
                'data': formData,
                'async': false,
                'dataType': 'json',
                'success': function(data){
                    
                    if(data.result == false ){
                        alert(data.error_message);
                    }
                    
                    var imgPath = data.absolute_url;
                    var imgResPath = data.absolute_res_url;
                    $('img[data-target='+target+']').attr( 'src', homeUrl + imgResPath );
                    $('input[data-target='+target+']').val(imgResPath);
                    $(self).css('background-color','#FFF');
                },
                'error': function(e){
                    $(self).css('background-color','#FFF');
                    alert('アップロードに失敗しました');
                }
            }
        );
        
        
        
        
        return false;
    })
    .on('dragenter', function () {
        $(this).css('background-color','#FFA0A2');
        // false を返してデフォルトの処理を実行しないようにする
        return false;
    })
    .on('dragover', function () {
        // false を返してデフォルトの処理を実行しないようにする
        return false;
    })
    .on('dragleave', function () {
        $(this).css('background-color','#FFF');
        // false を返してデフォルトの処理を実行しないようにする
        return false;
    })
    ;
    
    // ----------------------------------------------------
    
});