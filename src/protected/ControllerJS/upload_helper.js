/* =============================================================================
 * アップロードヘルパ
 * ========================================================================== */
$(document).ready(function(){
	$('.file_upload_on').on(
		'change',
		function(e){
			
			var cl = $(this).clone();
			$(cl).attr('name','FileUploadModel[image]');
			
			//アップロード後の表示個所
			var uplodedfilespace = $(this).attr('data-uplodedfilespace');
			
			//フォームでのパス保存場所
			var uplodedfileinput = $(this).attr('data-uplodedfileinput');
			
			//モードの指定
			var mode = $(this).attr( 'data-uploadmode');
			if( mode == undefined ){ mode = ''; }
			
			//フォームを作成
			var form = $('<form></form>').
						append(cl).
						append('<input type="hidden" name="FileUploadModel[mode]" value="'+mode+'" />' ).
						get(0);
			var formData = new FormData(form);
			
			//実行
			$.ajax(
				upload_helper_url,
				{
				'type': 'post',
				'contentType': false,
				'processData': false,
				'data': formData,
				'dataType': 'json',
				error: function() {
					alert('アップロードに失敗しました');
				},
				success: function( data ) {
					if( data.result == true ){
						alert('アップロードに成功しました');
						
						//表示領域があれば表示する
						if( ( uplodedfilespace != undefined ) && ( uplodedfilespace != '' )){
							$('#'+uplodedfilespace).attr( 'src', data.full_url );
						}
						
						//パスを保存
						$('#'+uplodedfileinput).val( data.absolute_url );
						
					}
					else{
						alert('アップロードエラーが発生' + "\r\n" + data.error_message );
					}
				}
			});
		}
	);
});