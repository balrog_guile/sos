<?php
/* =============================================================================
 * WordPress連携用ポスト用
 * ========================================================================== */
class WPPostsModel  extends CActiveRecord
{
	// ----------------------------------------------------
	/**
	 * DB定義
	 */
	public function getDbConnection()
	{
		$file = __DIR__ . '/db2Connect.php';
		if(file_exists($file))
		{
			$return = require $file;
		}
		else{
			$return = Yii::app()->db;
		}
		return $return;
	}
	
	// ----------------------------------------------------
	/**
	 * テーブル定義
	 */
	public function tableName()
	{
		return '{{posts}}';
	}
	// ----------------------------------------------------
	
	/**
	 * 自分自身を取得
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	
	/**
	 * リレーション
	 */
	public function relations()
	{
		return array(
			
			//メタモデル
			'postMeta' => array(
				self::HAS_MANY,
				'WPPostMetaModel',
				array( 'post_id' => 'ID' )
			),
			
			//カテゴリリレーション
			'termRelation' => array(
				self::HAS_MANY,
				'WPTermRelationshipsModel',
				array( 'object_id' => 'ID' ),
			),
			
			//タクソノミ
			'termTaxonomy' => array(
				self::HAS_MANY,
				'WPTermTaxonomyModel',
				array( 'term_taxonomy_id' => 'term_taxonomy_id'),
				'through' => 'termRelation'
			),
			
			//カテゴリ
			'category' => array(
				self::HAS_MANY,
				'WPTermsModel',
				array( 'term_id' => 'term_id' ),
				'through' => 'termTaxonomy'
			),
		);
	}
	// ----------------------------------------------------
}