<?php
/* =============================================================================
 * WordPress連携用Term用
 * ========================================================================== */
class WPTermRelationshipsModel  extends CActiveRecord
{
	// ----------------------------------------------------
	/**
	 * DB定義
	 */
	public function getDbConnection()
	{
		$file = __DIR__ . '/db2Connect.php';
		if(file_exists($file))
		{
			$return = require $file;
		}
		else{
			$return = Yii::app()->db;
		}
		return $return;
	}
	
	// ----------------------------------------------------
	/**
	 * テーブル定義
	 */
	public function tableName()
	{
		return '{{term_relationships}}';
	}
	// ----------------------------------------------------
	
	/**
	 * 自分自身を取得
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	
	/**
	 * リレーション
	 */
	public function relations()
	{
		return array();
	}
	// ----------------------------------------------------
}