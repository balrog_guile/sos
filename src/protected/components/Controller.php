<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
    /**
     * @var string the default layout for the controller view. Defaults to '//layouts/column1',
     * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
     */
    public $layout='//layouts/column1';
    /**
     * @var array context menu items. This property will be assigned to {@link CMenu::items}.
     */
    public $menu=array();
    /**
     * @var array the breadcrumbs of the current page. The value of this property will
     * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
     * for more details on how to specify this property.
     */
    public $breadcrumbs=array();

    /**
     * サブビューに渡すためのデータ保持
     */
    private $viewData = array();
    
    /**
     * デフォルトテーマセット
     */
    public $DEFAULT_THEME_SET = 'default/';
    
    
    //ショップデータモデルを持っておく
    public $shop = null;
    
    //表示言語
    public $lang = 'ja';
    
    //アセットヘルパー
    protected $assets_helper;
    
    
    // ----------------------------------------------------
    /**
     * 初期化
     */
    public function init()
    {
        parent::init();
        
        
        //ショップ情報
        if( (int)Yii::app()->user->id > 0 )
        {
            $user = UsersModel::model()->findByPk( Yii::app()->user->id );
            if( ( !is_null($user) )&&( (int)$user->shop_id > 0 ) )
            {
                $this->shop = ShopModel::model()->findByPk($user->shop_id);
            }
        }
        
        
        //最初表示時テーマ選択があればセット
        if( Input::GetPost('SOS_THEME_SELECT') != '' )
        {
            Yii::app()->session['SOS_THEME_SELECT'] = Input::GetPost('SOS_THEME_SELECT');
            $this->DEFAULT_THEME_SET = Input::GetPost('SOS_THEME_SELECT') . '/';
            $this->shop = ShopModel::model()->find(
                'shop_code = :shop_code',
                array( ':shop_code' => Input::GetPost('SOS_THEME_SELECT') )
            );
        }
        elseif(
            isset (Yii::app()->session['SOS_THEME_SELECT']) &&
            Yii::app()->session['SOS_THEME_SELECT'] != ''
        )
        {
            $this->DEFAULT_THEME_SET = Yii::app()->session['SOS_THEME_SELECT'] . '/';
            $this->shop = ShopModel::model()->find(
                'shop_code = :shop_code',
                array( ':shop_code' => Yii::app()->session['SOS_THEME_SELECT'] )
            );
        }
        
        //使用言語のチェック
        if( (int)Yii::app()->session['member_id'] > 0 )
        {
            $langList = explode(',', $_SERVER['HTTP_ACCEPT_LANGUAGE']);
            if( $langList[0] != 'ja' )
            {
                $langSet = explode( '-', $langList[0] );
                $this->lang = $langSet[0];
            }
            
        }
    }
    
    
    // ----------------------------------------------------
    
    /**
     * 既存のrenderFile()をオーバーライド。
     * $dataを保持し、サブビューに$dataを渡す。
     *
     * @param string $viewFile
     *   1. ファイルがある場合は該当のファイルを利用
     *   2. *.html指定の場合で実ファイルが存在しない場合は、*.phpを利用
     *   3. 拡張子の指定が無い場合は自動的に*.php、*.htmlの順にチェックし存在するファイルを利用
     */
    public function renderFile($viewFile, $data = NULL, $return = FALSE) {

        if (!file_exists($viewFile)) {
            $path_parts = pathinfo($viewFile);
            if (!isset($path_parts['extension']) || strcasecmp($path_parts['extension'], 'html') == 0) {
                $viewFile = $path_parts['dirname'] . '/' . $path_parts['filename'] . '.php';
                if (!file_exists($viewFile) && !isset($path_parts['extension'])) {
                    $viewFile = $path_parts['dirname'] . '/' . $path_parts['filename'] . '.html';
                }
            }
        }

        $this->viewData = $data;
        return parent::renderFile($viewFile, $data, $return);
    }
    
    // ----------------------------------------------------
    /**
     * サブビューを読み込んで表示する
     */
    public function renderSubViewFull($path, $data = NULL) {
        if ($data == NULL) {
            $data = $this->viewData;
        } else {
            $data = array_merge($this->viewData, $data);
        }
        $this->renderFile( $path, $data);
    }
    
    
    
    // ----------------------------------------------------

}