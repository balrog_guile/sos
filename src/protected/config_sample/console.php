<?php

// This is the configuration for yiic console application.
// Any writable CConsoleApplication properties can be configured here.
return array(
	
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'商品管理システム',
	
	// preloading 'log' component
	'preload'=>array('log'),
	
	//modules
	'modules'=>array(
		
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'7410asdf',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		
		//yii-userモジュール設定
		'user'=>array(
			//encrypting method (php hash function)
			'hash' => 'md5',
			//send activation email
			'sendActivationMail' => false,
			//allow access for non-activated users
			'loginNotActiv' => true,
			//activate user on registration (only sendActivationMail = false)
			'activeAfterRegister' => false,
			//automatically login from registration
			'autoLogin' => false,
			//registration path
			'registrationUrl' => array('/user/registration'),
			//recovery password path
			'recoveryUrl' => array('/user/recovery'),
			//login form path
			'loginUrl' => array('/user/login'),
			//page after login
			'returnUrl' => array('/user/profile'),
			//page after logout
			'returnLogoutUrl' => array('/user/login'),
		),
		
		
	),
	
	
	
	// application components
	'components'=>array(
		
		
		//DBを外に
		'db'=> array(
			'connectionString' => 'mysql:host=localhost;dbname=naha_regi;unix_socket=/Applications/MAMP/tmp/mysql/mysql.sock',
			'emulatePrepare' => true,
			'username' => 'naha_regi',
			'password' => '7410asdf',
			'charset' => 'utf8',
			'tablePrefix' => '',
		),
		
		//yii-user
		'user'=>array(
			// enable cookie-based authentication
			'class' => 'WebUser',
			'allowAutoLogin'=>true,
			'loginUrl' => array('/user/login'),
		),
		
		
		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
			),
		),
	),
);