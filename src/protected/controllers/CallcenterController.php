<?php
/* =============================================================================
 * コールセンター
 * ========================================================================== */
class CallcenterController extends Controller
{
    // ----------------------------------------------------
    /**
     * 初期処理
     */
    public function init() {
        parent::init();
        $this->assets_helper = new AssetsHelper( 'ControllerJS' );
    }
    
    // ----------------------------------------------------
    
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';
    
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    // ----------------------------------------------------
    
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(
                    'index', 'indexAsync'
                ),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    // ----------------------------------------------------
    /**
     * コールセンター
     */
    public function actionIndex()
    {
        $data = array();

        $data['connects'] = PeerConnectsModel::model()->findAll(array(
            'condition' => 'status = :status',
            'params' => array(
                ':status' => 1
            ),
            'order' => 'connect_date ASC'
        ));
        
        //セット
        $this->assets_helper->set_js( 'callcenter/index.js', 'end' );
        
        $this->render( 'index', $data );
    }
    
    // ----------------------------------------------------
    /**
     * コールセンター非同期
     */
    public function actionIndexAsync()
    {
        $datas = array();
        $res = array();

        $datas = PeerConnectsModel::model()->findAll(array(
            'condition' => 'status = :status',
            'params' => array(
                ':status' => 1
            ),
            'order' => 'connect_date ASC'
        ));

        foreach($datas as $data)
        {
            $res[] = $data->attributes;
        }

        header('Content-Type: application/json; charset=utf-8');
        echo json_encode($res, JSON_UNESCAPED_UNICODE);
    }

    // ----------------------------------------------------
    
}