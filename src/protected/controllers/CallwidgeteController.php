<?php

class CallwidgeteController extends Controller
{
	public function actionIndex( $page )
	{
		$themeset = 'default';
		$themeDirOutput = Yii::app()->getBaseUrl(true) . '/ec_theme/' . $themeset;
		$themeDirOutput = preg_replace( '/^http\:/', '', $themeDirOutput );
		$themeDirOutput = preg_replace( '/^https\:/', '', $themeDirOutput );
		
		$data = array(
			'template_path' => $themeset
		);
		$page = rawurldecode($page);
		$this->renderFile($page,$data);
	}
}