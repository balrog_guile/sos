<?php
/* =============================================================================
 * カート
 * ========================================================================== */
class CartController extends Controller
{
	
	//カート操作オブジェクト
	protected $haisai;
	
	//内部テーマディレクトリ
	protected $themeDirInside;
	
	//出力用テーマディレクトリ
	protected $themeDirOutput;
	
	//URLを保存
	public $application_url;
	
	
	// ----------------------------------------------------
	/**
	 * 初期化
	 */
	public function init()
	{
		parent::init();
		
		//カート操作オブジェクト
		$this->haisai = new HaisaiCart();
		
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * 確認画面
	 */
	public function actionConfirm()
	{
		//モデル
		$model = new HaisaiModel();
		$this->application_url = Yii::app()->createUrl('cart/confirm');
		
		//データ取得
		$calc = $this->haisai->calc();
		
		//在庫エラーがあればすっ飛ばす！
		if( $calc['calc']['stock_error'] === true )
		{
		}
		
		
		
		//数量チェック
		if( $calc['calc']['item_count'] == 0 )
		{
			$this->redirect( Yii::app()->createUrl('cart/index') );
			return;
		}
		
		//支払い
		$getPaymentList = $this->haisai->getPaymentList();
		$payment_method = $getPaymentList['rows'];
		$payment_method_values = $getPaymentList['values'];
		
		
		//配送方法別設定
		$getDelivery = $this->haisai->getDeliveryList();
		$delivery_list = $getDelivery['rows'];
		$delivery_list_values = $getDelivery['delivery_list_values'];
		$delivery_date_list = $getDelivery['delivery_date_list'];
		$delivery_date_time = $getDelivery['delivery_date_time'];
		
		
		
		//データ取得
		$this->getDatas($model);
		
		//データ整形
		$datas = array(
			'items' => $calc['items'],
			'calc' => $calc['calc'],
			'model' => $model,
			'template_path' => $this->themeDirOutput,
			'delivery_list' => $delivery_list,
			//'deliverTimeList' => $deliverTimeList,
			'delivery_list_values' => $delivery_list_values,
			'delivery_date_list' => $delivery_date_list,
			'delivery_date_time' => $delivery_date_time,
			'payment_method' => $payment_method,
			'payment_method_values' => $payment_method_values,
		);
		
		//共通ビュー処理
		Yii::app()->Cms->setCommonDataForViewData( $datas );
		
		
		$this->renderFile( Yii::app()->Cms->getThemeDirectory().'cart/confirm.php', $datas );
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * デフォルトメソッド(表示メソッド)
	 */
	public function actionIndex()
	{
		$session = Yii::app()->session;
		
		
		// データが有ればログイン状態
		$customers_id = Yii::app()->customer->getId();
		$user = CustomersModel::model()->findByPk($customers_id);
		
		
		//モデル
		$model = new HaisaiModel();
		
		
		//データ取得
		$calc = $this->haisai->calc();
		
		$this->application_url = Yii::app()->createUrl('cart');
		
		
		
		
		//支払い
		$getPaymentList = $this->haisai->getPaymentList();
		$payment_method = $getPaymentList['rows'];
		$payment_method_values = $getPaymentList['values'];
		
		
		//配送方法別設定
		$getDelivery = $this->haisai->getDeliveryList();
		$delivery_list = $getDelivery['rows'];
		$delivery_list_values = $getDelivery['delivery_list_values'];
		$delivery_date_list = $getDelivery['delivery_date_list'];
		$delivery_date_time = $getDelivery['delivery_date_time'];
		
		
		//エラーメッセージの取得
		$error_number = Yii::app()->session['cart_error_number'];
		unset( Yii::app()->session['cart_error_number'] );
		
		
		// 確認画面へジャンプ
		if(isset($_POST['HaisaiModel']))
		{
			$model->attributes = $_POST['HaisaiModel'];
			$res = $model->validate();
			
			if( $res === true )
			{
				$this->setDatas( $model );
				$this->redirect( Yii::app()->createUrl('cart/confirm') );
				return;
			}
		}
		
		
		// カート画面を表示
		$this->getDatas($model);
		
		//データ整形
		$data = array(
			'items' => $calc['items'],
			'calc' => $calc['calc'],
			'model' => $model,
			'error_number' => $error_number,
			'delivery_list' => $delivery_list,
			//'deliverTimeList' => $deliverTimeList,
			'delivery_list_values' => $delivery_list_values,
			'delivery_date_list' => $delivery_date_list,
			'delivery_date_time' => $delivery_date_time,
			'payment_method' => $payment_method,
			'user' => $user,
			'payment_method_values' => $payment_method_values,
		);
		
		
		//共通ビュー処理
		Yii::app()->Cms->setCommonDataForViewData( $data );
		
		$this->renderFile( Yii::app()->Cms->getThemeDirectory() .'cart/index.php', $data );
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * 注文履歴
	 */
	public function actionOrderList()
	{
		$model = new OrderHistoryModel();
		// $model->unsetAttributes();
		var_dump($model->orderSearchOne());
		$this->renderFile( $this->themeDirInside.'cart/order_list.html', $model );
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * データ取得
	 */
	public function getDatas( &$model )
	{
		$info = $this->haisai->getInfo();
		$pay = $this->haisai->getPayment();
		
		
		foreach( $info as $key => $val )
		{
			if(property_exists($model, $key ) )
			{
				$model->{$key} = $val;
			}
		}
		
		foreach( $pay as $key => $val )
		{
			if(property_exists($model, $key ) )
			{
				$model->{$key} = $val;
			}
		}
		
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * データをセッションに保存
	 */
	protected function setDatas( &$model )
	{

		//お客様/配送情報
		$info = array(
			'name1' => $model->name1,
			'name2' => $model->name2,
			'kana1' => $model->kana1,
			'kana2' => $model->kana2,
			'tel' => $model->tel,
			'mailaddress' => $model->mailaddress,
			'mailaddress2' => $model->mailaddress2,
			'zip' => $model->zip,
			'pref' => $model->pref,
			'address1' => $model->address1,
			'address2' => $model->address2,
			'address3' => $model->address3,
			
			'yobo' => $model->yobo,
			
			'd_check' => $model->d_check,
			
			'd_name1' => $model->d_name1,
			'd_name2' => $model->d_name2,
			'd_tel' => $model->d_tel,
			'd_zip' => $model->d_zip,
			'd_pref' => $model->d_pref,
			'd_address1' => $model->d_address1,
			'd_address2' => $model->d_address2,
			'd_address3' => $model->d_address3,
			
			
			
		);
		$this->haisai->setInfo( $info );
		
		
		
		//支払い方法
		$pay = array(
			'payment_method' => $model->payment_method,
			'delivery_method' => $model->delivery_method,
			'deliver_time' => $model->deliver_time,
			'deliver_date' => $model->deliver_date,
		);
		$this->haisai->setPayment( $pay );
		
		
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * 数量変更
	 */
	public function actionChangeqty()
	{
		$this->application_url = Yii::app()->createUrl('cart/changeqty');
		
		$delete_list = Input::GetPost('delete');
		if(!is_array($delete_list))
		{
			$delete_list = array();
		}
		$qty_list = array();
		foreach( array_keys($_POST) as $key )
		{
			$qty_list[$key] = Input::Post($key);
		}
		$this->haisai->changeQty( $delete_list, $qty_list );
		$this->redirect('index');
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * カート追加メソッド
	 */
	public function actionAddCart()
	{
		$item_id = Input::GetPost( 'item_id' );
		$item_name = Input::GetPost( 'item_name' );
		$base_price = (int)Input::GetPost( 'base_price' );
		$qty = (int)Input::GetPost( 'qty' );
		$option = Input::GetPost( 'option' );
		
		//空チェック
		if(
			( $item_id == '' )||
			( $item_name == '' )||
			( $base_price < 1 )
		)
		{
			$this->haisai->setError( 'カート項目が正しく設定されていません' );
			return;
		}
		//追加処理
		elseif( $qty > 0 )
		{
			$item = array(
				'item_id' => $item_id,
				'item_name' => $item_name,
				'base_price' => $base_price,
				'option' => ((!is_array($option))?array():$option),
				'qty' => $qty
			);
			
			$this->haisai->addItem( $item );
			
		}
		
		
		$this->redirect( $this->createUrl('index') );
	}
	
	
	
	
	// ----------------------------------------------------
	/**
	 * データベースファイルから呼び出し
	 */
	public function actionAddCartFromDB()
	{
		$skuId = Input::Post( 'skuId' );
		$qty = Input::Post( 'qty' );
		
		$sku = ProductSkuModel::model()->findByPk($skuId);
		
		//データがなければリダイレクト
		if( is_null($sku) )
		{
			$this->redirect( Yii::app()->createAbsoluteUrl('cart/index'));
			return;
		}
		
		//非表示なども処理しない
		if(
			( $sku->delete_flag == 1 )||
			( $sku->products->open_status != 1 )||
			( $sku->products->delete_flag == 1 )||
			( $sku->products->sale_status != 1 )||
			( $sku->products->sale_status != 1 )
		)
		{
			$this->redirect( Yii::app()->createAbsoluteUrl('cart/index'));
			return;
		}
		
		
		//金額判定
		if( $sku->sale_price != '' )
		{
			$set_price = $sku->sale_price;
		}
		else
		{
			$set_price = $sku->price;
		}
		
		
		//セッション用データ
		$item = array(
			'sku_id' => $skuId,
			'item_id' => $sku->products->item_id,
			'item_name' => $sku->products->item_name,
			'brunch_item_id' => $sku->brunch_item_id,
			'brunch_item_name' => $sku->brunch_item_name,
			'calc_price' => $set_price,
			'sale_price' => $sku->sale_price,
			'price' => $sku->price,
			'qty' => $qty,
		);
		$this->haisai->addItem( $item );
		
		
		$this->redirect( Yii::app()->createAbsoluteUrl('cart/index'));
		return;
	}
	
	
	
	// ----------------------------------------------------
	/**
	 *  データベースファイルから呼び出し(単数)
	 */
	public function actionAddCartFromDBOne( $sku )
	{
		//データ取得
		$productSkuModel = new ProductSkuModel();
		$sku = $productSkuModel->find( 'id = :id', array( ':id' => $sku ) );
		
		//何もなければリダイレクト
		if(is_null($sku))
		{
			$this->redirect( 'index' );
			return;
		}
		
		$qty = Input::Post('qty');
		
		
		//数量がなければリダイレクト
		if(
			( $qty == '' )||
			( $qty == 0 )
		)
		{
			$this->redirect( 'index' );
			return;
		}
		
		//オプションの追加
		$option = Input::Post('option');
		if(!is_array($option))
		{
			$option = array();
		}
		
		//追加データの作成
		$set_item = array(
			'item_id' => $sku->products->item_id . (( $sku->brunch_item_id != '' )?'-'.$sku->brunch_item_id:''),//品番
			'item_name' => $sku->products->item_name . (( $sku->brunch_item_name != '' )?' '.$sku->brunch_item_name:''),//品名
			'base_price' => $sku->sale_price,//基本金額
			'option' => $option,
			'qty' => $qty,//数量
			'skuid' => $sku->id,//SKUID
		);
		$call_post = array( $set_item );
		
		
		$this->haisai->addItem( $call_post );
		
		
		$this->redirect( $this->createUrl('index') );
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * 注文実行
	 */
	public function actionSubmit()
	{
		//モデル
		$model = new HaisaiModel();
		
		//データ取得
		$calc = $this->haisai->calc();
		
		//在庫エラーがあればすっ飛ばす！
		if( $calc['calc']['stock_error'] === true )
		{
			Yii::app()->session['cart_error_number'] = 1;
			$this->redirect('index');
			return;
		}
		
		//数量チェック
		if( $calc['calc']['item_count'] == 0 )
		{
			$this->redirect( Yii::app()->createUrl('cart/index') );
			return;
		}
		
		
		/////在庫を確保する
		$ProductSkuModel = new ProductSkuModel();
		
		//在庫確保処理
		//$res = $ProductSkuModel->ec_zaiko( $this->haisai->zaiko );
		
		$connection = Yii::app()->db;
		$transaction = $connection->beginTransaction();
		
		
		//在庫確保処理
		//$res = $ProductSkuModel->regi_zaiko2( $connection, $transaction, $this->haisai->zaiko );
		
		//トランザクションステータスチェック
		/*
		if( $res['truns_error'] === TRUE )
		{
			$transaction->rollback();
			throw new ExceptionClass('在庫確保エラー');
			return;
		}
		//在庫エラーチェック
		else if( $res['result'] === FALSE )
		{
			$transaction->rollback();
			$this->redirect( Yii::app()->createUrl('cart/index') );
			return;
		}
		*/
		
		////カートデータ取得
		$this->getDatas($model);
		
		
		//オーダー記録
		$OrderHistoryModel = new OrderHistoryModel();
		
		try
		{
			$order_post = array(
				//'customer_id',
				'order_date' => date('Y-m-d H:i:s'),
				'update_date' => date('Y-m-d H:i:s'),
				'order_status' => 1,
				'payment_status' => 0,
				'item_total' => $calc['calc']['item_total'],
				'payment_method' => '',
				'payment_fee' => $calc['calc']['commission_fee'],
				'delivery_fee' => $calc['calc']['delivery_fee'],
				'grand_total' => $calc['calc']['grand_total'],
				'name1' => $model->name1,
				'name2' => $model->name2,
				'kana1' => $model->kana1,
				'kana2' => $model->kana2,
				'zip' => $model->zip,
				'pref' => $model->pref,
				'address1' => $model->address1,
				'address2' => $model->address2,
				'address3' => $model->address3,
				'mailaddress' => $model->mailaddress,
				'tel' => $model->tel,
				//'fax' => $model->fax,
				'd_name1' => $model->d_name1,
				'd_name2' => $model->d_name2,
				//'d_kana1' => $model->d_name2,
				//'d_kana2',
				'd_zip' => $model->d_zip,
				'd_pref' => $model->d_pref,
				'd_address1' => $model->d_address1,
				'd_address2' => $model->d_address2,
				'd_address3' => $model->d_address3,
				'd_tel' => $model->tel,
			);
			if( (int)Yii::app()->customer->getId() > 0 ){
				$order_post['customer_id'] = Yii::app()->customer->getId();
			}
			$OrderHistoryModel->attributes = $order_post;
			$OrderHistoryModel->save();
			$order_id = (int)$OrderHistoryModel->id;
		}
		catch(Exception $e) // クエリの実行に失敗した場合、例外が発生します
		{
			$transaction->rollback();
			throw new ExceptionClass('通販用オーダー記録トランザクションエラー');
			return;
		}
		
		foreach( $calc['items'] as $key =>$item )
		{
			$orderDetailModel = new OrderDetailModel();
			$post = array(
				'order_id' => $order_id,
				'sku_id' => $item['sku_id'],
				'item_id' => $item['item_id'] . '-' . $item['brunch_item_id'],
				'name' => $item['item_name'] . ' ' . $item['brunch_item_name'],
				'remark' => '',
				'price' => $item['calc_price'],
				'qty' => $item['qty'],
				'sub_total' => $item['calc_price'] * $item['qty'],
				'create_date' => date('Y-m-d H:i:s'),
				
			);
			$orderDetailModel->attributes = $post;
			$orderDetailModel->save();
		}
		
		
		//統計用テーブル（レジ用テーブルに記録）
		/*
		try{
			$ReceiptModel = new ReceiptModel();
			
			$post = array(
				'item_total' => $calc['calc']['item_total'],
				'tax' => 0,
				'grand_total' => $calc['calc']['grand_total'],
				'payment' => $calc['calc']['grand_total'],
				'change_val' => 0,
				'order_status' => 10,
				'create_date' => date('Y-m-d H:i:s'),
				'update_date' => date('Y-m-d H:i:s'),
				'payment_date' => date('Y-m-d H:i:s'),
				'order_type' => 1,
				'order_history_id' => $order_id
			);
			$ReceiptModel->attributes = $post;
			$ReceiptModel->save();
			$receipt_id = $ReceiptModel->id;
			
		}
		catch(Exception $e) // クエリの実行に失敗した場合、例外が発生します
		{
			$transaction->rollback();
			throw new ExceptionClass('レシート記録エラー');
			return;
		}
		*/
		
		
		
		
		//オーダー詳細記録
		/*
		$i = 1;
		foreach( $calc['items'] as $key =>$item )
		{
			try
			{
				$ReceiptDetailModel = new ReceiptDetailModel();
				$post = array(
					'receipt_id' => $receipt_id,
					'item_id' => $item['item_id'] . '-' . $item['brunch_item_id'],
					'item_name' => $item['item_name'] . '-' . $item['brunch_item_name'],
					'remark' => '',
					'price' => $itemone['calc_price'],
					'qty' => $itemone['qty'],
					'sub_total' => $itemone['base_price'] * $itemone['qty'],
					'create_date' => date('Y-m-d H:i:s'),
					'update_date' => date('Y-m-d H:i:s'),
					'sku_id' => $itemone['skuid']
				);
				$ReceiptDetailModel->attributes = $post;
				$ReceiptDetailModel->save();
			}
			catch(Exception $e) // クエリの実行に失敗した場合、例外が発生します
			{
				$transaction->rollback();
				throw new ExceptionClass('明細記録エラー(' . $i . ')');
				return;
			}
		}
		*/
		
		
		//ここまでエラーがなければコミット
		$transaction->commit();
		
		
		
		//配送方法
		//$delivery_list = Yii::app()->params['cart_config']['deliver_method'];
		
		
		//メールライブラリ
		require_once 'qdmail.php';
		
		
		
		/////店舗あてメール
		/*
		$body = $this->renderFile(
				$this->themeDirInside.'mail_template/mail_shop.php',
				array(
					'model' => $model,
					'order_id' => $order_id,
					'items' => $calc['items'],
					'calc' => $calc['calc'],
					'payment_drop_down' => $payment_drop_down,
					'delivery_list' => $delivery_list,
				),
				true
			);
		
		mb_language('ja');
		
		$mail_setting = Yii::app()->params['cart_config']['shop2mail'];
		qd_send_mail(
				'text' ,
				array( $mail_setting['to'], $mail_setting['toName']),
				$mail_setting['subject'],
				$body,
				array( $model->mailaddress, $model->name1 . $model->name2 . '様' )
		);
		
		
		/////お客様宛メール
		$body = $this->renderFile(
				$this->themeDirInside.'mail_template/mail_customer.php',
				array(
					'model' => $model,
					'order_id' => $order_id,
					'items' => $calc['items'],
					'calc' => $calc['calc'],
					'payment_drop_down' => $payment_drop_down,
					'delivery_list' => $delivery_list,
				),
				true
			);
		
		$mail_setting = Yii::app()->params['cart_config']['customer2mail'];
		qd_send_mail(
				'text' ,
				array( $model->mailaddress, $model->name1 . $model->name2 . '様' ),
				$mail_setting['subject'],
				$body,
				array( $mail_setting['from'], $mail_setting['fromName'])
		);
		*/
		
		//データをセッションに保存
		Yii::app()->session['ordered_datas'] = $calc;
		Yii::app()->session['ordered_datas_model'] = $model->attributes;
		Yii::app()->session['order_id'] = $order_id;
		
		
		$this->redirect( Yii::app()->createUrl('cart/thanks'));
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * サンクス画面
	 */
	public function actionThanks()
	{
		//モデル
		$model = new HaisaiModel();
		
		$this->application_url = Yii::app()->createUrl('cart/thanks');
		
		//データ取得
		$calc = $this->haisai->calc();
		
		//データ取得
		$this->getDatas($model);
		
		//支払い方法リスト
		$payment_drop_down = array();
		$payment_drop_down[''] = '選択してください';
		foreach( $this->haisai->getPaymentList() as $key => $val )
		{
			$payment_drop_down[$key] = $val['name'];
		}
		
		//配送方法
		$delivery_list = Yii::app()->params['cart_config']['deliver_method'];
		
		//カートデータクリア
		$this->haisai->init( TRUE );
		
		
		//セッションに保存の情報を表示
		$calc = Yii::app()->session['ordered_datas'];
		
		
		//データ整形
		$datas = array(
			'items' => $calc['items'],
			'calc' => $calc['calc'],
			'model' => Yii::app()->session['ordered_datas_model'],
			'payment_drop_down' => $payment_drop_down,
			'template_path' => $this->themeDirOutput,
			'delivery_list' => $delivery_list,
			'order_id' => Yii::app()->session['order_id'],
		);
		
		$this->renderFile( $this->themeDirInside.'cart/thanks.html', $datas );
	}
	
	
	
	
	// ----------------------------------------------------
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}