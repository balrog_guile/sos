<?php
/* =============================================================================
 * 来店管理
 * ========================================================================== */

class CommingController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('latest','index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'admin','delete'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 顧客別最新表示（IM)
	 * @param int $id
	 */
	public function actionLatest( $id = null )
	{
		
		if(is_null($id))
		{
			$this->redirect('index');
			return;
		}
		
		if((int)Input::Get('target')>0)
		{
			$target = (int)Input::Get('target');
		}
		else
		{
			$target = null;
		}
		
		
		//メインのモデル
		$model = new CommingModel( 'getCustoemrSearch' );
		
		
		
		//顧客を取得
		$customer_model = new CustomersModel();
		$customer = $customer_model->find( 'id = :id', array( 'id' => $id ) );
		
		
		//対象
		$target_query = array(
			array(
				'field' => 'cusomer_id',
				'value' => $id,
				'operator' => '='
			)
		);
		if(!is_null($target))
		{
			$target_query[] = array(
				'field' => 'id',
				'value' => $target,
				'operator' => '='
			);
		}
		
		
		//IM設定
		$ds = array(
			
			//メイン情報
			array(
				'name' => 'comming',
				'records' => 1,
				'paging' => FALSE,
				'key' => 'id',
				'sort' => array(
					array(
						'field' => 'comming_date',
						'direction' => 'DESC'
					)
				),
				'query' => $target_query
			),
			
			
			//アセット情報
			array(
				'name' => 'comming_assets',
				'key' => 'id',
				'relation' => array(
					array(
						'foreign-key' => 'comming_id',
						'join-field' => 'id',
						'operator' => '='
					)
				),
				'repeat-control' => 'insert delete',
			),
			
			
			//お会計情報
			array(
				'name' => 'receipt',
				'key' => 'id',
				'relation' => array(
					array(
						'foreign-key' => 'comming_id',
						'join-field' => 'id',
						'operator' => '='
					)
				),
			),
			
		);
		
		
		//IMのURL発行
		$def = array();
		$ds2 = array();
		$url = ImHelper::getUrl(
				$ds,
				$def,
				$ds2
		);
		
		
		//アセットのアサイン
		$asset = new AssetsHelper( 'ControllerJS' );
		$asset->set_js_raw( $url, 'end' );
		$asset->set_js( 'comming/latest.js', 'end' );
		$asset->set_js( 'IM_helper.js', 'end' );
		$asset->set_js( 'upload_helper.js', 'end' );
		$asset->set_js( 'sha1.js', 'end' );
		$asset->set_js( 'mdetect.js', 'end' );
		$asset->set_js_raw( Yii::app()->baseUrl . '/mediaelement/mediaelement-and-player.min.js', 'end');
		$asset->set_css( Yii::app()->baseUrl . '/mediaelement/mediaelementplayer.css');
		$asset->add_js( 'var history_url = "' . $this->createUrl('comming/history', array( 'id' => $id )) . '"; var upload_helper_url = "' . Yii::app()->createUrl('fileupload') . '";var go2register = "' . Yii::app()->createUrl('register/history2register', array( 'customer_id' => $id, 'comming_id' => '____targe____' ) ) . '";/*$("video").mediaelementplayer();*/', 'end' );
		
		
		
		//ビューの表示
		$this->render(
			'latest',
			array(
				'model'=>$model,
				'customer' => $customer
			)
		);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 顧客別
	 */
	public function actionHistory( $id )
	{
		$model = new CommingModel( 'getCustoemrSearch' );
		
		
		//顧客を取得
		$customer_model = new CustomersModel();
		$customer = $customer_model->find( 'id = :id', array( 'id' => $id ) );
		
		
		if(isset($_GET['CommingModel']))
		{
			$model->attributes=$_GET['CommingModel'];
		}
		
		
		//アセットのアサイン
		$asset = new AssetsHelper( 'ControllerJS' );
		$asset->set_js( 'comming/history.js', 'end' );
		$asset->add_js( 'var set_new_url = "' . $this->createUrl('comming/set_new', array('id' => $id)) . '";', 'end' );
		
		$this->render('history',array(
			'model'=>$model,
			'customer_id' => $id,
			'customer' => $customer,
		));
		
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 新規来店顧客別
	 * @param int $id
	 */
	public function actionSet_new( $id )
	{
		$model = new CommingModel();
		$model->comming_date = date('Y-m-d H:i:s');
		$model->update_date = date('Y-m-d H:i:s');
		$model->staff_id = Yii::app()->user->id;
		$model->cusomer_id = $id;
		$model->save();
		
		$this->redirect(
			$this->createUrl( 'comming/latest', array('id'=> $id) )
		);
		
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->redirect( 'comming/admin' );
		return;
		
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	// ----------------------------------------------------
	
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CommingModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CommingModel']))
		{
			$model->attributes=$_POST['CommingModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CommingModel']))
		{
			$model->attributes=$_POST['CommingModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect( 'comming/admin' );
		return;
		
		/*
		$dataProvider=new CActiveDataProvider('CommingModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
		*/
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CommingModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CommingModel']))
			$model->attributes=$_GET['CommingModel'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CommingModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CommingModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CommingModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='comming-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
