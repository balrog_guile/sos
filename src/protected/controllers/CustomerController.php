<?php
/* =============================================================================
 * 顧客登録周り
 * ========================================================================== */
class CustomerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	public $themeset = 'default';
	
	
	// ----------------------------------------------------
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * Specifies the access control rules.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(
					'signup',
					'confirm','exec','siginup_complete',
					'complete', 'code_input',
				),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
				'deniedCallback' => array( 'customerAuth','deniedCallback' )
			),
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView()
	{
		$id = Yii::app()->customer->getId();
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CustomersModel;
		
		
		if(isset($_POST['CustomersModel']))
		{
			$model->attributes=$_POST['CustomersModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		if(isset($_POST['CustomersModel']))
		{
			$model->attributes=$_POST['CustomersModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('CustomersModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CustomersModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CustomersModel']))
			$model->attributes=$_GET['CustomersModel'];
		
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * 登録
	 */
	public function actionSignup()
	{
		require(YiiBase::getPathOfAlias( 'application.vendor' ) . '/qdmail.php');
		$data = array();
		$data['model'] = new CustomersModel;
		Yii::app()->Cms->setCommonDataForViewData( $data );
		
		
		//ポスト送信があったとき
		if(isset($_POST['CustomersModel']))
		{
			$securityManager = Yii::app()->getSecurityManager();
			
			
			//下処理
			$post = $_POST['CustomersModel'];
			$post['create_date'] = date('Y-m-d H:i:s');
			$post['update_date'] = date('Y-m-d H:i:s');
			$post['customer_status'] = 0;
			$data['model']->attributes = $post;
			
			
			
			if($data['model']->validate())
			{
				//追加処理
				$data['model']->password_hash
					= $securityManager->generateRandomString(16);
				$data['model']->password
					= sha1( $data['model']->password_hash . '%' . $data['model']->password );
				$data['model']->password_confirm = $data['model']->password;
				$data['model']->authentication_code
					= sha1($securityManager->generateRandomString(8));
				$data['model']->time_limit
					= date('Y-m-d H:i:s', strtotime('+24 hour'));
				$data['model']->save();
				
				
				// メール送信
				/*
				$config = Yii::app()->params['mail_config'];
				$from = $config['siginup_mail']['from'];
				$from_name = $config['siginup_mail']['fromName'];
				$subject = $config['siginup_mail']['subject'];
				$to = $data['model']->mailaddress;
				$to_name = $data['model']->name1 . ' ' . $data['model']->name2;
				$url['url'] = Yii::app()->getBaseUrl(true).$this->createUrl( 'siginup_complete', array( 'code'=>$post['authentication_code'] ) );
				$body = $this->renderPartial( 'mail_siginup', $url, true );
				$res = qd_send_mail(
						'text',
						array( $to, $to_name ),
						$subject,
						$body,
						array( $from, $from_name )
					);
				*/
				$this->redirect(array('code_input'));
			}
		}
		
		//テーマ処理
		$themeDir = Yii::app()->Cms->getThemeDirectory();
		$path = $themeDir . 'customer/register.php';
		
		
		//フォーム
		$this->renderFile( $path, $data );
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * コード認証
	 */
	public function actionCode_input()
	{
		$data = array();
		$data['model'] = new CustomersModel;
		Yii::app()->Cms->setCommonDataForViewData( $data );
		
		///初期処理
		$data['authorized'] = false;
		$data['authorizeMiss'] = false;
		
		
		//セッション判断
		if(
			(isset(Yii::app()->session['codeInputAuth']))&&
			( Yii::app()->session['codeInputAuth'] === TRUE )
		)
		{
			$data['authorized'] = TRUE;
		}
		
		
		//承認処理
		if(isset($_POST['CustomersModel']))
		{
			$criteria = new CDbCriteria();
			$criteria->compare(
					'authentication_code',
					$_POST['CustomersModel']['authentication_code']
				);
			$criteria->compare( 'time_limit', '> '.date('Y-m-d H:i:s') );
			$target = CustomersModel::model()->find($criteria);
			
			if(is_null($target))
			{
				$data['authorizeMiss'] = TRUE;
			}
			else
			{
				$target->setScenario('edit');
				$target->customer_status = 1;
				$target->update_date = date('Y-m-d H:i:s');
				$target->save();
				Yii::app()->session['codeInputAuth'] = true;
				
				$this->redirect(array('code_input'));
				return;
			}
			
		}
		
		
		
		//テーマ処理
		$themeDir = Yii::app()->Cms->getThemeDirectory();
		$path = $themeDir . 'customer/activate_form.php';
		
		
		$this->renderFile( $path, $data );
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * siginup comp
	 */
	public function actionSiginup_complete( $code = 0 )
	{
		$data = array();
		$this->themeDir( $data );
		// コードが空かどうか
		if( !empty($code) )
		{
			$model = CustomersModel::model()->find(
				'authentication_code = :authentication_code',
				array( ':authentication_code' => $code )
			);

			if($model !== null)
			{
				if( $model->customer_status == 1 ) // 登録済み
				{
					$data['text'] = 'すでに登録が完了しております。ログイン画面からログインしてください。';
					$this->renderFile( $data['insideDirPath'] . '/siginup/error.html', $data );
					return;
				}
				// 制限時間内なら登録
				if( strtotime($model->time_limit) >= strtotime('now') )
				{
					$command = Yii::app()->db->createCommand();
					$command->update('customers', array(
						'customer_status'=>1,
						'authentication_code'=>'',
					), 'id=:id', array(':id'=>$model->id));
					
					$this->redirect(array('complete'));
				}
				
				$data['text'] = '登録の期限がきれてしまいました。お手数ですがもう一度登録しなおしてください。';
				$this->renderFile( $data['insideDirPath'] . '/siginup/error.html', $data );
				return;
			}
		}
		
		$data['text'] = 'URlが違います。もう一度メールを確認してください。';
		$this->renderFile( $data['insideDirPath'] . '/siginup/error.html', $data );
	}
	
	
	// ----------------------------------------------------
	/**
	 * siginup exec
	 */
	public function actionComplete()
	{
		$data = array();
		$this->themeDir( $data );
		$this->renderFile( $data['insideDirPath'] . '/siginup/complete.html', $data );
	}
	
	
	// ----------------------------------------------------
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CustomersModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CustomersModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	// ----------------------------------------------------
	/**
	 * Performs the AJAX validation.
	 * @param CustomersModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customers-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
