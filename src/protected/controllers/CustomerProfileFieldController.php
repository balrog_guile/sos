<?php
/* =============================================================================
 * 顧客フィールドの設定
 * ========================================================================== */

class CustomerProfileFieldController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			/*
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array(),
				'users'=>array('*'),
			),
			*/
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array( 'reflection', 'index','view','create','update','admin','delete'),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array(),
			),
			*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CustomerProfileFieldModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CustomerProfileFieldModel']))
		{
			$model->attributes=$_POST['CustomerProfileFieldModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CustomerProfileFieldModel']))
		{
			$model->attributes=$_POST['CustomerProfileFieldModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//IMの設定
		$ds = array(
			array(
				'name' => 'customer_profile_field',
				'records' => 50,
				'paging' => FALSE,
				'key' => 'id',
				'repeat-control' => 'insert delete'
			),
			
		);
		
		
		//IMのURL発行
		$def = array();
		$ds2 = array();
		$url = ImHelper::getUrl(
				$ds,
				$def,
				$ds2
		);
		
		
		//アセットのアサイン
		$asset = new AssetsHelper( 'ControllerJS' );
		$asset->set_js_raw( $url, 'end' );
		$asset->set_js( 'CustomerProfileField/index.js', 'end' );
		$asset->add_js( 'var reflection_url = "' . $this->createUrl('customerProfileField/reflection') . '";', 'end' );
		
		
		
		$this->render(
			'index',
			array(
			)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 設定済み項目の反映
	 */
	public function actionReflection()
	{
		//データ取得
		$model = new CustomerProfileFieldModel();
		$datas = $model->findAll();
		
		//現在のカラム名を取得
		$exists = array_keys(Yii::app()->db->schema->tables['customers']->columns);
		
		
		
		//判定除外
		$out = array(
			'id',
			'name1',
			'name2',
			'kana1',
			'kana2',
			'mailaddress',
			'customer_status',
			'create_date',
			'update_date'
		);
		
		
		//なくなったものの判定
		$checke_delete = array();
		
		
		//実行SQLの追加
		$sql_list = array();
		
		
		//カラムデータを回して判定
		foreach( $datas as $data )
		{
			
			//データタイプを作成
			$datataype = $data->field_type;
			if( $data->required == 1 )
			{
				$datataype .= ' NOT NULL';
			}
			else
			{
				$datataype .= ' NULL';
			}
			
			/*
			//デフォルト値はモデルに任せることに
			if( $data->default_value != '' )
			{
				$datataype .= ' DEFAULT  ' . Yii::app()->db->quoteValue( $data->default_value );
			}
			*/
			
			
			
			//新規追加フィールド
			if(
				( ! in_array( $data->field_name, $exists ) )&&
				( ! in_array( $data->field_name, $out ) )
			)
			{
				//SQL組み立て
				//Yii::app()->db->createCommand()->execute();
				//Yii::app()->db->schema->refresh();
				$sql = Yii::app()->db->schema->addColumn( 'customers', $data->field_name, $datataype );
				//Yii::app()->db->createCommand($sql)->execute();
				$sql_list[] = $sql;
				
			}
			//アップデート用フィールド
			else if( ( ! in_array( $data->field_name, $out ) ) )
			{
				//echo $data->field_name . 'is update!<br />';
				$column = Yii::app()->db->schema->tables['customers']->columns[$data->field_name];
				
				//SQL組み立て
				//Yii::app()->db->createCommand()->execute();
				//Yii::app()->db->schema->refresh();
				$sql = Yii::app()->db->schema->alterColumn( 'customers', $data->field_name, $datataype );
				//Yii::app()->db->createCommand($sql)->execute();
				$sql_list[] = $sql;
				
			}
			
			
			//削除用チェック
			$checke_delete[] = $data->field_name;
			
		}
		
		
		//存在から外れたものをチェックして削除
		foreach( $exists as $field )
		{
			if(
				( !in_array( $field, $checke_delete ))&&
				( !in_array( $field, $out ))
			)
			{
				$sql = Yii::app()->db->schema->dropColumn( 'customers', $field );
				$sql_list[] = $sql;
			}
		}
		
		
		
		//SQL実行
		foreach( $sql_list as $sql )
		{
			Yii::app()->db->createCommand($sql)->execute();
		}
		
		
		
		$this->redirect( 'index' );
	}
	
	// ----------------------------------------------------
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new CustomerProfileFieldModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CustomerProfileFieldModel']))
			$model->attributes=$_GET['CustomerProfileFieldModel'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CustomerProfileFieldModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CustomerProfileFieldModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CustomerProfileFieldModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='customer-profile-field-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
