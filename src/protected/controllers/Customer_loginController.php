<?php
/* =============================================================================
 * ログインフォーム
 * ========================================================================== */
class Customer_loginController extends Controller
{
	public $themeset = 'default';
	
	// ----------------------------------------------------
	/**
	 * ログインフォーム
	 */
	
	public function actionIndex()
	{
		$data = array();
		$data['redirect'] = Input::GetPost('redirect');
		
		//ログイン状態なら飛ばす
		if( (int)Yii::app()->customer->getId()>0 )
		{
			if( $data['redirect'] != '' )
			{
				Yii::app()->controller->redirect( $data['redirect'] );
				return;
			}
			else
			{
				Yii::app()->controller->redirect(
					Yii::app()->createUrl('mypage')
				);
				return;
			}
		}
		
		
		//ログインチェック実行
		if(isset($_POST['mailaddress'])){
			$auth = new customerAuth(
				Input::Post('mailaddress'),
				Input::Post('password')
			);
			$res = $auth->authenticate();
			if( $res === TRUE )
			{
				Yii::app()->customer->login( $auth, 0 );
				if( $data['redirect'] != '' )
				{
					Yii::app()->controller->redirect( $data['redirect'] );
					return;
				}
				else
				{
					Yii::app()->controller->redirect(
						Yii::app()->createUrl('mypage')
					);
					return;
				}
			}
			else{
				$data['message'] = 'ログインに失敗しました';
			}
		}
		
		
		$this->commonSetter($data);
		$path = $data['insideDirPath'] . '/customer_login/index.html';
		$this->renderFile($path, $data);
	}
	
	// ----------------------------------------------------
	/**
	 * 共通処理
	 * @param type $data（参照）
	 */
	public function commonSetter( &$data )
	{
		$data['template_path'] = Yii::app()->getBaseUrl(true) . '/ec_theme/' . $this->themeset . '/';
		$data['insideDirPath'] = YiiBase::getPathOfAlias( 'webroot.ec_theme.' . $this->themeset);
	}
	
	// ----------------------------------------------------
	
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}