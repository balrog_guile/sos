<?php
/* =============================================================================
 * ECページ処理
 * ========================================================================== */
class EcpagesController extends Controller
{

    //テンプレートフォルダ（システム内）
    public $themeDirInside;
    
    //テンプレートフォルダ（出力用）
    public $themeDirOutput;
    
    //URLセグメントリスト
    public $segment = array();
    
    //読み込み中のテンプレートファイルを保存
    public $currentTemplate = '';
    
    //正規化された現在表示のURL
    public $application_url = '';
    
    // ----------------------------------------------------
    /**
     * 初期処理
     */
    public function init() {
        parent::init();
        unset($_GET['___e_pagemat']);
    }
    
    
    
    // ----------------------------------------------------
    /**
     * 処理フィルタ
     * @return type
     */
    public function filters()
    {
        // return the filter configuration for this controller, e.g.:
        return array(
            'accessControl',
        );
    }
    
    
    
    // ----------------------------------------------------
    /**
     * HMVCコール
     * 
     */
    public function hmvc()
    {
        //HTMLがあれば問答無用
        if(
            preg_match(
                '/\.html$/',
                $this->segment[count($this->segment)-1]
            )
        )
        {
            return false;
        }
        
        //カテゴリも無視
        if($this->segment[0] == 'category')
        {
            return false;
        }
        
        
        $x = count( $this->segment );
        $i = 0;
        $controllerPathOK = '';
        $method = '';
        while( $x > $i )
        {
            $y = 0;
            $controller = array();
            while( $y < ($i+1) )
            {
                $controller[] = $this->segment[$y];
                $y ++ ;
            }
            $controller[ $y - 1 ] =
                ucfirst($controller[ $y - 1 ]) . 'Controller';
            $controllerPath = Yii::getPathOfAlias('application.controllers') . '/' . implode( '/', $controller )  .'.php';
            
            
            //コントローラーファイルありなしチェック
            if(file_exists($controllerPath))
            {
                $controllerPathOK = $controllerPath;
                break;
            }
            $i ++;
        }
        if( $controllerPathOK != '' )
        {
            Yii::app()->runController(implode('/', $this->segment));
            return true;
        }
        
        return false;
    }
    
    
    
    // ----------------------------------------------------
    
    /**
     * アクセスフィルタリング
     */
    public function accessRules()
    {
        
        return array(
            
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array( 'index' ),
                'users'=> array( (Yii::app()->params['open'] === TRUE)?'*':'@' ),
            ),
            
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * ページ処理
     */
    public function actionIndex()
    {
        //セグメント処理
        $this->segment = explode('/', Yii::app()->request->pathInfo);
        $this->application_url = Yii::app()->request->pathInfo;
        
        
        //ここでシステムページの判定
        if( $this->hmvc() )
        {
            return;
        }
        
        
        //最終処理
        if(
            ( count($this->segment) > 1 )&&
            ( $this->segment[0] == '' )
        ){
            array_shift($this->segment);
            $this->application_url = '/';
        }
        
        
        //処理済みセグメントをアプリケーションに保存
        Yii::app()->Cms->segment = $this->segment;
        
        
        //商品ページ／一覧ページ／タグページ
        //商品ページ
        if($this->segment[0] == 'archives')
        {
            Yii::app()->runController('theme/Products/Products');
        }
        else if($this->segment[0] == 'category')
        {
            Yii::app()->runController('theme/Category');
        }
        else
        {
            Yii::app()->runController('theme/pages');
        }
        
        
        
        
        
    }
    // ----------------------------------------------------

    /**
     * 各ページ共通データ
     */
    protected function setCommonData( &$data )
    {
        //カテゴリリスト
        $categoryModel = new CategoryModel();
        $data['categoryList'] = $categoryModel->getCategoryListAll();
        
        //アプリケーションパス
        $data['template_path_inside'] = $this->themeDirInside;
        $data['webroot'] = Yii::app()->getBaseUrl(true);
        $data['webroot'] = preg_replace( '/^http\:/', '', $data['webroot'] );
        $data['webroot'] = preg_replace( '/^https\:/', '', $data['webroot'] );
        
        //このコントローラー
        $data['that'] = $this;
        
    }

    // ----------------------------------------------------

    /**
     * インデックスページ
     */
    public function pages_index()
    {
        $data = array();

        //必要データの追加
        $data['template_path'] = $this->themeDirOutput;

        //共通データ処理
        $this->setCommonData( $data );

        //ビューの表示
        $this->currentTemplate = $this->themeDirInside.'index.html';
        $this->renderFile( $this->currentTemplate, $data );
    }

    // ----------------------------------------------------

    /**
     * カテゴリページ
     */
    public function category()
    {
        $data = array();

        //必要データの追加
        $data['template_path'] = $this->themeDirOutput;


        //カテゴリデータの作成
        $file = $this->segment[count($this->segment)-1];
        $category = preg_replace( '/\.html$/', '', $file );

        $categoryModel = new CategoryModel();
        $data['category'] = $categoryModel->find( 'link_url = :link_url', array( ':link_url' => $category ) );
        if(is_null($data['category']))
        {
            $this->page404( 'カテゴリは見つかりません' );
        }

        //子カテゴリ
        $data['children'] = $categoryModel->getCategoryListAll( $data['category']->id );


        //商品データ
        $relProductCategoriesModel = new RelProductCategoriesModel();
        $criteria = new CDbCriteria();
        $criteria->compare( 'categories_id', $data['category']->id );
        //$criteria->order = 'create_date DESC';
        $find = $relProductCategoriesModel->findAll($criteria);
        $items = array();
        foreach( $find as $row )
        {
            if(
                (is_array($row->product_search))&&
                ( count($row->product_search) > 0 )
            )
            {
                $items[] = $row->product_search[0];
            }
        }
        $data['items'] = $items;


        //共通データ処理
        $this->setCommonData( $data );

        //ビューの表示
        $this->currentTemplate = $this->themeDirInside.'categories/index.html';
        $this->renderFile( $this->currentTemplate, $data );
    }

    // ----------------------------------------------------

    /**
     * 商品ページ
     */
    public function item_pages()
    {
        $data = array();

        //必要データの追加
        $data['template_path'] = $this->themeDirOutput;


        //商品ページデータ
        $file = $this->segment[count($this->segment)-1];
        $itempage = preg_replace( '/\.html$/', '', $file );


        //商品ページデータの作成
        $productsModel = new ProductsModel();
        $data['product'] = $productsModel->find( 'link_url = :link_url', array( ':link_url' => $itempage ));
        if( is_null($data['product']) )
        {
            $this->page404( '商品が見つかりません' );
            return;
        }
        if( $data['product']->open_status != 1 )
        {
            $this->page404( '商品が見つかりません' );
            return;
        }

        //カテゴリデータ取得
        $data['cateogries'] = array();
        foreach( $data['product']->categories_many as $row )
        {
            $one = array( $row );
            if( $row->parent_id > 0 )
            {
                $one = $this->getParentsCategory( $row->parent_id, $one );
            }
            $data['cateogries'][] = $one;
        }
        
        
        //共通データ処理
        $this->setCommonData( $data );
        
        
        //テンプレートファイルの選択
        $this->currentTemplate = $this->themeDirInside. 'item_pages/' . $data['product']->template_file;
        if(!file_exists($this->currentTemplate))
        {
            $this->currentTemplate = $this->themeDirInside.'item_pages/index.html';
        }
        
        //ビューの表示
        //$this->currentTemplate = $this->themeDirInside.'item_pages/index.html';
        $this->renderFile( $this->currentTemplate, $data );
    }

    // ----------------------------------------------------

    /**
     * カテゴリの親をたどる
     * @param int $id
     * @param array $list
     * @param array list
     */
    protected function getParentsCategory( $id, $list )
    {
        $CategoryModel = new CategoryModel();
        $row = $CategoryModel->find( 'id = :id', array(':id' => $id ));
        if(is_null( $row ))
        {
            return $list;
        }

        $list[] = $row;
        if( $row->parent_id > 0 )
        {
            $list = $this->getParentsCategory( $row->parent_id, $list );
        }

        return $list;
    }

    // ----------------------------------------------------

    /**
     * 通常ページ
     */
    public function pages()
    {
        $data = array();

        //必要データの追加
        $data['template_path'] = $this->themeDirOutput;

        //ページファイル
        //$page = $this->themeDirInside.Yii::app()->request->pathInfo;//$this->segment
        $page = $this->themeDirInside . '/' . implode( '/', $this->segment );
        $this->currentTemplate = $page;

        //なしのとき
        if(!file_exists($page))
        {
            $this->page404('ページがみつかりません');
            return;
        }

        //共通データ処理
        $this->setCommonData( $data );

        //ビューの表示
        $this->renderFile( $page, $data );
    }

    // ----------------------------------------------------

    /**
     * 404ページ
     * @param string $errMessage
     */
    public function page404( $errMessage )
    {
        $data = array();

        //必要データの追加
        $data['template_path'] = $this->themeDirOutput;
        $data['error_message'] = $errMessage;


        //共通データ処理
        $this->setCommonData( $data );

        //ビューの表示
        header("HTTP/1.1 404 Not Found");
        $this->currentTemplate = $this->themeDirInside.'errors/404.html';
        $this->renderFile( $this->currentTemplate, $data );
        return;
    }
    // ----------------------------------------------------

    /**
     * メールフォーム組み込みメソッド
     */
    public function setMailForm( $file = 'mail_attribute' )
    {
        $dir = dirname( $this->currentTemplate );
        require_once( $dir .'/' . $file . '.php' );
        $mailModel = new $file;

        //もしモードが確認画面だったらバリデーション
        if( $mailModel->mode == 'confirm' )
        {
            $res = $mailModel->execValidate();
            if( $res === FALSE )
            {
                $mailModel->mode = 'input';
            }
        }

        //再入力モード
        if( $mailModel->mode == 'reinput' )
        {
            $mailModel->setAttr();
            $mailModel->mode = 'input';
        }

        //メール送信モード
        if( $mailModel->mode == 'send' )
        {
            $res = $mailModel->execValidate();
            if( $res === FALSE )
            {
                $mailModel->mode = 'input';
            }
            else
            {
                $mailModel->sendMail();

                $url = $this->application_url;

                if(preg_match( '/\?/', $url ))
                {
                    $uri = str_replace( '?', '?mailstep=thanks&', $url );
                }
                else if(preg_match( '/\#/', $url ))
                {
                    $uri = str_replace( '#', '?mailstep=thanks#', $url );
                }
                else{
                    $uri = $url . '?mailstep=thanks';
                }
                $url = (empty($_SERVER["HTTPS"]) ? "http://" : "https://") . $_SERVER["HTTP_HOST"] .'/'. $uri;
                Yii::app()->controller->redirect($url);
                return;

                //$mailModel->mode = 'thanks';
            }
        }

        return $mailModel;
    }

    // ----------------------------------------------------
}