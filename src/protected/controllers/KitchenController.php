<?php
/* =============================================================================
 * 厨房表示
 * ========================================================================== */
class KitchenController extends Controller
{
    public $layout='//layouts/column2';
    
    protected $assets_helper;
    protected $jqueryui;
    
    // ----------------------------------------------------
    /**
     * コンストラクタ
     */
    public function __construct($id, $module = null) {
        parent::__construct($id, $module);
        $this->assets_helper = new AssetsHelper( 'ControllerJS' );
    }
    
    
    // ----------------------------------------------------
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array( 'accessControl' );
    }
    
    
    // ----------------------------------------------------
    /**
     * アクセスルール
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',
                'actions'=>array(
                    'index', 'get_order_now', 'detail_change_status',
                ),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * 注文データ
     */
    public function actionIndex()
    {
        $data = array();
        if( $this->shop === null )
        {
            throw new CHttpException( 403,'アクセス禁止！');
        }
        
        
        //URL定義
        $url = array(
            'getOrderNow' =>  Yii::app()->createAbsoluteUrl('kitchen/get_order_now'),
            'changeStatus' =>  Yii::app()->createAbsoluteUrl('kitchen/detail_change_status'),
        );
        
        
        //必要JS
        $this->assets_helper->add_js( 'var urlList = ' . json_encode($url) .';', 'end' );
        $this->assets_helper->set_js( 'kitchen/knockout.js', 'end' );
        $this->assets_helper->set_js( 'kitchen/viewModel.js', 'end' );
        $this->assets_helper->set_js( 'kitchen/kitchen.js', 'end' );
        
        $this->render('index',$data);
    }
    
    
    // ----------------------------------------------------
    /**
     * データ取得
     */
    public function actionGet_order_now()
    {
        $data = array();
        
        //条件作成
        $criteria = new CDbCriteria();
        $criteria->together = true;
        $criteria->with = array( 'detail' );
        $criteria->order = 'order_date DESC';
        $criteria->compare( 't.shop_id', $this->shop->id );
        $criteria->compare( 't.order_type', 0 );
        $criteria->compare( 'detail.canceled', 0 );
        $criteria->compare( 'detail.deliv_flag', 0 );
        
        
        //実行
        $orderList = OrderHistoryModel::model()->findAll( $criteria );
        $return = array(
            'orderList' => array()
        );
        foreach( $orderList as $order )
        {
            $set = $order->attributes;
            $set['detail'] = array();
            foreach( $order->detail as $detail )
            {
                $set2 = $detail->attributes;
                $set['detail'][] = $set2;
            }
            $return['orderList'][] = $set;
        }
        
        echo json_encode($return);
    }
    
    // ----------------------------------------------------
    /**
     * ステータス変更
     */
    public function actionDetail_change_status()
    {
        $id = Input::Post('id');
        $status = Input::Post('status');
        
        $model = OrderDetailModel::model()->findByPk( $id );
        if(is_null($model))
        {
            echo json_encode( false );
            return;
        }
        
        
        switch( $status )
        {
            case 'cooked':
                $model->cooked_flag = 1;
                break;
            
            case 'deliv':
                $model->deliv_flag = 1;
                break;
            
            case 'cancel':
                $model->canceled = 1;
                break;
        }
        
        $model->save();
        
        
        //キャンセルの場合カウントダウン
        if( $status == 'cancel' )
        {
            $parent = $model->parent;
            $parent->item_total -= $model->sub_total;
            $parent->grand_total -= $model->sub_total;
            $parent->save();
        }
        
        echo json_encode( true );
    }
    
    
    // ----------------------------------------------------
}