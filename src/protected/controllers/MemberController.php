<?php

class MemberController extends Controller
{
    //アクティベートの有効期限
    protected $activateExpire = 86400; //60 * 60 * 24
    
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';
    
    // ----------------------------------------------------
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array(
                    'index','view','registration','update', 'activate', 'login',
                ),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(),
                'users'=>array('@'),
            ),
            array('allow', // allow admin user to perform 'admin' and 'delete' actions
                'actions'=>array('admin','delete'),
                'users'=>array('admin'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }
    
    
    // ----------------------------------------------------
    /**
     * 登録
     */
    public function actionRegistration()
    {
        $model=new MemberModel;
        
        //登録開始
        if(isset($_POST['MemberModel']))
        {
            $model->attributes=$_POST['MemberModel'];
            
            //処理の書き直し
            $model->create_at = date('Y-m-d H:i:s');
            $model->update_at = date('Y-m-d H:i:s');
            $model->activate_expire = date(
                    'Y-m-d H:i:s',
                    strtotime(
                        '+ ' . $this->activateExpire . 'second'
                    )
                );
            $model->activate_hash =
                    Yii::app()->securityManager->generateRandomString(32);
            
            $model->password_hash =
                    Yii::app()->securityManager->generateRandomString(12);
            
            
            $model->member_status = 1;
            
            if($model->validate())
            {
                $pass = $model->password;
                $model->password =
                    sha1( $model->password_hash . '___' . $pass );
                $model->password_confirm =
                    sha1( $model->password_hash . '___' . $pass );
                $model->save();
                
                /////お客様宛メール
                require_once 'qdmail.php';
                $body = $this->renderPartial('mail_registration',
                        array(
                            'model' => $model,
                        ),
                        true
                    );
                $mail_setting = Yii::app()->params['member_config']['registration_mail'];
                $res = qd_send_mail(
                        'text' ,
                        array( $model->email, $model->name2 . '様' ),
                        $mail_setting['subject'],
                        $body,
                        array( $mail_setting['from'], $mail_setting['fromName'])
                );
                
                $this->redirect(array('activate'));
            }
            
            
        }
        
        
        
        
        
        
        //変数処理
        $data = array(
            'model'=>$model,
        );
        //共通ビュー処理
        Yii::app()->Cms->setCommonDataForViewData( $data );
        
        
        $this->render('register', $data);
        
    }
    
    // ----------------------------------------------------
    /**
     * アクティベートフォーム
     */
    public function actionActivate()
    {
        $model = new MemberModel;
        $authorized = false;
        $authorizeMiss = false;
        
        //承認処理
        if(
            (isset($_POST['MemberModel']))||
            (Input::Get('activate_hash')!= '')
        )
        {
            if(Input::Get('activate_hash')!= '')
            {
                $hash = Input::Get('activate_hash');
            }
            else
            {
                $hash = $_POST['MemberModel']['activate_hash'];
            }
            
            if( $hash === '' )
            {
                $hash = '絶対に無いコードだよこれはどうだどうだどうだどうだ';
            }
            
            $criteria = new CDbCriteria();
            $criteria->compare( 'activate_hash', $hash );
            $criteria->compare( 'activate_expire', '> '.date('Y-m-d H:i:s') );
            $target = MemberModel::model()->find($criteria);
            
            if( $target === null )
            {
                $authorizeMiss = true;
            }
            else
            {
                $target->setScenario('activate');
                $authorized = true;
                $target->activate_hash = '';
                $target->activate_expire = '';
                $target->member_status = 2;
                $target->update_at = date('Y-m-d H:i:s');
                $target->update();
                //var_dump( $target->getErrors() );
            }
            
        }
        
        
        
        //変数処理
        $data = array(
            'model'=>$model,
            'authorizeMiss' => $authorizeMiss,
            'authorized' => $authorized,
        );
        //共通ビュー処理
        Yii::app()->Cms->setCommonDataForViewData( $data );
        
        
        
        $this->render('activate', $data);
    }
    
    public function actionLogin()
    {
        $model = new MemberModel;
        
        $error = false;
        if(isset($_POST['MemberModel'])){
            $model->attributes = $_POST['MemberModel'];
            $member = $model->find('email = :email', array('email' => $model->email));
            if(is_null($member)){
                $error = true;
            }else if($member['password'] != sha1( $member['password_hash'] . '___' . $model->password )){
                $error = true;
            }else if($member['member_status'] != 2){
                $error = true;
            }else{
                Yii::app()->session['member_id'] = $member['id'];
                $this->redirect( Yii::app()->createUrl('?SOS_THEME_SELECT='.Yii::app()->session['sos_theme']) );
                return;
            }
        }
        
        //変数処理
        $data = array(
            'model' => $model,
            'error' => $error,
        );
        //共通ビュー処理
        Yii::app()->Cms->setCommonDataForViewData( $data );
        
        $this->render('login', $data);
    }
    
    public function actionLogout()
    {
        Yii::app()->session['member_id'] = null;
    }
    
    // ----------------------------------------------------
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['MemberModel']))
        {
            $model->attributes=$_POST['MemberModel'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }

    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        $this->loadModel($id)->delete();

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }

    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $dataProvider=new CActiveDataProvider('MemberModel');
        $this->render('index',array(
            'dataProvider'=>$dataProvider,
        ));
    }

    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new MemberModel('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['MemberModel']))
            $model->attributes=$_GET['MemberModel'];

        $this->render('admin',array(
            'model'=>$model,
        ));
    }

    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return MemberModel the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=MemberModel::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param MemberModel $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='member-model-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
