<?php
/* =============================================================================
 * マイページ
 * ========================================================================== */
class MypageController extends Controller
{

	public $themeset = 'default';
	
	// ----------------------------------------------------
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',
				'users'=>array('*'),
				'actions'=>array( 'login' ),
			),
			array('allow',
				'actions'=>array( 'index', 'edit', 'mail_edit', 'mail_exec', 'mail_complete', ),
				'expression' => array( 'customerAuth', 'accessControll' ),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
				'deniedCallback' => array( 'customerAuth','deniedCallback' )
			),
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * ダッシュボード
	 */
	public function actionIndex()
	{
		//変数処理
		$data = array();
		
		
		//共通ビュー
		Yii::app()->Cms->setCommonDataForViewData( $data );
		$themeDir = Yii::app()->Cms->getThemeDirectory();
		
		//顧客情報
		$data['memberData'] = CustomerHelper::getCustomerData();
		
		
		
		
		$path = $themeDir.'mypage/index.php';
		$this->renderFile($path, $data);
	}
	
	
	// ----------------------------------------------------
	/**
	 * ログイン
	 */
	public function actionLogin()
	{
		//変数処理
		$data = array();
		
		
		//モデル
		$data['model'] = new MemberModel;
		
		
		//メッセージ
		$data['message'] = '';
		
		
		//ログインチェック
		if(isset($_POST['MemberModel']))
		{
			$post = $_POST['MemberModel'];
			$auth = new customerAuth(
				$post['email'],
				$post['password']
			);
			
			//メンバーがいた
			$res = $auth->authenticate();
			if($res === TRUE )
			{
				Yii::app()->customer->login( $auth, 0 );
				$this->redirect(Yii::app()->createAbsoluteUrl('mypage'));
				return;;
			}
			$data['message'] = 'ログイン失敗';
		}
		
		
		
		//共通ビュー処理
		Yii::app()->Cms->setCommonDataForViewData( $data );
		$themeDir = Yii::app()->Cms->getThemeDirectory();
		
		
		$path = $themeDir.'mypage/login.php';
		$this->renderFile($path, $data);
	}
	
	
	// ----------------------------------------------------
	/**
	 * @param integer $id the ID of the model to be loaded
	 * @return CustomersModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CustomersModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	// ----------------------------------------------------
	
}