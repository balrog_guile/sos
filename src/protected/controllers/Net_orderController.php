<?php

class Net_orderController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	
	// ----------------------------------------------------
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('admin','delete', 'create','update','index','view'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new OrderHistoryModel;
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['OrderHistoryModel']))
		{
			$model->attributes=$_POST['OrderHistoryModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}	
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// 通販以外
		if( $model->order_type != 1 ){
			//$this->redirect(array('admin'));
		}
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['OrderHistoryModel']))
		{
			$model->attributes=$_POST['OrderHistoryModel'];
			if($model->save())
			{
				$this->redirect(array('admin'));
			}
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('OrderHistoryModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	// ----------------------------------------------------

	/**
	 * 通販 売上管理
	 */
	public function actionAdmin()
	{
		$model = new OrderHistoryModel('net_order_search');
		$model->unsetAttributes();


		//項目があれば。
		if(isset($_GET['OrderHistoryModel']))
		{
			$model->attributes = $_GET['OrderHistoryModel'];
		}
		else
		{
			//指定がなければ日付だけ今月の支払い済みのデータのみを対象にする
			$arr = array(
				'order_date_from' => date( 'Y-m-01' ),
				'order_date_to' => date( 'Y-m-t' ),
				'order_type' => 1,
			);
			$model->attributes = $arr;
		}
		
		/*
		////合計計算
		$calc = array();
		
		//件数
		$criteria = $model->getStasticscriteria();
		$calc['count'] = $model->count( $criteria );
		
		//商品合計
		$criteria2 = $model->getStasticscriteria();
		$criteria2->select = 'SUM( item_total ) as item_total_sum';
		$row = $model->find( $criteria2 );
		$calc['item_total_sum'] = $row->item_total_sum;
		
		//総合計
		$criteria3 = $model->getStasticscriteria();
		$criteria3->select = 'SUM( grand_total ) as grand_total_sum';
		$row = $model->find( $criteria3 );
		$calc['grand_total_sum'] = $row->grand_total_sum;
		*/
		
		$this->render('admin',array(
			'model' => $model,
			//'calc' => $calc,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return OrderHistoryModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=OrderHistoryModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param OrderHistoryModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='order-history-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
