<?php

class PageBlocksController extends Controller
{
	protected $assets_helper;
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct($id, $module = null) {
		parent::__construct($id, $module);
		$this->assets_helper = new AssetsHelper( 'ControllerJS' );
	}
	
	// ----------------------------------------------------
	
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	// ----------------------------------------------------
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl'
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index','view','create','update','admin','delete','rankchange'),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 登録
	 */
	public function actionCreate()
	{
		$model=new PageBlocksModel;
		
		
		//リクエスト処理
		$target = array(
			'url' => Input::GetPost('url'),
			'name' => Input::GetPost('name'),
			'is_global' => Input::GetPost('is_global'),
			'types' => Input::GetPost('types'),
			'types_decode' => json_decode(Input::GetPost('types')),
		);
		
		
		//////送信時
		if(isset($_POST['PageBlocksModel']))
		{
			$_POST['PageBlocksModel']['create_date'] = date('Y-m-d H:i:s');
			$_POST['PageBlocksModel']['update_date'] = date('Y-m-d H:i:s');
			if( $_POST['PageBlocksModel']['open_date'] == '' )
			{
				$_POST['PageBlocksModel']['open_date'] = null;
			}
			if( $_POST['PageBlocksModel']['close_date'] == '' )
			{
				$_POST['PageBlocksModel']['close_date'] = null;
			}
			
			//MAX
			$criteria=new CDbCriteria;
			$criteria->select='max(rank) AS max';
			if( $target['is_global'] == 1 ){
				$criteria->compare( 'global', 1 );
			}
			else{
				$criteria->compare( 'page_path', $_POST['PageBlocksModel']['page_path'] );
			}
			$criteria->compare( 'name', $target['name'] );
			$rank_row = PageBlocksModel::model()->find( $criteria );
			$max = $rank_row['max'] + 1;
			$_POST['PageBlocksModel']['rank'] = $max;
			
			
			$model->attributes = $_POST['PageBlocksModel'];
			if($model->save())
			{
				//URLに飛ぶ
				$url = Yii::app()->createUrl(
							'PageBlocks/admin',
							array(
								'url' => $target['url'],
								'name' => $target['name'],
								'is_global' => $target['is_global'],
								'types' => $target['types'],
							)
					);
				$this->redirect($url);
			}
		}
		//////ランディング
		else
		{
			$model->name = $target['name'];
			$model->page_path = $target['url'];
			$model->global = $target['is_global'];
			$model->name = $target['name'];
			$model->type = Input::GetPost('type');
		}
		
		//アセットの処理
		$this->assets_helper->set_js( 'pageBlocks/page_blocks.js', 'end' );
		$this->assets_helper->add_js( '
			var baseUrl = "' . Yii::app()->baseUrl .'";
			var homeUrl = "' . Yii::app()->homeUrl .'";
		', 'end');
		
		$this->render('create',array(
			'model'=>$model,
			'target' => $target,
			'type' => Input::GetPost('type'),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		//リクエスト処理
		$target = array(
			'url' => Input::GetPost('url'),
			'name' => Input::GetPost('name'),
			'is_global' => Input::GetPost('is_global'),
			'types' => Input::GetPost('types'),
			'types_decode' => json_decode(Input::GetPost('types')),
		);
		
		
		if(isset($_POST['PageBlocksModel']))
		{
			$_POST['PageBlocksModel']['create_date'] = date('Y-m-d H:i:s');
			$_POST['PageBlocksModel']['update_date'] = date('Y-m-d H:i:s');
			if( $_POST['PageBlocksModel']['open_date'] == '' )
			{
				$_POST['PageBlocksModel']['open_date'] = null;
			}
			if( $_POST['PageBlocksModel']['close_date'] == '' )
			{
				$_POST['PageBlocksModel']['close_date'] = null;
			}
			
			
			$model->attributes=$_POST['PageBlocksModel'];
			if($model->save())
			{
				//URLに飛ぶ
				$url = Yii::app()->createUrl(
							'PageBlocks/admin',
							array(
								'url' => $target['url'],
								'name' => $target['name'],
								'is_global' => $target['is_global'],
								'types' => $target['types'],
							)
					);
				$this->redirect($url);
			}
		}
		
		//アセットの処理
		$this->assets_helper->set_js( 'pageBlocks/page_blocks.js', 'end' );
		$this->assets_helper->add_js( '
			var baseUrl = "' . Yii::app()->baseUrl .'";
			var homeUrl = "' . Yii::app()->homeUrl .'";
		', 'end');
		
		$this->render('update',array(
			'model'=>$model,
			'target' => $target,
			'type' => $model->type,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		//リクエスト処理
		$target = array(
			'url' => Input::GetPost('url'),
			'name' => Input::GetPost('name'),
			'is_global' => Input::GetPost('is_global'),
			'types' => Input::GetPost('types'),
			'types_decode' => json_decode(Input::GetPost('types')),
		);
		
		//削除実行
		PageBlocksModel::model()->findByPk( $id )->delete();
		
		//順番替え
		$rows = PageBlocksModel::model()->findAll(
					'page_path = :page_path',
					array( ':page_path' =>  $target['url'] )
				);
		$i = 1;
		foreach( $rows as $row )
		{
			$row->rank = $i;
			$row->save();
			$i ++ ;
		}
		
		
		//URLに飛ぶ
		$url = Yii::app()->createUrl(
					'PageBlocks/admin',
					array(
						'url' => $target['url'],
						'name' => $target['name'],
						'is_global' => $target['is_global'],
						'types' => $target['types'],
					)
			);
		$this->redirect($url);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('PageBlocksModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * ページ別処理
	 */
	public function actionAdmin()
	{
		$model=new PageBlocksModel('search');
		$model->unsetAttributes();  // clear any default values


		//リクエスト処理
		$target = array(
			'url' => Input::Get('url'),
			'name' => Input::Get('name'),
			'is_global' => Input::Get('is_global'),
			'types' => Input::Get('types'),
			'types_decode' => json_decode(Input::Get('types')),
		);
		
		
		//検索用リクエスト処理
		$_GET['PageBlocksModel']['page_path'] = $target['url'];
		$_GET['PageBlocksModel']['name'] = $target['name'];
		
		if( $target['is_global'] == 1 )
		{
			$_GET['PageBlocksModel']['global'] = $target['is_global'];
			$_GET['PageBlocksModel']['page_path'] = null;
		}
		
		
		//検索内容チェック
		if(isset($_GET['PageBlocksModel']))
		{
			$model->attributes=$_GET['PageBlocksModel'];
		}
		
		
		
		//選択可能を設定
		$type_list = array();
		foreach( $target['types_decode'] as $type )
		{
			if( $type == 'html' )
			{
				$type_list['html'] = 'HTML入力';
			}
			if( $type == 'richText' )
			{
				$type_list['richText'] = 'WYSIWYG';
			}
			if( $type == 'image' )
			{
				$type_list['image'] = '画像';
			}
			if( $type == 'image_text' )
			{
				$type_list['image_text'] = '画像/テキスト';
			}
			if( $type == 'youtube' )
			{
				$type_list['youtube'] = 'youtube';
			}
		}
		
		
		//アセットの処理
		$this->assets_helper->set_js( 'pageBlocks/admin.js', 'end' );
		$this->assets_helper->add_js( '
			var baseUrl = "' . Yii::app()->baseUrl .'";
			var homeUrl = "' . Yii::app()->homeUrl .'";
		', 'end');
		
		
		$this->render('admin',array(
			'model'=>$model,
			'target' => $target,
			'type_list' => $type_list
		));
	}
	
	// ----------------------------------------------------
	/**
	 * 順番確定
	 */
	public function actionRankchange()
	{
		//リクエスト処理
		$target = array(
			'url' => Input::Post('url'),
			'name' => Input::Post('name'),
			'is_global' => Input::Post('is_global'),
			'types' => Input::Post('types'),
			'types_decode' => json_decode(Input::Post('types')),
		);
		
		$i = 1;
		foreach( Input::Post('id') as $id )
		{
			$model = PageBlocksModel::model()->findByPk($id);
			$model->rank = $i;
			$model->save();
			$i ++ ;
		}
		
		//URLに飛ぶ
		$url = Yii::app()->createUrl(
					'PageBlocks/admin',
					array(
						'url' => $target['url'],
						'name' => $target['name'],
						'is_global' => $target['is_global'],
						'types' => $target['types'],
					)
			);
		$this->redirect($url);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PageBlocksModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PageBlocksModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param PageBlocksModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='page-blocks-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
}
