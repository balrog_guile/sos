<?php
class PartnerController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'index','view', 'admin','delete', 'list' ),
				'users'=>array('@'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new PartnerModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['PartnerModel']))
		{
			$_POST['PartnerModel']['created_date'] = date('Y-m-d H:i:s');
			$_POST['PartnerModel']['code'] = '';
			$model->attributes=$_POST['PartnerModel'];
			if($model->save())
			{
				$model->code = $model->id;
				$model->save();
				//$this->redirect(array('view','id'=>$model->id));
				$this->redirect(array('admin'));
				return;
			}
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		
		if(isset($_POST['PartnerModel']))
		{
			$model->attributes=$_POST['PartnerModel'];
			if($model->save())
			{
				//$this->redirect(array('view','id'=>$model->id));
				$this->redirect(array('admin'));
			}
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		//$dataProvider=new CActiveDataProvider('PartnerModel');
		/*$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));\*/
		$this->redirect(array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PartnerModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PartnerModel']))
			$model->attributes=$_GET['PartnerModel'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PartnerModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PartnerModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param PartnerModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='partner-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
	
	/**
	 * 仕入先別仕入れリスト
	 * @param int $id
	 */
	public function actionList( $id = null )
	{
		$id = (int)$id;
		if( $id == 0 ){
			$this->redirect( array('admin') );
			return;
		}
		
		$data = array();
		
		$PartnerModel = new PartnerModel();
		$data['model'] = $PartnerModel;
		$data['target'] = $PartnerModel->find('id =:id', array( ':id' => $id ) );
		
		if( $data['target'] === null )
		{
			$this->redirect( array('admin') );
			return;
		}
		$data['id'] = $id;
		
		$data['stockingModel'] = new StockingModel;
		$this->render( 'list', $data );
	}
	// ----------------------------------------------------
}
