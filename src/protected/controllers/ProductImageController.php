<?php

class ProductImageController extends Controller
{
	public function actionIndex( $id, $type, $target = 2 )
	{
		//イメージサイズ
		$imageSizeList = Yii::app()->params['imageSizeList'];
		if(! isset( $imageSizeList[$type] ) )
		{
			//ビューの表示
			header("HTTP/1.1 404 Not Found");
			echo '';
			return;
		}
		
		
		//商品データ
		$ProductsModel = new ProductsModel();
		$product = $ProductsModel->find( 'id =:id', array( ':id' => $id ) );
		if(is_null($product))
		{
			//ビューの表示
			header("HTTP/1.1 404 Not Found");
			echo '';
			return;
		}
		
		//画像パス
		$target_path = '';
		switch( $target )
		{
			case 2:
				$target_path = $product->thumbnail;
				break;
			
			case 1:
			default:
				$target_path = $product->main_image;
				break;
		}
		
		//パスインフォをとる
		$target_path = YiiBase::getPathOfAlias('webroot') . '/'. $target_path;
		$pathinfo = pathinfo($target_path);
		$mtime = filemtime($target_path);
		$check_path = YiiBase::getPathOfAlias('webroot') . '/display_caches/'.
						$pathinfo['filename'] . '__' . $mtime . '.' . $pathinfo['extension'];
		
		//MIMEtype
		$mime =  CFileHelper::getMimeTypeByExtension( '.' . $pathinfo['extension'] );
		
		//ファイルがあるかチェック
		if(file_exists($check_path))
		{
			header( 'Content-type: ' . $mime );
			echo file_get_contents($check_path);
			return;
		}
		
		//ファイルがなければリサイズ
		$original = imagecreatefromjpeg($target_path);
		$x = imagesx($original);
		$y = imagesy($original);
		echo $x;
		
		
		
		
	}

	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
}