<?php

class RegisterController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('view', 'pdf'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('index', 'create','update','admin','delete', 'checkregistered', 'exec_payment' ),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array(),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 商品登録のありなしをチェック
	 */
	public function actionCheckregistered()
	{
		$sku_list = Input::Get( 'sku_list' );
		$skuModel = new ProductSkuModel();
		
		$i = 0;
		$return = array();
		foreach( $sku_list as $one )
		{
			$sku = $skuModel->find(
					' id = :id ',
					array(
						':id' => $one
					)
				);
			
			//登録が消えている
			if(is_null($sku))
			{
				$return[$one] = -1;
			}
			//販売禁止
			else if( $sku->delete_flag == 1)
			{
				$return[$one] = 0;
			}
			else
			{
				$return[$one] = 1;
			}
			
			$i ++ ;
		}
		
		echo json_encode( $return );
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い実行
	 */
	public function actionExec_payment()
	{
		$id = Input::GetPost('payment_id');
		$payment = Input::GetPost('payment');
		$payment_method_on = Input::GetPost('payment_method_on');
		
		$data = array();
		
		//モデルを用意
		$ReceiptModel = new ReceiptModel();
		$ReceiptDetailModel = new ReceiptDetailModel();
		$ProductsModel = new ProductsModel();
		$ProductSkuModel = new ProductSkuModel();
		
		
		//IDから明細を取得（在庫管理部分だけ）
		$receipt = $ReceiptModel->find( 'id = :id', array( ':id' => $id ) );
		$list = array();
		foreach( $receipt->detail as $detail )
		{
			if( (int)$detail->sku_id > 0 )
			{
				if( isset($list[ $detail->sku_id ]) )
				{
					$list[ $detail->sku_id ] += $detail->qty ;
				}
				else
				{
					$list[ $detail->sku_id ] = $detail->qty ;
				}
			}
		}
		
		//トランザクションをスタートしてチェック
		$connection = Yii::app()->db;
		$transaction = $connection->beginTransaction();
		
		//在庫処理
		$res = $ProductSkuModel->regi_zaiko2( $connection, $transaction, $list );
		$session = $res;
		
		//トランザクションステータスチェック
		if( $res['truns_error'] === TRUE )
		{
			$transaction->rollback();
		}
		//エラーチェック
		else if( $res['result'] === FALSE )
		{
			$transaction->rollback();
		}
		//問題なし
		else
		{
			//セーブデータの記録
			$receipt->change_val = (int)$payment - $receipt->grand_total_tax;
			$receipt->payment = (int)$payment;
			$receipt->order_status = 2;
			$receipt->payment_date = date( 'Y-m-d H:i:s' );
			$receipt->save();
			
			//データをコミット
			$transaction->commit();
		}
		
		
		//セッションにデータを保存
		Yii::app()->session['regi_exec_data'] = $session;
		
		//リダイレクト
		$this->redirect( Yii::app()->createUrl( 'register/index', array('id' => $id) ) );
		
	}
	// ----------------------------------------------------
	
	/**
	 * PDF用
	 */
	public function actionPdf( $id )
	{
		$model = new ReceiptModel();
		$record = $model->find( 'id = :id', array( ':id' => $id) );
		
		
		$this->renderPartial(
				'pef_rec',
				array(
					'receipt' => $record,
				)
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 顧客の来店履歴からお会計へ
	 * @param int $id 
	 */
	public function actionHistory2register( $customer_id, $comming_id)
	{
		$model = new ReceiptModel();
		$record = $model->find( '( customer_id = :customer_id ) AND ( comming_id = :comming_id )', array( 'customer_id' => $customer_id, 'comming_id' => $comming_id ) );
		
		if(is_null($record))
		{
			$model->customer_id = $customer_id;
			$model->comming_id = $comming_id;
			$model->create_date = date('Y-m-d H:i:s');
			$model->update_date = date('Y-m-d H:i:s');
			$model->save();
			$id = $model->id;
		}
		else
		{
			$id = $record->id;
		}
		
		$this->redirect( $this->createUrl('register/index', array( 'id' => $id ) ) );
	}
	
	// ----------------------------------------------------
	
	/**
	 * レジスターモード
	 */
	public function actionIndex( $id = 0 )
	{
		//IM設定
		$ds = array(
			array(
				'name' => 'receipt',
				'records' => 1,
				'paging' => FALSE,
				'key' => 'id',
				'sort' => array(
					array(
						'field' => 'create_date',
						'direction' => 'DESC'
					)
				)
			),
			
			//明細行
			array(
				'name' => 'receipt_detail',
				'key' => 'id',
				'relation' => array(
					array(
						'foreign-key' => 'receipt_id',
						'join-field' => 'id',
						'operator' => '='
					)
				),
				'repeat-control' => 'insert delete',
				'script' => array(
					array(
						'db-operation' => 'delete',
					)
				),
			),
			
			
			//来店
			array(
				'name' => 'comming',
				'key' => 'id',
				'relation' => array(
					array(
						'foreign-key' => 'id',
						'join-field' => 'comming_id',
						'operator' => '='
					)
				),
			),
			
			//商品
			array(
				'name' => 'products',
				'key' => 'id',
			),
			
			
			//商品SKU
			array(
				'name' => 'product_sku',
				'key' => 'id',
			),
			
		);
		
		//指定表示
		if((int)$id > 0 )
		{
			$to_newest = true;
			$ds[0]['query'] = array(array(
				'field' => 'id',
				'value' => (int)$id,
				'operator' => '='
			));
		}
		//最新表示
		else
		{
			$ds[0]['query'] = array(
				array(
					'field' => 'order_type',
					'value' => '0',
					'operator' => '='
				)
			);
			$to_newest = false;
		}
		
		
		//IMのURL発行
		$def = array();
		$ds2 = array();
		$url = ImHelper::getUrl(
				$ds,
				$def,
				$ds2
		);
		
		
		//実行ステータスセッションのデータ
		$regi_exec_data = Yii::app()->session['regi_exec_data'];
		if( is_null($regi_exec_data) )
		{
			$regi_exec_data = array(
				'result' => true,
				'truns_error' => false,
				'errors' => array(),
				'set' => false,
			);
		}
		else
		{
			$regi_exec_data['set'] = true;
			Yii::app()->session['regi_exec_data'] = null;
		}
		
		
		//アセットのアサイン
		$asset = new AssetsHelper( 'ControllerJS' );
		$asset->set_js_raw( $url, 'end' );
		$asset->set_js( 'register/index.js', 'end' );
		$asset->set_js( 'IM_helper.js', 'end' );
		$asset->add_js('
			var set_new_url = "' . $this->createUrl('register/setnew') . '";
			var history_url = "' . $this->createUrl('register/admin') . '";
			var comming_url = "' . Yii::app()->createUrl('comming/latest') . '";
			var to_newest_url = "' . Yii::app()->createUrl('register') . '";
			var search_url = "' . Yii::app()->createUrl('product/product/regiview') . '";
			var sku_url = "' . Yii::app()->createUrl('product/sku/getsku') . '";
			var zaikodown_url = "' . Yii::app()->createUrl('product/sku/regi_zaiko') . '";
			var receipt_url = "' . Yii::app()->getBaseUrl(true)  . Yii::app()->createUrl('register/pdf') . '";
			var checkregistered = "' . Yii::app()->getBaseUrl(true)  . Yii::app()->createUrl('register/checkregistered') . '";
			var tax_rate = ' . EcFunctions::getTaxRate() . ';
		', 'end' );
		
		/**
		$this->renderFile(
			YiiBase::getPathOfAlias( 'application.views.register' ) . '/index.php',
			array(
				'to_newest' => $to_newest
			));
		*/
		$this->render(
			'index',
			array(
				'to_newest' => $to_newest,
				'regi_exec_data' => $regi_exec_data,
			)
		);
		
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * 新規レコード作成
	 */
	public function actionSetnew()
	{
		$model = new ReceiptModel();
		$model->create_date = date( 'Y-m-d H:i:s' );
		$model->update_date = date( 'Y-m-d H:i:s' );
		$res = $model->save();
		$this->redirect( Yii::app()->createUrl( 'register') );
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ReceiptModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ReceiptModel']))
		{
			$model->attributes=$_POST['ReceiptModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ReceiptModel']))
		{
			$model->attributes=$_POST['ReceiptModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	/*
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ReceiptModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	*/
	
	// ----------------------------------------------------
	
	/**
	 * 売上管理
	 */
	public function actionAdmin()
	{
		$model=new ReceiptModel('search');
		$model->unsetAttributes();
		
		
		//項目があれば。
		if(isset($_GET['ReceiptModel']))
		{
			$model->attributes = $_GET['ReceiptModel'];
		}
		else
		{
			//指定がなければ日付だけ今月の支払い済みのデータのみを対象にする
			$arr = array(
				'payment_date_from' => date( 'Y-m-01' ),
				'payment_date_to' => date( 'Y-m-t' ),
				'order_status' => 2,
			);
			$model->attributes = $arr;
			
		}
		
		//ユーザーモデル
		$user = new User;
		$userList = VarHelper::createOptions($user->getUsers(), 'username');
		
		
		////合計計算
		$calc = array();
		
		//件数
		$criteria = $model->getStasticscriteria();
		$calc['count'] = $model->count( $criteria );
		
		//商品合計
		$criteria2 = $model->getStasticscriteria();
		$criteria2->select = 'SUM( item_total ) as item_total_sum';
		$row = $model->find( $criteria2 );
		$calc['item_total_sum'] = $row->item_total_sum;
		
		//総合計
		$criteria3 = $model->getStasticscriteria();
		$criteria3->select = 'SUM( grand_total ) as grand_total_sum';
		$row = $model->find( $criteria3 );
		$calc['grand_total_sum'] = $row->grand_total_sum;
		
		
		
		$this->render('admin',array(
			'model' => $model,
			'users' => $userList,
			'calc' => $calc,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ReceiptModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ReceiptModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ReceiptModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='receipt-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
