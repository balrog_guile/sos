<?php

class ShopController extends Controller
{
    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
    public $layout='//layouts/column2';
    
    
    // ----------------------------------------------------
    /**
     * @return array action filters
     */
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
            //'postOnly + delete', // we only allow deletion via POST request
        );
    }
    
    
    // ----------------------------------------------------
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(
                    'index','view', 'admin','delete', 'create','update',
                    'create_dir','contents'
            ),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }
    
    // ----------------------------------------------------
    
    /**
     * Displays a particular model.
     * @param integer $id the ID of the model to be displayed
     */
    public function actionView($id)
    {
        $this->render('view',array(
            'model'=>$this->loadModel($id),
        ));
    }
    
    // ----------------------------------------------------
    /**
     * ディレクトリ作成
     */
    public function actionCreate_dir( $id )
    {
        $shop = ShopModel::model()->findByPk($id);
        if( $shop === null )
        {
            $this->redirect(array('admin'));
            return;
        }
        
        //ターゲットパス
        $path = YiiBase::getPathOfAlias('webroot.ec_theme.'.$shop->shop_code);
        if(! is_dir($path) )
        {
            mkdir($path);
        }
        
        
        //各言語用
        foreach( Yii::app()->params['languageList'] as $lan )
        {
            $path2 = $path . '/' .$lan;
            if(! is_dir($path2) )
            {
                mkdir($path2);
            }
        }
        
        Yii::app()->user->setFlash('message', "ディレクトリ準備をしました");
        $this->redirect(array('admin'));
    }
    
    // ----------------------------------------------------
    /**
     * コンテンツ管理
     * @param $id
     */
    public function actionContents( $id )
    {
        $data = array();
        
		//店舗管理者の場合はIDを固定
		$user = UsersModel::model()->findByPk( Yii::app()->user->id );
		if($user->shop_id > 0){
			$id = $user->shop_id;
		}

        //対象ショップのデータを抜き出す
        $model = ShopContentsModel::model()->find(array(
            'condition' => 'shop_id = :shop_id',
            'params' => array(
                ':shop_id' => $id
            )
        ));
        //ない
        if( $model === null )
        {
            $model = new ShopContentsModel;
			$content_id = null;
        }else{
			$content_id = $model->id;
		}
		
		if(isset($_POST['ShopContentsModel']))
		{
			$model->attributes = $_POST['ShopContentsModel'];
			$model->update_date = date('Y-m-d H:i:s');
			if(!is_null($content_id)){
				$model->id = $content_id;
			}else{
				$model->shop_id = $id;
				$model->create_date = date('Y-m-d H:i:s');
			}
			$model->save();
			$data['msg'] = '更新しました';
		}
		
        $data['model'] = $model;
		$data['user'] = $user;
        
        //ショップデータ
        $data['shop'] = ShopModel::model()->findByPk( $id );
        
        
        
        $this->render( 'contents', $data );
    }
    
    
    // ----------------------------------------------------
    
    /**
     * Creates a new model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function actionCreate()
    {
        $model=new ShopModel;
        
        if(isset($_POST['ShopModel']))
        {
            $_POST['ShopModel']['update_date'] = date('Y-m-d H:i:s');
            $model->attributes = $_POST['ShopModel'];
            if($model->validate())
            {
                //ショップコードを発行
                $model->shop_code = sha1( rand(10000,99999) . '--' . time() );
                $model->code = sha1( rand(10000,99999) . '--' . time() );
                
                //パスワードハッシュを作成
                $model->pass_hash = sha1( rand(0,1000) . '--' . time() );
                $model->password = sha1( $model->pass_hash . '_' . $model->password );
                
                
                if( $model->save() )
                {
                    //ユーザー
                    $user = new RegistrationForm;
                    $user->noCaptcha = true;
                    $user->username = $model->login_id;
                    $user->email = $model->mailaddress;
                    $user->email = $model->mailaddress;
                    $user->status = User::STATUS_ACTIVE;
                    $user->shop_id = $model->id;
                    $user->activkey=UserModule::encrypting(microtime().$_POST['ShopModel']['password']);
                    $user->password=UserModule::encrypting($_POST['ShopModel']['password']);
                    $user->verifyPassword=UserModule::encrypting($_POST['ShopModel']['password']);
                    $res = $user->save();
                    
                    
                    //ユーザープロフィール
                    $profile = new Profile;
                    $profile->regMode = true;
                    $profile->attributes = array();
                    $profile->user_id = $user->id;
                    $res = $profile->save();
                    
                    
                    Yii::app()->user->setFlash('message', "ユーザーを追加しました");
                    $this->redirect(array('admin'));
                }
            }
        }
        
        $this->render('create',array(
            'model'=>$model,
        ));
    }
    
    // ----------------------------------------------------
    
    /**
     * Updates a particular model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id the ID of the model to be updated
     */
    public function actionUpdate($id)
    {
        $model=$this->loadModel($id);

        // Uncomment the following line if AJAX validation is needed
        // $this->performAjaxValidation($model);

        if(isset($_POST['ShopModel']))
        {
            $model->attributes=$_POST['ShopModel'];
            if($model->save())
                $this->redirect(array('view','id'=>$model->id));
        }

        $this->render('update',array(
            'model'=>$model,
        ));
    }
    
    // ----------------------------------------------------
    
    /**
     * Deletes a particular model.
     * If deletion is successful, the browser will be redirected to the 'admin' page.
     * @param integer $id the ID of the model to be deleted
     */
    public function actionDelete($id)
    {
        //削除の前にユーザー削除
        $user = User::model()->find('shop_id=:id',array(':id' => $id));
        if(!is_null($user))
        {
            Profile::model()->find('user_id=:id',array(':id' => $user->id))->delete();
            $user->delete();
        }
        
        //本丸削除
        $this->loadModel($id)->delete();
        
        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(!isset($_GET['ajax']))
            $this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
    }
    
    // ----------------------------------------------------
    
    /**
     * Lists all models.
     */
    public function actionIndex()
    {
        $this->redirect(array('admin'));
        return;
    }
    
    // ----------------------------------------------------
    
    /**
     * Manages all models.
     */
    public function actionAdmin()
    {
        $model=new ShopModel('search');
        $model->unsetAttributes();  // clear any default values
        if(isset($_GET['ShopModel']))
            $model->attributes=$_GET['ShopModel'];
        
        
        $message = Yii::app()->user->getFlash('message');
        
        $this->render('admin',array(
            'model'=>$model,
            'message' => $message,
        ));
    }
    
    
    // ----------------------------------------------------
    /**
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer $id the ID of the model to be loaded
     * @return ShopModel the loaded model
     * @throws CHttpException
     */
    public function loadModel($id)
    {
        $model=ShopModel::model()->findByPk($id);
        if($model===null)
            throw new CHttpException(404,'The requested page does not exist.');
        return $model;
    }

    /**
     * Performs the AJAX validation.
     * @param ShopModel $model the model to be validated
     */
    protected function performAjaxValidation($model)
    {
        if(isset($_POST['ajax']) && $_POST['ajax']==='shop-model-form')
        {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }
}
