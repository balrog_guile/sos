<?php

class Shop_post_contentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow authenticated user to perform 'create' and 'update' actions
                'actions'=>array(
                    'index','view', 'admin','delete', 'create','update',
            ),
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ShopPostContentModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ShopPostContentModel']))
		{
			$user = UsersModel::model()->findByPk( Yii::app()->user->id );
			$model->attributes=$_POST['ShopPostContentModel'];
			$model->shop_id = $user->shop_id;
			$model->create_date = date('Y-m-d H:i:s');
			$model->update_date = date('Y-m-d H:i:s');
			if($model->validate()){
				if($model->save()){
					Yii::app()->user->setFlash('message', "投稿を追加しました");
					$this->redirect(array('admin'));
				}
			}
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ShopPostContentModel']))
		{
			$model->attributes=$_POST['ShopPostContentModel'];
			$model->id = $id;
			$model->update_date = date('Y-m-d H:i:s');
			if($model->validate()){
				if($model->save()){
					Yii::app()->user->setFlash('message', "投稿を更新しました");
					$this->redirect(array('admin'));
				}
			}
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $this->redirect( $this->createUrl('shop_post_content/admin') );
        return;
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ShopPostContentModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ShopPostContentModel']))
			$model->attributes=$_GET['ShopPostContentModel'];

        $message = Yii::app()->user->getFlash('message');
        
        $this->render('admin',array(
            'model'=>$model,
            'message' => $message,
        ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ShopPostContentModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ShopPostContentModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param ShopPostContentModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='shop-post-content-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
