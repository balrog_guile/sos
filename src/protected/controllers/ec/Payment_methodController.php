<?php

class Payment_methodController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	
	// ----------------------------------------------------
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * 登録
	 */
	public function actionCreate()
	{
		$model=new PaymentMethodModel;
		
		if(isset($_POST['PaymentMethodModel']))
		{
			$model->attributes=$_POST['PaymentMethodModel'];
			$model->create_date = date('Y-m-d H:i:s');
			$model->update_date = date('Y-m-d H:i:s');
			
			$criteria = new CDbCriteria();
			$criteria->select = 'max(rank) as rank_max';
			$rank = PaymentMethodModel::model()->find($criteria);
			$model->rank = ( $rank->rank_max + 1 );
			
			if($model->save())
			{
				$this->redirect(array('admin'));
			}
		}
		
		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * 編集
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		if(isset($_POST['PaymentMethodModel']))
		{
			$model->attributes=$_POST['PaymentMethodModel'];
			$model->update_date = date('Y-m-d H:i:s');
			if($model->save())
			{
				$this->redirect(array('admin'));
			}
		}
		
		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();
		
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->redirect(array('admin'));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new PaymentMethodModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['PaymentMethodModel']))
			$model->attributes=$_GET['PaymentMethodModel'];
		
		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return PaymentMethodModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=PaymentMethodModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
	// ----------------------------------------------------
	/**
	 * Performs the AJAX validation.
	 * @param PaymentMethodModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='payment-method-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	// ----------------------------------------------------
}
