<?php

class SkuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	// ----------------------------------------------------
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('gencode', 'stockMod'),
				'users'=>array('*'),
			),
			
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','index','view','admin','delete', 'getsku', 'regi_zaiko'),
				'users'=>array('@'),
			),
			/*
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			*/
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 指定親IDのリストを取得
	 */
	public function actionGetsku()
	{
		$parent_id = Input::GetPost( 'parent_id' );
		$model=new ProductSkuModel;
		$datas = $model->findAll(array(
						'condition' => 'product_id = :product_id',
						'order' => 'rank ASC',
						'params' => array( 'product_id' => $parent_id ),
					));
		
		//var_dump( $datas );
		
		
		$this->renderPartial(
				'getsku',
				array(
					'datas' => $datas
				)
			);
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new ProductSkuModel;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProductSkuModel']))
		{
			$model->attributes=$_POST['ProductSkuModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['ProductSkuModel']))
		{
			$model->attributes=$_POST['ProductSkuModel'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('ProductSkuModel');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new ProductSkuModel('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['ProductSkuModel']))
			$model->attributes=$_GET['ProductSkuModel'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return ProductSkuModel the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=ProductSkuModel::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	// ----------------------------------------------------
	
	/**
	 * Performs the AJAX validation.
	 * @param ProductSkuModel $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='product-sku-model-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	// ----------------------------------------------------
	/**
	 * バーコードが重複してないかチェック
	 */
	public function checkcode($code)
	{
		$model = new ProductSkuModel;
		$datas = $model->find(array(
						'condition' => 'code_str = :code_str',
						'params' => array( 'code_str' => $code ),
					));
		return (($datas) ? false : true);
	}

	// ----------------------------------------------------
	/**
	 * バーコードを生成
	 */
	public function actionGencode()
	{
		do {
			$code = md5(uniqid(rand(),1));
			$code = substr($code, 0, 16);
		} while(!$cc = $this->checkCode($code));

		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($code);
		Yii::app()->end();
	}

	// ----------------------------------------------------
	/**
	 * ajaxで在庫を変更, 返り値ステータスjson
	 * 必須のパラメータは、(String)$pid, (String)$sid, (String)$stock
	 * @return json
	 */
	public function actionStockMod()
	{
		$model = new ProductSkuModel;
		$result = array();
		$params = array(
			'pid' => input::GetPost('pid'),
			'sid' => input::GetPost('sid'),
		);
		$stock = (String)input::GetPost('stock');

		// pid, sidパラメータがあるか判定
		if(empty($params['pid']) or empty($params['sid'])){
			$result = array('result' => false, 'error' => 400);

		// stockの先頭が+-数値判定
		}elseif(!preg_match('/[\+\-0-9]/', $stock[0], $matches)){
			$result = array('result' => false, 'error' => 400);

		// 条件に合う場合に、opt, stockをparamsへ登録後、modelのstockModメソッド実行
		}else{
			switch($matches[0])
			{
				case '+':
				$params['opt'] = true;
				$params['stock'] = substr($stock, 1);
				break;
				case '-':
				$params['opt'] = false;
				$params['stock'] = substr($stock, 1);
				break;
				default:
				$params['opt'] = null;
				$params['stock'] = $stock;
			}
			$result = $model->stockMod($params);
		}
		
		// JSON出力
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($result);
		Yii::app()->end();
	}
	
	// ----------------------------------------------------
	
	/**
	 * 在庫編集
	 */
	public function actionRegi_zaiko()
	{
		$datas = Input::GetPost('regi_zaiko');
		$model = new ProductSkuModel();
		$res = $model->regi_zaiko( $datas );
		
		header("Content-Type: application/json; charset=utf-8");
		echo json_encode($res);
		
		return;
	}
	
	// ----------------------------------------------------
}
