<?php
/* =============================================================================
 * ショップAPI
 * ========================================================================== */
class Shop_orderController extends Controller
{
    // ----------------------------------------------------
    /**
     * 仮状態に追加
     */
    public function actionAdd_Temp_Order()
    {
        $sku_id = (int)Input::GetPost('skuId');
        $qty = (int)Input::GetPost('qty');
        
        $model = new TempOrderModel();
        
        $result = array(
            'result' => false,
            'items' => array()
        );
        
        
        //SKU取得してなければアウト
        $sku = ProductSkuModel::model()->findByPk( $sku_id );
        if( (is_null($sku) )||( $qty == 0 ))
        {
        }
        else
        {
            $model->addSession( $sku, $qty );
            $result['result'] = true;
        }
        
        $result['items'] = $model->getTempOrder();
        echo json_encode( $result );
        
        
        return;
    }
    
    // ----------------------------------------------------
    /**
     * 削除する
     */
    public function actionDelete_Temp_Order()
    {
        $key = Input::GetPost('key');
        if( $key == '' ){
            $key = '01010101';
        }
        $model = new TempOrderModel();
        $model->deleteTempOrder( $key );
        $result['items'] = $model->getTempOrder();
        echo json_encode( $result );
    }
    
    
    // ----------------------------------------------------
    /**
     * オーダーを追加
     */
    public function actionOrder_exec()
    {
        $seatNumber = Input::GetPost('seat_number');
        $model = new TempOrderModel();
        $model->execOrder( $seatNumber );
        echo json_encode( true );
    }
    
    
    // ----------------------------------------------------
}