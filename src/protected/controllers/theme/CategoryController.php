<?php
/* ============================================================================
 * カテゴリ関連
 * ========================================================================== */
class CategoryController extends Controller
{
    // ----------------------------------------------------
    /**
     * カテゴリページ
     */
    public function actionIndex()
    {
        $data = array();
        
        //共通ビュー処理
        Yii::app()->Cms->setCommonDataForViewData( $data );
        
        
        //データ処理
        $model = new ProductsModel;
        
        
        //URI処理
        $param = $this->procSegment();
        
        //criteria
        $criteria = $model->getCriteria( $param );
        
        //データプロバイダ
        $dataProvider = $model->getDataProvider( $criteria, 50 );
        
        //カテゴリ情報取得
        $categoryLinkUrl = preg_replace( '/\.html$/', '', Yii::app()->Cms->segment[1] );
        $category = CategoryModel::model()->find(
            'link_url = :link_url',
            array(
                ':link_url' => $categoryLinkUrl
            )
        );
        if( is_null($category) && isset( $param['id'] ) )
        {
            $category = CategoryModel::model()->findByPk($param['id']);
        }
        $data['category'] = $category;
        
        //データ統合
        $data['model'] = $model;
        $data['dataProvider'] = $dataProvider;
        
        
        
        //ファイルパス処理
        $filePath = Yii::app()->Cms->getThemeDirectory() . 'category/index.php';
        $this->renderFile( $filePath, $data );
    }
    
    
    
    // ----------------------------------------------------
    /**
     * セグメント処理
     */
    protected function procSegment()
    {
        $segment = Yii::app()->Cms->segment;
        array_shift($segment);
        
        $list = array();
        
        //処理分割
        $len = count( $segment );
        $i = 0;
        while( $i < $len )
        {
            $name = $segment[$i];
            $i ++;
            if( isset($segment[$i]) )
            {
                $list[$name] = $segment[$i];
            }
            else
            {
                $list[$name] = '';
            }
            $i ++;
        }
        
        return $list;
    }
    
    
    // ----------------------------------------------------
    /**
     * 子IDを探す
     * @param array $idList
     * @
     */
    protected function searchChildren( $idLsit )
    {
        
    }
    
    // ----------------------------------------------------
}