<?php
/* ============================================================================
 * 通常ページ
 * ========================================================================== */
class PagesController extends Controller
{
    // ----------------------------------------------------
    /**
     * ページ処理
     */
    public function actionIndex()
    {
        //var_dump(Yii::app()->Cms->segment);
        //var_dump($data);
        $data = array();
        
        //ログインしていなければログインへ
        $member_id = Yii::app()->session['member_id'];
        if(empty($member_id)){
            $sos_theme = Yii::app()->request->getQuery('SOS_THEME_SELECT');
            if($sos_theme){
                Yii::app()->session['sos_theme'] = Yii::app()->request->getQuery('SOS_THEME_SELECT');
            }
            $this->redirect( Yii::app()->createUrl('member/login') );
        }
        
        //共通ビュー処理
        Yii::app()->Cms->setCommonDataForViewData( $data );
        
        //ファイル処理
        $segment = Yii::app()->Cms->segment;
        if( preg_match( '/\.html$/', $segment[count($segment)-1] ) )
        {
            $segment[count($segment)-1] = preg_replace('/\.html$/', '.php', $segment[count($segment)-1] );
        }
        else
        {
            $segment[] = 'index.php';
        }
        
        
        //ファイルの有り無し判断
        $filePath = Yii::app()->Cms->getThemeDirectory() . implode( '/', $segment );
        
        
        
        //ファイルがなければ処理404
        if(!file_exists($filePath))
        {
            Yii::app()->Cms->errorDisplay( '404', 'ページが見つかりません' );
            return;
        }
        
        $this->renderFile( $filePath, $data );
    }
    
    // ----------------------------------------------------
    
    
}