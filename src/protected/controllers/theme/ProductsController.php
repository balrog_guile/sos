<?php
/* ============================================================================
 * 製品ページ処理
 * ========================================================================== */
class ProductsController extends Controller
{
	// ----------------------------------------------------
	/**
	 * 商品ページ
	 */
	public function actionProducts()
	{
		$data = array();
		
		//共通ビュー処理
		Yii::app()->Cms->setCommonDataForViewData( $data );
		
		
		//商品データを取得
		$segment = Yii::app()->Cms->segment;
		$id = $segment[count($segment)-1];
		$id = preg_replace( '/\.html$/', '', $id );
		$data['product'] = ProductsModel::model()->find(array(
			'condition' => 'link_url = :link_url',
			'params' => array(
				':link_url' => $id
			)
		));
		if( $data['product'] === null )
		{
			$data['product'] = ProductsModel::model()->findByPk( $id );
		}
		
		
		
		//商品がなければパス
		if(is_null($data['product']))
		{
			Yii::app()->Cms->errorDisplay( '404', '商品が見つかりません' );
			return;
		}
		
		
		//ファイルパス処理
		$filePath = Yii::app()->Cms->getProductPageViewPath( $data['product'] );
		if( $filePath === false )
		{
			Yii::app()->Cms->errorDisplay( '404', 'テンプレートファイルがありません。' );
			return;
		}
		
		
		$this->renderFile( $filePath, $data );
	}
	
	// ----------------------------------------------------
	
	
}