<?php

/**
 * This is the model class for table "comming".
 *
 * The followings are the available columns in table 'comming':
 * @property string $id
 * @property string $name
 * @property string $comming_date
 * @property string $memo1
 * @property string $memo2
 * @property string $memo3
 * @property string $update_date
 * @property string $staff_id
 * @property int $cusomer_id
 */
class CommingModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'comming';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'length', 'max'=>255),
			array('staff_id', 'length', 'max'=>10),
			array('comming_date, memo1, memo2, memo3, update_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, comming_date, memo1, memo2, memo3, update_date, staff_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => '件名',
			'comming_date' => '来店日時',
			'memo1' => 'メモ1',
			'memo2' => 'メモ2',
			'memo3' => 'メモ3',
			'update_date' => 'Update Date',
			'staff_id' => '対応スタッフid',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('comming_date',$this->comming_date,true);
		$criteria->compare('memo1',$this->memo1,true);
		$criteria->compare('memo2',$this->memo2,true);
		$criteria->compare('memo3',$this->memo3,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('staff_id',$this->staff_id,true);
		$criteria->order = 'comming_date DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 顧客別検索
	 * @param int $id
	 * @return object
	 */
	public function getCustoemrSearch( $id )
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('comming_date',$this->comming_date,true);
		$criteria->compare('memo1',$this->memo1,true);
		$criteria->compare('memo2',$this->memo2,true);
		$criteria->compare('memo3',$this->memo3,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('staff_id',$this->staff_id,true);
		$criteria->order = 'comming_date DESC';
		$criteria->condition = 'cusomer_id = :cusomer_id';
		$criteria->params = array( 'cusomer_id' => $id );
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CommingModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
}
