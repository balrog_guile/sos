<?php

/**
 * This is the model class for table "customer_profile_field".
 *
 * The followings are the available columns in table 'customer_profile_field':
 * @property string $id
 * @property string $field_name
 * @property integer $field_type
 * @property integer $input_type
 * @property string $selecte_column
 * @property string $html_class
 * @property string $html_id
 * @property integer $search_flag
 * @property integer $view_display
 * @property string $customer_profile_fieldcol
 * @property integer $required
 * @property string $min_length
 * @property string $max_length
 * @property string $regrex
 * @property string $default_value
 * @property string $other_validation
 */
class CustomerProfileFieldModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer_profile_field';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('field_name', 'required'),
			array('field_type, input_type, search_flag, view_display, required', 'numerical', 'integerOnly'=>true),
			array('field_name, customer_profile_fieldcol', 'length', 'max'=>45),
			array('html_class, html_id, regrex', 'length', 'max'=>125),
			array('min_length, max_length', 'length', 'max'=>10),
			array('selecte_column, default_value, other_validation', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, field_name, field_type, input_type, selecte_column, html_class, html_id, search_flag, view_display, customer_profile_fieldcol, required, min_length, max_length, regrex, default_value, other_validation', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'field_name' => 'Field Name',
			'field_type' => 'データ型
0:INT
1:FLOAT
2:TEXT
3:LONGTEXT
4:DATE
5:TIME
6:DATETIME
7:FILE(未実装)
',
			'input_type' => '入力欄タイプ
0:テキスト
1:テキストエリア
2:チェックボックス
3:ラジオボタン
4:セレクトボックス',
			'selecte_column' => '入力項目',
			'html_class' => '出力時振るHTMLCLASS',
			'html_id' => '出力時振るHTMLID',
			'search_flag' => '検索対象にするか？',
			'view_display' => '一覧表示の対象？
0:表示しない
1:表示する',
			'customer_profile_fieldcol' => 'Customer Profile Fieldcol',
			'required' => '必須条項か',
			'min_length' => '最短文字数',
			'max_length' => '最大長',
			'regrex' => '正規表現データ',
			'default_value' => '初期値',
			'other_validation' => 'そのほかのバリーションを実行するか？(yiiでの指定の必要性）',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('field_name',$this->field_name,true);
		$criteria->compare('field_type',$this->field_type);
		$criteria->compare('input_type',$this->input_type);
		$criteria->compare('selecte_column',$this->selecte_column,true);
		$criteria->compare('html_class',$this->html_class,true);
		$criteria->compare('html_id',$this->html_id,true);
		$criteria->compare('search_flag',$this->search_flag);
		$criteria->compare('view_display',$this->view_display);
		$criteria->compare('customer_profile_fieldcol',$this->customer_profile_fieldcol,true);
		$criteria->compare('required',$this->required);
		$criteria->compare('min_length',$this->min_length,true);
		$criteria->compare('max_length',$this->max_length,true);
		$criteria->compare('regrex',$this->regrex,true);
		$criteria->compare('default_value',$this->default_value,true);
		$criteria->compare('other_validation',$this->other_validation,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomerProfileFieldModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
