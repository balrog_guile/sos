<?php

/**
 * This is the model class for table "customers".
 *
 * The followings are the available columns in table 'customers':
 * @property string $id
 * @property string $name1
 * @property string $name2
 * @property string $kana1
 * @property string $kana2
 * @property string $mailaddress
 * @property integer $customer_status
 * @property string $create_date
 * @property string $update_date
 * @property string $company_name
 * @property string $tel
 * @property string $tel2
 * @property string $fax
 * @property string $password_confirm
 */
class CustomersModel extends CActiveRecord
{
	//追加フィールド
	public $password_confirm;
	
	
	
	// ----------------------------------------------------
	/**
	 * テーブル名
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{customers}}';
	}
	
	
	// ----------------------------------------------------
	/**
	 * ルール
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		$rules = array(
			array('name1, name2, kana1, kana2, mailaddress, zip, pref, address_1, address_2, tel', 'required'),
			array('name1, name2, kana1, kana2', 'length', 'max'=>125),
			array('mailaddress', 'length', 'max'=>255),
			array( 'fax', 'safe' ),
			array('address_3, password_hash, create_date, update_date, authentication_code, time_limit, customer_status, new_mailaddress', 'safe'),
			
			array( 'tel2', 'safe' ),
			array( 'fax', 'safe' ),
			array( 'company_name', 'safe' ),
			array( 'password_confirm', 'safe' ),
			
			
			array('id, name1, name2, kana1, kana2, mailaddress, customer_status, create_date, update_date', 'safe', 'on'=>'search'),
			array('id, name1, name2, kana1, kana2, mailaddress, customer_status, create_date, update_date', 'safe', 'on'=>'search_one'),
		);
		
		
		
		if( $this->getScenario() == 'edit' )
		{
			$rules[] = array('password','safe');
		}
		else
		{
			$rules[] = array( 'password', 'compare', 'compareAttribute'=>'password_confirm' );
			$rules[] = array( 'password', 'required' );
			$rules[] = array( 'mailaddress', 'unique' );
		}
		
		
		return $rules;
	}
	
	
	// ----------------------------------------------------
	/**
	 * リレーション
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}
	
	
	
	// ----------------------------------------------------
	/**
	 * ラベル
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'name1' => '姓',
			'name2' => '名',
			'kana1' => '姓（かな）',
			'kana2' => '名（かな）',
			'mailaddress' => 'メールアドレス',
			'new_mailaddress' => '更新用メールアドレス',
			'password' => 'パスワード',
			'zip' => '郵便番号',
			'pref' => '都道府県',
			'address_1' => '市区町村',
			'address_2' => '町域番地',
			'address_3' => 'マンション名など',
			'tel' => '電話番号',
			'tel2' => '電話番号2',
			'fax' => 'FAX',
			'company_name' => '会社名など',
			'password_confirm' => 'パスワード再入力',
			'authentication_code' => '承認番号'
		);
	}
	
	
	// ----------------------------------------------------
	/**
	 * 重複されてないかどうか
	 */
	public function checkOverlap($attribute,$params)
	{
		$model[$attribute] = self::model()->count( $attribute . ' = :' . $attribute , array( ':' . $attribute  => $this->{$attribute} ) );
		
		//データがすでにあるとき
		if( $model[$attribute] > 0 )
		{
			$this->addError( $attribute, $this->{$attribute} . 'はすでに登録されています。' );
			return;
		}
		
		return;
	}
	
	
	// ----------------------------------------------------
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		
		$criteria=new CDbCriteria;
		
		$criteria->compare('id',$this->id,true);
		$criteria->compare('name1',$this->name1,true);
		$criteria->compare('name2',$this->name2,true);
		$criteria->compare('kana1',$this->kana1,true);
		$criteria->compare('kana2',$this->kana2,true);
		$criteria->compare('mailaddress',$this->mailaddress,true);
		$criteria->compare('customer_status',$this->customer_status);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function search_one()
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	// ----------------------------------------------------
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return CustomersModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	
	
	// ----------------------------------------------------
}
