<?php

/**
 * This is the model class for table "information".
 *
 * The followings are the available columns in table 'information':
 * @property string $id
 * @property string $subject
 * @property string $create_date
 * @property string $update_date
 * @property string $display_date
 * @property string $begin_date
 * @property string $end_date
 * @property integer $open_status
 * @property string $description
 * @property string $contents
 */
class InformationModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'information';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('subject, create_date, update_date, display_date', 'required'),
			array('open_status', 'numerical', 'integerOnly'=>true),
			array('subject, description', 'length', 'max'=>255),
			array('begin_date, end_date, contents', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, subject, create_date, update_date, display_date, begin_date, end_date, open_status, description, contents', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'subject' => 'タイトル',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'display_date' => '表示する日付',
			'begin_date' => '表示開始日',
			'end_date' => '表示終了日',
			'open_status' => '公開ステータス
-1:ゴミ箱
0:非公開
1:公開',
			'description' => '概要',
			'contents' => '本文',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('subject',$this->subject,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('display_date',$this->display_date,true);
		$criteria->compare('begin_date',$this->begin_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('open_status',$this->open_status);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('contents',$this->contents,true);
		$criteria->order = 'display_date DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return InformationModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	/**
	 * 公開ステータスリスト
	 */
	public static function getStatusList()
	{
		return array(
			'1' => '公開',
			'0' => '非公開',
			'-1' => 'ゴミ箱',
		);
	}
	
	// ----------------------------------------------------
}
