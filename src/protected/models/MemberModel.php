<?php

/**
 * This is the model class for table "member".
 *
 * The followings are the available columns in table 'member':
 * @property string $id
 * @property string $login_id
 * @property string $password
 * @property string $password_hash
 * @property string $email
 * @property integer $member_status
 * @property string $create_at
 * @property string $update_at
 * @property string $last_login_time
 * @property string $leave_at
 * @property string $activate_hash
 * @property string $activate_expire
 * @property string $company
 * @property string $name1
 * @property string $name2
 * @property string $kana1
 * @property string $kana2
 * @property string $zip
 * @property string $pref
 * @property string $address1
 * @property string $address2
 * @property string $address3
 * @property string $tel1
 * @property string $tel2
 * @property string $fax
 * @property string $memo
 * @property string $password_confirm
 * @property string $mailaddress
 */
class MemberModel extends CActiveRecord
{
	public  $password_confirm;


	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'member';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		$return = array(
			array('password, password_hash, member_status, create_at, update_at, name2, tel1', 'required'),
			array('member_status', 'numerical', 'integerOnly'=>true),
			array('login_id', 'length', 'max'=>255),
			array('password, password_hash, email, activate_hash', 'length', 'max'=>512),
			array('name2', 'length', 'max'=>256),
			array('tel1', 'length', 'max'=>45),
			array('last_login_time, leave_at, activate_expire, memo', 'safe'),
			
			//ログインID追加
			//array( 'login_id', 'unique' ),
			array( 'login_id', 'safe' ),
			
			//メール追加の設定
			array( 'email', 'unique' ),
			array( 'email', 'email' ),
			
			//パスワード一致チェック
			array( 'password_confirm', 'safe' ),
			
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, login_id, password, password_hash, email, member_status, create_at, update_at, last_login_time, leave_at, activate_hash, activate_expire, company, name1, name2, kana1, kana2, zip, pref, address1, address2, address3, tel1, tel2, fax, memo', 'safe', 'on'=>'search'),
		);
		
		
		//
		if( $this->getScenario() != 'activate' )
		{
			$return[] = array( 'password', 'compare', 'compareAttribute'=>'password_confirm' );
		}
		
		
		
		return $return;
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login_id' => 'ログインID',
			'password' => 'パスワード',
			'password_hash' => 'パスワードハッシュ',
			'email' => 'メールアドレス',
			'member_status' => '会員ステータス',
			'create_at' => '登録日',
			'update_at' => '更新日',
			'last_login_time' => '最終ログイン時間',
			'leave_at' => '退会時間',
			'activate_hash' => '承認番号認証',
			'activate_expire' => 'アクティベート有効期限',
			'company' => '会社名・屋号',
			'name1' => '姓',
			'name2' => '名',
			'kana1' => 'かな(姓)',
			'kana2' => 'かな(名)',
			'zip' => '郵便番号',
			'pref' => '都道府県',
			'address1' => '市区町村',
			'address2' => '町域番地',
			'address3' => 'マンション名など',
			'tel1' => '電話番号',
			'tel2' => '電話番号2',
			'fax' => 'FAX',
			'memo' => '運営利用メモ',
			'password_confirm' => 'パスワード再入力'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('login_id',$this->login_id,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('password_hash',$this->password_hash,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('member_status',$this->member_status);
		$criteria->compare('create_at',$this->create_at,true);
		$criteria->compare('update_at',$this->update_at,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('leave_at',$this->leave_at,true);
		$criteria->compare('activate_hash',$this->activate_hash,true);
		$criteria->compare('activate_expire',$this->activate_expire,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('name1',$this->name1,true);
		$criteria->compare('name2',$this->name2,true);
		$criteria->compare('kana1',$this->kana1,true);
		$criteria->compare('kana2',$this->kana2,true);
		$criteria->compare('zip',$this->zip,true);
		$criteria->compare('pref',$this->pref,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('address3',$this->address3,true);
		$criteria->compare('tel1',$this->tel1,true);
		$criteria->compare('tel2',$this->tel2,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('memo',$this->memo,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MemberModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
