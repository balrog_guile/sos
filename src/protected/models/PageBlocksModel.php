<?php

/**
 * This is the model class for table "page_blocks".
 *
 * The followings are the available columns in table 'page_blocks':
 * @property string $id
 * @property string $name
 * @property string $page_path
 * @property integer $global
 * @property string $rank
 * @property string $open_date
 * @property string $close_date
 * @property string $create_date
 * @property string $update_date
 * @property string $image_path
 * @property string $content
 */
class PageBlocksModel extends CActiveRecord
{
	public $max;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page_blocks';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, create_date, update_date, type', 'required'),
			array('global', 'numerical', 'integerOnly'=>true),
			array('name, page_path, image_path', 'length', 'max'=>255),
			array('rank', 'length', 'max'=>10),
			array('open_date, close_date, content, link_url, image_path, alt, open_status', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('open_status, type, id, name, page_path, global, rank, open_date, close_date, create_date, update_date, image_path, content', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'ページブロックの名前（日本語OK!）',
			'page_path' => 'ページパス（グローバルならなし）',
			'global' => '全ページ共通なら1',
			'rank' => '表示順',
			'open_date' => '表示開始日時',
			'close_date' => '表示終了日',
			'create_date' => '作成日',
			'update_date' => '編集日時',
			'image_path' => '画像パス',
			'content' => '内容',
			'type' => 'データタイプ',
			'open_status' => '公開'
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;
		$criteria->compare('name',$this->name);
		$criteria->compare('page_path',$this->page_path);
		$criteria->compare('global',$this->global);
		$criteria->order = 'rank';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PageBlocksModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
