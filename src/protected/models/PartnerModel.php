<?php

/**
 * This is the model class for table "partner".
 *
 * The followings are the available columns in table 'partner':
 * @property string $id
 * @property string $name
 * @property string $code
 * @property string $created_date
 * @property integer $partner_status
 */
class PartnerModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('partner_status', 'numerical', 'integerOnly'=>true),
			array('name, code', 'length', 'max'=>255),
			array('created_date, code', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, code, created_date, partner_status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//仕入れリスト
			'stocking' => array(
				self::HAS_MANY,
				'StockingModel',
				'partner_id',
			),
			
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => '取引先名',
			'code' => '取引先コード',
			'created_date' => '作成日',
			'partner_status' => '取引ステータス',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('partner_status',$this->partner_status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PartnerModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	/**
	 * 取引ステータスリスト
	 */
	public static function getStatusList( $undef = TRUE )
	{
		$list = array();
		if( $undef === TRUE )
		{
			$list[''] = '未選択';
		}
		$list['1'] = '取引中';
		$list['0'] = '商談中';
		$list['-1'] = '取引停止';
		return $list;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 登録された業者を取得
	 */
	public static function getAllListDropDown( $undef = TRUE )
	{
		$list = array();
		$list[''] = '未選択';
		$model = new PartnerModel();
		foreach( $model->findAll() as $one )
		{
			$list[$one->id] = $one->id . ':' .$one->name;
		}
		return $list;
	}
	// ----------------------------------------------------
	
	/**
	 * 指定パートナー
	 * @param int $id
	 * @return object
	 */
	public static function getPartner( $id )
	{
		$PartnerModel = new PartnerModel();
		return $PartnerModel->find( 'id = :id', array( ':id' => $id ));
	}
	
	// ----------------------------------------------------
	
}
