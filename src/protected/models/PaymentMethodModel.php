<?php

/**
 * This is the model class for table "payment_method".
 *
 * The followings are the available columns in table 'payment_method':
 * @property string $id
 * @property string $name
 * @property string $create_date
 * @property string $update_date
 * @property string $rank
 * @property integer $usage
 * @property integer $delete_flag
 * @property integer $shipment_type
 * @property string $method
 * @property string $json_data
 * @propertu int $rank_max
 */
class PaymentMethodModel extends CActiveRecord
{	
	public $rank_max;
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'payment_method';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, create_date, update_date, method, json_data', 'required'),
			array('usage, delete_flag, shipment_type', 'numerical', 'integerOnly'=>true),
			array('name, method', 'length', 'max'=>512),
			array('rank', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, create_date, update_date, rank, usage, delete_flag, shipment_type, method, json_data', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => '支払い方法名',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'rank' => '並び順',
			'usage' => '利用中か',
			'delete_flag' => '削除フラグ',
			'shipment_type' => '配送料金体型
1 一律
2 都道府県別',
			'method' => '計算実行メソッド',
			'json_data' => 'JSONデータ',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('rank',$this->rank,true);
		$criteria->compare('usage',$this->usage);
		$criteria->compare('delete_flag',$this->delete_flag);
		$criteria->compare('shipment_type',$this->shipment_type);
		$criteria->compare('method',$this->method,true);
		$criteria->compare('json_data',$this->json_data,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PaymentMethodModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
