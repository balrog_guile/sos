<?php

/**
 * This is the model class for table "peer_connects".
 *
 * The followings are the available columns in table 'peer_connects':
 * @property integer $id
 * @property string $peer_id
 * @property string $shop_id
 * @property integer $status
 * @property string $url
 * @property string $connect_date
 * @property string $disconnect_date
 */
class PeerConnectsModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'peer_connects';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('status', 'numerical', 'integerOnly'=>true),
			array('peer_id, url', 'length', 'max'=>255),
			array('shop_id', 'length', 'max'=>45),
			array('connect_date, disconnect_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, peer_id, shop_id, status, url, connect_date, disconnect_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'peer_id' => 'Peer',
			'shop_id' => 'Shop',
			'status' => 'Status',
			'url' => 'Url',
			'connect_date' => 'Connect Date',
			'disconnect_date' => 'Disconnect Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('peer_id',$this->peer_id,true);
		$criteria->compare('shop_id',$this->shop_id,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('connect_date',$this->connect_date,true);
		$criteria->compare('disconnect_date',$this->disconnect_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PeerConnectsModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
