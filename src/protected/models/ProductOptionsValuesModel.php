<?php

/**
 * This is the model class for table "product_options_values".
 *
 * The followings are the available columns in table 'product_options_values':
 * @property string $id
 * @property string $option_id
 * @property string $label
 * @property string $value
 * @property string $rank
 * @property integer $specific_price
 * @property integer $add_price
 * @property string $validation
 * @property string $create_date
 * @property string $update_date
 */
class ProductOptionsValuesModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'product_options_values';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('option_id, create_date, update_date', 'required'),
			array('specific_price, add_price', 'numerical', 'integerOnly'=>true),
			array('option_id, rank', 'length', 'max'=>10),
			array('label, value, validation', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, option_id, label, value, rank, specific_price, add_price, validation, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'option_id' => 'オプションのID',
			'label' => '表示ラベル',
			'value' => '値',
			'rank' => '表示順',
			'specific_price' => '金額特定をする場合の金額',
			'add_price' => '金額修正がある場合',
			'validation' => 'Validation',
			'create_date' => '作成日',
			'update_date' => '更新日',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('option_id',$this->option_id,true);
		$criteria->compare('label',$this->label,true);
		$criteria->compare('value',$this->value,true);
		$criteria->compare('rank',$this->rank,true);
		$criteria->compare('specific_price',$this->specific_price);
		$criteria->compare('add_price',$this->add_price);
		$criteria->compare('validation',$this->validation,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ProductOptionsValuesModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
