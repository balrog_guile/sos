<?php

/**
 * This is the model class for table "receipt_detail".
 *
 * The followings are the available columns in table 'receipt_detail':
 * @property string $id
 * @property string $receipt_id
 * @property string $produt_id
 * @property string $item_id
 * @property string $item_name
 * @property string $remark
 * @property string $price
 * @property string $qty
 * @property string $sub_total
 * @property string $create_date
 * @property string $update_date
 */
class ReceiptDetailModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'receipt_detail';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('receipt_id', 'required'),
			array('receipt_id, produt_id, price, qty, sub_total', 'length', 'max'=>10),
			array('item_id', 'length', 'max'=>125),
			array('item_name, remark', 'length', 'max'=>225),
			array('create_date, update_date,sku_id', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, receipt_id, produt_id, item_id, item_name, remark, price, qty, sub_total, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'receipt_id' => 'レシートID',
			'produt_id' => '商品テーブルのID',
			'item_id' => '商品番号',
			'item_name' => '商品名',
			'remark' => '備考',
			'price' => '単価',
			'qty' => '数量',
			'sub_total' => '小計',
			'create_date' => '作成日',
			'update_date' => '変更日',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('receipt_id',$this->receipt_id,true);
		$criteria->compare('produt_id',$this->produt_id,true);
		$criteria->compare('item_id',$this->item_id,true);
		$criteria->compare('item_name',$this->item_name,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('qty',$this->qty,true);
		$criteria->compare('sub_total',$this->sub_total,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReceiptDetailModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
