<?php

/**
 * This is the model class for table "receipt".
 *
 * The followings are the available columns in table 'receipt':
 * @property string $id
 * @property string $customer_id
 * @property string $id_hash
 * @property string $receipt_name
 * @property string $item_total
 * @property string $tax
 * @property string $grand_total
 * @property string $payment
 * @property string $change_val
 * @property integer $order_status
 * @property integer $deliv_status
 * @property integer $received
 * @property string $deliv_code
 * @property string $create_date
 * @property string $update_date
 * @property string $staff_id
 */
class ReceiptModel extends CActiveRecord
{
	//追加プロパティ
	public $create_date_from = null;
	public $create_date_to = null;
	public $payment_date_from = null;
	public $payment_date_to = null;
	public $item_total_sum = null;
	public $grand_total_sum = null;
	
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'receipt';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('', 'required'),
			array('order_status, deliv_status, received', 'numerical', 'integerOnly'=>true),
			array('customer_id, item_total, tax, grand_total, payment, change_val, staff_id', 'length', 'max'=>10),
			array('id_hash', 'length', 'max'=>125),
			array('receipt_name', 'length', 'max'=>256),
			array('deliv_code', 'length', 'max'=>225),
			array('create_date, update_date', 'safe'),
			
			
			array('order_type,order_history_id,payment_date', 'safe'),
			
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('create_date_from, create_date_to, payment_date_from, payment_date_to, id, customer_id, id_hash, receipt_name, item_total, tax, grand_total, payment, change_val, order_status, deliv_status, received, deliv_code, create_date, update_date, staff_id', 'safe', 'on'=>'search'),
			array('create_date_from, create_date_to, payment_date_from, payment_date_to, id, customer_id, id_hash, receipt_name, item_total, tax, grand_total, payment, change_val, order_status, deliv_status, received, deliv_code, create_date, update_date, staff_id', 'safe', 'on'=>'search2'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//明細行
			'detail' => array(
				self::HAS_MANY,
				'ReceiptDetailModel',
				'receipt_id'
			),
			
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'customer_id' => 'お客様ID',
			'id_hash' => 'コードIDのハッシュ',
			'receipt_name' => '案件名',
			'item_total' => '商品合計',
			'tax' => '税額',
			'grand_total' => 'お支払合計',
			'payment' => 'お支払い金額',
			'change_val' => '釣り銭',
			'order_status' => '注文ステータス',
			'deliv_status' => '発送ステータス',
			'received' => '受取フラグ',
			'deliv_code' => '配送伝票コード',
			'create_date' => '注文伝票作成日',
			'update_date' => '変更日',
			'staff_id' => '担当スタッフID',
			'payment_date' => '支払い日時',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('customer_id',$this->customer_id,true);
		$criteria->compare('id_hash',$this->id_hash,true);
		$criteria->compare('receipt_name',$this->receipt_name,true);
		$criteria->compare('item_total',$this->item_total,true);
		$criteria->compare('tax',$this->tax,true);
		$criteria->compare('grand_total',$this->grand_total,true);
		$criteria->compare('payment',$this->payment,true);
		$criteria->compare('change_val',$this->change_val,true);
		$criteria->compare('order_status',$this->order_status);
		$criteria->compare('deliv_status',$this->deliv_status);
		$criteria->compare('received',$this->received);
		$criteria->compare('deliv_code',$this->deliv_code,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('staff_id',$this->staff_id,true);
		$criteria->order = 'create_date DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 売上検索フィルター
	 * @return \CActiveDataProvider
	 */
	
	public function search2()
	{
		$criteria = $this->getStasticscriteria();
		$criteria->order = 'create_date DESC';
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 合計検索用条件を定義
	 */
	public function getStasticscriteria()
	{
		$criteria=new CDbCriteria;
		
		////日付検索条件
		if( $this->create_date_from != '' )
		{
			$criteria->compare( 'create_date', '>= '.$this->create_date_from );
		}
		if( $this->create_date_to != '' )
		{
			$criteria->compare(
					'create_date',
					'< ' . 
					date( 'Y-m-d H:i:s', strtotime( '+1 day', strtotime($this->create_date_to) ) )
				);
		}
		
		if( $this->payment_date_from != '' )
		{
			$criteria->compare( 'payment_date', '>= '.$this->payment_date_from );
		}
		if( $this->payment_date_to != '' )
		{
			$criteria->compare(
					'payment_date',
					'< ' . 
					date( 'Y-m-d H:i:s', strtotime( '+1 day', strtotime($this->payment_date_to) ) )
				);
		}
		
		
		$criteria->compare('customer_id',$this->customer_id,true);
		$criteria->compare('receipt_name',$this->receipt_name,true);
		$criteria->compare('grand_total',$this->grand_total,true);
		$criteria->compare('order_status',$this->order_status);
		
		return $criteria;
	}
	// ----------------------------------------------------
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ReceiptModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
