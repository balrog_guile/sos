<?php

/**
 * This is the model class for table "rel_option_sku".
 *
 * The followings are the available columns in table 'rel_option_sku':
 * @property string $product_id
 * @property integer $sku_id
 */
class RelOptionSkuModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rel_option_sku';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('product_id, sku_id', 'required'),
			array('sku_id', 'numerical', 'integerOnly'=>true),
			array('product_id', 'length', 'max'=>10),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('product_id, sku_id', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * リレーション
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
			
			//SKU
			'sku' => array(
				self::HAS_ONE,
				'ProductSkuModel',
				array( 'id' => 'sku_id' )
			),
			
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'product_id' => '商品のID',
			'sku_id' => 'SKUのID',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('product_id',$this->product_id,true);
		$criteria->compare('sku_id',$this->sku_id);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RelOptionSkuModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
