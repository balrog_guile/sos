<?php

/**
 * This is the model class for table "shop_post_content".
 *
 * The followings are the available columns in table 'shop_post_content':
 * @property string $id
 * @property string $shop_id
 * @property integer $status
 * @property string $subjct
 * @property string $content
 * @property string $publish_date
 * @property string $create_date
 * @property string $update_date
 * @property integer $deleted_flag
 * @property string $deleted_date
 * @property string $reject_date
 * @property string $reject_reason
 */
class ShopPostContentModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'shop_post_content';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('shop_id, content, create_date, update_date', 'required'),
			array('status, deleted_flag', 'numerical', 'integerOnly'=>true),
			array('shop_id', 'length', 'max'=>10),
			array('subjct', 'length', 'max'=>512),
			array('publish_date, deleted_date, reject_date, reject_reason', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, shop_id, status, subjct, content, publish_date, create_date, update_date, deleted_flag, deleted_date, reject_date, reject_reason', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'shop_id' => 'Shop',
			'status' => '投稿ステータス',
			'subjct' => 'タイトル',
			'content' => '本文',
			'publish_date' => '公開日',
			'create_date' => '作成日',
			'update_date' => '更新日',
			'deleted_flag' => '削除フラグ',
			'deleted_date' => '削除日時',
			'reject_date' => 'リジェクトした日付',
			'reject_reason' => 'リジェクト理由',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		
		$user = UsersModel::model()->findByPk( Yii::app()->user->id );

		$criteria->compare('id',$this->id,true);
		$criteria->compare('shop_id',$user->shop_id);
		$criteria->compare('status',$this->status);
		$criteria->compare('subjct',$this->subjct,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('publish_date',$this->publish_date,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);
		$criteria->compare('deleted_flag',$this->deleted_flag);
		$criteria->compare('deleted_date',$this->deleted_date,true);
		$criteria->compare('reject_date',$this->reject_date,true);
		$criteria->compare('reject_reason',$this->reject_reason,true);
		$criteria->order = 'publish_date DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ShopPostContentModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
