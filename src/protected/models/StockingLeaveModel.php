<?php

/**
 * This is the model class for table "stocking_leave".
 *
 * The followings are the available columns in table 'stocking_leave':
 * @property string $id
 * @property string $stocking_id
 * @property string $leaving_date
 * @property string $leaving_count
 * @property integer $leaving_status
 * @property string $canseled_date
 */
class StockingLeaveModel extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stocking_leave';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('stocking_id, leaving_date, leaving_count', 'required'),
			array('leaving_status', 'numerical', 'integerOnly'=>true),
			array('stocking_id, leaving_count', 'length', 'max'=>10),
			array('canseled_date', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, stocking_id, leaving_date, leaving_count, leaving_status, canseled_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'stocking_id' => '入庫ID',
			'leaving_date' => '出庫日',
			'leaving_count' => '出庫個数',
			'leaving_status' => '出庫ステータス
1:出庫
-1:キャンセル',
			'canseled_date' => 'Canseled Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('stocking_id',$this->stocking_id,true);
		$criteria->compare('leaving_date',$this->leaving_date,true);
		$criteria->compare('leaving_count',$this->leaving_count,true);
		$criteria->compare('leaving_status',$this->leaving_status);
		$criteria->compare('canseled_date',$this->canseled_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StockingLeaveModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function leaveskusearch( $stocking_id  )
	{
		$criteria = new CDbCriteria;
		$criteria->compare( 'stocking_id', $stocking_id, false );
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort'=>array(
				'defaultOrder'=>'leaving_date DESC',
			),
		));
	}
	
	// ----------------------------------------------------
}
