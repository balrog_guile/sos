<?php

/**
 * This is the model class for table "stocking".
 *
 * The followings are the available columns in table 'stocking':
 * @property string $id
 * @property string $sku_id
 * @property string $insert_date
 * @property string $purchase_price
 * @property string $leaving_date
 * @property string $leaving_qty
 */
class StockingModel extends CActiveRecord
{
	// ----------------------------------------------------
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'stocking';
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('sku_id, purchase_qty, insert_date, purchase_price', 'required'),
			array('sku_id, purchase_price, leaving_qty', 'length', 'max'=>10),
			array('leaving_date, memo,sub_total', 'safe'),
			array('purchase_qty, purchase_price', 'numerical' ),
			array( 'partner_id', 'safe' ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, sku_id, insert_date, purchase_price, leaving_date, leaving_qty', 'safe', 'on'=>'search'),
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//SKU
			'sku' => array( self::BELONGS_TO, 'ProductSkuModel', 'sku_id' ),
			
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'sku_id' => 'SKUのID',
			'insert_date' => '仕入れ日',
			'purchase_price' => '仕入れ金額',
			'purchase_qty' => '仕入れ数',
			'leaving_date' => '出庫日',
			'leaving_qty' => '出庫個数',
			'memo' => '仕入れメモ',
			'sub_total' => '仕入れ額小計',
			'partner_id' => '仕入業者',
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('sku_id',$this->sku_id,true);
		$criteria->compare('insert_date',$this->insert_date,true);
		$criteria->compare('purchase_price',$this->purchase_price,true);
		$criteria->compare('leaving_date',$this->leaving_date,true);
		$criteria->compare('leaving_qty',$this->leaving_qty,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * SKUごとの仕入れ確認用
	 * @param int $sku_id
	 */
	public function stockingSearch( $sku_id )
	{
		$criteria=new CDbCriteria;
		$criteria->compare('id',$this->id,true);
		$criteria->compare('sku_id',$this->sku_id,true);
		$criteria->compare('insert_date',$this->insert_date,true);
		$criteria->compare('purchase_price',$this->purchase_price,true);
		$criteria->compare('leaving_date',$this->leaving_date,true);
		$criteria->compare('leaving_qty',$this->leaving_qty,true);
		
		$criteria->order = 'insert_date DESC';
		$criteria->condition = 'sku_id = :sku_id';
		$criteria->params = array( ':sku_id' => $sku_id );
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return StockingModel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 業者別仕入れリスト用
	 * @param int $partner_id
	 * @return object
	 */
	public function getSearchPartnerList( $partner_id )
	{
		$criteria=new CDbCriteria;
		$criteria->compare('partner_id', $partner_id );
		$criteria->order = 'insert_date DESC';
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	// ----------------------------------------------------
}
