<?php
/* =============================================================================
 * 仮オーダーモデル
 * ========================================================================== */
class TempOrderModel extends CFormModel
{
    //ショップID
    public $shopId = null;
    
    // ----------------------------------------------------
    /**
     * 初期化
     */
    public function init() {
        parent::init();
        
        if( Yii::app()->controller->shop !== null )
        {
            $this->shopId = Yii::app()->controller->shop->id;
        }
        else
        {
            $this->shopId = 0;
        }
        
        $this->initSession();
    }
    
    // ----------------------------------------------------
    /**
     * カートセッションを作成
     * @param boolean $force
     */
    public function initSession( $force = false )
    {
        $session = Yii::app()->session['cart'];
        if( is_null($session) )
        {
            Yii::app()->session['cart'] = array();
            $session = Yii::app()->session['cart'];
        }
        
        if( !isset($session[$this->shopId]) || $force === true )
        {
            $session[$this->shopId] = array(
                'items' => array()
            );
            Yii::app()->session['cart'] = $session;
        }
    }
    
    
    
    // ----------------------------------------------------
    /**
     * セッションデータ取得
     */
    public function getSession()
    {
        $return = array();
        
        
        
        
    }
    
    
    
    // ----------------------------------------------------
    /**
     * ルール
     */
    public function rules()
    {
        return array();
    }
    
    
    
    // ----------------------------------------------------
    /**
     * 仮追加
     * @param object $id
     * @param int $qtys
     */
    public function addSession( $sku, $qty )
    {
        $session = Yii::app()->session['cart'];
        
        $set = array(
            'product' => $sku->products->attributes,
            'sku' => $sku->attributes,
            'qty' => $qty
        );
        
        $key = sha1( uniqid( '', true ) );
        $session[$this->shopId]['items'][$key] = $set;
        Yii::app()->session['cart'] = $session;
        return;
    }
    
    
    // ----------------------------------------------------
    /**
     * 中に入っているものを取得
     */
    public function getTempOrder()
    {
        $session = Yii::app()->session['cart'];
        return $session[$this->shopId]['items'];
    }
    
    
    // ----------------------------------------------------
    /**
     * 削除
     * @param string $key nullなら全削除
     */
    public function deleteTempOrder( $key = null )
    {
        $session = Yii::app()->session['cart'];
        if( is_null($key) )
        {
            $session = Yii::app()->session['cart'];
            $session[$this->shopId]['items'] = array();
        }
        else
        {
            $session = Yii::app()->session['cart'];
            unset( $session[$this->shopId]['items'][$key] );
        }
        Yii::app()->session['cart'] = $session;
    }
    
    
    // ----------------------------------------------------
    /**
     * 確定
     */
    public function execOrder( $seatNumber )
    {
        $condition = array(
            'condition' => 'payment_status = 0 AND shop_id = :shop_id AND sesseion_id = :sesseion_id',
            'params' => array(
                ':shop_id' => $this->shopId,
                ':sesseion_id' => Yii::app()->session->getSessionID()
            )
        );
        $model = OrderHistoryModel::model()->find( $condition );
        
        //データがなければ作成
        if(is_null($model))
        {
             $model = new OrderHistoryModel();
             $baseSet = array(
                 'order_date' => date('Y-m-d H:i:s'),
                 'update_date' => date('Y-m-d H:i:s'),
                 'order_status' => 0,
                 'payment_status' => 0,
                 'item_total' => 0,
                 'payment_method' => 0,
                 'payment_fee' => 0,
                 'delivery_fee' => 0,
                 'grand_total' => 0,
                 'order_type' => 0,
                 'shop_id' => $this->shopId,
                 'sesseion_id' => Yii::app()->session->getSessionID()
             );
             $model->attributes = $baseSet;
             $model->save();
             //var_dump( $model->getErrors() );
        }
        
        
        $session = $session = Yii::app()->session['cart'];
        $items = $session[$this->shopId]['items'];
        
        foreach( $items as $key => $item )
        {
            $detail = new OrderDetailModel;
            $detailSet = array(
                'order_id' => $model->id,
                'product_id' => $item['product']['id'],
                'sku_id' => $item['sku']['id'],
                'item_id' => $item['product']['item_id'] . '-' . $item['sku']['brunch_item_id'],
                'name' => $item['product']['item_name'] . '-' . $item['sku']['brunch_item_name'],
                'price' => 0,
                'qty' => $item['qty'],
                'sub_total' => 0,
                'create_date' => date('Y-m-d H:i:s'),
                'seat_code' => $seatNumber
            );
            
            if( (int)$item['sku']['sale_price'] == 0 )
            {
                $detailSet['price'] = $item['sku']['price'];
                $detailSet['price'] = $detailSet['price']*$item['qty'];
            }
            else
            {
                $detailSet['price'] = $item['sku']['sale_price'];
                $detailSet['price'] = $detailSet['price']*$item['qty'];
            }
            
            $model->item_total += $detailSet['price'];
            $model->grand_total += $detailSet['price'];
            
            $detail->attributes = $detailSet;
            $detail->save();
        }
        
        $model->save();
        
        $this->initSession(true);
    }
    
    
    // ----------------------------------------------------
}