<?php
/* =============================================================================
 * カテゴリ一覧取得
 * ========================================================================== */
class shopCategoriesWidget extends CWidget
{
    //カテゴリリスト
    public $categoryList = array();
    
    //ショップ
    public $shop = null;

	//現在のカテゴリ
	public $currentId = null;

    // ----------------------------------------------------
    /**
     * 初期化
     */
    
    public function init()
    {
        $shop = Yii::app()->controller->shop;
        $this->shop = $shop;
        if(is_null($shop))
        {
            $this->categoryList = array();
            return;
        }
        $this->categoryList = $this->getCategories( $this->shop->id, 0 );
		
		// URLに /category/id/[id] という並びがあるかの判別
		$segments = Yii::app()->Cms->segment;
		foreach($segments as $k => $v){
			if($v != 'category'){
				continue;
			}
			if($segments[$k + 1] != 'id'){
				break;
			}
			$currentId = $segments[$k + 2];
			if(!$this->getCategory( $this->shop->id, $currentId )){
				break;
			}
			$this->currentId = $currentId;
			break;
		}
    }
    
    // ----------------------------------------------------
    /**
     * カテゴリ再帰取得用
     * @param int $shopId
     * @param int $parentId
     * @return array
     */
    public function getCategories( $shopId, $parenteId )
    {
        return CategoryModel::model()->findAll(array(
            'condition' => 'shop_id = :shop_id AND parent_id = :parent_id',
            'order' => 'rank ASC',
            'params' => array(
                ':shop_id' => $shopId,
                ':parent_id' => $parenteId,
            )
        ));
    }
	
	/*
	 * 1件取得
	 */
    public function getCategory( $shopId, $id )
    {
        return CategoryModel::model()->find(array(
            'condition' => 'shop_id = :shop_id AND id = :id',
            'params' => array(
                ':shop_id' => $shopId,
                ':id' => $id,
            )
        ));
    }
    
    // ----------------------------------------------------
    
    /**
     * 最終キャプチャ
     */
    
    public function run()
    {
    }
    
    // ----------------------------------------------------
    
    /**
     * カテゴリ用リンクを作成
     */
    public function getLinkUri( $model )
    {
        
        return Yii::app()->getBaseUrl( TRUE ) . '/category/id/' . $model->id;
    }
    
    // ----------------------------------------------------
    
}