<?php
/* =============================================================================
 * コンテンツ取得
 * ========================================================================== */
class shopContentsWidget extends CWidget
{
    //ショップ
    public $shop = null;

    //タイトル
    public $subject = null;

    //コンテンツ
    public $body = null;

    // ----------------------------------------------------
    /**
     * 初期化
     */
    
    public function init()
    {
        $shop = Yii::app()->controller->shop;
        $this->shop = $shop;
        if(is_null($shop))
        {
            return;
        }
        $contents = $this->getContents( $this->shop->id);
        
        if(!is_null($contents)){
            $this->subject = $contents->subject;
            $this->body    = $contents->body;
        }
    }
    
    // ----------------------------------------------------
    /*
     * 1件取得
     */
    public function getContents( $shopId )
    {
        return ShopContentsModel::model()->find(array(
            'condition' => 'shop_id = :shop_id',
            'params' => array(
                ':shop_id' => $shopId,
            )
        ));
    }
    
    // ----------------------------------------------------
    
    /**
     * 最終キャプチャ
     */
    
    public function run()
    {
    }
    
}