<?php
/* =============================================================================
 * 投稿取得
 * ========================================================================== */
class shopPostContentWidget extends CWidget
{
    //ショップ
    public $shop = null;

    //投稿一覧
    public $contentList = array();

    //表示件数
    public $limit = 3;

    // ----------------------------------------------------
    /**
     * 初期化
     */
    
    public function init()
    {
        $shop = Yii::app()->controller->shop;
        $this->shop = $shop;
        if(is_null($shop))
        {
            return;
        }
        $this->contentList = $this->getContents( $this->shop->id, $this->limit);
    }
    
    // ----------------------------------------------------
    /*
     * 1件取得
     */
    public function getContents( $shopId, $limit )
    {
        return ShopPostContentModel::model()->findAll(array(
            'condition' => 'shop_id = :shop_id',
            'params' => array(
                ':shop_id' => $shopId,
            ),
            'order' => 'publish_date desc',
            'limit' => $limit,
        ));
    }
    
    // ----------------------------------------------------
    
    /**
     * 最終キャプチャ
     */
    
    public function run()
    {
    }
    
}