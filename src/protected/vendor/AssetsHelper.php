<?php
/* =============================================================================
 * アセット処理用ヘルパ
 * ========================================================================== */
class AssetsHelper
{
	//アセットパス
	public $asset_path;
	
	//アセットURL
	protected $_assetsUrl;
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 * @param string $name
	 */
	public function __construct( $name )
	{
		$this->asset_path = $name;
	}
	
	// ----------------------------------------------------
	
	/**
	 * JSセット
	 * @param string $path
	 */
	public function set_js( $path, $position = 'head' )
	{
		$pos = '';
		switch( $position )
		{
			case 'head':
				$pos = CClientScript::POS_HEAD;
				break;
			
			case 'begin':
				$pos = CClientScript::POS_BEGIN;
				break;
			
			case 'end':
				$pos = CClientScript::POS_END;
				break;
			
		}
		Yii::app()->clientScript->registerScriptFile( $this->getAssetsUrl() . '/' . $path, $pos );
	}
	
	// ----------------------------------------------------
	
	/**
	 * JSセット（アセットパス解決なし）
	 * @param string $path
	 */
	public static function set_js_raw( $path, $position = 'head')
	{
		$pos = '';
		switch( $position )
		{
			case 'head':
				$pos = CClientScript::POS_HEAD;
				break;
			
			case 'begin':
				$pos = CClientScript::POS_BEGIN;
				break;
			
			case 'end':
				$pos = CClientScript::POS_END;
				break;
			
		}
		Yii::app()->clientScript->registerScriptFile( $path, $pos );
	}
	// ----------------------------------------------------
	
	/**
	 * スクリプト追加
	 * @param string $js
	 * @param int $position
	 */
	public function add_js( $js, $position = 'head' )
	{
		$pos = '';
		switch( $position )
		{
			case 'head':
				$pos = CClientScript::POS_HEAD;
				break;
			
			case 'begin':
				$pos = CClientScript::POS_BEGIN;
				break;
			
			case 'end':
				$pos = CClientScript::POS_END;
				break;
			
		}
		Yii::app()->clientScript->registerScript(
			'1',
			$js,
			$pos
		);
	}
	
	// ----------------------------------------------------
	
	/**
	 * CSSファイルの追加
	 * @parma string $path
	 */
	public function set_css( $path )
	{
		Yii::app()->clientScript->registerCssFile(
				$path
			);
	}
	
	// ----------------------------------------------------
	
	/**
	 * CSSコードの追加
	 */
	
	
	// ----------------------------------------------------
	
	/**
	 * アセットのパスを取得
	 */
	protected function getAssetsUrl()
	{
		if ($this->_assetsUrl !== null)
		{
			return $this->_assetsUrl;
		}
		else	
		{
			$assetsPath = Yii::getPathOfAlias('application.' . $this->asset_path );
			
			if (YII_DEBUG)
			{
				$assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, true);
			}
			else
			{
				$assetsUrl = Yii::app()->assetManager->publish($assetsPath);
			}
			return $this->_assetsUrl = $assetsUrl;
		}
	}
	
	// ----------------------------------------------------
	
}