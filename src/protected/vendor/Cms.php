<?php
/* =============================================================================
 * CMS制御クラス
 * ========================================================================== */
class Cms
{
    //処理済みのセグメントを保存
    public $segment = array();
    
    //言語
    public $language = 'ja';
    
    //テーマセットデータ
    public $theme = array(
        
        //デフォルトディレクトリ
        'defaultDirectory' => 'default',
        
        //テーマディレクトリ
        'themeDirectory' => '',
        
        //内部テーマディレクトリ
        'directoryPathInside' => '',
        
        //テーマディレクトリURL
        'directoryUrlPath' => '',
        
    );
    
    
    
    // ----------------------------------------------------
    /**
     * 初期化
     */
    public function init()
    {
        //ディレクトリ処理
        $baseUrl = Yii::app()->getBaseUrl(true);
        $basePath = Yii::getPathOfAlias('webroot.ec_theme');
        
        //本来はここでテーマ切り替えに対応
        
        
        //テーマ名などをデータに入れる
        if( $this->theme['themeDirectory'] == '' )
        {
            $this->theme['themeDirectory'] = Yii::app()->controller->DEFAULT_THEME_SET .'/'.$this->language;
        }
        $this->theme['directoryPathInside']
                = $basePath . '/' . $this->theme['themeDirectory'] . '/';
        
        $this->theme['directoryUrlPath']
                = $baseUrl . '/ec_theme/' . $this->theme['themeDirectory'] . '/';
        
    }
    
    
    // ----------------------------------------------------
    /**
     * コントローラーを取得
     */
    
    protected static function getController()
    {
        return Yii::app()->controller;
    }
    
    
    // ----------------------------------------------------
    /**
     * テーマディレクトリファイルパスを取得
     */
    public function getThemeDirectory()
    {
        return $this->theme['directoryPathInside'];
    }
    
    
    // ----------------------------------------------------
    /**
     * テーマディレクトリURLを取得
     */
    public function getThemeUri()
    {
        return $this->theme['directoryUrlPath'];
    }
    
    
    // ----------------------------------------------------
    /**
     * 変数に渡すように共通処理
     * @param array(ref) $data
     */
    public function setCommonDataForViewData( &$data )
    {
        //アプリケーションパス
        $data['templatePathInside'] = $this->getThemeDirectory();
        $data['templatePath'] = $this->getThemeUri();
        $data['webroot'] = Yii::app()->getBaseUrl(true).'/';
        
    }
    
    // ----------------------------------------------------
    /**
     * エラー処理
     * @param int $type errorstatus
     * @param string $errroMessage エラーメッセージ
     * 
     */
    public function errorDisplay( $type, $errorMessage )
    {
        $data = array();
        $data['errorMessage'] = $errorMessage;
        $this->setCommonDataForViewData($data);
        
        $filePath = Yii::app()->Cms->getThemeDirectory() . 'error/' . $type . '.php';
        
        //ファイルがなければyiiの処理
        if(!file_exists($filePath))
        {
            throw new CHttpException( $type,$errorMessage);
        }
        
        header("HTTP/1.1 $type");
        Yii::app()->controller->renderFile( $filePath, $data );
        
    }
    // ----------------------------------------------------
    /**
     * 商品ページ用テンプレート処理
     * @param object $model 商品Modelを渡すとデータベースに記録のテンプレートを取得
     * @return string 商品ページテンプレートパス
     */
    public function getProductPageViewPath( $model )
    {
        if( $model->template_file == '' )
        {
            $template = 'index.php';
        }
        else
        {
            $template = $model->template_file;
        }
        
        //パス処理
        $path = $this->getThemeDirectory() . 'item_pages/' . $template;
        if(!file_exists($path))
        {
            return false;
        }
        
        return $path;
    }
    
    // ----------------------------------------------------
    /**
     * URLルートを取得
     */
    static public function getUrlRoot()
    {
        return Yii::app()->getBaseUrl(true).'/';
    }
    
    // ----------------------------------------------------
    /**
     * 共通利用アセット呼び出し
     * @param string $type
     * @return string
     */
    static public function getCommonAssets( $type = 'hooterJs' )
    {
        $return = array();
        if( $type == 'hooterJsCart' )
        {
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/js/knockout.js' );
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/js/cart/tempOrderModel.js' );
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/js/cart.js' );
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/jqueryUi/jquery-ui.min.js' );
            $array = array(
                'addTempOrder' => Yii::app()->createUrl( 'shop_api/Shop_order/add_temp_order' ),
                'deleteTempOrder' => Yii::app()->createUrl('shop_api/Shop_order/delete_temp_order'),
                'orderExec' => Yii::app()->createUrl('shop_api/Shop_order/order_exec')
            );
            $return[] = CHtml::script( 'var APIURL = ' . json_encode($array) . ';' );
        }
        if( $type == 'hooterJs' )
        {
            
        }
        if( $type == 'headerCss' )
        {
            $return[] = CHtml::cssFile( self::getUrlRoot() . 'commonUsageAssets/jqueryUi/jquery-ui.min.css' );
            $return[] = CHtml::cssFile( self::getUrlRoot() . 'commonUsageAssets/jqueryUi/jquery-ui.structure.min.css' );
            $return[] = CHtml::cssFile( self::getUrlRoot() . 'commonUsageAssets/jqueryUi/jquery-ui.theme.min.css' );
        }
        
        
        if( $type == 'helpcenter' )
        {
            $return[] = CHtml::cssFile( self::getUrlRoot() . 'commonUsageAssets/css/helpcenter.css' );
        }
        
        
        if( $type == 'peerUserJs' )
        {
            $return[] = CHtml::scriptFile( 'https://skyway.io/dist/0.3/peer.js' );
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/js/peer/user.js' );
        }
        
        
        if( $type == 'transJs' )
        {
            $transDatas = array(
                'lang' => Yii::app()->controller->lang,
            );
            
            $return[] = '<script type="text/javascript">var transDatas = ' . json_encode($transDatas) . ';</script>';
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/js/parseuri.js' );
            $return[] = CHtml::scriptFile( self::getUrlRoot() . 'commonUsageAssets/js/trans.js' );
            $return[] = CHtml::scriptFile( '//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit' );
        }
        
        return implode( "\n", $return );
    }
    
    
    // ----------------------------------------------------
    static public function getCommonParts()
    {
        return self::getController()->renderFile('commonUsageAssets/html/helpcenter.php');
    }
    
    
    // ----------------------------------------------------
}