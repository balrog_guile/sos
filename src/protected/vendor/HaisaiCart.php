<?php
/* =============================================================================
 * ハイサイカート操作クラス
 * ========================================================================== */
class HaisaiCart
{
	//在庫判定用
	public $zaiko = array();
	
	// ----------------------------------------------------
	
	/**
	 * コンストラクタ
	 */
	public function __construct()
	{
		$this->init();
	}
	
	// ----------------------------------------------------
	
	/**
	 * カート初期化
	 */
	public function init( $force = FALSE )
	{
		$data = Yii::app()->session['cart'];
		if( $force === TRUE )
		{
			$data = null;
		}
		if(!is_array($data))
		{
			$data = array(
				'items' => array(),
				'payment_method' => array(),
				'info' => array(),
				'other_data' => array(),
				'error_message' => '',//専用エラーメッセージ
			);
			Yii::app()->session['cart'] = $data;
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 専用エラーメッセージの格納
	 * @param string $message
	 */
	public function setError( $message )
	{
		$data = Yii::app()->session['cart'];
		$data['error_message'] = $message;
		Yii::app()->session['cart'] = $data;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 専用エラーメッセージの取得
	 * @param type $delete trueならメッセージ削除
	 * @return type 
	 */
	
	public function getError( $delete = TRUE )
	{
		$data = Yii::app()->session['cart'];
		$return = $data['error_message'];
		
		if( $delete === TRUE )
		{
			$data['error_message'] = '';
		Yii::app()->session['cart'] = $data;
		}
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い方法リストを取得
	 */
	public function getPaymentList()
	{
		$payment_method = PaymentMethodModel::model()->findAll(array(
			'condition' => 'delete_flag = 0 AND `usage` =  1',
			'order' => 'rank ASC'
		));
		$payment_method_values = array();
		foreach( $payment_method as $row )
		{
			$payment_method_values[$row->id] = $row->name;
		}
		return array(
			'rows' => $payment_method,
			'values' => $payment_method_values,
		);
	}
	
	// ----------------------------------------------------
	/**
	 * 配送方法設定を取得
	 */
	public function getDeliveryList()
	{
		$delivery_list = DeliveryMethodModel::model()->findAll(array(
			'condition' => 'delete_flag = 0 AND `usage` = 1',
			'order' => 'rank ASC'
		));
		
		$delivery_list_values = [];
		$delivery_date_list = [];
		$delivery_date_time = [];
		foreach( $delivery_list as $row )
		{
			$delivery_list_values[$row->id] = $row->name;
			$delivery_date_time[$row->id] = json_decode($row->delivery_time);
			
			$i = 0;
			$delivery_date_list[$row->id] = array();
			while( $i < 14 )
			{
				$cdate = $row->specific_interval + $i;
				$delivery_date_list[$row->id][] = date(
					'm月d日',
					strtotime(
						'+'.$cdate . ' day'
					)
				);
				$i ++ ;
			}
			
		}
		
		return array(
			'rows' => $delivery_list,
			'delivery_list_values' => $delivery_list_values,
			'delivery_date_list' => $delivery_date_list,
			'delivery_date_time' => $delivery_date_time,
		);
	}
	
	
	// ----------------------------------------------------
	
	/**
	 * 商品追加
	 * @param array $item
	 */
	public function addItem( $items )
	{
		//キー生成
		$key = sha1( time() . '-'. rand(0,10000) );
		
		
		//追加用変数
		$set_items = array();
		
		//追加実行
		$data = Yii::app()->session['cart'];
		$data['items'][$key] = $items;
		Yii::app()->session['cart'] = $data;
		
	}
	
	// ----------------------------------------------------
	
	/**
	 * 数量変更
	 * @param array $delete_list 削除リスト（対称キーが入った配列）
	 * @param array $qty_list 数量リスト（キー=>数量の配列）
	 */
	public function changeQty( $delete_list, $qty_list )
	{
		//元データ
		$datas = Yii::app()->session['cart'];
		
		
		//数量変更
		foreach( $qty_list as $key => $qty )
		{
			if(!array_key_exists($key, $datas['items']))
			{
				continue;
			}
			
			//数量変更ができるのはオーダーメイド以外だけのため
			//アイテムの最初だけを処理
			$datas['items'][$key][0]['qty'] = $qty;
		}
		
		
		//削除
		foreach( $delete_list as $key )
		{
			unset( $datas['items'][$key] );
		}
		
		
		//セッションに戻す
		Yii::app()->session['cart'] = $datas;
		
		
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * お客様情報セット
	 * @param array $list 
	 */
	
	public function setInfo( $list )
	{
		$data = Yii::app()->session['cart'];
		$data['info'] = $list;
		Yii::app()->session['cart'] = $data;
	}
	
	// ----------------------------------------------------
	
	/**
	 * お客様情報取得
	 * @param string $key
	 */
	public function getInfo( $key = null )
	{
		if(is_null($key))
		{
			return Yii::app()->session['cart']['info'];
		}
		
		$data = Yii::app()->session['cart']['info'];
		if( isset($data[$key]) )
		{
			return $data[$key];
		}
		return null;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払い情報保存
	 * @param array $list
	 */
	public function setPayment( $list )
	{
		$data = Yii::app()->session['cart'];
		$data['payment_method'] = $list;
		Yii::app()->session['cart'] = $data;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 支払情報取得
	 * @param type $key 
	 */
	
	
	public function getPayment( $key = null )
	{
		if(is_null($key))
		{
			return Yii::app()->session['cart']['payment_method'];
		}
		
		$data = Yii::app()->session['cart']['payment_method'];
		
		if(isset($data[$key]))
		{
			return $data[$key];
		}
		
		return null;
	}
	
	// ----------------------------------------------------\
	
	/**
	 * その他情報の記録
	 * @param string $key
	 * @param mix $val 
	 */
	
	public function setOther( $key, $val )
	{
		if(is_null($val))
		{
			unset( Yii::app()->session['cart']['other_data'][$key] );
			return;
		}
		
		Yii::app()->session['cart']['other_data'][$key] = $val;
	}
	
	// ----------------------------------------------------
	
	/**
	 * その他データの取得
	 * @param string $key
	 */
	public function getOhter( $key )
	{
		return Yii::app()->session['cart']['other_data'][$key];
	}
	
	// ----------------------------------------------------
	
	/**
	 * 計算して取得
	 */
	
	public function calc()
	{
		
		//返り値の定義
		$return = array(
			
			//商品情報の配列
			'items' => array(),
			
			//合計計算
			'calc' => array(
				'item_total' => 0,//商品代金合計
				'item_total_tax' => 0,//商品代金合計
				'item_count' => 0,//かごにはいっている数
				'tax' => 0,//消費税額
				'commission_fee' => 0,//支払い手数料
				'commission_fee_tax' => 0,//支払い手数料
				'delivery_fee' => 0,//配送手数料
				'delivery_fee_tax' => 0,//配送手数料
				'grand_total' => 0,//お支払い合計
				'grand_total_tax' => 0,//お支払い合計
				'stock_error' => false,//在庫エラーがある場合
			),
			
			//文字列データ
			'string' => array(
				'payment_method' => '',//支払い方法を保存
			),
			
		);
		
		
		//かごの中計算
		$this->calcItems( $return );
		
		
		//フィー計算
		$this->callFee( $return );
		
		
		return $return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 在庫計算用に追加
	 * @param int $sku_id
	 * @param int $qty
	 * @return void
	 */
	protected function setZaikoCalc( $sku_id, $qty )
	{
		if(!array_key_exists( $sku_id, $this->zaiko ))
		{
			$this->zaiko[ $sku_id ] = $qty;
		}
		else
		{
			$this->zaiko[ $sku_id ] += $qty;
		}
		return;
	}
	
	// ----------------------------------------------------
	
	/**
	 * 現在状態の指定SKUidの在庫をチェック
	 * @param int $sku_id
	 * @return array
	 */
	protected function checkZaiko( $sku_id )
	{
		//在庫チェックの対象に入っていない
		if(!array_key_exists( $sku_id, $this->zaiko ))
		{
			return null;
		}
		
		//SKUモデルに問い合わせ
		$ProductSkuModel = new ProductSkuModel();
		$sku = $ProductSkuModel->find( 'id = :id', array( ':id' => $sku_id ) );
		
		//データがなければ在庫チェックの対象外として処理
		if( is_null( $sku ) )
		{
			return null;
		}
		
		//stockがnullなら在庫チェックの対象外
		if( $sku->stock === null )
		{
			return null;
		}
		
		
		//チェック開始
		$current_qty = $this->zaiko[ $sku_id ];
		
		$return = array(
				'result' => true,
				'current_stock' => $sku->stock,
			);
		
		if( $current_qty > $sku->stock )
		{
			$return['result'] = false;
		}
		return $return;
	}
	// ----------------------------------------------------
	
	/**
	 * 計算 商品情報をループ
	 * @param ref $return 
	 */
	
	protected function calcItems( &$return )
	{
		
		foreach( Yii::app()->session['cart']['items'] as $key => $val )
		{
			$return['items'][$key] = $val;
			$return['items'][$key]['sub_total'] = $val['qty'] * $val['calc_price'];
			$return['calc']['item_total'] += $return['items'][$key]['sub_total'];
			$return['calc']['item_total_tax'] += $return['items'][$key]['sub_total'];
			$return['calc']['item_count'] ++ ;
		}
	}
	
		
	// ----------------------------------------------------
	
	/**
	 * 支払い手数料など計算
	 * @param type $return 
	 */
	
	protected function callFee( &$return )
	{
		//設定を取得
		$config = Yii::app()->params['cart_config'];
		
		
		//在庫チェックを発動
		foreach( $return['items']  as $key => $val )
		{
			
		}
		
		
		//支払い設定を取得
		/*
		$payment_method = $this->getPayment( 'payment_method' );
		
		if(!is_null($payment_method))
		{
			$payment = $config['payment_method'][ $payment_method ];
			$return['string']['payment_method'] = $payment['name'];
		}
		else
		{
			$return['string']['payment_method'] = null;
			$payment = array(
				'name' => null,
				'fee' => array()
			);
		}
		
		//支払い手数料
		foreach( $payment['fee'] as $one )
		{
			if( $one[0] <= $return['calc']['item_total'] )
			{
				$return['calc']['commission_fee'] = $one[1];
			}
		}
		*/
		//$return['calc']['commission_fee_tax'] = EcFunctions::getTaxInPrice( $return['calc']['commission_fee'] ); 
		$return['calc']['commission_fee'] = 0;
		$return['calc']['commission_fee_tax'] = 0;
		
		
		//送料計算
		/*
		$delivery_method = $this->getPayment( 'delivery_method' );
		if(!is_null($delivery_method))
		{
			//設定ファイルの特定
			$file = YiiBase::getPathOfAlias('application.config.deliv_fee.'.$delivery_method) .'.php';
			require $file;
			
			//全国一律送料
			if( $delivery_method::$eveness === TRUE )
			{
				foreach( $delivery_method::$eveness_config as $one )
				{
					if( $one[0] <= $return['calc']['item_total'] )
					{
						$return['calc']['delivery_fee'] = $one[1];
					}
				}
				
			}
			//県別送料
			else
			{
				//対象のチェック
				if( $this->getInfo('d_check') == 1 )
				{
					$target = $this->getInfo('d_pref');
				}
				else
				{
					$target = $this->getInfo('pref');
				}
				foreach( $delivery_method::$pref[$target] as $one )
				{
					if( $one[0] <= $return['calc']['item_total'] )
					{
						$return['calc']['delivery_fee'] = $one[1];
					}
				}
			}
		}
		$return['calc']['delivery_fee_tax'] = EcFunctions::getTaxInPrice($return['calc']['delivery_fee']);
		*/
		$return['calc']['delivery_fee'] = 0;
		$return['calc']['delivery_fee_tax'] = 0;
		
		//総合計を取得
		$return['calc']['grand_total'] = 
					$return['calc']['delivery_fee'] +
					$return['calc']['item_total'] +
					$return['calc']['commission_fee'];
		$return['calc']['grand_total_tax'] = 
					$return['calc']['delivery_fee_tax'] +
					$return['calc']['item_total_tax'] +
					$return['calc']['commission_fee_tax'];
		
		
		return;
	}
	
	// ----------------------------------------------------
}