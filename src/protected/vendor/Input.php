<?php
/* =============================================================================
 * 入力クラス
 * ========================================================================== */
class Input
{
	// ----------------------------------------------------
	
	/**
	 * GET
	 */
	public static function Get( $key, $default = false )
	{
		return Yii::app()->request->getQuery( $key, $default );
	}
	
	// ----------------------------------------------------
	
	/**
	 * POST
	 */
	public static function Post( $key, $default = false )
	{
		return Yii::app()->request->getPost($key, $default );
	}
	
	// ----------------------------------------------------
	
	/**
	 * GET or POST
	 */
	public static function GetPost( $key, $default = false )
	{
		return Yii::app()->request->getParam($key, $default );
	}
	
	// ----------------------------------------------------
}