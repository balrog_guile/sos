<?php
/* =============================================================================
 * お客様側ユーザー認証
 * ========================================================================== */

class customerAuth extends CUserIdentity
{
	//IDを保存
	private $_id;
	
	// ----------------------------------------------------
	
	/**
	 * 認証
	 * @return type
	 */
	
	public function authenticate()
	{
		$customer = MemberModel::model()->find(
				'( email = :email ) AND ( member_status = 2 )',
				array(':email' => $this->username ) 
		);
		
		
		//ユーザーなし
		if($customer===null)
		{
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		}
		//パスワード違い
		elseif($this->authPass($customer) === false )
		{
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		}
		//認証OK
		else
		{
			$this->_id=$customer->id;
			$this->errorCode=self::ERROR_NONE;
			//$this->email = $this->username;
			//$this->create_at = $customer->create_date;
			//$this->lastvisit_at = date('Y-m-d H:i:s');
		}
		return $this->errorCode==self::ERROR_NONE;
	}
	
	// ----------------------------------------------------
	
	/**
	 * パスワード認証
	 * @param $customer
	 */
	private function authPass( $customer )
	{
		$authPass = sha1( $customer->password_hash . '___' . $this->password );
		if( $authPass == $customer->password )
		{
			return true;
		}
		return false;
	}
	
	// ----------------------------------------------------
	
	/**
	 * ID取得
	 * @return type
	 */
	
	public function getId()
	{
		return $this->_id;
	}
	
	// ----------------------------------------------------
	
	
	/**
	 * 顧客ログインチェック用（アクセスコントロール)
	 */
	static public function accessControll()
	{
		if( Yii::app()->customer->getId() === null )
		{
			return FALSE;
		}
		return true;
	}
	// ----------------------------------------------------
	
	/**
	 * 顧客ログインしてないときのリダイレクト
	 */
	static public function deniedCallback()
	{
		Yii::app()->controller->redirect(array('mypage/login'));
	}
	// ----------------------------------------------------
}