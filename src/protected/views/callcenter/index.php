<?php
/* @var $this CallcenterController */

$this->breadcrumbs=array(
	'Callcenter',
);
?>
<h1>コールセンター業務</h1>
<hr />

<div class="streams">
  <ul>
    <li><video id="others-video" style="width: 300px;" autoplay></video></li>
    <li><video id="my-video" style="width: 100px;" autoplay muted></video></li>
  </ul>
  <p><button class="js-callclose btn btn-default callClose">接続を切る</button></p>
</div>

<style>
.streams ul {
  display: table;
  margin: 0 0 20px;
  padding: 0;
}
.streams li {
  display: table-cell;
  padding-right: 20px;
}
.streams video {
  display: none;
}
.streams video.is-active {
  display: block;
}
.callClose {
  display: none;
}
.callClose.is-active {
  display: block;
}
.streams {
  display: none;
}
.streams.is-active {
  display: block;
}
</style>

<div class="panel panel-default">
    <div class="panel-heading">
        コール待ちクライアント
    </div>
    <div class="panel-body">
        <table class="table">
          <tr>
            <th>peer id</th>
            <th>閲覧ページ</th>
            <th>接続時間</th>
          </tr>
          <tbody class="js-connects">
          <?php foreach($connects as $connect): ?>
          <tr>
            <td><a href="#" data-id="<?php echo $connect->id; ?>" class="js-call"><?php echo $connect->peer_id; ?></a></td>
            <td><a href="<?php echo $connect->url; ?>" target="_blank"><?php echo $connect->shop_id; ?></td>
            <td><?php echo $connect->connect_date; ?></td>
          </tr>
          <?php endforeach; ?>
          </tbody>
        </table>
    </div>
</div>


<script src="https://skyway.io/dist/0.3/peer.js"></script>
<script>
var peerValue = {
    'key': '<?php echo Yii::app()->params['peerJs']['keyList'][0]; ?>',
    'id': 'center-'+'<?php echo Yii::app()->session->getSessionId(); ?>'
};

var baseUrl = '<?php echo Yii::app()->getBaseUrl(true); ?>';
</script>