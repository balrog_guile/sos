<?php
/* =============================================================================
 * カートの全部確認
 * ========================================================================== */
?>
<h1>ご確認ください</h1>


<h2>カートの中を確認して下さい</h2>
<table class="table">
	<thead>
		<tr>
			<th>商品情報</th>
			<th>単価</th>
			<th>数量</th>
			<th>小計</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $items as $key =>$item ): ?>
			<tr>
				<td>
					<?php foreach( $item['items'] as $itemone ): ?>
						品番:<?php echo $itemone['item_id']; ?><br />
						品名:<?php echo $itemone['item_name']; ?><br />
					<?php endforeach; ?>
				</td>
				<td>
					<?php echo number_format($item['sub_calc']['base_price']); ?>円
				</td>
				<td>
					<?php if( $item['sub_calc']['is_ordermade'] == TRUE ): ?>
						<?php echo number_format($item['sub_calc']['qty']); ?>個
						<p>オーダーメイド商品は左記の商品をワンセットにてお届けします。</p>
					<?php else:?>
						<?php echo number_format($item['sub_calc']['qty']); ?>個
					<?php endif; ?>
				</td>
				<td>
					<?php echo number_format($item['sub_calc']['sub_total']); ?>円
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3" class="text_rigth">
				商品代金合計
			</td>
			<td>
				<?php echo number_format( $calc['item_total'] ); ?>円
			</td>
		</tr>
		<tr>
			<td colspan="3" class="text_rigth">
				送料
			</td>
			<td>
				<?php echo number_format( $calc['delivery_fee'] ); ?>円
			</td>
		</tr>
		<?php if( $calc['commission_fee'] > 0 ): ?>
			<tr>
				<td colspan="3" class="text_rigth">
					<?php echo $payment_drop_down[$model->payment_method]; ?>手数料
				</td>
				<td>
					<?php echo number_format( $calc['commission_fee'] ); ?>円
				</td>
			</tr>
		<?php endif; ?>
		<tr>
			<td colspan="3" class="text_rigth">
				総合計
			</td>
			<td>
				<?php echo number_format( $calc['grand_total'] ); ?>円
			</td>
		</tr>
	</tfoot>
</table>



<hr />



<h2>支払い方法</h2>
<table class="table">
	<tr>
		<th>支払い方法</th>
		<td>
			<?php echo $payment_drop_down[$model->payment_method]; ?>
		</td>
	</tr>
</table>


<hr />

<h2>お客様情報</h2>
<table class="table">
	<tr>
		<th>お名前</th>
		<td>
			<?php echo $model->name1; ?> <?php echo $model->name2; ?> 様
		</td>
	</tr>
	<tr>
		<th>かな</th>
		<td>
			<?php echo $model->kana1; ?> <?php echo $model->kana2; ?> さま
		</td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td>
			<?php echo $model->tel; ?>
		</td>
	</tr>
	<tr>
		<th>郵便番号</th>
		<td>
			<?php echo $model->zip; ?>
		</td>
	</tr>
	<tr>
		<th>ご住所</th>
		<td>
			<?php echo $model->pref; ?><br />
			<?php echo $model->address1; ?><br />
			<?php echo $model->address2; ?><br />
			<?php echo $model->address3; ?>
		</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td>
			<?php echo $model->mailaddress; ?>
		</td>
	</tr>
</table>


<hr />



<!-- 配送先が別の場合のみ表示 -->
<?php if( $model->d_check == 1 ): ?>

	<h2>配送先</h2>

	<table class="table">
		<tr>
			<th>お名前</th>
			<td>
				<?php echo $model->d_name1; ?> <?php echo $model->d_name2; ?> 様
			</td>
		</tr>
		<tr>
			<th>電話番号</th>
			<td>
				<?php echo $model->d_tel; ?>
			</td>
		</tr>
		<tr>
			<th>郵便番号</th>
			<td>
				<?php echo $model->d_zip; ?>
			</td>
		</tr>
		<tr>
			<th>ご住所</th>
			<td>
				<?php echo $model->d_pref; ?><br />
				<?php echo $model->d_address1; ?><br />
				<?php echo $model->d_address2; ?><br />
				<?php echo $model->d_address3; ?>
			</td>
		</tr>
	</table>

<?php endif; ?>
<!-- /配送先が別の場合のみ表示 -->



<hr />
<h2>ご要望</h2>
<div class="container-fluid">
	<div class="row">
		<?php echo nl2br($model->yobo); ?>
	</div>
</div>

<hr />


<div class="container-fluid">
	<div class="row">
		<div class="col-md-6">
			<a href="<?php echo Yii::app()->createUrl('cart'); ?>" class="btn btn-primary btn-lg">
				再入力
			</a>
		</div>
		<div class="col-md-6">
			<?php echo CHtml::form( Yii::app()->createUrl('cart/submit'), 'post', array() );?>
				<?php echo CHtml::submitButton('ご購入', array( 'class' => 'btn btn-success btn-lg' ) ); ?>
			<?php echo CHtml::endForm(); ?>
		</div>
	</div>
</div>