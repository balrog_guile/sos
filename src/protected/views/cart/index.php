<?php
/* =============================================================================
 * カートの中身確認画面
 * ========================================================================== */
?>
<h1>はいさいカート</h1>


<h2>カートの中を確認して下さい</h2>

<?php if( $calc['stock_error'] === TRUE ): ?>
	<div class="error-block">
		<?php if( $error_number == 1 ): ?>
		<div>お買い物手続き中に在庫数がなくなりました。</div>
		<?php endif; ?>
		在庫エラーがあります。
	</div>
<?php endif; ?>




<?php echo CHtml::form( Yii::app()->createUrl('cart/changeqty'), 'post', array( 'class' => '' ) ); ?>
<table class="table">
	<thead>
		<tr>
			<th>操作</th>
			<th>商品情報</th>
			<th>単価</th>
			<th>数量</th>
			<th>小計</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $items as $key =>$item ): ?>
			<tr>
				<td>
					<?php echo CHtml::checkBox( 'delete[]', false, array( 'value' => $key, 'class' => '' ) ); ?>
				</td>
				<td>
					<?php foreach( $item['items'] as $itemone ): ?>
						<?php if( $itemone['stock_error'] === TRUE ): ?>
						<div class="stock-error-one">
							在庫数が確保できません。現在の在庫数は「<?php echo $itemone['current_stock']; ?>」です。
						</div>
						<?php endif; ?>
						品番:<?php echo $itemone['item_id']; ?><br />
						品名:<?php echo $itemone['item_name']; ?><br />
					<?php endforeach; ?>
				</td>
				<td>
					<?php echo number_format($item['sub_calc']['base_price']); ?>円
				</td>
				<td>
					<?php if( $item['sub_calc']['is_ordermade'] == TRUE ): ?>
						<?php echo number_format($item['sub_calc']['qty']); ?>個
						<p>オーダーメイド商品は数量変更できません</p>
					<?php else:?>
						<?php echo CHtml::dropDownList(
									$key,
									$item['sub_calc']['qty'],
									array( '1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5' ),
									array(
										'class' => ''
									)
							);
						?>個
					<?php endif; ?>
				</td>
				<td>
					<?php echo number_format($item['sub_calc']['sub_total']); ?>円
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="5">
				<?php echo CHtml::submitButton( '削除／数量変更', array( 'class' => 'btn btn-lg btn-info') ); ?>
			</td>
		</tr>
		<tr>
			<td colspan="4" class="text_rigth">
				商品代金合計
			</td>
			<td>
				<?php echo number_format( $calc['item_total'] ); ?>
			</td>
		</tr>
		<tr>
			<td colspan="5" class="text_rigth">
				送料・決済手数料などは次の画面で表示されます。
			</td>
		</tr>
	</tfoot>
</table>
<?php echo CHtml::endForm(); ?>



<hr />


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'info-model-form',
	'enableAjaxValidation'=>false,
)); ?>


<h2>支払い方法</h2>
<table class="table">
	<tr>
		<th>支払い方法</th>
		<td>
			<?php
				echo $form->dropDownList(
					$model,
					'payment_method',
					$payment_drop_down,
					array( 'class' => '' )
				);
			?>
			<?php echo $form->error($model,'payment_method'); ?>
		</td>
	</tr>
</table>




<hr />

<h2>お客様情報</h2>
<table class="table">
	<tr>
		<th>お名前</th>
		<td>
			<?php echo $form->textField($model,'name1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->textField($model,'name2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'name1'); ?>
			<?php echo $form->error($model,'name2'); ?>
		</td>
	</tr>
	<tr>
		<th>かな</th>
		<td>
			<?php echo $form->textField($model,'kana1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->textField($model,'kana2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'kana1'); ?>
			<?php echo $form->error($model,'kana2'); ?>
		</td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td>
			<?php echo $form->textField($model,'tel',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'tel'); ?>
			0000-0000-0000の形式
		</td>
	</tr>
	<tr>
		<th>郵便番号</th>
		<td>
			<?php echo $form->textField($model,'zip',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'zip'); ?>
			000-0000の形式
		</td>
	</tr>
	<tr>
		<th>ご住所</th>
		<td>
			<?php echo $form->dropDownList( $model, 'pref', VarHelper::getPrefList() ); ?><br />
			<?php echo $form->textField($model,'address1',array('size'=>60,'maxlength'=>125)); ?><br />
			<?php echo $form->textField($model,'address2',array('size'=>60,'maxlength'=>125)); ?><br />
			<?php echo $form->textField($model,'address3',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'pref'); ?>
			<?php echo $form->error($model,'address1'); ?>
			<?php echo $form->error($model,'address2'); ?>
			<?php echo $form->error($model,'address3'); ?>
		</td>
	</tr>
	<tr>
		<th>メールアドレス</th>
		<td>
			<?php echo $form->textField($model,'mailaddress',array('size'=>60,'maxlength'=>125)); ?><br />
			<?php echo $form->textField($model,'mailaddress2', array('size'=>60,'maxlength'=>125)); ?>（確認入力）
			<?php echo $form->error($model,'mailaddress'); ?>
			<?php echo $form->error($model,'mailaddress2'); ?>
		</td>
	</tr>
</table>


<hr />


<h2>配送先</h2>

<table class="table">
	<tr>
		<th colspan="2">
			<?php echo $form->checkBox( $model, 'd_check', array( 'value' => 1, 'class' => '' )); ?>
			<?php echo $form->labelEx( $model, 'd_check' ); ?>
		</th>
	</tr>
	<tr>
		<th>お名前</th>
		<td>
			<?php echo $form->textField($model,'d_name1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->textField($model,'d_name2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_name1'); ?>
			<?php echo $form->error($model,'d_name2'); ?>
		</td>
	</tr>
	<tr>
		<th>電話番号</th>
		<td>
			<?php echo $form->textField($model,'d_tel',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_tel'); ?>
			0000-0000-0000の形式
		</td>
	</tr>
	<tr>
		<th>郵便番号</th>
		<td>
			<?php echo $form->textField($model,'d_zip',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_zip'); ?>
			000-0000の形式
		</td>
	</tr>
	<tr>
		<th>ご住所</th>
		<td>
			<?php echo $form->dropDownList( $model, 'd_pref', VarHelper::getPrefList() ); ?><br />
			<?php echo $form->textField($model,'d_address1',array('size'=>60,'maxlength'=>125)); ?><br />
			<?php echo $form->textField($model,'d_address2',array('size'=>60,'maxlength'=>125)); ?><br />
			<?php echo $form->textField($model,'d_address3',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_pref'); ?>
			<?php echo $form->error($model,'d_address1'); ?>
			<?php echo $form->error($model,'d_address2'); ?>
			<?php echo $form->error($model,'d_address3'); ?>
		</td>
	</tr>
</table>


<hr />
<h2>ご要望</h2>
<div class="container-fluid">
	<div class="row">
		<?php echo $form->textArea( $model, 'yobo', array( 'class' => '' ) ); ?>
	</div>
</div>

<hr />

<?php if( $calc['item_count'] == 0 ): ?>
	<div class="bg-danger">かごの中に商品が入っていません</div>
<?php elseif( $calc['stock_error'] === TRUE ): ?>
	<div class="error-block">
		在庫エラーがあります。
	</div>
<?php else: ?>
	<?php echo CHtml::submitButton( '確認画面へ', array( 'class' => 'btn btn-lg btn-info') ); ?>
<?php endif; ?>



<?php $this->endWidget(); ?>