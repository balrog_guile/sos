<?php
/* @var $this CommingController */
/* @var $model CommingModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'comming-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'comming_date'); ?>
		<?php echo $form->textField($model,'comming_date'); ?>
		<?php echo $form->error($model,'comming_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'memo1'); ?>
		<?php echo $form->textArea($model,'memo1',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'memo1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'memo2'); ?>
		<?php echo $form->textArea($model,'memo2',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'memo2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'memo3'); ?>
		<?php echo $form->textArea($model,'memo3',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'memo3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_date'); ?>
		<?php echo $form->textField($model,'update_date'); ?>
		<?php echo $form->error($model,'update_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'staff_id'); ?>
		<?php echo $form->textField($model,'staff_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'staff_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->