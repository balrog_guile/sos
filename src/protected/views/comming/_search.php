<?php
/* @var $this CommingController */
/* @var $model CommingModel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>
	
	
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->label($model,'name'); ?>
			<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		</div>

		<div class="span4">
			<?php echo $form->label($model,'comming_date'); ?>
				<?php echo $form->textField($model,'comming_date'); ?>
		</div>
		
		<div class="buttons span4">
			<?php echo CHtml::submitButton('Search', array( 'class' => 'btn btn-primary') ); ?>
		</div>
		
	</div>
	
	

<?php $this->endWidget(); ?>

</div><!-- search-form -->