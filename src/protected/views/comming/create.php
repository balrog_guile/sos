<?php
/* @var $this CommingController */
/* @var $model CommingModel */

$this->breadcrumbs=array(
	'Comming Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CommingModel', 'url'=>array('index')),
	array('label'=>'Manage CommingModel', 'url'=>array('admin')),
);
?>

<h1>Create CommingModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>