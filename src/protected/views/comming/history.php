<?php
/* @var $this CommingController */
/* @var $model CommingModel */

$this->breadcrumbs=array(
	'Comming Models'=>array('index'),
	'Manage',
);

$this->menu=array(
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#comming-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>顧客来店管理</h1>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well">
			<h2>
				<i class="icon-info-sign"></i>お客様
			</h2>
		</div>
		<div class="box-content">
			<div class="row-fluid">
				<div class="span6">
					<div>
						<strong>お名前</strong>: <?php echo $customer->name1; ?> <?php echo $customer->name2; ?>様
					</div>
					<div>
						<strong>かな</strong>: <?php echo $customer->kana1; ?> <?php echo $customer->kana2; ?>様
					</div>
					<div>
						<strong>メールアドレス</strong>: <?php echo $customer->mailaddress; ?>
					</div>
				</div>
				<div class="span6">
					<?php foreach( CustomerProfileFieldModel::model()->findAll('summary_display=1') as $one ): ?>
						<div>
							<strong><?php echo $one->field_label; ?></strong>: <?php echo $customer->{$one->field_name}; ?>
						</div>
					<?php endforeach; ?>
				</div>
			</div>
			<hr />
			<?php echo CHtml::link( 'お客様情報へ', Yii::app()->createUrl( 'customers/update', array( 'id' => $customer->id) ), array( 'class' => 'btn' ) ); ?>
		</div>
	</div>
</div>

<hr />


<div>
	<?php echo CHtml::button( '新規来店', array('class' => 'btn btn-large btn-info set_new') ); ?>
</div>


<hr />

<!-- 検索 -->
<div class="search-form">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div>
<!-- /検索 -->



<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'comming-model-grid',
	'dataProvider'=>$model->getCustoemrSearch( $customer_id ),
	'filter'=>$model,
	'columns'=>array(
		'name',
		'comming_date',
		'memo1',
		'memo2',
		'memo3',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{view}',
			'viewButtonImageUrl' => '',
			'buttons'=>array(
				'view' => array(
					'label'=>'履歴確認',
					'url'=>'Yii::app()->createUrl("comming/latest", array("id"=>$data->cusomer_id, "target" => $data->id))',
					'options' => array('class' => 'btn btn-success', 'style' => 'width: 150px; margin-bottom: 0.5em;'),
				),
			),
		),
	),
)); ?>
