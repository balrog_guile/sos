<?php
/* @var $this CommingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Comming Models',
);

$this->menu=array(
	array('label'=>'Create CommingModel', 'url'=>array('create')),
	array('label'=>'Manage CommingModel', 'url'=>array('admin')),
);
?>

<h1>Comming Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
