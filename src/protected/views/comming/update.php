<?php
/* @var $this CommingController */
/* @var $model CommingModel */

$this->breadcrumbs=array(
	'Comming Models'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CommingModel', 'url'=>array('index')),
	array('label'=>'Create CommingModel', 'url'=>array('create')),
	array('label'=>'View CommingModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CommingModel', 'url'=>array('admin')),
);
?>

<h1>Update CommingModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>