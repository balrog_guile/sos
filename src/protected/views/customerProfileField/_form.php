<?php
/* @var $this CustomerProfileFieldController */
/* @var $model CustomerProfileFieldModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customer-profile-field-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'field_name'); ?>
		<?php echo $form->textField($model,'field_name',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'field_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'field_type'); ?>
		<?php echo $form->textField($model,'field_type'); ?>
		<?php echo $form->error($model,'field_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'input_type'); ?>
		<?php echo $form->textField($model,'input_type'); ?>
		<?php echo $form->error($model,'input_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'selecte_column'); ?>
		<?php echo $form->textArea($model,'selecte_column',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'selecte_column'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'html_class'); ?>
		<?php echo $form->textField($model,'html_class',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'html_class'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'html_id'); ?>
		<?php echo $form->textField($model,'html_id',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'html_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'search_flag'); ?>
		<?php echo $form->textField($model,'search_flag'); ?>
		<?php echo $form->error($model,'search_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'view_display'); ?>
		<?php echo $form->textField($model,'view_display'); ?>
		<?php echo $form->error($model,'view_display'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_profile_fieldcol'); ?>
		<?php echo $form->textField($model,'customer_profile_fieldcol',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'customer_profile_fieldcol'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'required'); ?>
		<?php echo $form->textField($model,'required'); ?>
		<?php echo $form->error($model,'required'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'min_length'); ?>
		<?php echo $form->textField($model,'min_length',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'min_length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'max_length'); ?>
		<?php echo $form->textField($model,'max_length',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'max_length'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'regrex'); ?>
		<?php echo $form->textField($model,'regrex',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'regrex'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'default_value'); ?>
		<?php echo $form->textArea($model,'default_value',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'default_value'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'other_validation'); ?>
		<?php echo $form->textArea($model,'other_validation',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'other_validation'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->