<?php
/* @var $this CustomerProfileFieldController */
/* @var $model CustomerProfileFieldModel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'field_name'); ?>
		<?php echo $form->textField($model,'field_name',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'field_type'); ?>
		<?php echo $form->textField($model,'field_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'input_type'); ?>
		<?php echo $form->textField($model,'input_type'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'selecte_column'); ?>
		<?php echo $form->textArea($model,'selecte_column',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'html_class'); ?>
		<?php echo $form->textField($model,'html_class',array('size'=>60,'maxlength'=>125)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'html_id'); ?>
		<?php echo $form->textField($model,'html_id',array('size'=>60,'maxlength'=>125)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'search_flag'); ?>
		<?php echo $form->textField($model,'search_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'view_display'); ?>
		<?php echo $form->textField($model,'view_display'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customer_profile_fieldcol'); ?>
		<?php echo $form->textField($model,'customer_profile_fieldcol',array('size'=>45,'maxlength'=>45)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'required'); ?>
		<?php echo $form->textField($model,'required'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'min_length'); ?>
		<?php echo $form->textField($model,'min_length',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'max_length'); ?>
		<?php echo $form->textField($model,'max_length',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'regrex'); ?>
		<?php echo $form->textField($model,'regrex',array('size'=>60,'maxlength'=>125)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'default_value'); ?>
		<?php echo $form->textArea($model,'default_value',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'other_validation'); ?>
		<?php echo $form->textArea($model,'other_validation',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->