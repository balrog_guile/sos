<?php
/* @var $this CustomerProfileFieldController */
/* @var $model CustomerProfileFieldModel */

$this->breadcrumbs=array(
	'Customer Profile Field Models'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List CustomerProfileFieldModel', 'url'=>array('index')),
	array('label'=>'Create CustomerProfileFieldModel', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#customer-profile-field-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Customer Profile Field Models</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'customer-profile-field-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'field_name',
		'field_type',
		'input_type',
		'selecte_column',
		'html_class',
		/*
		'html_id',
		'search_flag',
		'view_display',
		'customer_profile_fieldcol',
		'required',
		'min_length',
		'max_length',
		'regrex',
		'default_value',
		'other_validation',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
