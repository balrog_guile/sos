<?php
/* @var $this CustomerProfileFieldController */
/* @var $model CustomerProfileFieldModel */

$this->breadcrumbs=array(
	'Customer Profile Field Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List CustomerProfileFieldModel', 'url'=>array('index')),
	array('label'=>'Manage CustomerProfileFieldModel', 'url'=>array('admin')),
);
?>

<h1>Create CustomerProfileFieldModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>