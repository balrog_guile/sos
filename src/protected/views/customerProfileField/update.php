<?php
/* @var $this CustomerProfileFieldController */
/* @var $model CustomerProfileFieldModel */

$this->breadcrumbs=array(
	'Customer Profile Field Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustomerProfileFieldModel', 'url'=>array('index')),
	array('label'=>'Create CustomerProfileFieldModel', 'url'=>array('create')),
	array('label'=>'View CustomerProfileFieldModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustomerProfileFieldModel', 'url'=>array('admin')),
);
?>

<h1>Update CustomerProfileFieldModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>