<?php
/* @var $this CustomerProfileFieldController */
/* @var $model CustomerProfileFieldModel */

$this->breadcrumbs=array(
	'Customer Profile Field Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List CustomerProfileFieldModel', 'url'=>array('index')),
	array('label'=>'Create CustomerProfileFieldModel', 'url'=>array('create')),
	array('label'=>'Update CustomerProfileFieldModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete CustomerProfileFieldModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CustomerProfileFieldModel', 'url'=>array('admin')),
);
?>

<h1>View CustomerProfileFieldModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'field_name',
		'field_type',
		'input_type',
		'selecte_column',
		'html_class',
		'html_id',
		'search_flag',
		'view_display',
		'customer_profile_fieldcol',
		'required',
		'min_length',
		'max_length',
		'regrex',
		'default_value',
		'other_validation',
	),
)); ?>
