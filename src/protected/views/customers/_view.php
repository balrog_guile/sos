<?php
/* @var $this CustomersController */
/* @var $data CustomersModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name1')); ?>:</b>
	<?php echo CHtml::encode($data->name1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name2')); ?>:</b>
	<?php echo CHtml::encode($data->name2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kana1')); ?>:</b>
	<?php echo CHtml::encode($data->kana1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kana2')); ?>:</b>
	<?php echo CHtml::encode($data->kana2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mailaddress')); ?>:</b>
	<?php echo CHtml::encode($data->mailaddress); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_status')); ?>:</b>
	<?php echo CHtml::encode($data->customer_status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	*/ ?>

</div>