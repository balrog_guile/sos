<?php
/* @var $this CustomersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Customers Models'=>array('index'),
	'Create',
);

// $this->menu=array(
// 	array('label'=>'List CustomersModel', 'url'=>array('index')),
// 	array('label'=>'Manage CustomersModel', 'url'=>array('admin')),
// );
?>

<h1>Customers Models</h1>

<table>
	<tbody>
		<tr>
			<th>姓</th>
			<td><?php echo $model->attributes['name1']; ?></td>
		</tr>
		<tr>
			<th>名</th>
			<td><?php echo $model->attributes['name2']; ?></td>
		</tr>
		<tr>
			<th>姓（かな）</th>
			<td><?php echo $model->attributes['kana1']; ?></td>
		</tr>
		<tr>
			<th>名（かな）</th>
			<td><?php echo $model->attributes['kana1']; ?></td>
		</tr>
		<tr>
			<th>郵便番号</th>
			<td><?php echo $model->attributes['zip']; ?></td>
		</tr>
		<tr>
			<th>都道府県</th>
			<td><?php echo $model->attributes['pref']; ?></td>
		</tr>
		<tr>
			<th>住所1</th>
			<td><?php echo $model->attributes['address_1']; ?></td>
		</tr>
		<tr>
			<th>住所2</th>
			<td><?php echo $model->attributes['address_2']; ?></td>
		</tr>
		<tr>
			<th>住所3</th>
			<td><?php echo $model->attributes['address_3']; ?></td>
		</tr>
		<tr>
			<th>電話番号</th>
			<td><?php echo $model->attributes['tell']; ?></td>
		</tr>
		<tr>
			<th>FAX</th>
			<td><?php echo $model->attributes['fax']; ?></td>
		</tr>
		<tr>
			<th>パスワード</th>
			<td>安全のため非表示にしています。</td>
		</tr>
		<tr>
			<th>メールアドレス</th>
			<td><?php echo $model->attributes['mailaddress']; ?></td>
		</tr>
	</tbody>
</table>


<?php $form=$this->beginWidget('CActiveForm'); ?>
<?php echo $form->errorSummary($model); ?>

<?php foreach ($model->attributeLabels() as $key => $value): ?>
	<div class="row">
		<?php echo $form->hiddenField($model, $key, array('value'=>$model->attributes[$key])); ?>
	</div>
<?php endforeach; ?>
<?php echo $form->hiddenField($model, 'mode', array('value'=>'reedit')); ?>
<?php echo CHtml::submitButton('再入力'); ?>
<?php $this->endWidget(); ?>



<?php $form=$this->beginWidget('CActiveForm'); ?>
<?php echo $form->errorSummary($model); ?>

<?php foreach ($model->attributeLabels() as $key => $value): ?>
	<div class="row">
		<?php echo $form->hiddenField($model, $key, array('value'=>$model->attributes[$key])); ?>
	</div>
<?php endforeach; ?>
<?php echo CHtml::hiddenField('mode','exec'); ?>
<?php echo CHtml::submitButton('確認'); ?>
<?php $this->endWidget(); ?>

