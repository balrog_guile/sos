<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */

$this->breadcrumbs=array(
	'Customers Models'=>array('index'),
	$model->id,
);

// $this->menu=array(
// 	array('label'=>'List CustomersModel', 'url'=>array('index')),
// 	array('label'=>'Create CustomersModel', 'url'=>array('create')),
// 	array('label'=>'Update CustomersModel', 'url'=>array('update', 'id'=>$model->id)),
// 	array('label'=>'Delete CustomersModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
// 	array('label'=>'Manage CustomersModel', 'url'=>array('admin')),
// );
?>

<h1>仮登録完了 #<?php echo $model->id; ?></h1>

<p>仮登録完了しました。<br>メールを確認ください。</p>