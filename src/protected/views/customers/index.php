<?php
/* @var $this CustomersController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Customers Models',
);

$this->menu=array(
	array('label'=>'Create CustomersModel', 'url'=>array('create')),
	array('label'=>'Manage CustomersModel', 'url'=>array('admin')),
);
?>

<h1>Customers Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
