<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */

$this->breadcrumbs=array(
	'Customers Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List CustomersModel', 'url'=>array('index')),
	array('label'=>'Create CustomersModel', 'url'=>array('create')),
	array('label'=>'View CustomersModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage CustomersModel', 'url'=>array('admin')),
);
?>

<h1>Update CustomersModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>