<?php
/* @var $this Delivery_methodController */
/* @var $model DeliveryMethodModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'delivery-method-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><span class="required">*</span>は必須項目です。</p>

	<?php echo $form->errorSummary($model); ?>
	
	<div class="">
		<?php echo $form->labelEx($model,'usage'); ?>
		<?php echo $form->checkBox( $model, 'usage' ); ?>
		<?php echo $form->error($model,'usage'); ?>
	</div>
	
	<div class="">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>512)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	<div class="">
		<?php echo $form->labelEx($model,'method'); ?>
		<?php echo $form->textField($model,'method'); ?>
		<?php echo $form->error($model,'method'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'json_data'); ?>
		<?php echo $form->textArea($model,'json_data',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'json_data'); ?>
	</div>

	<div class="">
		<?php echo $form->labelEx($model,'limit_theme'); ?>
		<?php echo $form->textArea($model,'limit_theme',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'limit_theme'); ?>
	</div>
	
	<div class="">
		<?php echo $form->labelEx($model,'delivery_time'); ?>
		<?php echo $form->textArea($model,'delivery_time',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'delivery_time'); ?>
	</div>
	
	
	<div class="">
		<?php echo $form->labelEx($model,'specific_interval'); ?>
		<?php echo $form->textField($model,'specific_interval'); ?>
		<?php echo $form->error($model,'specific_interval'); ?>
	</div>
	
	<hr />
	
	<div class="buttons">
		<?php
			echo CHtml::submitButton(
				($model->isNewRecord ? '作成' : '編集'),
				[
					'class' => 'btn btn-info'
				]
			);
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->