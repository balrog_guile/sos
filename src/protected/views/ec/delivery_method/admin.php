<?php
/* @var $this Delivery_methodController */
/* @var $model DeliveryMethodModel */

$this->breadcrumbs=array();

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#delivery-method-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>配送方法設定</h1>

<hr />
<a
	href="<?php echo Yii::app()->createUrl('ec/delivery_method/create'); ?>"
	class="btn btn-success"
>
	作成
</a>

<hr />

<?php $data_provider = $model->search(); ?>
<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>利用中</th>
			<th>支払い名</th>
			<th>実行メソッド</th>
			<th>計算用JSON</th>
			<th>表示順</th>
			<th>編集</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $data_provider->getData() as $model ):?>
			<tr>
				<td>
					<?php if( $model->usage == 1): ?>
						利用中
					<?php else: ?>
						利用停止
					<?php endif; ?>
				</td>
				<td>
					<?php echo $model->name; ?>
				</td>
				<td>
					<?php echo $model->method; ?>
				</td>
				<td>
					<?php echo nl2br($model->json_data); ?>
				</td>
				<td>
					<?php
						echo CHtml::textField(
							'rank[' . $model->id . ']',
							$model->rank,
							array(
								'class' => 'input-mini'
							)
						);
					?>
				</td>
				<td>
					<a
						href="<?php echo $this->createUrl( 'update', array('id' => $model->id)); ?>"
						class="btn btn-info"
					>
						編集
					</a>
					<br />
				</td>
			</tr>
		<?php endforeach; ?>
	</tbody>
</table>