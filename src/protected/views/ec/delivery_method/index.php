<?php
/* @var $this Delivery_methodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Delivery Method Models',
);

$this->menu=array(
	array('label'=>'Create DeliveryMethodModel', 'url'=>array('create')),
	array('label'=>'Manage DeliveryMethodModel', 'url'=>array('admin')),
);
?>

<h1>Delivery Method Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
