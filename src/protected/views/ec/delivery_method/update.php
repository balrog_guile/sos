<?php
/* @var $this Delivery_methodController */
/* @var $model DeliveryMethodModel */

$this->breadcrumbs=array();

$this->menu=array();
?>

<h1>配送方法編集</h1>
<hr />

<?php $this->renderPartial('_form', array('model'=>$model)); ?>