<?php
/* @var $this Delivery_methodController */
/* @var $model DeliveryMethodModel */

$this->breadcrumbs=array(
	'Delivery Method Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List DeliveryMethodModel', 'url'=>array('index')),
	array('label'=>'Create DeliveryMethodModel', 'url'=>array('create')),
	array('label'=>'Update DeliveryMethodModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete DeliveryMethodModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage DeliveryMethodModel', 'url'=>array('admin')),
);
?>

<h1>View DeliveryMethodModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'create_date',
		'update_date',
		'rank',
		'usage',
		'delete_flag',
		'method',
		'json_data',
		'limit_theme',
	),
)); ?>
