<?php
/* @var $this Payment_methodController */
/* @var $model PaymentMethodModel */
/* @var $form CActiveForm */
?>

<div class="row-fluid">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'payment-method-model-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><span class="required">*</span>は必須。</p>

	<?php echo $form->errorSummary($model); ?>
	
	
	<div class="">
		<?php echo $form->labelEx($model,'usage'); ?>
		<?php echo $form->checkBox( $model, 'usage' ); ?>
		<?php echo $form->error($model,'usage'); ?>
	</div>
	
	
	<div class="">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('class' => 'input-xxlarge')); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>
	
	
	<div class="">
		<?php echo $form->labelEx($model,'method'); ?>
		<?php echo $form->textField($model,'method',array('class' => 'input-xxlarge')); ?>
		<?php echo $form->error($model,'method'); ?>
	</div>
	
	
	<div class="">
		<?php echo $form->labelEx($model,'json_data'); ?>
		<?php echo $form->textArea($model,'json_data',array( 'rows' => 7, 'class' => 'input-xxlarge')); ?>
		<?php echo $form->error($model,'json_data'); ?>
	</div>
	
	
	
	<hr />
	
	<div class="buttons">
		<?php
			echo CHtml::submitButton(
				($model->isNewRecord ? '登録' : '編集'),
				array(
					'class' => 'btn btn-success'
				)
			);
		?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->