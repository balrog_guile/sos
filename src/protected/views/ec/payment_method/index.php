<?php
/* @var $this Payment_methodController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Payment Method Models',
);

$this->menu=array(
	array('label'=>'Create PaymentMethodModel', 'url'=>array('create')),
	array('label'=>'Manage PaymentMethodModel', 'url'=>array('admin')),
);
?>

<h1>Payment Method Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
