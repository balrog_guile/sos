<?php
/* @var $this Payment_methodController */
/* @var $model PaymentMethodModel */

$this->breadcrumbs=array();

$this->menu=array();
?>

<h1>支払い方法編集</h1>

<hr />

<?php $this->renderPartial('_form', array('model'=>$model)); ?>