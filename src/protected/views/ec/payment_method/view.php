<?php
/* @var $this Payment_methodController */
/* @var $model PaymentMethodModel */

$this->breadcrumbs=array(
	'Payment Method Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PaymentMethodModel', 'url'=>array('index')),
	array('label'=>'Create PaymentMethodModel', 'url'=>array('create')),
	array('label'=>'Update PaymentMethodModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PaymentMethodModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PaymentMethodModel', 'url'=>array('admin')),
);
?>

<h1>View PaymentMethodModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'create_date',
		'update_date',
		'rank',
		'usage',
		'delete_flag',
		'shipment_type',
	),
)); ?>
