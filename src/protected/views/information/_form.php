<?php
/* @var $this InformationController */
/* @var $model InformationModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'information-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note"><span class="required">*</span>は必須</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'subject'); ?>
			<?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>255, 'class'=> 'input-xxlarge' )); ?>
			<?php echo $form->error($model,'subject'); ?>
		</div>
	</div>
	
	
	<div class="row-fluid">
		<!--
		<div class="span6">
			<?php echo $form->labelEx($model,'create_date'); ?>
			<?php echo $form->textField($model,'create_date'); ?>
			<?php echo $form->error($model,'create_date'); ?>
		</div>
		<div class="span6">
			<?php echo $form->labelEx($model,'update_date'); ?>
			<?php echo $form->textField($model,'update_date'); ?>
			<?php echo $form->error($model,'update_date'); ?>
		</div>
		-->
		<div class="span6">
			<?php echo $form->labelEx($model,'display_date'); ?>
			<?php echo $form->textField($model,'display_date'); ?>
			<?php echo $form->error($model,'display_date'); ?>
		</div>
	</div>

	
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->labelEx($model,'begin_date'); ?>
			<?php echo $form->textField($model,'begin_date'); ?>
			<?php echo $form->error($model,'begin_date'); ?>
		</div>
		<div class="span4">
			<?php echo $form->labelEx($model,'end_date'); ?>
			<?php echo $form->textField($model,'end_date'); ?>
			<?php echo $form->error($model,'end_date'); ?>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->labelEx($model,'open_status'); ?>
			<?php echo $form->dropDownList($model, 'open_status', InformationModel::getStatusList(), array() ); ?>
			<?php echo $form->error($model,'open_status'); ?>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'description'); ?>
			<?php echo $form->textArea($model,'description',array( 'row' => 2, 'cols'=>50,'class'=> 'input-xxlarge')); ?>
			<?php echo $form->error($model,'description'); ?>
		</div>
	</div>
	
	<div class="row-fluid">
		<div class="span12">
			<?php echo $form->labelEx($model,'contents'); ?>
			<?php echo $form->textArea($model,'contents',array('rows'=>6, 'cols'=>50,'class'=> 'input-xxlarge cleditor')); ?>
			<?php echo $form->error($model,'contents'); ?>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span12">
			<div class="button-column buttons">
				<?php echo CHtml::submitButton(
						$model->isNewRecord ? '投稿！' : '編集！',
						array(
							'class' => 'btn btn-large btn-info'
						)
					); ?>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->