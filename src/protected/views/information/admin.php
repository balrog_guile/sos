<?php
/* @var $this InformationController */
/* @var $model InformationModel */

$this->breadcrumbs=array();

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#information-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>お知らせ記事</h1>


<div class="search-form" style="">
<?php //$this->renderPartial('_search',array(
	//'model'=>$model,
//)); ?>
</div><!-- search-form -->


<!-- 操作パネル -->
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 操作パネル</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<div class="row-fluid">
				
				
				<?php echo CHtml::link(
						'新規登録',
						Yii::app()->createUrl('information/create'),
						array( 'class' => 'btn btn-large btn-primary')
					);
				?>
			</div>
		</div>
	</div>
</div>
<!-- /操作パネル -->


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'information-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'display_date',
		'subject',
		'begin_date',
		'end_date',
		'description',
		array(
			'name' => 'open_status',
			'value' => function( $data ){
				$list = InformationModel::getStatusList();
				return $list[$data->open_status];
			}
		),
		array(
			'class'=>'CButtonColumn',
			'template' => '{update} {delete}'
		),
	),
)); ?>
