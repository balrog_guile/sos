<?php
/* @var $this InformationController */
/* @var $model InformationModel */

$this->breadcrumbs=array();

$this->menu=array();
?>

<h1>お知らせ作成</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>