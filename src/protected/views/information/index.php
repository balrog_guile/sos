<?php
/* @var $this InformationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Information Models',
);

$this->menu=array(
	array('label'=>'Create InformationModel', 'url'=>array('create')),
	array('label'=>'Manage InformationModel', 'url'=>array('admin')),
);
?>

<h1>Information Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
