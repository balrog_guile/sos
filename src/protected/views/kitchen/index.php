<?php $this->breadcrumbs=array(); ?>
<h1>オーダーが入りました</h1>
<div class="panel panel-default">
    <div class="panel-body">
        <table class="table table-bordered table-hovered table-striped">
            <thead>
                <tr>
                    <th width="20%">オーダー日時</th>
                    <th>詳細</th>
                </tr>
            </thead>
            <tbody data-bind="foreach:orderList">
                <tr>
                    <td>
                        <span data-bind="text: order_date"></span><hr />
                        <a
                            href="javascript:void(0);"
                            class="col-xs-12 btn btn-xs btn-success"
                            >
                                全調理済
                        </a>
                        <a
                            href="javascript:void(0);"
                            class="col-xs-12 btn btn-xs btn-danger"
                            >
                                全キャンセル
                        </a>
                    </td>
                    <td>
                        <!-- 詳細テーブル -->
                        <table class="table table-bordered table-hovered table-striped">
                            <thead>
                                <tr>
                                    <th>テーブル番号</th>
                                    <th>商品</th>
                                    <th>個数</th>
                                    <th>操作</th>
                                </tr>
                            </thead>
                            <tbody data-bind="foreach: detail">
                                <tr>
                                    <td><span data-bind="text: seat_code"></span></td>
                                    <td><span data-bind="text: name"></span></td>
                                    <td><span data-bind="text: qty"></span></td>
                                    <td>
                                        <span data-bind="if:cooked_flag==0">
                                            <a
                                                href="javascript:void(0);"
                                                class="col-xs-6 btn btn-xs btn-info"
                                                data-bind="click: changeStatus.bind($data, $element )"
                                                data-status="cooked"
                                            >
                                                    調理済みにする
                                            </a>
                                        </span>
                                        <span data-bind="if:cooked_flag==1">
                                            <a
                                                href="javascript:void(0);"
                                                class="col-xs-6 btn btn-xs btn-success"
                                                data-bind="click: changeStatus.bind($data, $element )"
                                                data-status="deliv"
                                            >
                                                    配膳済みにする
                                            </a>
                                        </span>
                                            <a
                                                href="javascript:void(0);"
                                                class="col-xs-6 btn btn-xs btn-danger"
                                                >
                                                    キャンセル
                                            </a>
                                        
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                        <!-- /詳細テーブル -->
                    </td>
                </tr>
            </tobody>
        </table>
    </div>
</div>