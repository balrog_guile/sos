
<!-- navbar side -->
<nav class="navbar-default navbar-static-side" role="navigation">
    
    <!-- sidebar-collapse -->
    <div class="sidebar-collapse">
        <!-- side-menu -->
        <ul class="nav" id="side-menu">
            
            
            
            <!-- user info -->
            <?php  if( (int)Yii::app()->user->id > 0 ): ?>
                <li>
                    <div class="user-section">
                        <div class="user-section-inner">
                            <img src="<?= Cms::getUrlRoot(); ?>adminAssets/assets/img/user.jpg" alt="">
                        </div>
                        <div class="user-info">
                            <div class="g-font-j">
                            <?php if( Yii::app()->user->id == 1 ): ?>
                                管理ログイン
                            <?php elseif( Yii::app()->controller->shop !== null ): ?>
                                <?php echo Yii::app()->controller->shop->name; ?>さん
                                
                                <div class="row">
                                        <a
                                            href="<?php echo Yii::app()->getBaseUrl(true); ?>?SOS_THEME_SELECT=<?php echo Yii::app()->controller->shop->shop_code; ?>"
                                            target="_blank"
                                            class="col-xs-12 alert alert-info"
                                            style="margin-top: 0.5em; text-align: center; font-size: 0.5em; padding: 0.3em;"
                                        >
                                            ショップトップ
                                        </a>
                                </div>
                            <?php else: ?>
                                こんにちは！
                            <?php endif; ?>
                            </div>
                            <div class="user-text-online">
                                
                                <!--
                                <span class="user-circle-online btn btn-success btn-circle "></span>&nbsp;Online
                                -->
                                <br />
                            </div>
                        </div>
                    </div>
                    <!--end user image section-->
                </li>
            <?php endif; ?>
            <!-- /user info -->
            
            
            
            <!-- 検索 -->
            <!--
            <li class="sidebar-search">
                <div class="input-group custom-search-form">
                    <input type="text" class="form-control" placeholder="Search...">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="button">
                            <i class="fa fa-search"></i>
                        </button>
                    </span>
                </div>
            </li>
            -->
            <!-- /検索 -->
            
            <!--
            <li class="selected">
                <a href="index.html"><i class="fa fa-dashboard fa-fw"></i>Dashboard</a>
            </li>
            -->
            
            <!-- ショップ管理 -->
            <li>
                <a href="<?= Yii::app()->createAbsoluteUrl('shop/admin'); ?>">
                    <i class="fa fa-hospital-o fa-fw"></i>
                    店舗<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
					<?php if( Yii::app()->user->id == 1 ): ?>
                    <li>
                        <a href="<?= Yii::app()->createAbsoluteUrl('shop/admin'); ?>">店舗管理</a>
                    </li>
					<?php else: ?>
                    <li>
                        <a href="<?= Yii::app()->createAbsoluteUrl('shop_post_content/admin/'); ?>">投稿管理</a>
                        <a href="<?= Yii::app()->createAbsoluteUrl('shop/contents/id/'); ?>">コンテンツ管理</a>
                    </li>
					<?php endif; ?>
                </ul>
                <!-- second-level-items -->
            </li>
            <!-- /ショップ管理 -->
            
            
            <!-- 商品管理 -->
            <li>
                <a href="<?= Yii::app()->createAbsoluteUrl('shop/admin'); ?>">
                    <i class="fa fa-hospital-o fa-fw"></i>
                    商品<span class="fa arrow"></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?= Yii::app()->createAbsoluteUrl('product/categories'); ?>">商品カテゴリ</a>
                    </li>
                    <li>
                        <a href="<?= Yii::app()->createAbsoluteUrl('product/product/admin'); ?>">商品管理</a>
                    </li>
                    <li>
                        <a href="morris.html">商品追加</a>
                    </li>
                </ul>
                <!-- second-level-items -->
            </li>
            <!-- /ショップ管理 -->
            
            
            <!-- コールセンター業務 -->
            <li>
                <a href="<?= Yii::app()->createAbsoluteUrl('callcenter'); ?>">
                    <i class="fa fa-hospital-o fa-fw"></i>
                    コールセンター<span class="fa arrow"></span>
                </a>
            </li>
            <!-- /コールセンター業務 -->
            
            
        </ul>
        <!-- end side-menu -->
    </div>
    <!-- end sidebar-collapse -->
</nav>
<!-- end navbar side -->

