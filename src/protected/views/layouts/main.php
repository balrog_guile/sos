<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title><?php echo CHtml::encode($this->pageTitle); ?></title>
    
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/bootstrap/bootstrap.css" rel="stylesheet" />
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/pace/pace-theme-big-counter.css" rel="stylesheet" />
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/css/style.css" rel="stylesheet" />
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/css/main-style.css" rel="stylesheet" />
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/morris/morris-0.4.3.min.css" rel="stylesheet" />
    
    <!-- add styles -->
    <link rel="stylesheet" href="//fonts.googleapis.com/earlyaccess/notosansjapanese.css">
    <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href="<?= Cms::getUrlRoot(); ?>adminAssets/assets/css/addStyle.css" rel="stylesheet" />
    
    <!-- /add styles -->
    
   </head>
<body>
    <!--  wrapper -->
    <div id="wrapper">
        <!-- navbar top -->
        <?= Yii::app()->controller->renderSubViewFull( __DIR__.'/adminTopNavigation' ); ?>
        <!-- end navbar top -->
        
        <!-- サイドナビ -->
        <?= Yii::app()->controller->renderSubViewFull( __DIR__.'/adminSideNavi' ); ?>
        <!-- /サイドナビ -->
        
        <!--  メイン -->
        <div id="page-wrapper">
            <?php echo $content; ?>
        </div>
        <!--  /メイン -->
    </div>
    <!-- end wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/jquery-1.10.2.js"></script>
    <script src="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/bootstrap/bootstrap.min.js"></script>
    <script src="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/metisMenu/jquery.metisMenu.js"></script>
    <script src="<?= Cms::getUrlRoot(); ?>adminAssets/assets/plugins/pace/pace.js"></script>
    <script src="<?= Cms::getUrlRoot(); ?>adminAssets/assets/scripts/siminta.js"></script>
    <!-- Page-Level Plugin Scripts-->
    
    

</body>

</html>
<?php
// icon参考
//http://fontawesome.io/icons/
?>