<?php
/* @var $this MemberController */
/* @var $model MemberModel */

$this->breadcrumbs=array(
	'Member Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MemberModel', 'url'=>array('index')),
	array('label'=>'Manage MemberModel', 'url'=>array('admin')),
);
?>

<h1>Create MemberModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>