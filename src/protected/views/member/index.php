<?php
/* @var $this MemberController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Member Models',
);

$this->menu=array(
	array('label'=>'Create MemberModel', 'url'=>array('create')),
	array('label'=>'Manage MemberModel', 'url'=>array('admin')),
);
?>

<h1>Member Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
