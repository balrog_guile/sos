<?php
/* @var $this MemberController */
/* @var $model MemberModel */

$this->breadcrumbs=array(
	'Member Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List MemberModel', 'url'=>array('index')),
	array('label'=>'Create MemberModel', 'url'=>array('create')),
	array('label'=>'View MemberModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage MemberModel', 'url'=>array('admin')),
);
?>

<h1>Update MemberModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>