<?php
/* @var $this MemberController */
/* @var $model MemberModel */

$this->breadcrumbs=array(
	'Member Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List MemberModel', 'url'=>array('index')),
	array('label'=>'Create MemberModel', 'url'=>array('create')),
	array('label'=>'Update MemberModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete MemberModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MemberModel', 'url'=>array('admin')),
);
?>

<h1>View MemberModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'login_id',
		'password',
		'password_hash',
		'email',
		'member_status',
		'create_at',
		'update_at',
		'last_login_time',
		'leave_at',
		'activate_hash',
		'activate_expire',
		'company',
		'name1',
		'name2',
		'kana1',
		'kana2',
		'zip',
		'pref',
		'address1',
		'address2',
		'address3',
		'tel1',
		'tel2',
		'fax',
		'memo',
	),
)); ?>
