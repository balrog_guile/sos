<?php
/* @var $this CustomersController */
/* @var $model CustomersModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'customers-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name1'); ?>
		<?php echo $form->textField($model,'name1',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'name1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'name2'); ?>
		<?php echo $form->textField($model,'name2',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'name2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kana1'); ?>
		<?php echo $form->textField($model,'kana1',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'kana1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kana2'); ?>
		<?php echo $form->textField($model,'kana2',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'kana2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'zip'); ?>
		<?php echo $form->textField($model,'zip'); ?>
		<?php echo $form->error($model,'zip'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'pref'); ?>
		<?php echo $form->dropDownList($model,'pref',$pref); ?>
		<?php echo $form->error($model,'pref'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_1'); ?>
		<?php echo $form->textField($model,'address_1'); ?>
		<?php echo $form->error($model,'address_1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_2'); ?>
		<?php echo $form->textField($model,'address_2'); ?>
		<?php echo $form->error($model,'address_2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address_3'); ?>
		<?php echo $form->textField($model,'address_3'); ?>
		<?php echo $form->error($model,'address_3'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tell'); ?>
		<?php echo $form->textField($model,'tell'); ?>
		<?php echo $form->error($model,'tell'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax'); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'password'); ?>
		<?php echo $form->passwordField($model,'password'); ?>※編集しない場合は空に
		<?php echo $form->error($model,'password'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::hiddenField('mode','confirm'); ?>
		<?php echo CHtml::error($model,'mode'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->