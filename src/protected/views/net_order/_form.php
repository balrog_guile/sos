<?php
/* @var $this Net_orderController */
/* @var $model OrderHistoryModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'order-history-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

		<?php echo $form->errorSummary($model);?>

	<h2>注文履歴一覧</h2>

	<hr>

	<h3>注文情報</h3><br>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'customer_id'); ?>
			<?php echo $model->customer_id; ?>
			<?php if( !empty( $model->customer_id ) ): ?>
				<a href="<?php echo $this->createUrl( '/customers/update/', array( 'id' => $model->customer_id ) ); ?>">顧客情報へ</a>
			<?php endif; ?>
			<?php echo $form->hiddenField($model,'customer_id',array('value'=>$model->customer_id)); ?>
			<?php echo $form->error($model,'customer_id'); ?>
			</div>
		</div>

		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'order_date'); ?>
			<?php echo $model->order_date; ?>
			<?php echo $form->hiddenField($model,'order_date',array('value'=>$model->order_date)); ?>
			<?php echo $form->error($model,'order_date'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'update_date'); ?>
			<?php echo $model->update_date; ?>
			<?php echo $form->hiddenField($model,'update_date',array('value'=>$model->update_date)); ?>
			<?php echo $form->error($model,'update_date'); ?>
			</div>
		</div>

		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'order_status'); ?>
			<?php echo $form->dropDownList($model,'order_status', VarHelper::order_history_status(false, true)); ?>
			<?php echo $form->error($model,'order_status'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'payment_status'); ?>
			<?php echo $form->dropDownList($model,'order_status', VarHelper::payment_status(false, true)); ?>
			<?php echo $form->error($model,'payment_status'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'item_total'); ?>
			<?php echo $form->textField($model,'item_total'); ?>
			<?php echo $form->error($model,'item_total'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'payment_method'); ?>
			<?php echo $form->textField($model,'payment_method',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'payment_method'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'payment_fee'); ?>
			<?php echo $form->textField($model,'payment_fee'); ?>
			<?php echo $form->error($model,'payment_fee'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'delivery_fee'); ?>
			<?php echo $form->textField($model,'delivery_fee'); ?>
			<?php echo $form->error($model,'delivery_fee'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'grand_total'); ?>
			<?php echo $model->grand_total; ?>
			<?php echo $form->hiddenField($model,'grand_total',array('value'=>$model->grand_total)); ?>
			<?php echo $form->error($model,'grand_total'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'payment_date'); ?>
			<?php echo $form->textField($model,'payment_date'); ?>
			<?php echo $form->error($model,'payment_date'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'canceld_date'); ?>
			<?php echo $form->textField($model,'canceld_date'); ?>
			<?php echo $form->error($model,'canceld_date'); ?>
			</div>
		</div>
	</div>

	<hr />

	<h3>顧客情報</h3><br>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'name1'); ?>
			<?php echo $form->textField($model,'name1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'name1'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'name2'); ?>
			<?php echo $form->textField($model,'name2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'name2'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'kana1'); ?>
			<?php echo $form->textField($model,'kana1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'kana1'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'kana2'); ?>
			<?php echo $form->textField($model,'kana2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'kana2'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'zip'); ?>
			<?php echo $form->textField($model,'zip',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'zip'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'pref'); ?>
			<?php echo $form->textField($model,'pref',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'pref'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'address1'); ?>
			<?php echo $form->textField($model,'address1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'address1'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'address2'); ?>
			<?php echo $form->textField($model,'address2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'address2'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'address3'); ?>
			<?php echo $form->textField($model,'address3',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'address3'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'mailaddress'); ?>
			<?php echo $form->textField($model,'mailaddress',array('size'=>60,'maxlength'=>255)); ?>
			<?php echo $form->error($model,'mailaddress'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'tel'); ?>
			<?php echo $form->textField($model,'tel',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'tel'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'fax'); ?>
			<?php echo $form->textField($model,'fax',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'fax'); ?>
			</div>
		</div>
	</div>

	<hr />

	<h3>配送情報</h3><br>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_name1'); ?>
			<?php echo $form->textField($model,'d_name1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_name1'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_name2'); ?>
			<?php echo $form->textField($model,'d_name2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_name2'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_kana1'); ?>
			<?php echo $form->textField($model,'d_kana1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_kana1'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_kana2'); ?>
			<?php echo $form->textField($model,'d_kana2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_kana2'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_zip'); ?>
			<?php echo $form->textField($model,'d_zip',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'d_zip'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_pref'); ?>
			<?php echo $form->textField($model,'d_pref',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_pref'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_address1'); ?>
			<?php echo $form->textField($model,'d_address1',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_address1'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_address2'); ?>
			<?php echo $form->textField($model,'d_address2',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_address2'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_address3'); ?>
			<?php echo $form->textField($model,'d_address3',array('size'=>60,'maxlength'=>125)); ?>
			<?php echo $form->error($model,'d_address3'); ?>
			</div>
		</div>


		<div class="span6">
			<div class="">
			<?php echo $form->labelEx($model,'d_tel'); ?>
			<?php echo $form->textField($model,'d_tel',array('size'=>45,'maxlength'=>45)); ?>
			<?php echo $form->error($model,'d_tel'); ?>
			</div>
		</div>
	</div>

	<div class="row-fluid">
		<div class="span6">
			<div class="">
			<?php echo $form->hiddenField($model,'order_type',array('value'=>$model->order_type)); ?>
			<?php echo $form->error($model,'order_type'); ?>
			</div>
		</div>
	</div>

	<hr />

		<div class="buttons">
			<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save',array('class'=>'btn btn-info')); ?>
		</div>

	<hr />

<?php $this->endWidget(); ?>

</div><!-- form -->