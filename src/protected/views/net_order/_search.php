<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */
/* @var $form CActiveForm */
?>

<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 検索</h2>
			<div class="box-icon">
			</div>
		</div>

		<div class="box-content">
			<div class="wide form">
			<?php $form=$this->beginWidget('CActiveForm', array(
				'action'=>Yii::app()->createUrl($this->route),
				'method'=>'get',
			)); ?>
				<div class="row-fluid">
					<div class="span3">
						<?php echo $form->label($model,'order_status'); ?>
						<?php echo $form->dropDownList($model,'order_status', VarHelper::order_history_status(false, true), array('class' => 'input-block-level')); ?>
					</div>

					<div class="span3">
						<?php echo $form->label($model,'order_date'); ?>
						<?php echo $form->textField($model,'order_date_from', array('class' => 'input-block-level datepicker')); ?>
						〜
						<?php echo $form->textField($model,'order_date_to', array('class' => 'input-block-level datepicker')); ?>
					</div>

					<div class="span3">
						<?php echo $form->label($model,'payment_date'); ?>
						<?php echo $form->textField($model,'payment_date_from', array('class' => 'input-block-level datepicker')); ?>
						〜
						<?php echo $form->textField($model,'payment_date_to', array('class' => 'input-block-level datepicker')); ?>
					</div>

					<div class="span3">
						<?php echo $form->hiddenField($model,'order_type', array('class' => 'input-block-level datepicker', 'value' => 1)); ?>
					</div>

				</div>

				<div class="row-fluid">
					<div class="span12 buttons">
						<?php echo CHtml::submitButton('Search', array('class' => 'btn btn-info') ); ?>
					</div>
				</div>

			<?php $this->endWidget(); ?>

			</div>
		</div>
	</div>
</div>