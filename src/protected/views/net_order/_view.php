<?php
/* @var $this Net_orderController */
/* @var $data OrderHistoryModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_date')); ?>:</b>
	<?php echo CHtml::encode($data->order_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_status')); ?>:</b>
	<?php echo CHtml::encode($data->order_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_status')); ?>:</b>
	<?php echo CHtml::encode($data->payment_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_total')); ?>:</b>
	<?php echo CHtml::encode($data->item_total); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_method')); ?>:</b>
	<?php echo CHtml::encode($data->payment_method); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_fee')); ?>:</b>
	<?php echo CHtml::encode($data->payment_fee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delivery_fee')); ?>:</b>
	<?php echo CHtml::encode($data->delivery_fee); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grand_total')); ?>:</b>
	<?php echo CHtml::encode($data->grand_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_date')); ?>:</b>
	<?php echo CHtml::encode($data->payment_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('canceld_date')); ?>:</b>
	<?php echo CHtml::encode($data->canceld_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name1')); ?>:</b>
	<?php echo CHtml::encode($data->name1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name2')); ?>:</b>
	<?php echo CHtml::encode($data->name2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kana1')); ?>:</b>
	<?php echo CHtml::encode($data->kana1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('kana2')); ?>:</b>
	<?php echo CHtml::encode($data->kana2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('zip')); ?>:</b>
	<?php echo CHtml::encode($data->zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('pref')); ?>:</b>
	<?php echo CHtml::encode($data->pref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address1')); ?>:</b>
	<?php echo CHtml::encode($data->address1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address2')); ?>:</b>
	<?php echo CHtml::encode($data->address2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('address3')); ?>:</b>
	<?php echo CHtml::encode($data->address3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mailaddress')); ?>:</b>
	<?php echo CHtml::encode($data->mailaddress); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tel')); ?>:</b>
	<?php echo CHtml::encode($data->tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('fax')); ?>:</b>
	<?php echo CHtml::encode($data->fax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_name1')); ?>:</b>
	<?php echo CHtml::encode($data->d_name1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_name2')); ?>:</b>
	<?php echo CHtml::encode($data->d_name2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_kana1')); ?>:</b>
	<?php echo CHtml::encode($data->d_kana1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_kana2')); ?>:</b>
	<?php echo CHtml::encode($data->d_kana2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_zip')); ?>:</b>
	<?php echo CHtml::encode($data->d_zip); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_pref')); ?>:</b>
	<?php echo CHtml::encode($data->d_pref); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_address1')); ?>:</b>
	<?php echo CHtml::encode($data->d_address1); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_address2')); ?>:</b>
	<?php echo CHtml::encode($data->d_address2); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_address3')); ?>:</b>
	<?php echo CHtml::encode($data->d_address3); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('d_tel')); ?>:</b>
	<?php echo CHtml::encode($data->d_tel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_type')); ?>:</b>
	<?php echo CHtml::encode($data->order_type); ?>
	<br />

	*/ ?>

</div>