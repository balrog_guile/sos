<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */

$this->breadcrumbs=array();

$this->menu=array(
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#receipt-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>売上管理</h1>

<!-- 検索 -->
<div class="search-form">
<?php $this->renderPartial(
	'_search',
	array(
		'model'=>$model,
	)
); ?>
</div>
<!-- /検索 -->

<!-- 合計表示 -->
<?php /*
<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 検索対象の合計</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<div class="wide form">
				<div class="row-fluid">
					<div class="span3">
						<h5>件数</h5>
						<p><?php echo number_format($calc['count']); ?>件</p>
					</div>
					<div class="span3">
						<h5>商品代金合計</h5>
						<p><?php echo number_format($calc['item_total_sum']); ?>円</p>
					</div>
					<div class="span3">
						<h5>お支払い総額</h5>
						<p><?php echo number_format($calc['grand_total_sum']); ?>円</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
 */?>
<!-- /合計表示 -->

<?php $data_provider = $model->net_order_search(); ?>
<div class="adminsave">
	<table class="table table-bordered table-striped">
		<thead>
			<tr>
				<th>顧客ID</th>
				<th>オーダー日</th>
				<th>ステータス</th>
				<th>支払い</th>
				<th>商品代金</th>
				<th>お支払い額</th>
				<th>支払い方法</th>
				<th>支払日</th>
				<th>姓</th>
				<th>名</th>
				<th>都道府県</th>
				<th>詳細</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach( $data_provider->getData() as $data ):?>
			<tr>
				<td>
					<?php echo $data->customer_id; ?>
				</td>
				<td>
					<?php echo $data->order_date; ?>
				</td>
				<td>
					<?php echo VarHelper::order_history_status($data->order_status, true);?>
				</td>
				<td>
					<?php echo ($data->payment_status == 0 ? '未払い' : '支払い済み' ); ?>
				</td>
				<td>
					<?php echo $data->item_total; ?>円
				</td>
				<td>
					<?php echo $data->grand_total; ?>円
				</td>
				<td>
					<?php echo $data->payment_method; ?>
				</td>
				<td>
					<?php echo $data->payment_date; ?>
				</td>
				<td>
					<?php echo $data->name1; ?>
				</td>
				<td>
					<?php echo $data->name2; ?>
				</td>
				<td>
					<?php echo $data->pref; ?>
					<?= $data->id; ?>
				</td>
				<td>
					<?php
						echo CHtml::link(
							'編集',
							Yii::app()->createUrl( 'net_order/update/id/'.$data->id  ),
							array( 'class' => 'btn btn-info' )
						);
					?>
				</td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>

<!-- ページャー -->
<?php
$this->widget(
	'CLinkPager',
	array(
		'pages' => $data_provider->getPagination(),
	)
);?>
<!-- /ページャー -->