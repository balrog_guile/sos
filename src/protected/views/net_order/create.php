<?php
/* @var $this Net_orderController */
/* @var $model OrderHistoryModel */

$this->breadcrumbs=array(
	'Order History Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List OrderHistoryModel', 'url'=>array('index')),
	array('label'=>'Manage OrderHistoryModel', 'url'=>array('admin')),
);
?>

<h1>Create OrderHistoryModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>