<?php
/* @var $this Net_orderController */
/* @var $model OrderHistoryModel */

$this->breadcrumbs=array(
	'Order History Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

// $this->menu=array(
// 	array('label'=>'List OrderHistoryModel', 'url'=>array('index')),
// 	array('label'=>'Create OrderHistoryModel', 'url'=>array('create')),
// 	array('label'=>'View OrderHistoryModel', 'url'=>array('view', 'id'=>$model->id)),
// 	array('label'=>'Manage OrderHistoryModel', 'url'=>array('admin')),
// );
?>

<h1>Update OrderHistoryModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>

<table class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>製品名</th>
			<th>単価</th>
			<th>数量</th>
			<th>合計</th>
		</tr>
	</thead>
	<tbody>
		<?php foreach( $model->detail as $detail ): ?>
		<tr>
			<td><?php echo $detail->name; ?></td>
			<td><?php echo $detail->price; ?>円</td>
			<td><?php echo $detail->qty; ?>個</td>
			<td><?php echo $detail->sub_total; ?>円</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
</table>