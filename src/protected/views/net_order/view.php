<?php
/* @var $this Net_orderController */
/* @var $model OrderHistoryModel */

$this->breadcrumbs=array(
	'Order History Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List OrderHistoryModel', 'url'=>array('index')),
	array('label'=>'Create OrderHistoryModel', 'url'=>array('create')),
	array('label'=>'Update OrderHistoryModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete OrderHistoryModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage OrderHistoryModel', 'url'=>array('admin')),
);
?>

<h1>View OrderHistoryModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customer_id',
		'order_date',
		'update_date',
		'order_status',
		'payment_status',
		'item_total',
		'payment_method',
		'payment_fee',
		'delivery_fee',
		'grand_total',
		'payment_date',
		'canceld_date',
		'name1',
		'name2',
		'kana1',
		'kana2',
		'zip',
		'pref',
		'address1',
		'address2',
		'address3',
		'mailaddress',
		'tel',
		'fax',
		'd_name1',
		'd_name2',
		'd_kana1',
		'd_kana2',
		'd_zip',
		'd_pref',
		'd_address1',
		'd_address2',
		'd_address3',
		'd_tel',
		'order_type',
	),
)); ?>
