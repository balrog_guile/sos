<?php
/* @var $this PageBlocksController */
/* @var $model PageBlocksModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'page-blocks-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<?php echo $form->errorSummary($model); ?>

	<!-- 初期設定 -->
	<?php echo $form->hiddenField($model, 'name' );?>
	<?php echo $form->hiddenField($model, 'page_path' );?>
	<?php echo $form->hiddenField($model, 'global' );?>
	<?php echo $form->hiddenField($model, 'type' );?>
	<!-- /初期設定 -->

	<!-- 持ち越し設定 -->
	<?php foreach( $target as $k => $v ):if( $k == 'types_decode'){continue;} ?>
		<?php  echo CHtml::hiddenField($k, $v ); ?>
	<?php endforeach; ?>
	<!-- /持ち越し設定 -->
	
	
	<hr />
	
	
	<!-- 公開 -->
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->labelEx($model,'open_status'); ?>
			<?php echo $form->checkBox( $model, 'open_status' ); ?>
		</div>
	</div>
	<!-- /公開 -->
	
	
	<hr />
	
	
	
	<!-- 公開日設定 -->
	<div class="row-fluid">
		<div class="span4">
			<?php echo $form->labelEx($model,'open_date'); ?>
			<?php echo $form->textField($model,'open_date'); ?>
			<?php echo $form->error($model,'open_date'); ?>
		</div>
		<div class="span4">
			<?php echo $form->labelEx($model,'close_date'); ?>
			<?php echo $form->textField($model,'close_date'); ?>
			<?php echo $form->error($model,'close_date'); ?>
		</div>
	</div>
	<!-- /公開日設定 -->


	<hr />


	<!-- HTMLテキストの場合 ----------------------------------------------------->
	<?php if( $type == 'html'): ?>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx($model,'content'); ?>
				<?php echo $form->textArea(
						$model,
						'content',
						array(
							'style' => 'width:100%; height: 200px;'
						)
					);
			?>
				<?php echo $form->error($model,'content'); ?>
			</div>
		</div>
		<!-- 表示しない項目 -->
		<?php echo $form->hiddenField($model, 'image_path'); ?>
		<!-- /表示しない項目 -->
	<?php endif; ?>
	<!-- /HTMLテキストの場合 ---------------------------------------------------->



	<!-- WYSIWYG -------------------------------------------------------------->
	<?php if( $type == 'richText'): ?>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx($model,'content'); ?>
				<?php echo $form->textArea(
						$model,
						'content',
						array(
							'style' => 'width:100%; height: 200px;',
							'class' => 'cleditor'
						)
					);
			?>
				<?php echo $form->error($model,'content'); ?>
			</div>
		</div>
		<!-- 表示しない項目 -->
		<?php echo $form->hiddenField($model, 'image_path'); ?>
		<!-- /表示しない項目 -->
	<?php endif; ?>
	<!-- /WYSIWYG ------------------------------------------------------------->
	
	
	
	
	<!-- 画像 -------------------------------------------------------------->
	<?php if( $type == 'image'): ?>
		<div class="row-fluid">
			<div id="thumbnail" class="span8">
				<?php echo $form->labelEx($model,'image_path'); ?>
				
				<?php if( $model->image_path != '' ): ?>
					<img src="<?php echo Yii::app()->baseUrl; ?><?php echo $model->image_path; ?>" alt="サムネイル画像" style="width:100px;">
				<?php endif; ?>
				
				<?php echo $form->hiddenField($model,'image_path'); ?>
				<?php
					echo CHtml::fileField(
						'image_path',
						'',
						array(
							'data-no-uniform' => 'true',
						)
					)
				?>
				<?php echo $form->error($model,'image_path'); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx(
					$model,
					'alt',
					array(
						'style' => ' margin-top:20px;'
					)
				); ?>
				<?php echo $form->textField(
					$model,
					'alt',
					array(
						'style' => 'width:80%;'
					)
				); ?>
				<?php echo $form->error($model,'alt'); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx(
					$model,
					'link_url',
					array(
						'style' => ' margin-top:20px;'
					)
				); ?>
				<?php echo $form->textField(
					$model,
					'link_url',
					array(
						'style' => 'width:80%;'
					)
				); ?>
				<?php echo $form->error($model,'link_url'); ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- /画像 ------------------------------------------------------------->
	
	
	
	<!-- 画像/テキスト -------------------------------------------------------------->
	<?php if( $type == 'image_text'): ?>
		<div class="row-fluid">
			<div id="thumbnail" class="span8">
				<?php echo $form->labelEx($model,'image_path'); ?>
				
				<?php if( $model->image_path != '' ): ?>
					<img src="<?php echo Yii::app()->baseUrl; ?><?php echo $model->image_path; ?>" alt="サムネイル画像" style="width:100px;">
				<?php endif; ?>
				
				<?php echo $form->hiddenField($model,'image_path'); ?>
				<?php echo CHtml::fileField('image_path', '',array('data-no-uniform' => 'true',)) ?>
				<?php echo $form->error($model,'image_path'); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx(
					$model,
					'alt',
					array(
						'style' => ' margin-top:20px;'
					)
				); ?>
				<?php echo $form->textField(
					$model,
					'alt',
					array(
						'style' => 'width:80%;'
					)
				); ?>
				<?php echo $form->error($model,'alt'); ?>
			</div>
		</div>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx(
					$model,
					'link_url',
					array(
						'style' => ' margin-top:20px;'
					)
				); ?>
				<?php echo $form->textField(
					$model,
					'link_url',
					array(
						'style' => 'width:80%;'
					)
				); ?>
				<?php echo $form->error($model,'link_url'); ?>
			</div>
			<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx(
					$model,
					'content',
					array(
						'style' => ' margin-top:20px;'
					)
				); ?>
				<?php echo $form->textArea(
					$model,
					'content',
					array(
						'style' => 'width:100%; height: 200px;'
					)
				); ?>
				<?php echo $form->error($model,'content'); ?>
			</div>
		</div>
	<?php endif; ?>
	<!-- /画像/テキスト ------------------------------------------------------------->
	
	
	
	<!-- YOUTUBE -------------------------------------------------------------->
	<?php if( $type == 'youtube'): ?>
		<div class="row-fluid">
			<div class="span8">
				<?php echo $form->labelEx($model,'link_url'); ?>
				<?php echo $form->textField(
					$model,
					'link_url',
					array(
						'style' => 'width:80%;'
					)
				); ?>
				<?php echo $form->error($model,'link_url'); ?>
			</div>
		</div>
		<!-- 表示しない項目 -->
		<?php echo $form->hiddenField($model, 'image_path'); ?>
		<!-- /表示しない項目 -->
	<?php endif; ?>
	<!-- /YOUTUBE ------------------------------------------------------------->
	
	
	
	<!-- 順番 -->
	<?php if( $model->isNewRecord !== TRUE ): ?>
		<?php echo $form->hiddenField( $model, 'rank' ); ?>
	<?php endif; ?>
	<!-- /順番 -->
	
	
	<!-- 実行ボタン -->
	<hr />
	<div class="">
		<?php echo CHtml::submitButton(
				($model->isNewRecord ? '追加' : '編集'),
				array( 'class' => 'btn btn-info')
			);
		?>
	</div>
	<!-- /実行ボタン -->

<?php $this->endWidget(); ?>

</div><!-- form -->