<?php
/* @var $this PageBlocksController */
/* @var $model PageBlocksModel */

$this->breadcrumbs=array();

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#page-blocks-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>ページブロックを編集</h1>

<hr />
<?php echo CHtml::link( '　　　　ページヘ戻る　　　　', '/'.$target['url'], array( 'class' => 'btn btn-success') ); ?>
<hr />
<?php echo CHtml::form(
		Yii::app()->createUrl('PageBlocks/create'),
		'get',
		array()
	);
?> 
<?php echo CHtml::dropDownList('type', '', $type_list ); ?>
<?php echo CHtml::submitButton( '追加', array( 'class' => 'btn btn-info') ); ?>
<?php foreach( $target as $k => $v ):if( $k == 'types_decode'){continue;} ?>
	<?php  echo CHtml::hiddenField($k, $v ); ?>
<?php endforeach; ?>
<?php echo CHtml::endForm(); ?>
<hr />

<?php echo CHtml::form( Yii::app()->createUrl('PageBlocks/rankchange') ); ?>
<?php foreach( $target as $k => $v ):if( $k == 'types_decode'){continue;} ?>
	<?php  echo CHtml::hiddenField($k, $v ); ?>
<?php endforeach; ?>
<?php $data_provider = $model->search(); ?>
<table class="table table-bordered table-bordered">
	<thead>
		<tr>
			<th>公開</th>
			<th>タイプ</th>
			<th>公開日</th>
			<th>画像</th>
			<th>内容</th>
			<th>URL</th>
			<th>並べ替え</th>
			<th>操作</th>
		</tr>
	</thead>
	<tbody class="datarow">
		<?php foreach( $data_provider->getData() as $data ): ?>
		<tr>
			<td>
				<?php if($data->open_status == 1 ): ?>
					公開
				<?php else: ?>
					非公開
				<?php endif; ?>
			</td>
			<td>
				<?php echo CHtml::hiddenField( 'id[]', $data->id ); ?>
				<?php echo $type_list[$data->type]; ?>
			</td>
			<td>
				<?php echo $data->open_date; ?><br />
				〜<br />
				<?php echo $data->close_date; ?>
			</td>
			<td>
				<?php if( ($data->type == 'image' )||($data->type == 'image_text' )): ?>
					<?php echo CHtml::image( $data->image_path, '', array( 'width' => '120px' ) ); ?>
				<?php else: ?>
					画像なし
				<?php endif; ?>
			</td>
			<td><?php echo nl2br( htmlspecialchars($data->content, ENT_QUOTES, 'UTF-8' ) ); ?></td>
			<td>
				<?php echo $data->link_url; ?>
			</td>
			<td nowrap>
				<?php echo Chtml::link(
						'▲',
						'',
						array(
							'class' => 'btn btn-mini rankchange',
							'data-changetype' => 'up',
						)
					);
				?>
				<?php echo Chtml::link(
						'▼',
						'',
						array(
							'class' => 'btn btn-mini rankchange',
							'data-changetype' => 'down',
						)
					);
				?>
			</td>
			<td>
				
				<?php echo CHtml::link(
						'編集',
						Yii::app()->createUrl( 'PageBlocks/update', array('id' => $data->id)) . '?' . http_build_query($target),
						array(
							'class' => 'btn btn-info'
						)
					);
				?>
				<br />
				<?php
					echo CHtml::link(
						'削除',
						Yii::app()->createUrl( 'PageBlocks/delete', array('id' => $data->id)) . '?' . http_build_query($target),
						array('class' => 'btn btn-danger')
					);
				?>
			</td>
		</tr>
		<?php endforeach; ?>
	</tbody>
	<tfoot>
		<tr>
			<th colspan="7">
				<?php echo CHtml::submitButton( '表示順確定', array('class' => 'btn btn-info') ); ?>
			</th>
		</tr>
	</tfoot>
</table>
<?php echo CHtml::endForm(); ?>