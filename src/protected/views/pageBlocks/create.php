<?php
/* @var $this PageBlocksController */
/* @var $model PageBlocksModel */

$this->breadcrumbs=array();
$this->menu=array();
?>

<h1>ブロック追加</h1>

<?php $this->renderPartial(
		'_form',
		array(
			'model'=>$model,
			'target' => $target,
			'type' => $type,
		)
	);
?>