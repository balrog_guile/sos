<?php
/* @var $this PageBlocksController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Page Blocks Models',
);

$this->menu=array(
	array('label'=>'Create PageBlocksModel', 'url'=>array('create')),
	array('label'=>'Manage PageBlocksModel', 'url'=>array('admin')),
);
?>

<h1>Page Blocks Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
