<?php
/* @var $this PageBlocksController */
/* @var $model PageBlocksModel */

$this->breadcrumbs=array(
	'Page Blocks Models'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array();
?>

<h1>ブロック編集</h1>

<?php $this->renderPartial('_form', array('model'=>$model,'target' => $target,'type' => $type,)); ?>