<?php
/* @var $this PageBlocksController */
/* @var $model PageBlocksModel */

$this->breadcrumbs=array(
	'Page Blocks Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List PageBlocksModel', 'url'=>array('index')),
	array('label'=>'Create PageBlocksModel', 'url'=>array('create')),
	array('label'=>'Update PageBlocksModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PageBlocksModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PageBlocksModel', 'url'=>array('admin')),
);
?>

<h1>View PageBlocksModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'page_path',
		'global',
		'rank',
		'open_date',
		'close_date',
		'create_date',
		'update_date',
		'image_path',
		'content',
	),
)); ?>
