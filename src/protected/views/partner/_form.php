<?php
/* @var $this PartnerController */
/* @var $model PartnerModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'partner-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>
	<?php echo $form->errorSummary($model); ?>

		<div class="row-fluid">
			<div class="span12">
				<?php echo $form->labelEx($model,'name'); ?>
				<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
				<?php echo $form->error($model,'name'); ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="span12">
				<?php echo $form->labelEx($model,'partner_status'); ?>
				<?php echo $form->dropDownList($model, 'partner_status', PartnerModel::getStatusList(), array() ); ?>
				<?php echo $form->error($model,'partner_status'); ?>
			</div>
		</div>
		
		<div class="row-fluid">
			<div class="buttons span12">
				<?php echo CHtml::submitButton($model->isNewRecord ? '登録' : '編集保存', array( 'class' => 'btn btn-large btn-info' ) ); ?>
			</div>
		</div>
	</div>
<?php $this->endWidget(); ?>

</div><!-- form -->