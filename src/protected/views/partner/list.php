<?php
/* @var $this PartnerController */
/* @var $model PartnerModel */

$this->breadcrumbs=array();

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-form form').submit(function(){
	$('#partner-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>業者別仕入れ履歴</h1>


<div class="search-form" style="">
<?php 
	//$this->renderPartial('_searchList',array( 'stockingModel'=>$stockingModel, 'id' => $id ) );
?>
</div>




<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'partner-model-grid',
	'dataProvider'=>$stockingModel->getSearchPartnerList( $id ),
	'filter'=>$stockingModel,
	'columns'=>array(
		
		array(
			'header' => '商品',
			'type' => 'raw',
			'value' => function($data){ 
				$itemname = $data->sku->products->item_name;
				$itemname .= $data->sku->brunch_item_name;
				return $itemname;
			},
			'htmlOptions' => array(
				'width' => '18%'
			)
		),
		
		array(
			'name' => 'memo',
			'htmlOptions' => array(
				'width' => '18%'
			)
		),
		array(
			'name' => 'purchase_price',
			'htmlOptions' => array(
				'width' => '18%'
			)
		),
		array(
			'name' => 'purchase_qty',
			'htmlOptions' => array(
				'width' => '18%'
			)
		),
		array(
			'name' => 'leaving_qty',
			'htmlOptions' => array(
				'width' => '18%'
			)
		),
	),
)); ?>
