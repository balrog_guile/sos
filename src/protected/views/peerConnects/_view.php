<?php
/* @var $this PeerConnectsController */
/* @var $data PeerConnectsModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('peer_id')); ?>:</b>
	<?php echo CHtml::encode($data->peer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shop_id')); ?>:</b>
	<?php echo CHtml::encode($data->shop_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('url')); ?>:</b>
	<?php echo CHtml::encode($data->url); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('connect_date')); ?>:</b>
	<?php echo CHtml::encode($data->connect_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('disconnect_date')); ?>:</b>
	<?php echo CHtml::encode($data->disconnect_date); ?>
	<br />


</div>