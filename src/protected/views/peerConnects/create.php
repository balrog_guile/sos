<?php
/* @var $this PeerConnectsController */
/* @var $model PeerConnectsModel */

$this->breadcrumbs=array(
	'Peer Connects Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List PeerConnectsModel', 'url'=>array('index')),
	array('label'=>'Manage PeerConnectsModel', 'url'=>array('admin')),
);
?>

<h1>Create PeerConnectsModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>