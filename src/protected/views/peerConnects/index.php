<?php
/* @var $this PeerConnectsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Peer Connects Models',
);

$this->menu=array(
	array('label'=>'Create PeerConnectsModel', 'url'=>array('create')),
	array('label'=>'Manage PeerConnectsModel', 'url'=>array('admin')),
);
?>

<h1>Peer Connects Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
