<?php
/* @var $this PeerConnectsController */
/* @var $model PeerConnectsModel */

$this->breadcrumbs=array(
	'Peer Connects Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List PeerConnectsModel', 'url'=>array('index')),
	array('label'=>'Create PeerConnectsModel', 'url'=>array('create')),
	array('label'=>'View PeerConnectsModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage PeerConnectsModel', 'url'=>array('admin')),
);
?>

<h1>Update PeerConnectsModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>