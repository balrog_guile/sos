<?php
/* @var $this PeerConnectsController */
/* @var $model PeerConnectsModel */

$this->breadcrumbs=array(
	'Peer Connects Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List PeerConnectsModel', 'url'=>array('index')),
	array('label'=>'Create PeerConnectsModel', 'url'=>array('create')),
	array('label'=>'Update PeerConnectsModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete PeerConnectsModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage PeerConnectsModel', 'url'=>array('admin')),
);
?>

<h1>View PeerConnectsModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'peer_id',
		'shop_id',
		'status',
		'url',
		'connect_date',
		'disconnect_date',
	),
)); ?>
