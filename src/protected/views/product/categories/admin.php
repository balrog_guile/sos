<?php
/* @var $this CategoriesController */
/* @var $model CategoryModel */

$this->breadcrumbs=array();

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#category-model-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1>カテゴリ管理</h1>

<div class="panel panel-default">
    <div class="panel-heading">
        操作
    </div>
    <div class="panel-body">
        <div class="row-fluid">
            <?php echo CHtml::link( '新規作成', Yii::app()->createUrl( 'product/categories/create'), array('class' => 'btn btn-large btn-info') ); ?>
        </div>
    </div>
</div>

    
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->

<?php //echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<!-- <div class="search-form" style="display:none"> -->
<?php //$this->renderPartial('_search',array( 'model'=>$model, )); ?>
<!-- </div> -->

<div class="panel panel-default">
    <div class="panel-heading">
        カテゴリ一覧
    </div>
    <div class="panel-body">

        <form
            action="<?php echo Yii::app()->createUrl('product/categories/rankChange'); ?>"
            method="post"
        >
            <table class="table table-bordered table-striped table-condensed">
                <thead>
                    <tr>
                        <th width="15%">メインカテゴリ<br />として取り扱う</th>
                        <th>カテゴリ名</th>
                        <th>URL</th>
                        <th width="10%">表示順</th>
                        <th width="15%">操作</th>
                    </tr>
                    <tr>
                        <td colspan="5">
                            <?php
                                echo CHtml::submitButton(
                                    '表示順変更',
                                    array(
                                        'class' => 'btn btn-info'
                                    )
                                );
                            ?>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        if( Yii::app()->controller->shop !== null )
                        {
                            $condition = array(
                                'condition' => '( parent_id = 0 AND shop_id = :shop_id )',
                                'order' => 'rank',
                                'params' => array(':shop_id' => Yii::app()->controller->shop->id )
                            );
                        }
                        else{
                            $condition = array(
                                'condition' => 'parent_id = 0',
                                'order' => 'rank'
                            );
                        }
                        foreach(
                        CategoryModel::model()->findAll($condition)
                        as
                        $row
                    ): ?>
                        <?php
                            $this->renderPartial(
                                'adminTr',
                                array(
                                'model'=>$model,
                                'row' => $row
                            ));
                        ?>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="5">
                            <?php
                                echo CHtml::submitButton(
                                    '表示順変更',
                                    array(
                                        'class' => 'btn btn-info'
                                    )
                                );
                            ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </form>
    </div>
</div>