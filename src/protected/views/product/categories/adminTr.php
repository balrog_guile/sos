<tr>
	<td style="text-align: center;">
		<?php echo CHtml::hiddenField('item_page_disp['.$row->id.']', 0 ); ?>
		<?php
			echo CHtml::checkBox(
				'item_page_disp['.$row->id.']',
				( ($row->item_page_disp==1)?true:false ),
				array(
					'value' => 1,
					'data-no-uniform' => 'true',
				)
			);
		?>
	</td>
	<td>
		<?php echo str_repeat( '　', $row->depth ); ?>
		<?php if( (int)$row->depth > 0 ): ?>-<?php endif; ?>
		<?php echo $row->categori_name; ?>
	</td>
	<td><?php echo $row->link_url; ?></td>
	<td>
		<?php
			echo CHtml::textField(
				'rank[' . $row->id .']',
				$row->rank,
				array(
					'class' => 'input-mini'
				)
			);
		?>
	</td>
	<td style="text-align: center;">
		<?php
			echo CHtml::link(
				'編集',
				Yii::app()->createUrl(
					'product/categories/update',
					array(
						'id' => $row->id,
					)
				),
				array(
					'class' => 'btn btn-info'
				)
			);
		?>
		<?php
			echo CHtml::link(
				'削除',
				Yii::app()->createUrl(
					'product/categories/delete',
					array(
						'id' => $row->id,
					)
				),
				array(
					'class' => 'btn btn-danger'
				)
			);
		?>
	</td>
</tr>
<?php foreach( $row->children as $child ): ?>
	<?php
		$this->renderPartial(
			'adminTr',
			array(
			'model'=>$model,
			'row' => $child
		));
	?>
<?php endforeach; ?>
