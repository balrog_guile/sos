<table>
	<tr>
		<td>
			<?php echo CHtml::radioButton( 'parent', false, array('class' => 'parent', 'id' => 'prent_tab_0' ,'value' => 0 ) ); ?>
		</td>
		<td>
			<label for="prent_tab_0">
				親選択なし
			</label>
		</td>
	</tr>
	<?php
		$i = 1;
		foreach( $datas as $row ):
	?>
		<tr>
			<tr>
				<td>
					<?php echo CHtml::radioButton( 'parent', false, array('class' => 'parent', 'id' => 'prent_tab_'.$i ,'value' => $row->id ) ); ?>
				</td>
				<td>
					<label for="prent_tab_<?php echo $i; ?>">
						<?php echo str_repeat( '─', $row->depth ); ?>
						<?php echo $row->categori_name; ?>
					</label>
				</td>
			</tr>
		</tr>
	<?php
		$i ++ ;
		endforeach;
	?>
</table>