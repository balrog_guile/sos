<?php
/* @var $this ProductController */
/* @var $model ProductsModel */
/* @var $form CActiveForm */
?>

<div class="panel panel-default">
    <div class="panel-body">

<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'products-model-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
)); ?>
    <?php if( ( $model->hasErrors() === FALSE )&&( $skucheck === TRUE ) ): ?>
        <a href="<?php echo Yii::app()->createUrl('product/product/create'); ?>" class="btn btn-info">
            　　新規作成へ　　
        </a>
    <?php endif; ?>
    <?php if(!$model->isNewRecord): ?>
        <?php
            echo CHtml::link(
                '商品ページへ',
                Yii::app()->getBaseUrl(true) . '/archives/' . $model->link_url . '.html',
                array(
                    'class' => 'btn btn-success',
                    'target' => '_blank'
                )
            );
        ?>
    <?php endif; ?>
    <hr />
    <p>
        *は必須項目です
    </p>
    <hr />
    
    <?php if( ( $model->hasErrors() )||( $skucheck === false ) ): ?>
    <div class="alert alert-error">
        エラーがあります。各項目を見なおしてください。
    </div>
    <?php endif; ?>
    
    
    <?php if( $create_ok === true ): ?>
    <div class="alert alert-success alert-input">
        <button type="button" class="close" data-dismiss="alert-input">&times;</button>
        <h4>商品登録／編集に成功しました。</h4>
        在庫登録が必要な場合はSKUから入荷管理をおこなってください。
    </div>
    <?php endif; ?>
    
    
    
    <div class="row">
        <div class="col-xs-6">
            <?php echo $form->labelEx($model,'item_id'); ?>
            <?php echo $form->textField($model,'item_id',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'item_id'); ?>
        </div>
        <div class="col-xs-6">
            <?php echo $form->labelEx($model,'item_name'); ?>
            <?php echo $form->textField($model,'item_name',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'item_name'); ?>
            
            
            <!-- EC関連で不要なものは隠す -->
            <?php echo $form->hiddenField($model, 'link_url'); ?>
            <?php echo $form->hiddenField($model, 'page_key'); ?>
            <?php echo $form->hiddenField($model, 'page_desc'); ?>
            <!-- /EC関連で不要なものは隠す -->
            
        </div>
    </div>
    
    
    
    <hr />
    
    
    
    <!-- カテゴリ選択 -->
    <div style="max-height: 250px; overflow: scroll;">
        <table class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th colspan="2"><h3>カテゴリ選択</h3>※下にスクロールできます</th>
                </tr>
                <tr>
                    <th width="7%">選択</th>
                    <th>カテゴリ名</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    if( Yii::app()->controller->shop !== null )
                    {
                        $condition = array(
                            'condition' => 'parent_id = 0 AND shop_id = :shop_id',
                            'params' => array(
                                ':shop_id' => Yii::app()->controller->shop->id
                            ),
                            'order' => 'rank ASC'
                        );
                    }
                    else
                    {
                        $condition = array(
                            'condition' => 'parent_id = 0',
                            'order' => 'rank ASC'
                        );
                    }
                foreach( CategoryModel::model()->findAll($condition) as $row ): ?>
                    <?php $this->renderPartial( '_formTr', array( 'row' => $row, 'alReadyCategory' => $alReadyCategory ) ); ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /カテゴリ選択 -->
    
    
    <hr />
    
    
    <!-- 自由追加情報 -->
    <?php /*SOSには不要なので消す
    <table class="table table-bordered table-striped" id="baseMetaInfo">
        <thead>
            <tr>
                <th colspan="3">自由追加情報</th>
            </tr>
            <tr>
                <th>項目名</th>
                <th>値</th>
                <th>操作</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    <?php echo $form->hiddenField($baseMetaInfo, '[0]id', array('class'=>'base-metainfo-id') ); ?>
                    <?php echo $form->hiddenField($baseMetaInfo, '[0]create_date', array() ); ?>
                    <?php echo $form->hiddenField($baseMetaInfo, '[0]product_id', array() ); ?>
                    
                    <?php echo $form->textField( $baseMetaInfo, '[0]meta_title'); ?>
                    <?php echo $form->error($baseMetaInfo,'[0]meta_title'); ?>
                </td>
                <td>
                    <?php echo $form->textField( $baseMetaInfo, '[0]meta_value'); ?>
                    <?php echo $form->error($baseMetaInfo,'[0]meta_value'); ?>
                </td>
                <td>
                    <?php echo CHtml::button( '▲', array( 'class' => 'btn btn-mini base-metaeta-info-rank') ); ?>
                    <?php echo CHtml::button( '▼', array( 'class' => 'btn btn-mini base-metaeta-info-rank') ); ?>
                    　|　
                    <?php echo CHtml::button( '削除', array( 'class' => 'btn btn-mini btn-danger base-metainfo-delete') ); ?>
                </td>
            </tr>
            <?php $i = 0; foreach( $metaInfoList as $metainfomodel ): ?>
                <tr>
                    <td>
                        <?php echo $form->hiddenField($metainfomodel, '[' . $i . ']id', array('class'=>'base-metainfo-id') ); ?>
                        <?php echo $form->hiddenField($metainfomodel, '[' . $i . ']create_date', array() ); ?>
                        <?php echo $form->hiddenField($metainfomodel, '[' . $i . ']product_id', array() ); ?>

                        <?php echo $form->textField( $metainfomodel, '[' . $i . ']meta_title'); ?>
                        <?php echo $form->error($metainfomodel,'[' . $i . ']meta_title'); ?>
                    </td>
                    <td>
                        <?php echo $form->textField( $metainfomodel, '[' . $i . ']meta_value'); ?>
                        <?php echo $form->error($metainfomodel,'[' . $i . ']meta_value'); ?>
                    </td>
                    <td>
                        <?php echo CHtml::button( '▲', array( 'class' => 'btn btn-mini base-metaeta-info-rank') ); ?>
                        <?php echo CHtml::button( '▼', array( 'class' => 'btn btn-mini base-metaeta-info-rank') ); ?>
                          |　
                        <?php echo CHtml::button( '削除', array( 'class' => 'btn btn-mini btn-danger base-metainfo-delete') ); ?>
                    </td>
                </tr>
            <?php $i ++ ;endforeach; ?>
        </tbody>
        <tfoot>
            <tr>
                <td colspan="4">
                    <?php echo CHtml::button('追加', array( 'class' => 'btn btn-mini btn-info base-metaeta-info-add') ); ?>
                </td>
            </tr>
        </tfoot>
    </table>
        <!-- 削除リストをここに入れる -->
        <div id="MetainfoDeleteList">
        </div>
        <!-- /削除リストをここに入れる -->
    */ ?>
    <!-- /自由追加情報 -->
    
    
    
    <!-- タグ機能 -->
    <?php /*SOSには不要なので消す
    <label>タグ</label>
    <div class="tags_area" style="margin-bottom: 0.8em; width: 100%; word-break: break-all;">
        <span class="label tags_element"
              id="tag_element_source"
              style="margin-right: 0.3em; display:inline-block"
              data-check-label=""
        >
            <span class="tags_element_label">Default</span>
            <a href="javascript:void(0);">[×]</a>
            <?php echo CHtml::hiddenField( 'tags[]', '', array( 'class' => 'tags_element_hidden' ,'id' => '') ); ?>
        </span>
        <?php foreach( $tags as $tag ): ?>
            <span class="label tags_element"
                  id="tag_element_source"
                  style="margin-right: 0.3em; display:inline-block; font-size: 1.0em; font-weight: normal;"
                  data-check-label="<?php echo $tag; ?>"
            >
                <span class="tags_element_label"><?php echo $tag; ?></span>
                <a href="javascript:void(0);">[×]</a>
                <?php echo CHtml::hiddenField( 'tags[]', $tag, array( 'class' => 'tags_element_hidden' ,'id' => '') ); ?>
            </span>
        <?php endforeach; ?>
    </div>
    <?php echo CHtml::textField( 'addtags', '', array() ); ?>
    <?php echo CHtml::button( 'タグ追加', array( 'class' => 'btn btn-mini addtagsbtn') ); ?>
    */?>
    <!-- /タグ機能 -->
    
    
    
    
    <hr />
    
    
    <!-- テンプレート選択  -->
    <div class="">
        <?php echo $form->labelEx($model,'template_file'); ?>
        <?php echo $form->dropDownList($model,'template_file',$templateFiles,array('class' => 'form-control')); ?>
        <?php echo $form->error($model,'template_file'); ?>
    </div>
    <!-- テンプレート選択  -->
    
    <hr />
    
    
    <!-- オプションテンプレート  -->
    <div class="">
        <?php echo $form->labelEx($model,'option_template_file'); ?>
        <?php echo $form->dropDownList($model,'option_template_file',$optionTemplateFiles,array('class' => 'form-control')); ?>
        <?php echo $form->error($model,'option_template_file'); ?>
    </div>
    <!-- /オプションテンプレート  -->
    
    
    
    <hr />
    
    
    <div class="row-fluid sortable ui-sortable">
        <h2>SKU（小品番）</h2>
        
        <?php if( $skucheck === false ): ?>
            <div class="alert alert-error">
                SKUがありません。
            </div>
        <?php endif; ?>
        
        <div class="box-content">
            <table id="children_data_table" class="table table-striped">
                <tbody>
                    
                    <?php $i = 0;foreach( $skumodel as $sku ): ?>
                        <?php if( $i == 0 ){$x = 0; }else{ $x = $i-1; } ?>
                        <tr class="children_data_tr" <?php if($i == 0): ?>id="children_data_tr"<?php endif; ?>>
                            <!--
                            <td class="sku_delete_flag_row">
                                <?php if( $sku->delete_flag == 1 ): ?>
                                    販売停止
                                <?php else: ?>
                                    販売表示
                                <?php endif; ?>
                            </td>
                            -->
                            <td>
                                
                                <?php echo $form->hiddenField($sku,'[' . $x. ']id', array() ); ?>
                                <?php echo $form->hiddenField($sku,'[' . $x. ']product_id', array() ); ?>
                                
                                <div class="row">
                                    <div class="col-xs-4 sku_name_set_area">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']brunch_item_id'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textField($sku,'[' . $x .']brunch_item_id',array('class' => 'form-control')); ?>
                                                    <?php echo $form->error($sku,'[' . $x .']brunch_item_id'); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']brunch_item_name'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textField($sku,'[' . $x .']brunch_item_name',array('class' => 'form-control')); ?>
                                                    <?php echo $form->error($sku,'[' . $x .']brunch_item_name'); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']code_str'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textField($sku,'[' . $x .']code_str',array('size'=>10, 'class' => 'form-control', 'readonly' => 'raedonly' )); ?>
                                                    <?php echo Chtml::button( '読込', array( 'class' => 'btn btn-link sku_code_read', 'data-index' => $x ) ); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']price'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textField($sku,'[' . $x .']price',array('class' => 'form-control' )); ?>
                                                    <?php echo $form->error($sku,'[' . $x .']price'); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']sale_price'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textField($sku,'[' . $x .']sale_price',array('class' => 'form-control' )); ?>
                                                    <?php echo $form->error($sku,'[' . $x .']sale_price'); ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']stock'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textField($sku,'[' . $x .']stock',array( 'readonly'=> 'readonly', 'class' => 'form-control' )); ?>
                                                    <?php echo $form->error($sku,'[' . $x .']stock'); ?>
                                                    <?php if( $create_mode===TRUE): ?>
                                                        <br />在庫追加は商品登録後行うことができます。
                                                    <?php endif; ?>
                                                    <?php if( $create_mode===FALSE): ?>
                                                        <?php echo Chtml::button( '入荷管理', array( 'class' => 'btn btn-link sku_insert_stocking', 'data-index' => $x, 'data-skuid' => $sku->id ) ); ?>
                                                    <?php endif; ?>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div class="col-xs-4">
                                        <table style="width: 100%;">
                                            <tr>
                                                <th>
                                                    <?php echo $form->labelEx($sku,'[' . $x .']content'); ?>
                                                </th>
                                                <td>
                                                    <?php echo $form->textArea($sku,'[' . $x .']content',array( 'class' => 'form-control', 'style' => 'width: 100%', 'rows' => 5 )); ?>
                                                    <?php echo $form->error($sku,'[' . $x .']content'); ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <hr />
                                        <div class="row-fluid" style="">
                                            <div class="span6">
                                                <?php echo $form->labelEx($sku,'[' . $x .']delete_flag'); ?>
                                                <?php echo $form->checkBox($sku, '[' . $x .']delete_flag', array('value' => 1, 'data-no-uniform' => 'true', 'class' => 'form-control sku_delete_flag')); ?>
                                                非表示にする
                                                <br />
                                                <?php echo Chtml::button( '完全削除', array( 'class' => 'btn btn-danger sku_remove_all', 'data-target_id' => $sku->id) ); ?>
                                            </div>
                                            <div class="span6" style=" text-align: center;">
                                                <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-primary sku_rank_up' ) ); ?>
                                                <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-primary sku_rank_down' ) ); ?>
                                                <?php echo $form->hiddenField($sku,'[' . $x .']rank',array( 'class' => 'sku_rank' )); ?>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <!-- SKU画像 -->
                                    <div class="col-xs-4 sku_image_set_area" style="overflow: scroll;">
                                        <h4>画像</h4>
                                        <div class="sku_image_drop_area" style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;">
                                            画像をドラッグ＆ドロップ
                                        </div>
                                        <input type="file" class="sku_image_select_area" />
                                        <table style="width: 100%;" class="sku_image_add_area">
                                            <thead>
                                                <tr>
                                                    <th>画像</th>
                                                    <th>操作</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>
                                                        <input type="hidden" name="sku_image_path[0][]" value="" />
                                                        <img src="" width="120" />
                                                    </td>
                                                    <td style="text-align: center;">
                                                        <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-primary sku_image_rank_change' , 'data-sku-image-rank-type' =>'up', 'data-skuimage-index' => $x ) ); ?>
                                                        <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-primary sku_image_rank_change' , 'data-sku-image-rank-type' =>'down', 'data-skuimage-index' => $x ) ); ?>
                                                        <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-warning sku_image_rank_delete' , 'data-skuimage-index' => $x ) ); ?>
                                                    </td>
                                                </tr>
                                                <?php
                                                    if(!is_null($sku->error_images ))
                                                    {
                                                        $product_sku_images = $sku->error_images;
                                                    }
                                                    else
                                                    {
                                                        $product_sku_images = $sku->product_sku_images;
                                                    }
                                                    foreach( $product_sku_images as $sku_image ):
                                                ?>
                                                    <tr>
                                                        <td>
                                                            <input type="hidden" name="sku_image_path[<?php echo $x; ?>][]" value="<?php echo $sku_image->image; ?>" />
                                                            <img src="<?php echo Yii::app()->getBaseUrl(true); ?><?php echo $sku_image->image; ?>" width="120" />
                                                        </td>
                                                        <td style="text-align: center;">
                                                            <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-primary sku_image_rank_change', 'data-sku-image-rank-type' =>'up', 'data-skuimage-index' => $x ) ); ?>
                                                            <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-primary sku_image_rank_change', 'data-sku-image-rank-type' =>'down', 'data-skuimage-index' => $x ) ); ?>
                                                            <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-warning sku_image_rank_delete' , 'data-skuimage-index' => $x ) ); ?>
                                                        </td>
                                                    </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /SKU画像 -->
                                    
                                </div>
                            </td>
                        </tr>
                    <?php $i ++ ;endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <hr />
        
        <div class="row-fluid">
            <div class="span12 right">
                <?php echo Chtml::button( '追加', array( 'class' => 'btn btn-primary sku_add' ) ); ?>
            </div>
        </div>

        <div id="modal_code_read" class="hide">
            <label id="modal_code_read_label">
                バーコードを読み込んでください
                <?php echo Chtml::textField('tmp_code', '', array( 'id' => 'modal_code_read_input' ) ); ?>
            </label>
        </div>
        
    </div>
    
    
    <hr />
    
    
    <div id="children_data_area" class="">
    </div>
    
    
    
    
    
    <!-- ------------共通オプションオプション ------------- --->
    <?php /* SOSでは一旦隠す
    <div class="row-fluid">
        <div class="box-header well" data-original-title="">
            <h2>共通オプション設定</h2>
        </div>
        <div class="box-content">
            <h4>共通オプション設定</h4>
            <div class="row-fluid">
                <div class="span4">
                    <label>項目名</label>
                    <?php echo CHtml::textField( 'optionname', '', array() );?>
                </div>
                <div class="span4">
                    <label>タイプ</label>
                    <?php
                        $adoptintype = array(
                            '' => '選択してください',
                            '1' => 'セレクトボックス',
                            '2' => 'ラジオボタン',
                            '3' => 'チェックボックス',
                            '4' => 'テキストフィールド',
                            '5' => 'テキストエリア',
                        );
                    ?>
                    <?php echo Chtml::dropDownList(
                            'addCommonOption',
                            '',
                            $adoptintype,
                            array()
                    ); ?>
                </div>
            </div>
            
            <!-- 値設定 -->
            <div class="row-fluid" id="option_values_area">
                <div class="span12">
                    <table class="table table-striped table-bordered" id="optionvaluestable">
                        <thead>
                            <tr>
                                <th>オプション</th>
                                <th>値</th>
                                <th>ラベル</th>
                                <th>操作</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div>
                                        <label>
                                        <?php
                                            echo CHtml::radioButton(
                                                'optionvalues_default',
                                                false,
                                                array(
                                                    'value' => 1,
                                                    'data-no-uniform' => 'true'
                                                )
                                            );
                                        ?>
                                        初期値
                                        </label>
                                    </div>
                                </td>
                                <td>
                                    <?php echo CHtml::textField( 'optionvalues', '', array() ); ?>
                                </td>
                                <td>
                                    <?php echo CHtml::textField( 'optionlabels', '', array() ); ?>
                                </td>
                                <td>
                                    <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link optionvaluesrabnk', 'data-rank-type' =>'up', 'data-skuimage-index' => $x ) ); ?>
                                    <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link optionvaluesrabnk', 'data-rank-type' =>'down', 'data-skuimage-index' => $x ) ); ?>
                                    <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger optionvaluesdelete' , 'data-skuimage-index' => $x ) ); ?>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>
                                    <?php echo CHtml::button( '値追加', array( 'class' => 'btn btn-mini btn-addoptionvalue' ) ); ?>
                                </td>
                                <td colspan="3" style="text-align: right;">
                                </td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <!-- /値設定 -->
            
            <!-- テキストフィールド -->
            <div class="row-fluid" id="option_field_area">
                <div class="span4">
                    <label>初期値</label>
                    <?php echo CHtml::textField( 'optionvalues_init', '', array() ); ?>
                </div>
                <div class="span4">
                    <div>
                        <label>
                            <?php
                                echo CHtml::checkBox(
                                    'optionvalues_required',
                                    false,
                                    array(
                                        'value' => 1,
                                        'data-no-uniform' => 'true'
                                    )
                                );
                            ?>
                            必須
                        </label>
                    </div>
                </div>
            </div>
            <!-- /テキストフィールド -->
            
            <hr />
            <div class="row-fluid">
                <div class="span12 right">
                    <?php echo CHtml::button( '追加／編集', array('class' => 'btn btnaddoptionexec') ); ?>
                </div>
            </div>
            
            
            <hr />
            
            
            <h4>追加済みオプション</h4>
            <table class="table table-striped table-bordered" id="alreadyaddoptiontable">
                <thead>
                    <tr>
                        <th>データタイプ</th>
                        <th>項目名</th>
                        <th>データ内容</th>
                        <th width="15%">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <span class="datatype"></span>
                        </td>
                        <td>
                            <span class="dataname"></span>
                        </td>
                        <td>
                            <!-- 値選択表示用 -->
                            <div class="dataoptionvaluesdisp">
                                <table class="table table-bordered table-striped table1">
                                    <thead>
                                        <tr>
                                            <th>初期値</th>
                                            <th>値</th>
                                            <th>ラベル</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /値選択表示用 -->
                            <!-- 初期値 -->
                            <div class="dataoptiontextdisp">
                                <table class="table table-bordered table-striped table1">
                                    <thead>
                                        <tr>
                                            <th>初期値</th>
                                            <th>必須</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td class="dataoptiontextdisp_init"></td>
                                            <td class="dataoptiontextdisp_required"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /初期値 -->
                            <!-- 隠し項目 -->
                            <?php echo CHtml::hiddenField( 'additional_option[]', '', array() ); ?>
                            <!-- /隠し項目 -->
                        </td>
                        <td>
                            <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link optionreadyrank', 'data-rank-type' =>'up' ) ); ?>
                            <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link optionreadyrank', 'data-rank-type' =>'down' ) ); ?>
                            <br />
                            <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger optionreadydelete' ) ); ?>
                        </td>
                    </tr>
                    <!-- 登録済みデータ -->
                    <?php
                        //var_dump($model->reinput_add_option);
                        if($model->reinput_add_option===null)
                        {
                            $add_options = $model->add_options;
                        }
                        else
                        {
                            $add_options = $model->reinput_add_option;
                        }
                        //foreach( $model->add_options as $adoption ):
                        foreach( $add_options as $adoption ):
                    ?>
                        <tr>
                            <td>
                                <span class="datatype">
                                    <?php echo $adoptintype[$adoption->type]; ?>
                                </span>
                            </td>
                            <td>
                                <span class="dataname">
                                    <?php echo $adoption->name; ?>
                                </span>
                            </td>
                            <td>
                                
                                <?php if(
                                    ( $adoption->type == 1 )||
                                    ( $adoption->type == 2 )||
                                    ( $adoption->type == 3 )
                                ): ?>
                                    <!-- 値選択表示用 -->
                                    <div class="dataoptionvaluesdisp">
                                        <table class="table table-bordered table-striped table1">
                                            <thead>
                                                <tr>
                                                    <th>初期値</th>
                                                    <th>値</th>
                                                    <th>ラベル</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach( $adoption->values as $values ): ?>
                                                <tr>
                                                    <td>
                                                        <?php if( $values->init == 1 ): ?>
                                                            初期
                                                        <?php endif; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $values->value; ?>
                                                    </td>
                                                    <td>
                                                        <?php echo $values->label; ?>
                                                    </td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /値選択表示用 -->
                                
                                <?php else: ?>
                                    <!-- 初期値 -->
                                    <div class="dataoptiontextdisp">
                                        <table class="table table-bordered table-striped table1">
                                            <thead>
                                                <tr>
                                                    <th>初期値</th>
                                                    <th>必須</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td class="dataoptiontextdisp_init">
                                                        <?php echo $adoption->init; ?>
                                                    </td>
                                                    <td class="dataoptiontextdisp_required">
                                                        <?php if( Yii::app()->controller->addOptionValidation( $adoption->validate )===TRUE): ?>
                                                            必須
                                                        <?php endif; ?>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /初期値 -->
                                <?php endif; ?>
                                
                                    
                                <!-- 隠し項目 -->
                                <?php
                                    echo CHtml::hiddenField(
                                        'additional_option[]',
                                        Yii::app()->controller->addOptionGetJson( $adoption ),
                                        array()
                                    );
                                ?>
                                <!-- /隠し項目 -->
                                
                            </td>
                            <td>
                                <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link optionreadyrank', 'data-rank-type' =>'up' ) ); ?>
                                <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link optionreadyrank', 'data-rank-type' =>'down' ) ); ?>
                                <br />
                                <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger optionreadydelete' ) ); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                    <!-- /登録済みデータ -->
                    
                </tbody>
            </table>
            
        </div>
    </div>
    */?>
    <!-- ------------/共通オプションオプション ------------- --->
    
    
    
    
    <div class="row">
        <div class="col-xs-4">
            <div class="">
                <?php echo $form->labelEx($model,'open_status'); ?>
                <?php echo $form->dropDownList(
                    $model,
                    'open_status',
                    array( '1'=> '公開', '2' => '非公開' ),
                    array( 'class' => 'form-control' )
                ); ?>
                <?php echo $form->error($model,'open_status'); ?>
            </div>
            
            <div class="">
                <?php echo $form->labelEx($model,'item_status'); ?>
                <?php echo $form->listBox($model,'item_status',VarHelper::getItemStatus(), array('multiple'=>'multiple', 'class' => 'form-control') ); ?>
                <?php echo $form->error($model,'item_status'); ?>
            </div>
            
            
            <div class="">
                <?php echo $form->labelEx($model,'sale_status'); ?>
                <?php echo $form->dropDownList(
                    $model,
                    'sale_status',
                    array( '1'=> '通常販売', '2' => '問い合わせのみ', '3' => '商品紹介のみ' ),
                    array(
                        'class' => 'form-control'
                    )
                ); ?>
                <?php echo $form->error($model,'sale_status'); ?>
            </div>
            
        </div>
        <div class="col-xs-4">
            <div class="">
                <?php echo $form->labelEx($model,'postage'); ?>
                <?php echo $form->textField($model,'postage',array(
                    'class' => 'form-control'
                )); ?>
                <?php echo $form->error($model,'postage'); ?>
            </div>
            
            <div class="">
                <?php echo $form->labelEx($model,'point_rate'); ?>
                <?php echo $form->textField($model,'point_rate',array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'point_rate'); ?>
            </div>
            
            <div class="">
                <?php echo $form->labelEx($model,'shipment'); ?>
                <?php echo $form->textField($model,'shipment',array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'shipment'); ?>
            </div>
            
            <div class="">
                <?php echo $form->labelEx($model,'qty_limit'); ?>
                <?php echo $form->textField($model,'qty_limit',array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'qty_limit'); ?>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="">
                <?php echo $form->labelEx($model,'serch_word'); ?>
                <?php echo $form->textArea($model,'serch_word',array('rows'=>6, 'cols'=>50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'serch_word'); ?>
            </div>
            
            <div class="">
                <?php echo $form->labelEx($model,'remark'); ?>
                <?php echo $form->textArea($model,'remark',array('rows'=>6, 'cols'=>50, 'class' => 'form-control')); ?>
                <?php echo $form->error($model,'remark'); ?>
            </div>
        </div>
    </div>
    
    <br />
    <hr />
    
    
    
    <div class="row">
        <div class="col-xs-4">
            <div class="">
                <?php echo $form->labelEx($model,'thumbnail'); ?>
                <div class="upload_result_area" data-target="thumbnail">
                    <?php if(!empty($model->thumbnail)): ?>
                        <?php echo Chtml::image(Yii::app()->getBaseUrl(true) . $model->thumbnail, null, array('width' => 100, 'id' => 'thumbnail_img')); ?>
                    <?php endif; ?>
                </div>
                
                <div
                    class="sku_product_drop_area"
                    style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
                    data-target="thumbnail"
                >
                    画像をドラッグ＆ドロップ
                </div>
                
                <?php echo $form->hiddenField($model,'thumbnail', array('id' => 'thumbnail_hidden')); ?>
                <?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'thumbnail', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
                <?php echo $form->error($model,'thumbnail'); ?>
            </div>
            <hr />
            <div class="">
                <?php echo $form->labelEx($model,'main_image'); ?>
                <div class="upload_result_area" data-target="main_image">
                    <?php if(!empty($model->main_image)): ?>
                        <?php echo Chtml::image(Yii::app()->baseUrl . $model->main_image, null, array('width' => 100, 'id' => 'main_image_img')); ?>
                    <?php endif; ?>
                </div>
                <div
                    class="sku_product_drop_area"
                    style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
                    data-target="main_image"
                >
                    画像をドラッグ＆ドロップ
                </div>
                <?php echo $form->hiddenField($model,'main_image', array('id' => 'main_image_hidden')); ?>
                <?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'main_image', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
                <?php echo $form->error($model,'main_image'); ?>
            </div>
            <hr />
            <div class="">
                <?php echo $form->labelEx($model,'main_big_image'); ?>
                <div class="upload_result_area" data-target="main_big_image">
                    <?php if(!empty($model->main_big_image)): ?>
                        <?php echo Chtml::image(Yii::app()->baseUrl . $model->main_big_image, null, array('width' => 100, 'id' => 'main_big_image_img')); ?>
                    <?php endif; ?>
                </div>
                <div
                    class="sku_product_drop_area"
                    style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
                    data-target="main_big_image"
                >
                    画像をドラッグ＆ドロップ
                </div>
                <?php echo $form->hiddenField($model,'main_big_image', array('id' => 'main_big_image_hidden')); ?>
                <?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image', 'data-target' => 'main_big_image', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
                <?php echo $form->error($model,'main_big_image'); ?>
            </div>
        </div>
        <div class="col-xs-8">
            <div class="">
                <?php echo $form->labelEx($model,'main_view_comment'); ?>
                <?php echo $form->textArea($model,'main_view_comment',array('rows'=>6, 'cols'=>50, 'class' => 'form-control' )); ?>
                <?php echo $form->error($model,'main_view_comment'); ?>
            </div>
            <div class="">
                <?php echo $form->labelEx($model,'main_detail_comment'); ?>
                <?php echo $form->textArea($model,'main_detail_comment',array('rows'=>6, 'cols'=>50, 'class' => 'form-control' )); ?>
                <?php echo $form->error($model,'main_detail_comment'); ?>
            </div>
        </div>
    </div>
    
    
    
    <hr />
    
    <div class="row">
        <div class="col-xs-12">
            <?php echo $form->labelEx($model,'youtube'); ?>
            <?php echo $form->textField($model,'youtube',array('class'=>'form-control')); ?>
            <?php echo $form->error($model,'youtube'); ?>
        </div>
    </div>
    
    <hr />
    
    
    
    <div style="margin: 1em 0;">
        <?php echo CHtml::button('拡張画像エリア開閉', array('class' => 'btn btn-mini subimageareabtn') ); ?>
    </div>
    
    
    
    
    
    <!-- 拡張エリア -->
    <div class="subimagearea" style="display: none;">
        
        <?php for ($i = 1; $i <= 5; $i++) : ?>
        <hr />
        <div class="row">
            <div class="col-xs-4">
                <div class="">
                    <?php echo $form->labelEx($model,'sub'.$i.'_img'); ?>
                    <div class="upload_result_area" data-target="sub<?php echo $i; ?>_img">
                        <?php if(!empty($model->sub1_img)): ?>
                            <?php echo Chtml::image(Yii::app()->baseUrl . $model->{'sub'.$i.'_img'}, null, array('width' => 100, 'id' => 'sub'.$i.'_img_img')); ?>
                        <?php endif; ?>
                    </div>
                    <div
                        class="sku_product_drop_area"
                        style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
                        data-target="sub<?php echo $i; ?>_img"
                    >
                        画像をドラッグ＆ドロップ
                    </div>
                    <?php echo $form->hiddenField($model,'sub'.$i.'_img', array('id' => 'sub'.$i.'_img_hidden')); ?>
                    <?php echo Chtml::fileField('FileUploadModel', null, array('class' => 'upload_image form-control', 'data-target' => 'sub'.$i.'_img', 'data-no-uniform' => 'true', 'data-mode' => 'product_images')); ?>
                    <?php echo $form->error($model,'sub'.$i.'_img'); ?>
                </div>
            </div>
            <div class="col-xs-8">
                <div class="">
                    <?php echo $form->labelEx($model,'sub'.$i.'_title'); ?>
                    <?php echo $form->textfield($model,'sub'.$i.'_title',array('class' => 'form-control')); ?>
                    <?php echo $form->error($model,'sub'.$i.'_title'); ?>
                </div>
                <div class="">
                    <?php echo $form->labelEx($model,'sub'.$i.'_text'); ?>
                    <?php echo $form->textArea($model,'sub'.$i.'_text',array('rows'=>6, 'cols'=>50, 'class' => 'form-control' )); ?>
                    <?php echo $form->error($model,'sub'.$i.'_text'); ?>
                </div>
            </div>
        </div>
        <?php endfor;; ?>
    </div>
    <!-- /拡張エリア -->
    
    
    
    
    <hr />
    
    
    <!-- /追加画像 -->
    <div class="row-fluid sortable ui-sortable">
        <div class="box-content">
            <h2>追加画像</h2>
            <table class="table table-striped" id="add_images">
                <thead>
                    <tr>
                        <th>画像</th>
                        <th>コメント</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <img src="" width="150">
                            <?php echo CHtml::hiddenField( 'add_images[]', '',array() ); ?>
                        </td>
                        <td>
                            <textarea name="add_images_content[]" style="width: 200px"></textarea>
                        </td>
                        <td>
                            <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'up' ) ); ?>
                            <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'down' ) ); ?>
                            <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger add_images_delete') ); ?>
                        </td>
                    </tr>
                    <?php
                        if(!is_null($model->error_product_images))
                        {
                            $product_images = $model->error_product_images;
                        }
                        else
                        {
                            $product_images = $model->product_images;
                        }
                        foreach( $product_images as $add_image):
                    ?>
                        <tr>
                            <td>
                                <img src="<?php echo Yii::app()->getBaseUrl(true); ?><?php echo $add_image->image; ?>" width="150">
                                <?php echo CHtml::hiddenField( 'add_images[]', $add_image->image ,array() ); ?>
                            </td>
                            <td>
                                <textarea name="add_images_content[]" style="width: 200px"><?php echo $add_image->content; ?></textarea>
                            </td>
                            <td>
                                <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'up' ) ); ?>
                                <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link add_images_rank_change', 'data-rank-type' =>'down' ) ); ?>
                                <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger add_images_delete') ); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3">
                            <div
                                class="add_image_drop_area"
                                style="margin: 0.8em 0px; border: 2px #FFA0A2 dashed; height: 40px; padding: 0.3em; text-align: center; background-color: #FFF;"
                            >
                                画像をドラッグ＆ドロップ
                            </div>
                            <?php echo Chtml::fileField( 'add_images_btn', '', array() ); ?>
                        </td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- /追加画像 -->
    
    
    
    <hr />
    
    
    <!-- FAQ -->
    <?php /* SOSでは不要
    <div class="row-fluid">
        <div class="box-header well" data-original-title="">
            <h2>FAQ</h2>
        </div>
        <div class="box-content">
            <table class="table table-striped" id="faq_table">
                <thead>
                    <tr>
                        <th>質問</th>
                        <th width="15%">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <?php echo $form->hiddenField($faq_list['set'], '[0000]id' ); ?>
                            <?php echo $form->label( $faq_list['set'], '[0000]question' ); ?>
                            <?php echo $form->textArea( $faq_list['set'], '[0000]question', array( 'style' => 'width: 100%; height: 50px;' ) ); ?>
                            <hr>
                            <?php echo $form->label( $faq_list['set'], '[0000]answer' ); ?>
                            <?php echo $form->textArea( $faq_list['set'], '[0000]answer', array('style' => 'width: 100%; height: 50px;') ); ?>
                        </td>
                        <td>
                            <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'up' ) ); ?>
                            <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'down' ) ); ?>
                            <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger faq_delete') ); ?>
                        </td>
                    </tr>
                    <?php $count = 0; foreach( $faq_list['models'] as $faq_model ): ?>
                        <tr>
                            <td>
                                <?php echo $form->hiddenField($faq_model, '[' . $count .']id' ); ?>
                                <?php echo $form->label( $faq_model, '[' . $count .']question' ); ?>
                                <?php echo $form->textArea( $faq_model, '[' . $count .']question', array( 'style' => 'width: 100%' ) ); ?>
                                <?php echo $form->error( $faq_model, '[' . $count .']question'); ?>
                                <hr>
                                <?php echo $form->label( $faq_model, '[' . $count .']answer' ); ?>
                                <?php echo $form->textArea( $faq_model, '[' . $count .']answer', array('style' => 'width: 100%') ); ?>
                                <?php echo $form->error( $faq_model, '[' . $count .']answer'); ?>
                            </td>
                            <td>
                                <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'up' ) ); ?>
                                <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link faq_rank_change', 'data-rank-type' =>'down' ) ); ?>
                                <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger faq_delete') ); ?>
                            </td>
                        </tr>
                    <?php $count ++ ; endforeach; ?>
                </tbody>
                <tfoot>
                    <tr>
                        <th colspan="2">
                            <?php echo CHtml::button( '追加', array( 'class' => 'btn faq_add') ); ?>
                        </th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
    <!-- /FAQ -->
    
    <hr />
    */?>
    
    
    
    
    <!-- 関連商品 -->
    <?php /* SOSでは不要
    <div class="row-fluid sortable ui-sortable">
        <div class="box-header well" data-original-title="">
            <h2>関連商品</h2>
        </div>
        
        <div class="rel_products">
            
        </div>
        
        <hr />
        <?php echo CHtml::hiddenField( 'rel_json', rawurlencode($rel_json), array( 'class' => 'rel_json' ) ); ?>
        <input type="button" data-id="<?php echo $model->id; ?>" value="関連商品検索" class="get-rel btn" />
        
    </div>
    <!-- /関連商品 -->
    
    
    <hr />
     */
    ?>
     <!-- SOS用 -->
     <?php echo CHtml::hiddenField( 'rel_json', rawurlencode('[]'), array( 'class' => 'rel_json' ) ); ?>
     <!-- /SOS用 -->
    
    
    
    
    <!-- 他商品のSKUをオプションとして呼ぶ機能 -->
    <div class="row" style="display:  none;">
        <div class="box-header well" data-original-title="">
            <h2>他商品のSKUをオプション品として呼ぶ機能</h2>
        </div>
        <div class="box-content">
            <table class="table table-striped" id="sku_option_add_table">
                <thead>
                    <tr>
                        <th>画像</th>
                        <th>商品名</th>
                        <th width="15%">操作</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>
                            <img src="" class="skuimage" width="150">
                        </td>
                        <td>
                            <?php echo CHtml::hiddenField('optionsku[]', '', array() ); ?>
                            <span class="product"></span>
                            <span class="sku"></span>
                        </td>
                        <td>
                            <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'up' ) ); ?>
                            <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'down' ) ); ?>
                            <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger skuoptiondelete') ); ?>
                        </td>
                    </tr>
                    <?php $op = ((!is_null($model->reinpit_optionsku)?$model->reinpit_optionsku:$model->optionsku));foreach( $op as $optsku): ?>
                        <tr>
                            <td>
                                <?php if( (isset($optsku->sku->product_sku_images[0]))&&($optsku->sku->product_sku_images[0]->image != '' )): ?>
                                    <img
                                        src="<?php echo $optsku->sku->product_sku_images[0]->image; ?>"
                                        class="skuimage"
                                        width="150"
                                    >
                                <?php else: ?>
                                    画像なし
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php echo CHtml::hiddenField('optionsku[]', $optsku->sku->id, array() ); ?>
                                <span class="product">
                                    <?php echo $optsku->sku->products->item_id ; ?>
                                     
                                    <?php echo $optsku->sku->products->item_name ; ?>
                                </span>
                                <span class="sku">
                                    <?php echo $optsku->sku->brunch_item_id ; ?>
                                     
                                    <?php echo $optsku->sku->brunch_item_name ; ?>
                                </span>
                            </td>
                            <td>
                                <?php echo Chtml::button( '▲', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'up' ) ); ?>
                                <?php echo Chtml::button( '▼', array( 'class' => 'btn btn-link skuoptionrank', 'data-rank-type' =>'down' ) ); ?>
                                <?php echo Chtml::button( '削除', array( 'class' => 'btn btn-danger skuoptiondelete') ); ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            <hr />
            <?php echo CHtml::button( 'オプション選択', array('class' => 'btn btn select_adds') ); ?>
        </div>
    </div>
    <!-- /他商品のSKUをオプションとして呼ぶ機能 -->
    
    
    <div class="row-fluid">
        <?php
            echo CHtml::submitButton(
                $model->isNewRecord ? '作成' : '編集',
                array( 'class' => 'btn btn-large btn-success col-xs-12')
            );
        ?>
    </div>

<?php $this->endWidget(); ?>

</div>
</div>



<!-- 関連商品選択モーダル用 -->
<div id="relmodal" style="display: none;">
</div>
<!-- /関連商品選択モーダル用 -->



<!-- オプション品モーダル用 -->
<div id="option_adds_modal" style="display: none;">
</div>
<!-- /オプション品用 -->



<!-- ローディングモーダル用 -->
<div id="loading_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <!--
            <div class="modal-header">
                <h4>アップロード中</h4>
            </div>
            -->
            <div class="modal-body" style="text-align: center;">
                <p><img src="<?php echo Yii::app()->request->baseUrl; ?>/regi_files/img/loading.gif" /></p>
            </div>
            <!--
            <div class="modal-footer">
            </div>
            -->
        </div>
    </div>
</div>
