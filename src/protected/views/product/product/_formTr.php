<tr>
	<td style="text-align: center;">
		<?php
			echo CHtml::checkBox(
				'ProductsModel[categories_many][]',
				((in_array($row->id, $alReadyCategory))?TRUE:FALSE),
				array(
					'value' => $row->id
				)
			);
		?>
	</td>
	<td>
		<?php echo str_repeat( "　", $row->depth ); ?>
		<?php if( $row->depth > 0 ): ?>-<?php endif; ?>
		<?php echo $row->categori_name; ?>
	</td>
</tr>
<?php foreach( $row->children as $child ): ?>
	<?php $this->renderPartial( '_formTr', array( 'row' => $child, 'alReadyCategory' => $alReadyCategory ) ); ?>
<?php endforeach; ?>
