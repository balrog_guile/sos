<?php
/* @var $this ProductController */
/* @var $model ProductsModel */
/* @var $form CActiveForm */
?>


<?php $form=$this->beginWidget(
    'CActiveForm', array(
    'action'=>Yii::app()->createUrl($this->route),
    'method'=>'get',
)); ?>
    <div class="row">
        <div class="col-xs-3">
            <?php echo $form->label($model,'item_id'); ?>
            <?php echo $form->textField($model,'item_id',array('class' => 'input-block-level form-control')); ?>
        </div>
        <div class="col-xs-3">
            <?php echo $form->label($model,'item_name'); ?>
            <?php echo $form->textField($model,'item_name',array('class' => 'input-block-level form-control')); ?>
        </div>
        <div class="col-xs-3">
            <?php echo $form->label($model,'open_status'); ?>
            <?php echo $form->dropDownList($model,'open_status', VarHelper::getStatus(), array( 'class' => 'input-small form-control') ); ?>
        </div>
    </div>
    
    <hr />
    
    <div class="row" style="max-height: 150px; overflow: scroll; padding: 0.3em; border: 1px dotted #666;">
        <div class="col-xs-12">
            <table class="table table-bordered table-condensed table-striped">
                <thead>
                    <tr>
                        <th colspan="2">カテゴリ</th>
                    </tr>
                    <tr>
                        <th> </th>
                        <th>
                            カテゴリ名
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( CategoryModel::model()->findAll(array(
                            'condition' => 'parent_id = 0',
                            'order' => 'rank ASC'
                        ))
                        as
                        $row
                    ): ?>
                        <?php $this->renderPartial( '_searchTr', array( 'row' => $row ) ); ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
    
    <hr />
    
    <div class="row">
        <div class="col-xs-12">
            <?php echo CHtml::submitButton('　　　検索　　　', array( 'class' => 'btn btn-info') ); ?>
            <a href="<?php echo Yii::app()->createUrl('product/product/admin'); ?>" class="btn btn btn-danger">
                全商品表示
            </a>
        </div>
    </div>
</div>

<?php $this->endWidget(); ?>
