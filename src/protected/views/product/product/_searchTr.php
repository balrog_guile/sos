<?php 
	$checked = Input::Get('ProductsModel');
	if(
		(!isset($checked['categories_many']))||
		(!is_array($checked['categories_many']))
	){
		$checked['categories_many'] = array();
	}
?>
<tr>
	<td style="text-align: center;">
		<?php
			echo CHtml::checkBox(
				'ProductsModel[categories_many][]',
				((in_array( $row->id, $checked['categories_many'] ))?TRUE:FALSE),
				array(
					'value' => $row->id
				)
			);
		?>
	</td>
	<td>
		<?php echo str_repeat( "　", $row->depth ); ?>
		<?php if( $row->depth > 0 ): ?>-<?php endif; ?>
		<?php echo $row->categori_name; ?>
	</td>
</tr>
<?php foreach( $row->children as $child ): ?>
	<?php $this->renderPartial( '_searchTr', array( 'row' => $child ) ); ?>
<?php endforeach; ?>
