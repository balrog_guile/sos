<?php
/* @var $this ProductController */
/* @var $data ProductsModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_id')); ?>:</b>
	<?php echo CHtml::encode($data->item_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_name')); ?>:</b>
	<?php echo CHtml::encode($data->item_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('price')); ?>:</b>
	<?php echo CHtml::encode($data->price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sale_price')); ?>:</b>
	<?php echo CHtml::encode($data->sale_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('open_status')); ?>:</b>
	<?php echo CHtml::encode($data->open_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_status')); ?>:</b>
	<?php echo CHtml::encode($data->item_status); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('postage')); ?>:</b>
	<?php echo CHtml::encode($data->postage); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('point_rate')); ?>:</b>
	<?php echo CHtml::encode($data->point_rate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shipment')); ?>:</b>
	<?php echo CHtml::encode($data->shipment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qty_limit')); ?>:</b>
	<?php echo CHtml::encode($data->qty_limit); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('serch_word')); ?>:</b>
	<?php echo CHtml::encode($data->serch_word); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remark')); ?>:</b>
	<?php echo CHtml::encode($data->remark); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_view_comment')); ?>:</b>
	<?php echo CHtml::encode($data->main_view_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_detail_comment')); ?>:</b>
	<?php echo CHtml::encode($data->main_detail_comment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('thumbnail')); ?>:</b>
	<?php echo CHtml::encode($data->thumbnail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_image')); ?>:</b>
	<?php echo CHtml::encode($data->main_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('main_big_image')); ?>:</b>
	<?php echo CHtml::encode($data->main_big_image); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub1_title')); ?>:</b>
	<?php echo CHtml::encode($data->sub1_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub1_text')); ?>:</b>
	<?php echo CHtml::encode($data->sub1_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub1_img')); ?>:</b>
	<?php echo CHtml::encode($data->sub1_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub2_title')); ?>:</b>
	<?php echo CHtml::encode($data->sub2_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub2_text')); ?>:</b>
	<?php echo CHtml::encode($data->sub2_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub2_img')); ?>:</b>
	<?php echo CHtml::encode($data->sub2_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub3_title')); ?>:</b>
	<?php echo CHtml::encode($data->sub3_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub3_img')); ?>:</b>
	<?php echo CHtml::encode($data->sub3_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub3_text')); ?>:</b>
	<?php echo CHtml::encode($data->sub3_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub4_title')); ?>:</b>
	<?php echo CHtml::encode($data->sub4_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub4_text')); ?>:</b>
	<?php echo CHtml::encode($data->sub4_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub4_img')); ?>:</b>
	<?php echo CHtml::encode($data->sub4_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub5_title')); ?>:</b>
	<?php echo CHtml::encode($data->sub5_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub5_text')); ?>:</b>
	<?php echo CHtml::encode($data->sub5_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub5_img')); ?>:</b>
	<?php echo CHtml::encode($data->sub5_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub6_title')); ?>:</b>
	<?php echo CHtml::encode($data->sub6_title); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub6_text')); ?>:</b>
	<?php echo CHtml::encode($data->sub6_text); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sub6_img')); ?>:</b>
	<?php echo CHtml::encode($data->sub6_img); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delete_flag')); ?>:</b>
	<?php echo CHtml::encode($data->delete_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	*/ ?>

</div>