<?php
/* @var $this ProductController */
/* @var $model ProductsModel */

$this->breadcrumbs=array(
    'Products Models'=>array('index'),
    'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#products-model-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>


<h1>商品管理</h1>


<!-- 検索フォーム -->
<div class="panel panel-default">
    <div class="panel-heading">
        検索
    </div>
    <div class="panel-body">
        
        <?php $this->renderPartial(
            '_search',
            array(
                'model'=>$model,
                'categories' => $categories_list,
            )
        ); ?>
        
    </div>
</div>
<!-- 検索フォーム -->


<!-- 操作パネル -->
<div class="panel panel-default">
    <div class="panel-heading">
        検索
    </div>
    <div class="panel-body">
            <?php echo CHtml::link(
                    '新規商品追加',
                    Yii::app()->createUrl('product/product/create'),
                    array( 'class' => 'btn btn-large btn-primary')
                );
            ?>


            <?php echo CHtml::link(
                    'バーコード印刷',
                    Yii::app()->createUrl('product/product/barcode'),
                    array( 'class' => 'btn btn-large btn-primary')
                );
            ?>


            <?php echo CHtml::link(
                    '全一覧',
                    Yii::app()->createUrl('product/product/allview'),
                    array( 'class' => 'btn btn-large btn-primary')
                );
            ?>
    </div>
</div>
<!-- /操作パネル -->



<!-- エラーメッセージ -->
<?php if( $product_error_message != '' ): ?>
<div class="alert alert-error">
    <?php echo $product_error_message; ?>
</div>
<?php endif; ?>
<!-- /エラーメッセージ -->




<div class="panel panel-default">
    <div class="panel-body">
        
        
        <!-- テーブル -->
        <?php $data_provider = $model->search(); ?>
        <div class="adminsave">
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>画像</th>
                        <th>品番／品名</th>
                        <th>公開ステータス</th>
                        <th>SKU</th>
                        <th>操作</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $data_provider->getData() as $data ):?>
                    <tr>
                        <td>
                            <?php
                                echo CHtml::image(
                                    Yii::app()->getBaseUrl(true) . $data->main_image,
                                    '',
                                    array('width' => 100)
                                );
                            ?>
                        </td>
                        <td>
                            <?php echo $data->item_id; ?><br />
                            <?php echo $data->item_name; ?><br />
                        </td>
                        <td>
                            <?php if( $data->open_status == 1 ): ?>
                                公開
                            <?php else: ?>
                                非公開
                            <?php endif; ?>
                        </td>
                        <td>
                            <!-- SKU -->
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>品番・品名</th>
                                        <th>定価・売価</th>
                                        <th>在庫</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach( $data->sku as $sku ): ?>
                                    <tr>
                                        <td>
                                            <?php echo $sku->brunch_item_id; ?><br />
                                            <?php echo $sku->brunch_item_name; ?>
                                        </td>
                                        <td>
                                            <?php echo number_format($sku->price); ?>円<br />
                                            <?php echo number_format($sku->sale_price); ?>円
                                        </td>
                                        <td>
                                            <?php echo $sku->stock; ?>
                                        </td>
                                    </tr>
                                    <?php endforeach; ?>
                                </tbody>
                            </table>
                            <!-- /SKU -->
                        </td>
                        <td>
                            <?php
                                echo CHtml::link(
                                    '編集',
                                    Yii::app()->createUrl( 'product/product/update', array( 'id' => $data->id ) ),
                                    array( 'class' => 'btn btn-info' )
                                );
                            ?>
                            <br />
                            <?php
                                echo CHtml::link(
                                    '削除',
                                    Yii::app()->createUrl( 'product/product/delete', array( 'id' => $data->id ) ),
                                    array( 'class' => 'btn btn-danger' )
                                );
                            ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        <!-- /テーブル -->

        <!-- ページャー -->
        <?php
        $this->widget(
            'CLinkPager',
            array(
                'pages' => $data_provider->getPagination(),
            )
        );?>
        <!-- /ページャー -->
        
        
    </div>
</div>
