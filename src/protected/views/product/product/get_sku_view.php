<!-- 結果表示 -->
<div class="row-fluid">
	<div class="box-header well" data-original-title="">
		<h2>インポートするSKUを選択</h2>
	</div>
	<div class="box-content">
		<div class="row-fluid">
			<table class="table table-bordered">
				<thead>
					<tr>
						<td>選択<br /><a href="javascript:void(0);" class="addskucheckall">all</a></td>
						<td>画像</td>
						<td>SKU</td>
						<td>価格</td>
					</tr>
				</thead>
				<tbody>
					<?php foreach( $product->sku as $sku ): ?>
					<tr>
						<td>
							<?php $json = array(
								'product' => $product->attributes,
								'sku' => $sku->attributes,
								'image' => (isset($sku->product_sku_images[0])?$sku->product_sku_images[0]->image:'') );
							?>
							<?php echo CHtml::checkBox( 'checks', false, array( 'value' => json_encode($json) ) ); ?>
						</td>
						<td>
							<?php
								if( count($sku->product_sku_images) > 0 ):
									echo CHtml::image(
										Yii::app()->getBaseUrl(true) . $sku->product_sku_images[0]->image,
										'',
										array(
											'width' => 150
										)
									);
								else:
							?>
									画像なし
							<?php endif; ?>
						</td>
						<td>
							<?php echo $product->item_id; ?> <?php echo $product->item_name; ?><br />
							<?php echo $sku->brunch_item_id; ?> <?php echo $sku->brunch_item_name; ?>
						</td>
						<td>
							<?php echo number_format($sku->price); ?>円<br />
							<?php echo number_format($sku->sale_price); ?>円
						</td>
					</tr>
					<?php endforeach; ?>
				</tbody>
				<tfoot>
					<tr>
						<td colspan="3">
							<?php echo Chtml::button( '選択', array('class' => 'btn btn-info btn-mini skuaddselect')); ?>
							<?php echo CHtml::button( '戻る', array( 'class' => 'btn btn-danger btn-mini skuaddreturn') ); ?>
						</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>