<br />
<ul class="regi_select_item row-fluid">
	<?php foreach( $items as $item ): ?>
		<li class="span3" style="color: #000">
			<?php if( $item->thumbnail != '' ): ?>
				<div class="row-fluid">
					<div class="span12">
						<?php echo CHtml::image(Yii::app()->baseUrl . $item->thumbnail, '', array( 'style' => 'width: 100%') ); ?>
					</div>
				</div>
			<?php endif; ?>
			<div class="row-fluid">
				<div class="span9">
					<?php echo $item->item_name; ?>
				</div>
				<div class="span3">
					<?php echo CHtml::button( '削除', array( 'class' => 'btn btn-danger rel_delete', 'data-delete' => $item->id ) ); ?>
				</div>
			</div>
			
			<!-- 隠しタグ -->
			<?php echo CHtml::hiddenField( 'rel_products[]', $item->id ); ?>
			
		</li>
	<?php endforeach; ?>
</ul>