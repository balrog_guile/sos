<!-- さがすところ -->
<?php
$data_provider = $model->regiSearch();
$form=$this->beginWidget(
	'CActiveForm',
	array(
		'action'=>Yii::app()->createUrl($this->route),
		'method'=>'get',
		'htmlOptions' => array(
			'class' => 'regi_search_form',
		),
	)
); ?>
<div class="row-fluid">
	<div class="box-header well" data-original-title="">
		<h2>検索</h2>
	</div>
	<div class="box-content">
		<div class="row-fluid">
			<div class="span3">
				<?php echo $form->label($model,'item_id'); ?>
				<?php echo $form->textField($model,'item_id',array('size'=>60,'maxlength'=>125)); ?>
			</div>
			<div class="span3">
				<?php echo $form->label($model,'item_name'); ?>
				<?php echo $form->textField($model,'item_name',array('size'=>60,'maxlength'=>225)); ?>
			</div>
		</div>
		<hr />
		<div class="row-fluid">
			<div class="span4">
				<?php echo CHtml::submitButton('　検索　', array( 'class' => 'btn btn-info') ); ?>
				<?php echo CHtml::link( '検索解除', Yii::app()->createUrl( 'product/product/SearchOptionAdds' ), array( 'class' => 'btn btn-danger') ); ?>
			</div>
		</div>
	</div>
</div>
<?php $this->endWidget(); ?>
<!-- /さがすところ -->


<hr />


<!-- 結果表示 -->
<div class="row-fluid">
	<div class="box-header well" data-original-title="">
		<h2>結果</h2>
	</div>
	<div class="box-content">
		<div class="row-fluid">
			<ul class="regi_select_item row-fluid">
				<?php $i = 0; foreach( $data_provider->getData() as $row ): ?>
				<li class="span3" style="<?php if($i == 4): ?>clear:both;<?php endif; ?>">
					<?php if( $row->thumbnail != '' ): ?>
						<a href="<?php echo Yii::app()->createUrl('product/product/GetSkuView', array('id' => $row->id)); ?>" data-products-id="<?php echo $row->id; ?>" data-item-id="item_id">
							<?php echo CHtml::image(Yii::app()->baseUrl . $row->thumbnail, '', array( 'style' => 'width: 100%') ); ?>
						</a>
					<?php endif; ?>
					<hr />
					<?php echo CHtml::link(
							'品番:' . $row->item_id . '<br />' . '品名:' . $row->item_name,
							Yii::app()->createUrl('product/product/GetSkuView', array('id' => $row->id)),
							array(
								'class' => '',
								'data-products-id' => $row->id,
								'data-item-id' => $row->item_id,
							)
						);
					?>
				</li>
				<?php $i ++ ;endforeach; ?>
			</ul>
		</div>
		
		<hr />
		
		<?php
			$this->widget(
				'CLinkPager',
				array(
					'pages' => $data_provider->getPagination(),
				)
			);
		?>
	</div>
</div>
<!-- /結果表示 -->