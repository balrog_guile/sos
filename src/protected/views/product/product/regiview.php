<?php
$data_provider = $model->regiSearch();
?>

<div class="search-form row-fluid" style="">
<?php $this->renderPartial(
		'_regisearch',
		array(
			'model'=>$model,
			'categories' => $categories_list,
		)
	);
?>
</div>

<hr />

<ul class="regi_select_item row-fluid">
	<?php foreach( $data_provider->getData() as $row ): ?>
	<li class="span3">
		
		<?php if( $row->thumbnail != '' ): ?>
			<a href="javascript:void(0);" class="next_step" data-products-id="<?php echo $row->id; ?>" data-item-id="item_id">
				<?php echo CHtml::image(Yii::app()->baseUrl . $row->thumbnail, '', array( 'style' => 'width: 100%') ); ?>
			</a>
		<?php endif; ?>
		<hr />
		<?php echo CHtml::link(
				'品番:' . $row->item_id . '<br />' . '品名:' . $row->item_name,
				'javascript:void(0);',
				array(
					'class' => 'next_step',
					'data-products-id' => $row->id,
					'data-item-id' => $row->item_id,
				)
			);
		?>
	</li>
	<?php endforeach; ?>
</ul>

<hr />

<?php
$this->widget(
	'CLinkPager',
	array(
		'pages' => $data_provider->getPagination(),
	)
);?>