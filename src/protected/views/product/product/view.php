<?php
/* @var $this ProductController */
/* @var $model ProductsModel */

$this->breadcrumbs=array(
	'Products Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ProductsModel', 'url'=>array('index')),
	array('label'=>'Create ProductsModel', 'url'=>array('create')),
	array('label'=>'Update ProductsModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ProductsModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ProductsModel', 'url'=>array('admin')),
);
?>

<h1>View ProductsModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item_id',
		'item_name',
		'price',
		'sale_price',
		'open_status',
		'item_status',
		'postage',
		'point_rate',
		'shipment',
		'qty_limit',
		'serch_word',
		'remark',
		'main_view_comment',
		'main_detail_comment',
		'thumbnail',
		'main_image',
		'main_big_image',
		'sub1_title',
		'sub1_text',
		'sub1_img',
		'sub2_title',
		'sub2_text',
		'sub2_img',
		'sub3_title',
		'sub3_img',
		'sub3_text',
		'sub4_title',
		'sub4_text',
		'sub4_img',
		'sub5_title',
		'sub5_text',
		'sub5_img',
		'sub6_title',
		'sub6_text',
		'sub6_img',
		'delete_flag',
		'create_date',
		'update_date',
	),
)); ?>
