<?php
/* @var $this SkuController */
/* @var $model ProductSkuModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'product-sku-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'product_id'); ?>
		<?php echo $form->textField($model,'product_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'product_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'brunch_item_id'); ?>
		<?php echo $form->textField($model,'brunch_item_id',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'brunch_item_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'brunch_item_name'); ?>
		<?php echo $form->textField($model,'brunch_item_name',array('size'=>60,'maxlength'=>225)); ?>
		<?php echo $form->error($model,'brunch_item_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'price'); ?>
		<?php echo $form->textField($model,'price',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sale_price'); ?>
		<?php echo $form->textField($model,'sale_price',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'sale_price'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'stock'); ?>
		<?php echo $form->textField($model,'stock',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'stock'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'rank'); ?>
		<?php echo $form->textField($model,'rank'); ?>
		<?php echo $form->error($model,'rank'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'delete_flag'); ?>
		<?php echo $form->textField($model,'delete_flag'); ?>
		<?php echo $form->error($model,'delete_flag'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'code_str'); ?>
		<?php echo $form->textField($model,'code_str',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'code_str'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->