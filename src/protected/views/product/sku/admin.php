<?php
/* @var $this SkuController */
/* @var $model ProductSkuModel */

$this->breadcrumbs=array(
	'Product Sku Models'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List ProductSkuModel', 'url'=>array('index')),
	array('label'=>'Create ProductSkuModel', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#product-sku-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Product Sku Models</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'product-sku-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'product_id',
		'brunch_item_id',
		'brunch_item_name',
		'price',
		'sale_price',
		/*
		'stock',
		'rank',
		'delete_flag',
		'code_str',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
