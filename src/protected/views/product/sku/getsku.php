<?php
/* =============================================================================
 * レジサーチ用SKUリスト
 * ========================================================================== */
?>
<ul class="regi_select_item clearfix">
	<?php $i = 0;foreach( $datas as $row ): ?>
	<li class="<?php if($i==4): $i = 0;?>regi_clear<?php endif; ?>">
		<a
			href="javascript:void(0);"
			data-product-id="<?php echo $row->product_id; ?>"
			data-product-item-id="<?php echo $row->products->item_id; ?>"
			data-product-name="<?php echo $row->products->item_name; ?>"
			data-id="<?php echo $row->id; ?>"
			data-brunch-item-id="<?php echo $row->brunch_item_id; ?>"
			data-brunch-item-name="<?php echo $row->brunch_item_name; ?>"
			data-sale-price="<?php echo $row->sale_price; ?>"
			data-price="<?php echo $row->price; ?>"
			data-stock="<?php echo $row->stock; ?>"
			class="btn btn-success"
		>
			<h2><?php echo $row->products->item_name; ?><?php echo $row->brunch_item_name; ?></h2>
			<h3><?php echo $row->products->item_id; ?><?php echo $row->brunch_item_id; ?></h3>
			<?php echo number_format($row->sale_price); ?>円<br />
			<?php if( $row->stock == '' ): ?>在庫設定は有りません<?php else: ?>在庫:<?php echo $row->stock; ?><?php endif; ?>
		</a>
		
	</li>
	<?php $i ++ ;endforeach; ?>
</ul>

<hr />

<div class="row-fluid">
	<div class="span12">
		<?php echo CHtml::button('検索に戻る', array( 'class' => 'return_button btn btn-large btn-info' ) ); ?>
	</div>
</div>