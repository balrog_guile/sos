<?php
/* @var $this SkuController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Product Sku Models',
);

$this->menu=array(
	array('label'=>'Create ProductSkuModel', 'url'=>array('create')),
	array('label'=>'Manage ProductSkuModel', 'url'=>array('admin')),
);
?>

<h1>Product Sku Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
