<?php
/* @var $this SkuController */
/* @var $model ProductSkuModel */

$this->breadcrumbs=array(
	'Product Sku Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ProductSkuModel', 'url'=>array('index')),
	array('label'=>'Create ProductSkuModel', 'url'=>array('create')),
	array('label'=>'View ProductSkuModel', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ProductSkuModel', 'url'=>array('admin')),
);
?>

<h1>Update ProductSkuModel <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>