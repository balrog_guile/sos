<?php
/* @var $this StockingController */
/* @var $data StockingModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sku_id')); ?>:</b>
	<?php echo CHtml::encode($data->sku_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insert_date')); ?>:</b>
	<?php echo CHtml::encode($data->insert_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('purchase_price')); ?>:</b>
	<?php echo CHtml::encode($data->purchase_price); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leaving_date')); ?>:</b>
	<?php echo CHtml::encode($data->leaving_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leaving_qty')); ?>:</b>
	<?php echo CHtml::encode($data->leaving_qty); ?>
	<br />


</div>