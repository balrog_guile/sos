<?php
/* @var $this StockingController */
/* @var $model StockingModel */

$this->breadcrumbs=array(
	'Stocking Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List StockingModel', 'url'=>array('index')),
	array('label'=>'Manage StockingModel', 'url'=>array('admin')),
);
?>

<h1>Create StockingModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>