<?php
/* @var $this StockingController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Stocking Models',
);

$this->menu=array(
	array('label'=>'Create StockingModel', 'url'=>array('create')),
	array('label'=>'Manage StockingModel', 'url'=>array('admin')),
);
?>

<h1>Stocking Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
