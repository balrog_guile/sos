<?php
/* @var $this StockingController */
/* @var $model StockingModel */

$this->breadcrumbs=array();
$this->menu=array();

?>

<h1>在庫追加</h1>

<div>
	<?php echo CHtml::link(
				'商品管理に戻る',
				Yii::app()->createUrl( 'product/product/update', array( 'id' => $target_sku->products->id ) ),
				array(
					'class' => 'btn btn-large btn-success'
				)
			); ?>
</div>


<!-- 対象商品の確認 -->
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 仕入れ対象商品</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<strong>商品:</strong>
			<?php echo $target_sku->products->item_name; ?>
			(<?php echo $target_sku->products->item_id; ?>)
			　　　
			<strong>SKU:</strong>
			<?php echo $target_sku->brunch_item_name; ?>
			(<?php echo $target_sku->brunch_item_id; ?>)
			
			<hr />
			<strong>現在在庫:</strong>
			<?php echo number_format($target_sku->stock); ?>
			　　
			<strong>現在資産:</strong>
			<?php echo number_format( $assets ); ?>円
		</div>
	</div>
</div>
<!-- /対象商品の確認 -->



<!-- 入荷処理 -->
<div class="row-fluid sortable">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 仕入れ処理</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<?php $form=$this->beginWidget(
				'CActiveForm', array(
				'id'=>'products-model-form',
				'enableAjaxValidation'=>false,
			)); ?>
			<div class="row-fluid">
				<div class="span3">
					<div class="control-group">
						<?php echo $form->labelEx( $model, 'insert_date' ); ?>
						<div class="controls">
							<?php echo $form->textField( $model, 'insert_date', array('class' => 'input-medium') ); ?>
							<?php echo $form->error($model,'insert_date'); ?>
						</div>
					</div>
				</div>
				<div class="span3">
					<?php echo $form->labelEx( $model, 'partner_id' ); ?>
					<div class="controls">
							<?php echo $form->dropdownlist( $model, 'partner_id', PartnerModel::getAllListDropDown(), array( 'class' => 'input-medium') ); ?>
							<?php echo $form->error($model,'partner_id'); ?>
						</div>
				</div>
				<div class="span2">
					<div class="control-group">
						<?php echo $form->labelEx( $model, 'memo' ); ?>
						<div class="controls">
							<?php echo $form->textField( $model, 'memo', array('class' => 'input-small') ); ?>
							<?php echo $form->error($model,'memo'); ?>
						</div>
					</div>
				</div>
				<div class="span2">
					<div class="control-group">
						<?php echo $form->labelEx( $model, 'purchase_price' ); ?>
						<div class="controls">
							<?php echo $form->textField( $model, 'purchase_price', array('class' => 'input-small') ); ?>
							<?php echo $form->error($model,'purchase_price'); ?>
						</div>
					</div>
				</div>
				<div class="span2">
					<div class="control-group">
						<?php echo $form->labelEx( $model, 'purchase_qty' ); ?>
						<div class="controls">
							<?php echo $form->textField( $model, 'purchase_qty', array( 'class' => 'input-small') ); ?>
							<?php echo $form->error($model,'purchase_qty'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="row-fluid">
				<div class="span2">
					<?php echo Chtml::submitButton( '仕入れ登録', array( 'class' => 'btn btn-success btn-large' ) ); ?>
				</div>
			</div>
			<?php $this->endWidget(); ?>
			
		</div>
	</div>
</div>
<!-- /入荷処理 -->


<hr />


<!-- 仕入れ履歴 -->
<h2>仕入れ履歴</h2>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'stocking-model-grid',
	'dataProvider'=>$model->stockingSearch( $sku_id ),
	'filter'=>$model,
	'columns'=>array(
		'insert_date',
		array(
			'name' => 'partner_id',
			'value' => function( $data ){
				$find =  PartnerModel::getPartner( $data->partner_id );
				return $find['name'];
			},
		),
		'memo',
		'purchase_price',
		'purchase_qty',
		'sub_total',
		'leaving_qty',
		
		//出庫実績へ
		array(
			'header' => '出庫実績',
			'type' => 'raw',
			'htmlOptions' => array(
				'width' => '40%'
			),
			'value' => function($data){
				echo CHtml::link(
						'出庫実績一覧へ',
						Yii::app()->createAbsoluteUrl( 'product/stockingleave/sku', array( 'stocking_id' => $data->id) ),
						array(
							'class' => 'btn btn-info',
							'data-stocking_id' => $data->id,
							'target' => '_blank',
						)
					);
			}
		),
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update} {delete_stock}',
			'buttons'=>array(
				'delete_stock' => array(
					'label'=>'削除',
					'icon'=>'user',
					'url'=>'Yii::app()->createUrl("product/stocking/delete_stock", array("id"=>$data->id))',
					'options' => array(
						'class' => 'btn btn-large btn-danger',
						'onclick' => 'if(! confirm("削除してよろしいですか？") ){ return false; }',
					),
				),
			)
		),
	),
)); ?>
<!-- /仕入れ履歴 -->