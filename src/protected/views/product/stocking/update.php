<?php
/* @var $this StockingController */
/* @var $model StockingModel */

$this->breadcrumbs=array(
	'Stocking Models'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array();
?>

<h1>入庫個数の変更</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>