<?php
/* @var $this StockingController */
/* @var $model StockingModel */

$this->breadcrumbs=array(
	'Stocking Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List StockingModel', 'url'=>array('index')),
	array('label'=>'Create StockingModel', 'url'=>array('create')),
	array('label'=>'Update StockingModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete StockingModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage StockingModel', 'url'=>array('admin')),
);
?>

<h1>View StockingModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'sku_id',
		'insert_date',
		'purchase_price',
		'leaving_date',
		'leaving_qty',
	),
)); ?>
