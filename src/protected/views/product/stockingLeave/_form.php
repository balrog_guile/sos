<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'stocking-leave-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'stocking_id'); ?>
		<?php echo $form->textField($model,'stocking_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'stocking_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'leaving_date'); ?>
		<?php echo $form->textField($model,'leaving_date'); ?>
		<?php echo $form->error($model,'leaving_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'leaving_count'); ?>
		<?php echo $form->textField($model,'leaving_count',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'leaving_count'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'leaving_status'); ?>
		<?php echo $form->textField($model,'leaving_status'); ?>
		<?php echo $form->error($model,'leaving_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'canseled_date'); ?>
		<?php echo $form->textField($model,'canseled_date'); ?>
		<?php echo $form->error($model,'canseled_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->