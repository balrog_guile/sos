<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'stocking_id'); ?>
		<?php echo $form->textField($model,'stocking_id',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leaving_date'); ?>
		<?php echo $form->textField($model,'leaving_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leaving_count'); ?>
		<?php echo $form->textField($model,'leaving_count',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'leaving_status'); ?>
		<?php echo $form->textField($model,'leaving_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'canseled_date'); ?>
		<?php echo $form->textField($model,'canseled_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->