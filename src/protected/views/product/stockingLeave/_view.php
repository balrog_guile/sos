<?php
/* @var $this StockingLeaveController */
/* @var $data StockingLeaveModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('stocking_id')); ?>:</b>
	<?php echo CHtml::encode($data->stocking_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leaving_date')); ?>:</b>
	<?php echo CHtml::encode($data->leaving_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leaving_count')); ?>:</b>
	<?php echo CHtml::encode($data->leaving_count); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leaving_status')); ?>:</b>
	<?php echo CHtml::encode($data->leaving_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('canseled_date')); ?>:</b>
	<?php echo CHtml::encode($data->canseled_date); ?>
	<br />


</div>