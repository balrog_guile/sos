<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */

$this->breadcrumbs=array(
	'Stocking Leave Models'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List StockingLeaveModel', 'url'=>array('index')),
	array('label'=>'Create StockingLeaveModel', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#stocking-leave-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Stocking Leave Models</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'stocking-leave-model-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'stocking_id',
		'leaving_date',
		'leaving_count',
		'leaving_status',
		'canseled_date',
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
