<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */

$this->breadcrumbs=array(
	'Stocking Leave Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List StockingLeaveModel', 'url'=>array('index')),
	array('label'=>'Manage StockingLeaveModel', 'url'=>array('admin')),
);
?>

<h1>Create StockingLeaveModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>