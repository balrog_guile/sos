<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */

$this->breadcrumbs=array();
$this->menu=array();

?>

<h1>出庫実績</h1>


<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'stocking-leave-model-grid',
	'dataProvider'=>$model->leaveskusearch( $stocking_id ),
	'filter'=>$model,
	'columns'=>array(
		'leaving_date',
		'leaving_count',
		/*
		array(
			'class'=>'CButtonColumn',
		),
		*/
	),
)); ?>
