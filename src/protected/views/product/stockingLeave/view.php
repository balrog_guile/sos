<?php
/* @var $this StockingLeaveController */
/* @var $model StockingLeaveModel */

$this->breadcrumbs=array(
	'Stocking Leave Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List StockingLeaveModel', 'url'=>array('index')),
	array('label'=>'Create StockingLeaveModel', 'url'=>array('create')),
	array('label'=>'Update StockingLeaveModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete StockingLeaveModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage StockingLeaveModel', 'url'=>array('admin')),
);
?>

<h1>View StockingLeaveModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'stocking_id',
		'leaving_date',
		'leaving_count',
		'leaving_status',
		'canseled_date',
	),
)); ?>
