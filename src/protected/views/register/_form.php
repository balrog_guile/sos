<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'receipt-model-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'customer_id'); ?>
		<?php echo $form->textField($model,'customer_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'customer_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_hash'); ?>
		<?php echo $form->textField($model,'id_hash',array('size'=>60,'maxlength'=>125)); ?>
		<?php echo $form->error($model,'id_hash'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'receipt_name'); ?>
		<?php echo $form->textField($model,'receipt_name',array('size'=>60,'maxlength'=>256)); ?>
		<?php echo $form->error($model,'receipt_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'item_total'); ?>
		<?php echo $form->textField($model,'item_total',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'item_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tax'); ?>
		<?php echo $form->textField($model,'tax',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'tax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'grand_total'); ?>
		<?php echo $form->textField($model,'grand_total',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'grand_total'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'payment'); ?>
		<?php echo $form->textField($model,'payment',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'payment'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'change'); ?>
		<?php echo $form->textField($model,'change',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'change'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'order_status'); ?>
		<?php echo $form->textField($model,'order_status'); ?>
		<?php echo $form->error($model,'order_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deliv_status'); ?>
		<?php echo $form->textField($model,'deliv_status'); ?>
		<?php echo $form->error($model,'deliv_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'received'); ?>
		<?php echo $form->textField($model,'received'); ?>
		<?php echo $form->error($model,'received'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'deliv_code'); ?>
		<?php echo $form->textField($model,'deliv_code',array('size'=>60,'maxlength'=>225)); ?>
		<?php echo $form->error($model,'deliv_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'create_date'); ?>
		<?php echo $form->textField($model,'create_date'); ?>
		<?php echo $form->error($model,'create_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'update_date'); ?>
		<?php echo $form->textField($model,'update_date'); ?>
		<?php echo $form->error($model,'update_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'staff_id'); ?>
		<?php echo $form->textField($model,'staff_id',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'staff_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->