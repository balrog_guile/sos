<?php
/* @var $this RegisterController */
/* @var $data ReceiptModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customer_id')); ?>:</b>
	<?php echo CHtml::encode($data->customer_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_hash')); ?>:</b>
	<?php echo CHtml::encode($data->id_hash); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('receipt_name')); ?>:</b>
	<?php echo CHtml::encode($data->receipt_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item_total')); ?>:</b>
	<?php echo CHtml::encode($data->item_total); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tax')); ?>:</b>
	<?php echo CHtml::encode($data->tax); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('grand_total')); ?>:</b>
	<?php echo CHtml::encode($data->grand_total); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('payment')); ?>:</b>
	<?php echo CHtml::encode($data->payment); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('change')); ?>:</b>
	<?php echo CHtml::encode($data->change); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('order_status')); ?>:</b>
	<?php echo CHtml::encode($data->order_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deliv_status')); ?>:</b>
	<?php echo CHtml::encode($data->deliv_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('received')); ?>:</b>
	<?php echo CHtml::encode($data->received); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deliv_code')); ?>:</b>
	<?php echo CHtml::encode($data->deliv_code); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('staff_id')); ?>:</b>
	<?php echo CHtml::encode($data->staff_id); ?>
	<br />

	*/ ?>

</div>