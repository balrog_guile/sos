<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */

$this->breadcrumbs=array();

$this->menu=array(
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#receipt-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>売上管理</h1>

<!-- 検索 -->
<div class="search-form">
<?php $this->renderPartial(
	'_search',
	array(
		'model'=>$model,
		'users'=>$users,
	)
); ?>
</div>
<!-- /検索 -->


<hr />

<!-- 合計表示 -->
<div class="row-fluid">
	<div class="box span12">
		<div class="box-header well" data-original-title>
			<h2><i class="icon-th"></i> 検索対象の合計</h2>
			<div class="box-icon">
			</div>
		</div>
		<div class="box-content">
			<div class="wide form">
				<div class="row-fluid">
					<div class="span3">
						<h5>件数</h5>
						<p><?php echo number_format($calc['count']); ?>件</p>
					</div>
					<div class="span3">
						<h5>商品代金合計</h5>
						<p><?php echo number_format($calc['item_total_sum']); ?>円</p>
					</div>
					<div class="span3">
						<h5>お支払い総額</h5>
						<p><?php echo number_format($calc['grand_total_sum']); ?>円</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /合計表示 -->


<hr />


<!-- 一覧表示 -->
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'receipt-model-grid',
	'dataProvider'=>$model->search2(),
	//'filter'=>$model,
	'filter'=>null,
	'columns'=>array(
		'id',
		'receipt_name',
		'grand_total',
		array(
			'name' => 'order_status',
			'value' => 'VarHelper::order_status($data->order_status)',
		),
		'create_date',
		'payment_date',
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}',
			'updateButtonImageUrl' => '',
			'buttons'=>array(
				'update' => array(
					'label'=>'詳細確認',
					'url'=>'Yii::app()->createUrl("register/index", array("id"=>$data->id))',
					'options' => array('class' => 'btn btn-success', 'style' => 'width: 150px; margin-bottom: 0.5em;'),
				),
			)
		),
	),
)); ?>
<!-- /一覧表示 -->