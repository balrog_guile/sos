<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */

$this->breadcrumbs=array(
	'Receipt Models'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ReceiptModel', 'url'=>array('index')),
	array('label'=>'Manage ReceiptModel', 'url'=>array('admin')),
);
?>

<h1>Create ReceiptModel</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>