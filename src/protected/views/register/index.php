<?php
/* @var $this RegisterController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Receipt Models',
);
$this->menu=array(
	/*
	array('label'=>'Create ReceiptModel', 'url'=>array('create')),
	array('label'=>'Manage ReceiptModel', 'url'=>array('admin')),
	 * 
	 */
);
?>

<!-- 支払い処理後 -->
<?php if( $regi_exec_data['set'] === TRUE ): ?>
	<?php if( $regi_exec_data['truns_error'] === TRUE ): ?>
		<div class="alert alert-danger">
			システムエラーが発生！
		</div>
	<?php elseif( $regi_exec_data['result'] === FALSE ): ?>
		<div class="alert alert-danger">
			お手続き中に在庫切れとなった商品があります。
		</div>
	<?php else: ?>
		<div class="alert alert-success">
			お買い物手続きが完了しました。
		</div>
	<?php endif; ?>
<?php endif; ?>
<!-- /支払い処理後 -->


<div>
	<?php if($to_newest): ?>
		<?php echo CHtml::button( '　　最新のお買い上げへ　　', array('class' => 'btn btn-large btn-info to_newest') ); ?>
	<?php endif; ?>
	<?php echo CHtml::button( '　　新規お買い上げ　　', array('class' => 'btn btn-large btn-info set_new') ); ?>
	<?php echo CHtml::button( '　　履歴　　', array('class' => 'btn btn-large btn-info history') ); ?>
</div>


<div class="_im_enclosure">
<div class="_im_repeater">
	
	<!-- 基礎キー -->
	<?php echo CHtml::hiddenField( 'id', '', array( 'class' => 'IM[receipt@id]', 'id' => 'record-id' ) ); ?>
	
	<div class="row-fluid sortable">
		<div class="box span12">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-edit"></i> レジスターモード</h2>
			</div>
			<div class="box-content">
				
				<form class="form-horizontal">
					
					<label class="btn btn-info barcodeinputlabel" style="width: 90%;">
						バーコード読み取りモード
						<?php echo CHtml::textField( 'barcod', '', array( 'class' => 'barcodeinput' ) ); ?>
					</label>
					
					<hr />
					
					<fieldset>
						<div class="control-group">
							<label class="control-label" for="focusedInput">案件名</label>
							<div class="controls">
								<?php echo CHtml::textField( 'receipt_name', '', array( 'class' => 'IM[receipt@receipt_name] barcode_focus') ); ?>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="focusedInput">注文ステータス</label>
							<div class="controls">
								<?php echo CHtml::hiddenField( 'order_status', '', array( 'class' => 'IM[receipt@order_status] order_status_check') ); ?>
								<?php echo CHtml::dropDownList( 'order_status', '', VarHelper::order_status(), array( 'class' => 'IM[receipt@order_status] order_status') ); ?>
							</div>
						</div>
						
						
						<div class="control-group">
							<label class="control-label">作成日</label>
							<div class="controls">
								<?php echo CHtml::textField( 'create_date', '', array( 'class' => 'IM[receipt@create_date]', 'readonly' => 'readonly' ) ); ?>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label">支払日</label>
							<div class="controls">
								<?php echo CHtml::textField( 'payment_date', '', array( 'class' => 'IM[receipt@payment_date] payment_date', 'readonly' => 'readonly' ) ); ?>
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="focusedInput">税抜き支払い総額</label>
							<div class="controls">
								<?php echo CHtml::textField( 'grand_total', '', array( 'class' => 'IM[receipt@grand_total] grand_total', 'readonly' => 'readonly' ) ); ?>円
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="focusedInput">支払総額（税込み）</label>
							<div class="controls">
								<?php echo CHtml::textField( 'grand_total_tax', '', array( 'class' => 'IM[receipt@grand_total_tax] grand_total_tax', 'readonly' => 'readonly' ) ); ?>円
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="focusedInput">お客様支払い</label>
							<div class="controls">
								<?php echo CHtml::textField( 'payment', '', array( 'class' => 'IM[receipt@payment] payment', 'readonly' => 'readonly' ) ); ?>円
							</div>
						</div>
						
						<div class="control-group">
							<label class="control-label" for="focusedInput">釣り銭</label>
							<div class="controls">
								<?php echo CHtml::textField( 'change_val', '', array( 'class' => 'IM[receipt@change_val] change_val', 'readonly' => 'readonly' ) ); ?>円
							</div>
						</div>
						
					</fieldset>
					
					<hr />
					<?php echo CHtml::button( '　　現金支払い　　', array( 'class' => 'pay_with_money btn btn-inverse input_checker_buttons', 'style' => '' ) ); ?>
					<?php echo CHtml::button( '　　クレジット支払い　　', array( 'class' => 'pay_with_card btn btn-inverse input_checker_buttons', 'style' => '' ) ); ?>
					<?php echo CHtml::button( '　　レシート表示　　', array( 'class' => 'go_receipt btn btn-info', 'style' => '' ) ); ?>
					
				</form>
			</div>
		</div>
	</div>
	
	
	<!-- 来訪記録 -->
	<div class="_im_enclosure"><div class="_im_repeater">
		<?php echo CHtml::hiddenField( 'id', '', array( 'class' => 'IM[receipt@id]', 'id' => 'record-id' ) ); ?>
		<div class="row-fluid">
			<div class="box span12">
				<div class="box-header well">
					<h2>
						<i class="icon-info-sign"></i>来訪記録
					</h2>
					<div class="box-icon">
						<a class="btn btn-minimize btn-round" href="#">
							<i class="icon-chevron-up"></i>
						</a>
						<a class="btn btn-close btn-round" href="#">
							<i class="icon-remove"></i>
						</a>
					</div>
				</div>
				<div class="box-content">
					<?php echo CHtml::button( '　　来店記録へ　　', array('class' => 'btn btn-large btn-info comming_history') ); ?>
					<?php echo CHtml::hiddenField( 'id', '', array( 'class' => 'IM[comming@id] comming_id') ); ?>
					<?php echo CHtml::hiddenField( 'customer_id', '', array( 'class' => 'IM[comming@cusomer_id] customer_id') ); ?>
					<div class="row-fluid">
						<div class="span2">来訪日</div>
						<div class="span10 IM[comming@comming_date]"></div>
					</div>
					<div class="row-fluid">
						<div class="span6">
							<label>メモ1</label>
							<?php echo CHtml::textArea( 'memo1', '', array( 'class' => 'IM[comming@memo1] input-xlarge focused') ); ?>
						</div>
						<div class="span6">
							<label>メモ2</label>
							<?php echo CHtml::textArea( 'memo2', '', array( 'class' => 'IM[comming@memo2] input-xlarge focused') ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div></div>
	<!-- /来訪記録 -->
	
	
	
	<!-- 明細行 -->
	<div id="detail_box" class="row-fluid sortable">
		<?php echo CHtml::hiddenField( 'id', '', array( 'class' => 'IM[receipt@id]', 'id' => 'record-id' ) ); ?>
		<div class="box">
			<div class="box-header well" data-original-title>
				<h2><i class="icon-edit"></i> 明細</h2>
			</div>
			<div class="box-content">
				<div>
					<?php echo CHtml::button( '商品選択', array( 'class' => 'btn btn-success select_product' ) ); ?>
				</div>
				<hr />
				<label class="btn btn-info barcodeinputlabel" style="width: 90%;">
						バーコード読み取りモード
						<?php echo CHtml::textField( 'barcod', '', array( 'class' => 'barcodeinput' ) ); ?>
					</label>
					
					<hr />
				<table class="table">
					<thead>
						<tr>
							<th>品番<br />品名<!--<br />備考--></th>
							<th>単価</th>
							<th>数量</th>
							<th>小計</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>
								<!-- 隠し項目 -->
								<?php echo CHtml::hiddenField( 'id', '', array('class' => 'IM[receipt_detail@id] repeat-id') ); ?>
								<?php echo CHtml::hiddenField( 'sku_id', '', array('class' => 'IM[receipt_detail@sku_id] sku_id') ); ?>
								<?php echo CHtml::hiddenField( 'parent_id', '', array('class' => 'IM[receipt_detail@product_id] product_id') ); ?>
								<!-- /隠し項目 -->
								
								<?php echo CHtml::textField( 'product_id', '', array( 'class' => 'IM[receipt_detail@item_id] input_product_id input_checker' ) ); ?><br />
								<?php echo CHtml::textField( 'item_name', '', array( 'class' => 'IM[receipt_detail@item_name] item_name input_checker' ) ); ?>
								
								<!--
								<hr />
								<?php echo CHtml::textArea( 'remark', '', array( 'class' => 'IM[receipt_detail@remark] input_checker' ) ); ?>
								-->
							</td>
							<td class="center">
								<?php echo CHtml::numberField( 'price', '', array( 'class' => 'IM[receipt_detail@price] repeat-price input-small input_checker' ) ); ?>
								<?php echo CHtml::numberField( 'price_tax', '', array( 'class' => 'IM[receipt_detail@price_tax] repeat-price_tax input-small input_checker' ) ); ?>
							</td>
							<td class="center">
								<?php echo CHtml::numberField( 'qty', '', array( 'class' => 'IM[receipt_detail@qty] repeat-qty input-mini input_checker' ) ); ?>
							</td>
							<td class="center">
								<?php echo CHtml::numberField( 'sub_total', '', array( 'class' => 'IM[receipt_detail@sub_total] sub_total input-small input_checker', 'readonly' => 'readonly' ) ); ?>
								<?php echo CHtml::numberField( 'sub_total_tax', '', array( 'class' => 'IM[receipt_detail@sub_total_tax] sub_total_tax input-small input_checker', 'readonly' => 'readonly' ) ); ?>
							</td>
						</tr>
					</tbody>
				</table>
				
				<hr />
				<label class="btn btn-info barcodeinputlabel" style="width: 90%;">
					バーコード読み取りモード
					<?php echo CHtml::textField( 'barcod', '', array( 'class' => 'barcodeinput' ) ); ?>
				</label>
				
			</div>
		</div>
	</div>
	<!-- /明細行 -->
	
</div>
</div>




<!-- 支払い用モーダル -->
<div id="payment_mode_modal" style="display: none;">
	<div class="row-fluid">
		<div class="span8">
			<?php echo CHtml::textField( 'input_payment', '', array( 'class' => 'input_payment' ) ); ?>円　<?php echo CHtml::button( 'お支払い', array( 'class' => 'btn btn-inverse exec_input_payment', 'style' => 'margin-left: 10px;' ) ); ?>
		</div>
		<div class="span4">
			<table>
				<tr>
					<td width="30%">
						<?php echo CHtml::button( '7', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td width="30%">
						<?php echo CHtml::button( '8', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td width="30%">
						<?php echo CHtml::button( '9', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo CHtml::button( '4', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '5', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '6', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo CHtml::button( '1', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '2', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '3', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo CHtml::button( '  0 ', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( ' 00 ', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '0000', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'input_payment', 'style' => 'width: 100%;') ); ?>
					</td>
				</tr>
			</table>
		</div>
	</div>
</div>
<!-- /支払い用モーダル -->


<!-- 商品選択モーダル -->
<div id="item_select_mode_modal" style="display: none;">
	<div class="row-fluid">
		<div class="span6">
			
			<div class="row-fluid">
				<div class="span4">品番</div>
				<div class="span8 item_select_item_id"></div>
				
			</div>
			
			<div class="row-fluid">
				<div class="span4">品名</div>
				<div class="span8 item_select_item_name"></div>
				
			</div>
			
			<div class="row-fluid">
				<div class="span4">金額</div>
				<div class="span8">
					<div class="item_select_price"></div>
					<div class="item_select_sale_price"></div>
				</div>
				
			</div>
			
			
			
		</div>
		<div class="span6">
			<div>
				<?php echo CHtml::textField(
						'qty_disp_area',
						'',
						array(
							'class' => 'qty_disp_area'
						)
					);
				?>個
			</div>
			<div>
				<?php echo CHtml::button( '確定', array('class' => 'btn btn-large btn-info add_item', 'style' => 'width: 100px;') ); ?>
			</div>
		</div>
		
		<!--
		<div class="span3">
			<table>
				<tr>
					<td>
						<?php echo CHtml::button( '7', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '8', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '9', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo CHtml::button( '4', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '5', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '6', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo CHtml::button( '1', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '2', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '3', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
				</tr>
				<tr>
					<td>
						<?php echo CHtml::button( '0', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '00', array('class' => 'btn btn-large btn-info input_num', 'data-target-input' => 'qty_disp_area', 'style' => 'width: 40px;') ); ?>
					</td>
					<td>
						<?php echo CHtml::button( '確定', array('class' => 'btn btn-large btn-info add_item', 'style' => 'width: 40px;') ); ?>
					</td>
				</tr>
			</table>
		</div>
		-->
	</div>
</div>
<!-- /商品選択モーダル -->



<!-- 商品選択（カテゴリから） -->
<div id="view_select_modal" style="display: none;">
	<div id="view_select_modal_content">
		テスト
	</div>
</div>
<!-- /商品選択（カテゴリから） -->


<!-- 支払い実行用隠しフォーム -->
<?php echo CHtml::form( Yii::app()->createAbsoluteUrl('register/exec_payment' ), 'post', array('class'=>'payment_form') ); ?>
	<?php echo CHtml::hiddenField( 'payment_id', '', array( 'class' => 'payment_id') ); ?>
	<?php echo CHtml::hiddenField( 'payment', '', array( 'class' => 'payment_price') ); ?>
	<?php echo CHtml::hiddenField( 'payment_method_on', '1', array( 'class' => 'payment_method_on') ); ?>
<?php echo CHtml::endForm(); ?>
<!-- 支払い実行用隠しフォーム -->