<?php
/* @var $this RegisterController */
/* @var $model ReceiptModel */

$this->breadcrumbs=array(
	'Receipt Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ReceiptModel', 'url'=>array('index')),
	array('label'=>'Create ReceiptModel', 'url'=>array('create')),
	array('label'=>'Update ReceiptModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ReceiptModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ReceiptModel', 'url'=>array('admin')),
);
?>

<h1>View ReceiptModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'customer_id',
		'id_hash',
		'receipt_name',
		'item_total',
		'tax',
		'grand_total',
		'payment',
		'change',
		'order_status',
		'deliv_status',
		'received',
		'deliv_code',
		'create_date',
		'update_date',
		'staff_id',
	),
)); ?>
