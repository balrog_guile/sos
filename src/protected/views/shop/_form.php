<?php
/* @var $this ShopController */
/* @var $model ShopModel */
/* @var $form CActiveForm */
?>

<div class="panel panel-default">
    <!--
    <div class="panel-heading">
    </div>
    -->
    <div class="panel-body">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'shop-model-form',
            // Please note: When you enable ajax validation, make sure the corresponding
            // controller action is handling ajax validation correctly.
            // There is a call to performAjaxValidation() commented in generated controller code.
            // See class documentation of CActiveForm for details on this.
            'enableAjaxValidation'=>false,
        )); ?>
        
        
        
            <?php echo $form->errorSummary($model); ?>
        
            <div class="">
                <?php echo $form->labelEx($model,'name'); ?>
                <?php echo $form->textField($model,'name',array( 'class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'name'); ?>
            </div>
            <div class="">
                <?php echo $form->labelEx($model,'login_id'); ?>
                <?php echo $form->textField($model,'login_id',array( 'class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'login_id'); ?>
            </div>
            <div class="">
                <?php echo $form->labelEx($model,'address1'); ?>
                <?php echo $form->textField($model,'address1',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'address1'); ?>
            </div>

            <div class="">
                <?php echo $form->labelEx($model,'address2'); ?>
                <?php echo $form->textField($model,'address2',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'address2'); ?>
            </div>

            <div class="">
                <?php echo $form->labelEx($model,'address3'); ?>
                <?php echo $form->textField($model,'address3',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'address3'); ?>
            </div>

            <div class="">
                <?php echo $form->labelEx($model,'tel'); ?>
                <?php echo $form->textField($model,'tel',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'tel'); ?>
            </div>

            <div class="">
                <?php echo $form->labelEx($model,'fax'); ?>
                <?php echo $form->textField($model,'fax',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'fax'); ?>
            </div>

            <div class="">
                <?php echo $form->labelEx($model,'mailaddress'); ?>
                <?php echo $form->textField($model,'mailaddress',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'mailaddress'); ?>
            </div>


            <div class="">
                <?php echo $form->labelEx($model,'password'); ?>
                <?php echo $form->passwordField($model,'password',array('class' => 'input-large form-control' )); ?>
                <?php echo $form->error($model,'password'); ?>
            </div>



            <hr />


            <div class="buttons">
                <?php
                    echo CHtml::submitButton(
                        $model->isNewRecord ? '登録' : '編集',
                        array(
                            'class' => 'btn btn-success'
                        )
                    );
                ?>
            </div>
            
            
            
        <?php $this->endWidget(); ?>
    </div>
</div><!-- form -->