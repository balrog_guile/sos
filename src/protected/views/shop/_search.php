<?php
/* @var $this ShopController */
/* @var $model ShopModel */
/* @var $form CActiveForm */
?>

<div class="wide form row-fluid">
    <div class="span12">
        <?php $form=$this->beginWidget('CActiveForm', array(
            'action'=>Yii::app()->createUrl($this->route),
            'method'=>'get',
        )); ?>
        
            <div class="row-fluid">
                <div class="span6">
                    <?php echo $form->label($model,'name'); ?>
                    <?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>125)); ?>
                </div>
                <div class="span6">
                    <?php echo $form->label($model,'tel'); ?>
                    <?php echo $form->textField($model,'tel',array('size'=>45,'maxlength'=>45)); ?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <?php echo $form->label($model,'fax'); ?>
                    <?php echo $form->textField($model,'fax',array('size'=>45,'maxlength'=>45)); ?>
                </div>
                <div class="span6">
                    <?php echo $form->label($model,'mailaddress'); ?>
                    <?php echo $form->textField($model,'mailaddress',array('size'=>60,'maxlength'=>255)); ?>
                </div>
            </div>
            <div class="row-fluid">
                <div class="buttons span6">
                    <?php echo CHtml::submitButton('検索',array('class' => 'btn btn-primary')); ?>
                </div>
            </div>
        <?php $this->endWidget(); ?>
    </div>
</div><!-- search-form -->