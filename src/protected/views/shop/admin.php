<?php
/* @var $this ShopController */
/* @var $model ShopModel */

$this->breadcrumbs=array(
    'Shop Models'=>array('index'),
    'Manage',
);

$this->menu=array();

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
    $('.search-form').toggle();
    return false;
});
$('.search-form form').submit(function(){
    $('#shop-model-grid').yiiGridView('update', {
        data: $(this).serialize()
    });
    return false;
});
");
?>

<h1>ショップ管理</h1>

<hr />
<!-- 検索フォーム -->
<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div>
<!-- /検索フォーム -->


<!-- 操作パネル -->
<div class="panel panel-default">
    <div class="panel-heading">
        操作
    </div>
    <div class="panel-body">
         <?php echo CHtml::link(
            '新規登録', 
            Yii::app()->createUrl('shop/create' ),
            array(
                'class' => 'btn btn-success'
            )
        ); ?>
    </div>
</div>
<!-- /操作パネル -->





<div class="panel panel-default">
        <div class="panel-heading">
            ショップ一覧
        </div>
        <div class="panel-body">
            
            <?php if( $message != '' ): ?>
            <div class="alert alert-success">
                <?php echo $message; ?>
            </div>
            <?php endif; ?>
            
            
            <?php $dataProvider = $model->search(); ?>
            <table class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>ショップ名</th>
                        <th>ログインID</th>
                        <td>電話番号</td>
                        <td>メールアドレス</td>
                        <td>ショップコード</td>
                        <td>操作</td>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach( $dataProvider->getData() as $data ):?>
                        <tr>
                            <td>
                                <a
                                    href="<?php echo Yii::app()->getBaseUrl(true); ?>/?SOS_THEME_SELECT=<?php echo $data->shop_code; ?>"
                                    target="_blank"
                                 >
                                    <?php echo $data->name; ?>
                                </a>
                            </td>
                            <td>
                                <?php echo $data->login_id; ?>
                            </td>
                            <td>
                                <?php echo $data->tel; ?>
                            </td>
                            <td>
                                <?php echo $data->mailaddress; ?>
                            </td>
                            <td>
                                <?php echo $data->shop_code; ?>
                            </td>
                            <td>
                                <?php
                                    echo CHtml::link(
                                        'ディレクトリ作成',
                                        Yii::app()->createUrl('shop/create_dir/id/'.$data->id),
                                        array(
                                            'class' => 'btn btn-success'
                                        )
                                    );
                                ?>
                                <br />
                                <?php
                                    echo CHtml::link(
                                        'コンテンツ管理',
                                        Yii::app()->createUrl('shop/contents/id/'.$data->id),
                                        array(
                                            'class' => 'btn btn-success'
                                        )
                                    );
                                ?>
                                <?php
                                    echo CHtml::link(
                                        '編集',
                                        '',
                                        array(
                                            'class' => 'btn btn-primary'
                                        )
                                    );
                                ?>
                                <?php
                                    echo CHtml::link(
                                        '削除',
                                        Yii::app()->createUrl('shop/delete/id/'.$data->id),
                                        array(
                                            'class' => 'btn btn-danger'
                                        )
                                    );
                                ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
            
        </div>
        <!--
        <div class="panel-footer">
            Panel Footer
        </div>
        -->
    </div>


