<?php
/* @var $this ShopController */
/* @var $model ShopModel */
/* @var $form CActiveForm */
?>

<h1><?php echo $shop->name; ?>のコンテンツ管理</h1>



<?php if(isset($msg)): ?>
<p class="alert alert-success"><?php echo $msg; ?></p>
<?php endif; ?>
<?php if($user->shop_id == 0): ?>
<hr />
<?php
    echo CHtml::link(
        'ショップ管理へ戻る',
        Yii::app()->createUrl('shop'),
        array(
            'class' => 'btn btn-success'
        )
    );
?>
<?php endif; ?>
<hr />

<div class="form">
<?php $form=$this->beginWidget('CActiveForm', array(
    'id'=>'shop-model-form',
    // Please note: When you enable ajax validation, make sure the corresponding
    // controller action is handling ajax validation correctly.
    // There is a call to performAjaxValidation() commented in generated controller code.
    // See class documentation of CActiveForm for details on this.
    'enableAjaxValidation'=>false,
	'htmlOptions' => array(
		'class' => 'form',
	),
)); ?>



    <?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model,'subject'); ?>
        <?php echo $form->textField($model,'subject',array( 'class' => 'form-control' )); ?>
        <?php echo $form->error($model,'name'); ?>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model,'body'); ?>
        <?php echo $form->textArea($model,'body',array('class' => 'form-control', 'style' => 'height: 500px;' )); ?>
        <?php echo $form->error($model,'body'); ?>
    </div>

    
    <hr />
    
    
    <div class="buttons">
        <?php
            echo CHtml::submitButton(
                $model->isNewRecord ? '登録' : '編集',
                array(
                    'class' => 'btn btn-success'
                )
            );
        ?>
    </div>
    
    
    
<?php $this->endWidget(); ?>

</div><!-- form -->