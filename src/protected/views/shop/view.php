<?php
/* @var $this ShopController */
/* @var $model ShopModel */

$this->breadcrumbs=array(
	'Shop Models'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List ShopModel', 'url'=>array('index')),
	array('label'=>'Create ShopModel', 'url'=>array('create')),
	array('label'=>'Update ShopModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ShopModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShopModel', 'url'=>array('admin')),
);
?>

<h1>View ShopModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'address1',
		'address2',
		'address3',
		'tel',
		'fax',
		'mailaddress',
		'rank',
		'create_date',
	),
)); ?>
