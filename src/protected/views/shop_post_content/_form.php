<?php
/* @var $this Shop_post_contentController */
/* @var $model ShopPostContentModel */
/* @var $form CActiveForm */
?>

<div class="panel panel-default">
	<div class="panel-body">

		<?php $form=$this->beginWidget('CActiveForm', array(
			'id'=>'shop-post-content-model-form',
			// Please note: When you enable ajax validation, make sure the corresponding
			// controller action is handling ajax validation correctly.
			// There is a call to performAjaxValidation() commented in generated controller code.
			// See class documentation of CActiveForm for details on this.
			'enableAjaxValidation'=>false,
		)); ?>

			<?php echo $form->errorSummary($model); ?>

			<div class="">
				<?php echo $form->labelEx($model,'publish_date'); ?>
				<?php echo $form->textField($model,'publish_date', array('class' => 'form-control')); ?>
				<?php echo $form->error($model,'publish_date'); ?>
			</div>

			<div class="">
				<?php echo $form->labelEx($model,'content'); ?>
				<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50, 'class' => 'form-control')); ?>
				<?php echo $form->error($model,'content'); ?>
			</div>

			<hr>

			<div class="buttons">
				<?php echo CHtml::submitButton($model->isNewRecord ? '登録' : '編集', array(
					'class' => 'btn btn-success',
				)); ?>
			</div>

		<?php $this->endWidget(); ?>

	</div>
</div>