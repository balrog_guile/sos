<?php
/* @var $this Shop_post_contentController */
/* @var $data ShopPostContentModel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shop_id')); ?>:</b>
	<?php echo CHtml::encode($data->shop_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('subjct')); ?>:</b>
	<?php echo CHtml::encode($data->subjct); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('content')); ?>:</b>
	<?php echo CHtml::encode($data->content); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('publish_date')); ?>:</b>
	<?php echo CHtml::encode($data->publish_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_flag')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_flag); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deleted_date')); ?>:</b>
	<?php echo CHtml::encode($data->deleted_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reject_date')); ?>:</b>
	<?php echo CHtml::encode($data->reject_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reject_reason')); ?>:</b>
	<?php echo CHtml::encode($data->reject_reason); ?>
	<br />

	*/ ?>

</div>