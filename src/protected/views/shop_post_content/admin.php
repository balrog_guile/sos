<?php
/* @var $this Shop_post_contentController */
/* @var $model ShopPostContentModel */

$this->breadcrumbs=array(
	'Shop Post Content Models'=>array('index'),
	'Manage',
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#shop-post-content-model-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>投稿管理</h1>

<hr>

<!-- 検索フォーム -->
<div class="search-form">
<?php $this->renderPartial('_search',array(
    'model'=>$model,
)); ?>
</div>
<!-- /検索フォーム -->

<!-- 操作パネル -->
<div class="panel panel-default">
    <div class="panel-heading">
        操作
    </div>
    <div class="panel-body">
         <?php echo CHtml::link(
            '新規登録', 
            Yii::app()->createUrl('shop_post_content/create' ),
            array(
                'class' => 'btn btn-success'
            )
        ); ?>
    </div>
</div>
<!-- /操作パネル -->

<div class="panel panel-default">
	<div class="panel-heading">
		投稿一覧
	</div>
	<div class="panel-body">
		<?php if( $message != '' ): ?>
		<div class="alert alert-success">
			<?php echo $message; ?>
		</div>
		<?php endif; ?>
		<?php $dataProvider = $model->search(); ?>
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th>公開日</th>
					<th>内容</th>
					<td>操作</td>
				</tr>
			</thead>
			<tbody>
				<?php foreach( $dataProvider->getData() as $data ):?>
					<tr>
						<td>
							<?php echo $data->publish_date; ?>
						</td>
						<td>
							<?php echo $data->content; ?>
						</td>
						<td>
							<?php
								echo CHtml::link(
									'編集',
									Yii::app()->createUrl('shop_post_content/update/id/'.$data->id),
									array(
										'class' => 'btn btn-primary'
									)
								);
							?>
							<?php
								echo CHtml::link(
									'削除',
									Yii::app()->createUrl('shop_post_content/delete/id/'.$data->id),
									array(
										'class' => 'btn btn-danger'
									)
								);
							?>
						</td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
	</div>
</div>


