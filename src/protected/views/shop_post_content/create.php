<?php
/* @var $this Shop_post_contentController */
/* @var $model ShopPostContentModel */

$this->breadcrumbs=array(
	'Shop Post Content Models'=>array('index'),
	'Create',
);

?>

<h1>投稿管理</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>