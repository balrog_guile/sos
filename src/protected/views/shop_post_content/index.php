<?php
/* @var $this Shop_post_contentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Shop Post Content Models',
);

$this->menu=array(
	array('label'=>'Create ShopPostContentModel', 'url'=>array('create')),
	array('label'=>'Manage ShopPostContentModel', 'url'=>array('admin')),
);
?>

<h1>Shop Post Content Models</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
