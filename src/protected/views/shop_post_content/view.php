<?php
/* @var $this Shop_post_contentController */
/* @var $model ShopPostContentModel */

$this->breadcrumbs=array(
	'Shop Post Content Models'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ShopPostContentModel', 'url'=>array('index')),
	array('label'=>'Create ShopPostContentModel', 'url'=>array('create')),
	array('label'=>'Update ShopPostContentModel', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ShopPostContentModel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ShopPostContentModel', 'url'=>array('admin')),
);
?>

<h1>View ShopPostContentModel #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'shop_id',
		'status',
		'subjct',
		'content',
		'publish_date',
		'create_date',
		'update_date',
		'deleted_flag',
		'deleted_date',
		'reject_date',
		'reject_reason',
	),
)); ?>
