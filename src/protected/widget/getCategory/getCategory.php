<?php
/* =============================================================================
 * 小カテゴリを取得
 * ========================================================================== */
class getCategory extends CWidget
{
	//親カテゴリ
	public $target = '';//URLで指定
	
	//カテゴリ情報
	public $category = null;
	
	
	// ----------------------------------------------------
	
	/**
	 * 初期化
	 */
	
	public function init()
	{
		$this->category = CategoryModel::model()->find(array(
			'condition' => 'link_url = :link_url',
			'params' => array(
				':link_url' => $this->target
			)
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 最終キャプチャ
	 */
	
	public function run()
	{
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリ用リンクを作成
	 */
	public function getLinkUri( $model )
	{
		return Yii::app()->getBaseUrl( TRUE ) . '/category/' . $model->link_url . '.html';
	}
	
	// ----------------------------------------------------
	
}