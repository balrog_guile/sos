<?php
/* =============================================================================
 * ホットアイテム
 * ========================================================================== */
class getHot extends CWidget
{
	//ターゲットステータス
	public $status = 1;
	
	//いくつ取得するか
	public $limit = 8;
	
	//商品情報格納
	public $products = array();
	
	
	// ----------------------------------------------------
	
	/**
	 * 初期化
	 */
	
	public function init()
	{
		$this->products = ProductsModel::model()->findAll(array(
			'condition' => '( item_status = :item_status1 ) OR ( item_status LIKE :item_status2 ) OR ( item_status LIKE :item_status3 )',
			'params' => array(
				':item_status1' => $this->status,
				':item_status2' => $this->status . "\n" . '%',
				':item_status3' => "\n" . $this->status . "\n" . '%',
			),
			'limit' => $this->limit,
			'order' => 'create_date DESC'
		));
	}
	
	// ----------------------------------------------------
	
	/**
	 * 最終キャプチャ
	 */
	
	public function run()
	{
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリ用リンクを作成
	 */
	public function getLinkUri( $model )
	{
		return Yii::app()->getBaseUrl( TRUE ) . '/archives/' . $model->link_url . '.html';
	}
	
	// ----------------------------------------------------
	
}