<?php
/* =============================================================================
 * プロダクトを品番指名で取得
 * ========================================================================== */
class getProducts extends CWidget
{
	//取得ターゲット
	public $target = array();
	
	//商品情報格納
	public $products = array();
	
	
	// ----------------------------------------------------
	
	/**
	 * 初期化
	 */
	
	public function init()
	{
		if(
			(!is_array($this->target))||
			( count($this->target) == 0 )
		)
		{
			return;
		}
		
		//条件設定
		$criteria = new CDbCriteria();
		$criteria->addInCondition( 'item_id', $this->target );
		$criteria->compare( 'open_status', 1 );
		
		$this->products = ProductsModel::model()->findAll($criteria);
	}
	
	// ----------------------------------------------------
	
	/**
	 * 最終キャプチャ
	 */
	
	public function run()
	{
	}
	
	// ----------------------------------------------------
	
	/**
	 * カテゴリ用リンクを作成
	 */
	public function getLinkUri( $model )
	{
		return Yii::app()->getBaseUrl( TRUE ) . '/archives/' . $model->link_url . '.html';
	}
	
	// ----------------------------------------------------
	
}