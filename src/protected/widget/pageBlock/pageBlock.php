<?php
/* =============================================================================
 * ページブロックウィジェット
 * ========================================================================== */
class pageBlock extends CWidget
{
	protected $user;
	
	public $name;
	public $types = array(
		'html',//HTML入力
		'richText',//WYSIWYG
		'image',//画像
		'image_text',//画像＆テキスト
		'youtube',//youtube
	);
	public $isGlobal = false;
	public $enableAdd = TRUE;
	public $enableRank = TRUE;
	public $enableEdit = TRUE;
	public $applicaton_url;
	
	public $contents = array();
	
	
	public function init()
	{
		$this->user = Yii::app()->user;
		
		if(!isset(Yii::app()->controller->application_url))
		{
			$this->applicaton_url = $_SERVER['REQUEST_URI'];
		}
		
		
		//時間周り
		$start = new CDbCriteria();
		$start->addCondition( 'open_date is null');
		$start->compare( 'open_date', '<= '. date('Y-m-d H:i:s'), FALSE, 'OR' );
		
		$end = new CDbCriteria();
		$end->addCondition( 'close_date is null');
		$end->compare( 'close_date', '>= '. date('Y-m-d H:i:s'), FALSE,  'OR' );
		
		
		
		//データ取得
		$criteria = new CDbCriteria();
		if( $this->isGlobal === TRUE )
		{
			$criteria2 = new CDbCriteria();
			$criteria2->compare( 'global', 1 );
			$criteria2->compare( 'name', $this->name );
		}
		else
		{
			$criteria2 = new CDbCriteria();
			$criteria2->compare( 'page_path', $this->applicaton_url );
			$criteria2->compare( 'name', $this->name );
			
		}
		$criteria->mergeWith($criteria2);
		$criteria->mergeWith($start);
		$criteria->mergeWith($end);
		$criteria->compare('open_status', 1 );
		$criteria->order = 'rank';
		
		$this->contents = PageBlocksModel::model()->findAll($criteria);
	}
 
	public function run()
	{
		$data = array();
		$data['controller'] = Yii::app()->controller;
		$data['application_url'] = $this->applicaton_url;
		$data['that'] = $this;
		if( $this->user->getId()>0 )
		{
			$this->render(
					'edit',
					$data
				);
		}
	}
}