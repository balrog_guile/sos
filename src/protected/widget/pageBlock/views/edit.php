<div style="border: 1px dashed #38b44a; padding: 3px;">
<?php
	echo CHtml::link(
		'このブロックを編集',
		Yii::app()->createUrl(
				'PageBlocks/admin',
				array(
					'url' => $application_url,
					'name' => $that->name,
					'is_global' => ( ($that->isGlobal===TRUE)?1:0 ),
					'types' => json_encode($that->types),
				)
		),
		array()
	);
?>
</div>